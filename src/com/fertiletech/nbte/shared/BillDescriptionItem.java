/**
 * 
 */
package com.fertiletech.nbte.shared;

import java.io.Serializable;
import java.util.LinkedHashSet;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillDescriptionItem implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String comments;
	private double amount;
	
	public BillDescriptionItem(){}
	
	public BillDescriptionItem( String name, String comments, double amount)
	{
		this.name = name;
		this.comments = comments;
		this.amount = amount;
	}
	
	private BillDescriptionItem(BillDescriptionItem other, int factor)
	{
		this.name = other.name;
		this.comments = other.comments;
		this.amount = other.amount / factor;
	}
	
    @Override
    public int hashCode()
    {
        return (name == null ? 0 : name.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillDescriptionItem other = (BillDescriptionItem) obj;
        if (this.name != other.name && (this.name == null || !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

	public String getName() {
		return name;
	}

	public String getComments() {
		return comments;
	}

	public double getAmount() {
		return amount;
	}
	
	public static LinkedHashSet<BillDescriptionItem> duplicateBillDescriptions(LinkedHashSet<BillDescriptionItem> billDescriptions, int factor)
	{
		LinkedHashSet<BillDescriptionItem> duplicateList = new LinkedHashSet<BillDescriptionItem>(billDescriptions.size());
		for(BillDescriptionItem b : billDescriptions)
			duplicateList.add(new BillDescriptionItem(b, factor));
		return duplicateList;
	}
}
