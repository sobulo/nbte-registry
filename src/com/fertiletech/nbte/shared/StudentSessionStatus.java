package com.fertiletech.nbte.shared;

public enum StudentSessionStatus {
	PENDING_PAYMENT_VERIFICATION, PAYMENT_VERIFIED;
	
	public String getDisplayString()
	{
		return this.toString().replace("_", " ");
	}
}
