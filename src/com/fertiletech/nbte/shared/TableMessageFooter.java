package com.fertiletech.nbte.shared;

public class TableMessageFooter extends TableMessage{

	public TableMessageFooter(int numOfTextFields, int numOfDoubleFields, int numOfDateFields) {
		super(numOfTextFields, numOfDoubleFields, numOfDateFields);
	}
}
