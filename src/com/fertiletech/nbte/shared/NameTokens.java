package com.fertiletech.nbte.shared;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.TreeMap;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * @author Joshua Godi
 */
public class NameTokens {
	//
	public final static String HELP_ADDRESS = "9jedu-helpdesk@fcahptib.edu.ng";
	public final static String BASE_ASSET_URL = "http://fcahptib-media.9jeducation.org/";
	/*
	Computer Science
	Statistics
	Animal Health and Production Technology
	Science Laboratory Technology
	Fisheries Technology
	Animal Health Technology
	Animal Production Technology
	Animal Health and Production Technology*/
	//home page
    public static final String WELCOME = "welcome";
    // Credit Pages
    public static final String APPLY = "apply";
    public static final String PRINT = "print";
    public static final String STATUS = "status";
    //category pages
    public static final String CS = "com";
    public static final String STA = "sta";
    public static final String FSH = "fit";
    public static final String SLT = "slt";
    public static final String AHP = "ahp";
    public static final String ANH = "anh";
    public static final String APT = "apt";
    public static final String AEM = "aem";
    
    //brand pages
    public static final String ND = "ND";
    public static final String HND = "HND";
    public static final String NDX = "NDX";

    //other pages
    public static final String DETAILS = "details";
    public static final String CART = "register";
    public static final String MYORDER = "registered-courses";
    public static final String THANKORDER = "registration-submitted";
    public static final String ORDER_HISTORY = "registration-history";
    public static final String STUD_BIO = "student-bio";
    public static final String UPLOADS = "file-upload";
    public static final String DOWNLOADS = "print-registration";
    
    public final static String ALT_PHONE = "MOBILE";
    public final static String ALT_CS = "Computer Science";
    public final static String ALT_SLT = "Science Laboratory Technology";
    public final static String ALT_AHP = "Animal Health & Production Technology";
    public final static String ALT_ANH = "Animal Health Technology";    
    public final static String ALT_APT = "Animal Production Technology";
    public final static String ALT_AEM = "Agricultural Extension & Management";
    public final static String ALT_FSH = "Fisheries Technology";
    public final static String ALT_STA = "Statistics";
    public final static String ALT_ND = "National Diploma";
    public final static String ALT_HND = "Higher National Diploma";
    public final static String ALT_YEAR1 = "Year 1";
    public final static String ALT_YEAR2 = "Year 2";
    public final static String ALT_SEMESTER1 = "First Semester";
    public final static String ALT_SEMESTER2 = "Second Semester";
    
    public final static HashSet<String> DIPLOMAS = new LinkedHashSet<String>();
    public final static HashSet<String> YEARS = new LinkedHashSet<String>();
    public final static HashSet<String> SEMESTERS = new LinkedHashSet<String>();
    public final static TreeMap<String, HashSet<String>> DIPLM_DEPTS = new TreeMap();    
    // Getters for UiBinders
    static{
        DIPLOMAS.add(NameTokens.ND);
        DIPLOMAS.add(NameTokens.HND);
        YEARS.add(ALT_YEAR1);
        YEARS.add(ALT_YEAR2);
        SEMESTERS.add(ALT_SEMESTER1);
        SEMESTERS.add(ALT_SEMESTER2);
        
        
        HashSet<String> deptList = new HashSet<String>();
        deptList.add(NameTokens.ALT_AHP);
        deptList.add(NameTokens.ALT_STA);
        deptList.add(NameTokens.ALT_CS);
        deptList.add(NameTokens.ALT_SLT);
        deptList.add(NameTokens.ALT_FSH);
        DIPLM_DEPTS.put(ALT_ND, deptList);
        
        deptList = new HashSet<String>();
        deptList.add(NameTokens.ALT_APT);
        deptList.add(NameTokens.ALT_ANH);
        deptList.add(NameTokens.ALT_AEM);
        DIPLM_DEPTS.put(ALT_HND, deptList);      
    }
    
    public final static int getSemesterInt(String semester)
    {
    	if(semester.equals(ALT_SEMESTER1))
    		return 1;
    	else if(semester.equals(ALT_SEMESTER2))
    		return 2;
    	else
    		throw new IllegalArgumentException("Unrecognized Semester: " + semester + " Use " + getAllowedValues(SEMESTERS));
    }
    
    public static HashMap<String, String> getSampleAcademicYears()
    {
		HashMap<String, String> args = new HashMap<String, String>();
		args.put(NameTokens.getAcademicYearString(2015), 
				String.valueOf(2015));
		args.put(NameTokens.getAcademicYearString(2016), 
				String.valueOf(2016));
		return args;    	
    }
    
    public static HashMap<String, String> getSampleSemesters()
    {
		HashMap<String, String> args = new HashMap<String, String>();    	
		args.put(NameTokens.ALT_SEMESTER1, 
				String.valueOf(NameTokens.getSemesterInt(NameTokens.ALT_SEMESTER1)));
		args.put(NameTokens.ALT_SEMESTER2, 
				String.valueOf(NameTokens.getSemesterInt(NameTokens.ALT_SEMESTER2)));
    	return args;
    }
    
    public final static String getAllowedDiplomas()
    {
    	return getAllowedValues(DIPLOMAS);
    }
    
    public final static String getAllowedValues(HashSet<String> values)
    {
    	String result = "";
    	for(String s : values)
    		result += s + ", ";
    	return result;	
    	
    }
    

	static HashSet<String> allDepartments = null;
    public final static HashSet<String> getAllDepartments()
    {
    	if(allDepartments == null)
    	{
	    	allDepartments = new HashSet<String>();
	    	for(HashSet<String> departments : DIPLM_DEPTS.values())
	    		allDepartments.addAll(departments);
    	}
    	return allDepartments;
    }
    
    public final static String getAllowedDepartments()
    {
    	return getAllowedValues(getAllDepartments());
    } 

	static HashMap<String, String> nameTargetMap;
    public final static HashMap<String, String> getNameTargetMap()
    {
    	if(nameTargetMap == null)
    		nameTargetMap = new LinkedHashMap<String, String>();
    	else return nameTargetMap;
    	
    	nameTargetMap.put(ALT_CS, CS);
    	nameTargetMap.put(ALT_ANH, ANH);
    	nameTargetMap.put(ALT_AHP, AHP);
    	nameTargetMap.put(ALT_APT, APT);
    	nameTargetMap.put(ALT_STA, STA);
    	nameTargetMap.put(ALT_AEM, AEM);
    	nameTargetMap.put(ALT_SLT, SLT);
    	nameTargetMap.put(ALT_FSH, FSH);
    	nameTargetMap.put(ND, ALT_ND);
    	nameTargetMap.put(HND, ALT_HND);
    	return nameTargetMap;
    }
        
	public static String getWelcome() {
		return WELCOME;
	}

	public static String getAltCS() {
		return ALT_CS;
	}

	public static String getAltSLT() {
		return ALT_SLT;
	}



	public static String getAltAHP() {
		return ALT_AHP;
	}



	public static String getAltANH() {
		return ALT_ANH;
	}



	public static String getAltAPT() {
		return ALT_APT;
	}



	public static String getAltAEM() {
		return ALT_AEM;
	}



	public static String getAltFSH() {
		return ALT_FSH;
	}



	public static String getAltSTA() {
		return ALT_STA;
	}



	public static String getAltND() {
		return ALT_ND;
	}

	public static String getAltHND() {
		return ALT_HND;
	}

	public static String getAcademicYearString(int academicYear)
	{
		return (academicYear-1) + "/" + academicYear;
	}
	
	public static String getAcademicSessionSemesterString(int academicYear, int semester)
	{
		return getAcademicYearString(academicYear) + " - " + getSemester(semester);
	}
	
	static HashMap<Integer, String> termNames = new HashMap<Integer, String>();
	static
	{
		termNames.put(1, NameTokens.ALT_SEMESTER1);
		termNames.put(2, NameTokens.ALT_SEMESTER2);
	}
	
	public static String getSemester(int semester)
	{
		return termNames.get(semester);
	}	
	
	private static String getDepartmentShortName(String departmentName)
	{
		if(getAllDepartments().contains(departmentName))
			return getNameTargetMap().get(departmentName);
		return "";
	}
	
	public static String getGASlideUrl(String deptName)
	{
		String deptCode = getDepartmentShortName(deptName).toLowerCase();
		return BASE_ASSET_URL + "slide-ga-" + deptCode + ".jpg"; 
	}
	
	public static String getPublicSlideUrl(String deptName)
	{
		String deptCode = getDepartmentShortName(deptName).toLowerCase();
		return BASE_ASSET_URL + "slide-" + deptCode + ".jpg"; 
	}
	
	public static String getDepartmentListingImageUrl(String deptName, boolean isBig)
	{
		String deptCode = getDepartmentShortName(deptName).toLowerCase();
		String suffix = isBig?"big":"small";
		return BASE_ASSET_URL + deptCode + "-" + suffix + ".jpg"; 
	}	
	
	public static String getProviderID(String dipl, String dept, String year)
	{
		return dipl + "-" + getDepartmentShortName(dept).toUpperCase() + "-" + year;
	}
	
	public static String getAccountID(String sortCode, String acctNum)
	{
		return sortCode + "-" + acctNum;
	}
}
