package com.fertiletech.nbte.shared;

import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;

public class ClientUtils
{
    private final static String SESSION_ID_COOKIE         = "npmbfertiletechmap_session_id";
    private final static String AUTH_PROVIDER_COOKIE      = "npmbfertiletechmap_provider";
    private final static String AUTH_PROVIDER_NAME_COOKIE = "npmbfertiletechmap_provider_name";
    private final static String USERNAME_COOKIE           = "npmbfertiletechmap_user";
    private final static String USEREMAIL_COOKIE          = "npmbfertiletechmap_email";
    private final static String USERLOANID_COOKIE         = "npmbfertiletechmap_mapcst";
    private final static String USEROPS_COOKIE            = "npmbfertiletechmap_mapops";

    private final static String USERDEPT_COOKIE          = "npmbfertiletechmap_dept";
    private final static String USERDIPL_COOKIE          = "npmbfertiletechmap_dipl";
    private final static String USERLEVEL_COOKIE         = "npmbfertiletechmap_level";
    private final static String USERTERM_COOKIE          = "npmbfertiletechmap_term";
    private final static String USERACADYEAR_COOKIE      = "npmbfertiletechmap_acadyear";

    private final static String REDIRECT_URL_COOKIE       = "gwtgoogleoauthtest_redirect_url";
    
    public final static int DIALOG_X_POSITION = 20;
    public final static int DIALOG_Y_POSITION = 110;

    
    public static final String SESSION_EXPIRE_MESSAGE = "Your session seems to have expired!\n" +  "You will be logged out";
    public static final String SESSION_STALE_MESSAGE = "Your session seems to be stale!\n" + "You will be logged out";
    
    
    private static String[] authProviders = 
    {
       "Facebook", "Google",   "Twitter",     "Yahoo!",
       "Flickr",   "ImgUr",    "LinkedIn",    "Windows Live",
       "Instagram","github",   "Vimeo",       "Evernote",
       "tumblr.", "foursquare",DTOConstants.COMPANY_NAME
    };
    
    public final static int UNKNOWN  =     -1;
    public final static int DEFAULT  =     0;
    public final static int FACEBOOK =     1;
    public final static int GOOGLE   =     2;
    public final static int TWITTER  =     3;
    public final static int YAHOO    =     4;
    public final static int FLICKR   =     5;
    public final static int IMGUR   =      6;
    public final static int LINKEDIN =     7;
    public final static int WINDOWS_LIVE = 8;
    public final static int INSTAGRAM    = 9;
    public final static int GITHUB       = 10;
    public final static int VIMEO        = 11;
    public final static int EVERNOTE     = 12;
    public final static int TUMBLR       = 13;
    public final static int FOURSQUARE   = 14;
    public final static int BANK       = 15;
    
    public static String[] getAuthProviders()
    {
        return authProviders;
    }    
    
    public static int getAuthProvider(String authProviderName)
    {
        int authProvider = DEFAULT;
        authProviderName = authProviderName.toLowerCase();
        if (authProviderName.equals("facebook"))
            return FACEBOOK;
        else if (authProviderName.equals("google"))
            return GOOGLE;
        else if (authProviderName.equals("twitter"))
            return TWITTER;
        else if (authProviderName.equals("yahoo!"))
            return YAHOO;
        else if (authProviderName.equals("yahoo"))
            return YAHOO;
        else if (authProviderName.equals("flickr"))
            return FLICKR;
        else if (authProviderName.equals("imgur"))
            return IMGUR;
        else if (authProviderName.equals("linkedin"))
            return LINKEDIN;
        else if (authProviderName.equals("windows live"))
            return WINDOWS_LIVE;
        else if (authProviderName.equals("instagram"))
            return INSTAGRAM;
        else if (authProviderName.equals("github"))
            return GITHUB;
        else if (authProviderName.equals("vimeo"))
            return VIMEO;
        else if (authProviderName.equals("evernote"))
            return EVERNOTE;
        else if (authProviderName.equals("tumblr."))
            return TUMBLR;
        else if (authProviderName.equals("foursquare"))
            return FOURSQUARE;
        else if (authProviderName.equalsIgnoreCase(DTOConstants.COMPANY_NAME))
            return BANK;
        
        return authProvider;
    }
    
    public static String getAuthProviderName(int authProvider)
    {
        if (authProvider == FACEBOOK)
            return "Facebook";
        else if (authProvider == GOOGLE)
            return "Google";
        else if (authProvider == TWITTER)
            return "Twitter";
        else if (authProvider == YAHOO)
            return "Yahoo!";
        else if (authProvider == FLICKR)
            return "Flicker";
        else if (authProvider == IMGUR)
            return "ImgUr";
        else if (authProvider == LINKEDIN)
            return "LinkedIn";
        else if (authProvider == WINDOWS_LIVE)
            return "Windows Live";
        else if (authProvider == INSTAGRAM)
            return "Instagram";
        else if (authProvider == GITHUB)
            return "github";
        else if (authProvider == VIMEO)
            return "vimeo";
        else if (authProvider == EVERNOTE)
            return "Evernote";
        else if (authProvider == TUMBLR)
            return "tumblr.";
        else if (authProvider == FOURSQUARE)
            return "foursquare";
        else if (authProvider == BANK)
            return DTOConstants.COMPANY_NAME;
        
        return "Default";
    }
    
    
    public static String getCallbackUrl()
    {
        return OurCallbackUrl.APP_CALLBACK_URL;
    }
    
    public static String getProctedResourceUrl(int authProvider)
    {
        switch(authProvider)
        {
            case FACEBOOK:
            {
                return OurProtectedUrls.FACEBOOK_PROTECTED_RESOURCE_URL;
            }
            
            case BANK:
            case GOOGLE:
            {
                return OurProtectedUrls.GOOGLE_PROTECTED_RESOURSE_URL;
            }

            case TWITTER:
            {
                return OurProtectedUrls.TWITTER_PROTECTED_RESOURCE_URL;
            }
            
            case YAHOO:
            {
                return OurProtectedUrls.YAHOO_PROTECTED_RESOURCE_URL;
            }
            
            case LINKEDIN:
            {
                return OurProtectedUrls.LINKEDIN_PROTECTED_RESOURCE_URL;
            }
            
            case INSTAGRAM:
            {
                return OurProtectedUrls.INSTAGRAM_PROTECTED_RESOURCE_URL;
            }
            
            case IMGUR:
            {
                return OurProtectedUrls.IMGUR_PROTECTED_RESOURCE_URL;
            }
            
            case GITHUB:
            {
                return OurProtectedUrls.GITHUB_PROTECTED_RESOURCE_URL;
            }
            
            case FLICKR:
            {
                return OurProtectedUrls.FLICKR_PROTECTED_RESOURCE_URL;
            }
            
            case VIMEO:
            {
                return OurProtectedUrls.VIMEO_PROTECTED_RESOURCE_URL;
            }
            
            case WINDOWS_LIVE:
            {
                return OurProtectedUrls.LIVE_PROTECTED_RESOURCE_URL;
            }
            
            case TUMBLR:
            {
                return OurProtectedUrls.TUMBLR_PROTECTED_RESOURCE_URL;
            }
            
            case FOURSQUARE:
            {
                return OurProtectedUrls.FOURSQUARE_PROTECTED_RESOURCE_URL;
            }            
            
            default:
            {
                return null;
            }
        }
    }
    
    public static class BankUserCookie
    {
    	private String userName;
    	private String email;
    	private String loanID;
    	private String opsRole = LoginRoles.ROLE_SCHOOL_PUBLIC.toString();
    	private String department;
    	private String diploma;
    	private String levelYear;
    	private String academicYear;
    	private String term;
		public BankUserCookie(String userName, String email, String loanID,
				String opsRole, String department, String diploma, String levelYear, 
				String academicYear, String term) 
		{
			super();
			this.userName = userName;
			this.email = email;
			this.loanID = loanID;
			this.opsRole = opsRole;
			this.department = department;
			this.diploma = diploma;
			this.levelYear = levelYear;
			this.academicYear = academicYear;
			this.term = term;
		}
		
		public String getDepartment() {
			return getNullOrValue(department);
		}
		public String getDiploma() {
			return getNullOrValue(diploma);
		}
		public String getLevelYear() {
			return getNullOrValue(levelYear);
		}		
		public String getAcademicYear() {
			return getNullOrValue(academicYear);
		}				
		public String getAcademicTerm() {
			return getNullOrValue(term);
		}
		
		public String getUserName() {
			return getNullOrValue(userName);
		}

		public String getEmail() {
			return getNullOrValue(email);
		}

		public String getLoanID() {
			return getNullOrValue(loanID);
		}
		
		public LoginRoles getRole()
		{
			String role = getNullOrValue(opsRole);
			if(role == null) return LoginRoles.ROLE_SCHOOL_PUBLIC;
			return LoginRoles.valueOf(role);
		}
		
		private String getNullOrValue(String value)
		{
			if(value == null || value.equals("null") || value.equals("undefined")) return null;
			return value;
		}
		
		public static void removeCookie()
		{
	        removeDomainCookie(USERNAME_COOKIE);
	        removeDomainCookie(USEREMAIL_COOKIE);
	        removeDomainCookie(USERLOANID_COOKIE);
	        removeDomainCookie(USEROPS_COOKIE);			
	        
	        removeDomainCookie(USERDEPT_COOKIE);
	        removeDomainCookie(USERDIPL_COOKIE);
	        removeDomainCookie(USERACADYEAR_COOKIE);
	        removeDomainCookie(USERTERM_COOKIE);			
	        removeDomainCookie(USERLEVEL_COOKIE);				        
		}
		
		public static BankUserCookie getCookie()
		{			

			BankUserCookie cookie = new BankUserCookie(getUsernameFromCookie(), getUseremailFromCookie(),
					getUserloanFromCookie(), getOpsFromCookie(), getDeptFromCookie(), getDiplFromCookie(),
					getLevelFromCookie(), getAcadYearFromCookie(), getTermFromCookie());
			return cookie;
		}
		
		public static void setCookie(BankUserCookie userCookie)
		{
			saveUseremail(userCookie.email);
			saveUserLoanId(userCookie.loanID);
			saveUserOps(userCookie.opsRole);
			saveUsername(userCookie.userName);
			
			saveTerm(userCookie.term);
			saveAcad(userCookie.academicYear);
			saveDept(userCookie.department);
			saveDipl(userCookie.diploma);
			saveLevel(userCookie.levelYear);
		}
		
		public static void showCookie(String title, BankUserCookie userCookie)
		{
			String message = title + " [Email: " + userCookie.email + "] " + " [Name: " + userCookie.userName + "] " +
					" [ops: " + userCookie.opsRole + "] " + 	" [loanid: " + userCookie.loanID + "] - " + 
					(userCookie.loanID==null?"FOUND NULL" : userCookie.loanID.length());
			Window.alert(message);
		}
		

		
	    private static String getDeptFromCookie()
	    {
	        return Cookies.getCookie(USERDEPT_COOKIE);
	    }
	    
	    private static String getDiplFromCookie()
	    {
	        return Cookies.getCookie(USERDIPL_COOKIE);
	    }    

	    private static String getLevelFromCookie()
	    {
	        return Cookies.getCookie(USERLEVEL_COOKIE);
	    }
	    
	    private static String getAcadYearFromCookie()
	    {
	        return Cookies.getCookie(USERACADYEAR_COOKIE);
	    }		
	    private static String getTermFromCookie()
	    {
	        return Cookies.getCookie(USERTERM_COOKIE);
	    }		
		
	    private static String getUsernameFromCookie()
	    {
	        return Cookies.getCookie(USERNAME_COOKIE);
	    }
	    
	    private static String getUseremailFromCookie()
	    {
	        return Cookies.getCookie(USEREMAIL_COOKIE);
	    }
	    
	    private static String getUserloanFromCookie()
	    {
	        return Cookies.getCookie(USERLOANID_COOKIE);
	    }
	    
	    private static String getOpsFromCookie()
	    {
	        return Cookies.getCookie(USEROPS_COOKIE);
	    }  	 
	    
	    private static void saveDept(String dept)
	    {
	        setDomainCookie(USERDEPT_COOKIE,dept);
	    }
	    private static void saveDipl(String dipl)
	    {
	        setDomainCookie(USERDIPL_COOKIE,dipl);
	    }    
	    private static void saveLevel(String lev)
	    {
	        setDomainCookie(USERLEVEL_COOKIE,lev);
	    }    		
	    private static void saveAcad(String acad)
	    {
	        setDomainCookie(USERACADYEAR_COOKIE, acad);
	    }    
	    private static void saveTerm(String term)
	    {
	        setDomainCookie(USERTERM_COOKIE, term);
	    }  	    
	    
	    private static void saveUsername(String username)
	    {
	        setDomainCookie(USERNAME_COOKIE,username);
	    }
	    
	    private static void saveUseremail(String username)
	    {
	        setDomainCookie(USEREMAIL_COOKIE,username);
	    }    
	    
	    private static void saveUserLoanId(String username)
	    {
	        setDomainCookie(USERLOANID_COOKIE,username);
	    }    
	    
		
	    private static void saveUserOps(String opsRole)
	    {
	        setDomainCookie(USEROPS_COOKIE, opsRole);
	    }    
    }
    
    private static void setDomainCookie(String name, String value)
    {
    	Cookies.setCookie(name, value, null, null, "/", false);
    }
    
    private static void removeDomainCookie(String name)
    {
    	Cookies.removeCookie(name, "/");
    }    
    
    public static void clearCookies()
    {
        removeDomainCookie(SESSION_ID_COOKIE);
        removeDomainCookie(AUTH_PROVIDER_COOKIE);
        removeDomainCookie(AUTH_PROVIDER_NAME_COOKIE);
        removeDomainCookie(REDIRECT_URL_COOKIE);
        BankUserCookie.removeCookie();
    }
    
    public static String getSessionIdFromCookie()
    {
        return Cookies.getCookie(SESSION_ID_COOKIE);
    }
    
    public static String getAuthProviderFromCookie()
    {
        return Cookies.getCookie(AUTH_PROVIDER_COOKIE);
    }
    
    public static int getAuthProviderFromCookieAsInt()
    {
       String authProviderStr = getAuthProviderFromCookie();
       int authProvider = UNKNOWN;
       if (authProviderStr != null)
       {
           try
           {
               authProvider = Integer.parseInt(authProviderStr);
               
           }
           catch(NumberFormatException e)
           {
               return UNKNOWN;
           }
       }
       
       return authProvider;
    }
    
    public static String getAuthProviderNameFromCookie()
    {
        return Cookies.getCookie(AUTH_PROVIDER_NAME_COOKIE);
    }
        
    public static boolean alreadyLoggedIn()
    {
        if (getSessionIdFromCookie() != null)
            return true;
        return false;
    }
    
    public static void saveSessionId(String sessionId)
    {
        setDomainCookie(SESSION_ID_COOKIE,sessionId);
    }
    
    public static void saveAuthProvider(int authProvider)
    {
       setDomainCookie(AUTH_PROVIDER_COOKIE,Integer.toString(authProvider)); 
       String authProviderName = getAuthProviderName(authProvider);
       saveAuthProviderName(authProviderName);
    }
    
    public static void saveAuthProviderName(String authProviderName)
    {
       setDomainCookie(AUTH_PROVIDER_NAME_COOKIE,authProviderName); 
    }
        
    public static void saveRediretUrl(String url)
    {
        setDomainCookie(REDIRECT_URL_COOKIE,url);
    }
    
    public static String getRedirectUrlFromCookie()
    {
        return Cookies.getCookie(REDIRECT_URL_COOKIE);
    }
    
    public static void redirect(String url)
    {
    	//Window.alert("Redirecting to: " + url + " ...all state will be lost, session id cookie is: " + ClientUtils.getSessionIdFromCookie());
        Window.Location.assign(url);
    }
    
    public static boolean redirected()
    {
        String authProvider = getAuthProviderFromCookie();
        if (authProvider == null)
        {
            return false;
        }
        
        if (Location.getParameter("code") != null) //facebook,google,github,windows live
            return true;
        
        if (Location.getParameter("oauth_token") != null) // twitter,yahoo,flickr
            return true;
        
        if (Location.getParameter("oauth_verifier") != null) // Flickr
            return true;
        
        String error = Location.getParameter("error");
        if (error != null)
        {
            String errorMessage = Location.getParameter("error_description");
            //Window.alert("Error: " + error + ":" + errorMessage);
            reload();
            return false;
        }
        
        return false;
    }
    
    public static void reload()
    {
        String appUrl = getRedirectUrlFromCookie();
        clearCookies();
        //Window.alert("Redirect URL: " + appUrl);
        if (appUrl != null)
        {
            redirect(appUrl);
        }
   }
    
    public static Credential getCredential() throws OurException
    {
        String authProvider = getAuthProviderFromCookie();
        if (authProvider == null)
            return null;
        int ap = DEFAULT;
        
        try
        {
            ap = Integer.parseInt(authProvider);
        }
        catch(Exception e)
        {
            throw new OurException("Could not convert authProvider " + authProvider + " to Integer");
        }
        
        switch(ap)
        {
            case DEFAULT:
            {
                Credential credential = new Credential();
                credential.setAuthProvider(ap);
                return credential;
            }
            case FACEBOOK:
            case INSTAGRAM:
            case LINKEDIN:
            case IMGUR:
            case GITHUB:
            {
                Credential credential = new Credential();
                credential.setAuthProvider(ap);
                credential.setState(Location.getParameter("state"));
                credential.setVerifier(Location.getParameter("code"));
                return credential;
            }
            case BANK:
            case GOOGLE:
            case WINDOWS_LIVE:
            case FOURSQUARE:
            {
                Credential credential = new Credential();
                credential.setAuthProvider(ap);
                credential.setVerifier(Location.getParameter("code"));
                return credential;
            }
            case TWITTER:
            case YAHOO:
            case FLICKR:
            case VIMEO:
            case TUMBLR:
            {
                Credential credential = new Credential();
                credential.setAuthProvider(ap);
                credential.setVerifier(Location.getParameter("oauth_verifier"));
                return credential;
            }
            
            default:
            {
                throw new OurException("ClientUtils.getCredential: Auth Provider " + authProvider + " Not implemented yet");
            }
        }
    }
    
    public static void handleException(Throwable caught, boolean showMessage)
    {
    	if(caught instanceof OurException)
    	{
    		if(((OurException) caught).forceLogout)
    		{
    			showSessionExpires(caught.getMessage(), showMessage);
    			return;
    		}
    	}
    	
    	if(showMessage)
    		showGenericException(caught);
    }
    
    public static void showSessionExpires(String msg, boolean showMessage)
    {
    	if(showMessage)
    		Window.alert(msg);
        ClientUtils.reload();
    }
    
    public static void showGenericException(Throwable caught)
    {               
        String message = "Exception: " + caught;
        //message += "Stack Trace:\n" + st;
        Window.alert(message);
    }    
}

