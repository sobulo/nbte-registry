package com.fertiletech.nbte.shared;

@SuppressWarnings("serial")
public class ManualVerificationException extends Exception {
	public ManualVerificationException(){}
	
	public ManualVerificationException(String s)
	{
		super(s);
	}
}
