package com.fertiletech.nbte.shared;

import java.util.HashMap;

import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;



public class ApplicationFormConstants {
	
	//section A fields
	public static String SURNAME = "Surname";
	public static String FIRST_NAME = "Other Names";
	public static String ADDRESS = "Address";
	public static String PRIMARY_NO = "Primary Tel No.";
	public static String MOBILE_NO = "Alternate Tel No";
	public static String EMAIL = "College Email";
	public static String ALTERNATE_EMAIL = "Personal Email";
	public static String MATRIC_NO = "Matric No.";
	public static String JAMB_NO = "Jamb Reg. No.";
	public static String SEMESTER = "Semester";
	public static String YEAR = "Year";
	public static String DEPARTMENT = "Department";
	public static String DIPLOMA = "Diploma";
	public static String GENDER = "Gender";
	public static String BDAY  = "Birthday";
	public static String STUD_ID = "selection";

	//course fields
	public static String COURSE_UNITS = "Unit";
	public static String COURSE_NAME = "Title";
	public static String COURSE_CODE = "Code";
	public static String LECTURE_NOTE = "Notes Url";
	public static String DESCR = "Description Url";
	public static String FEATURES = "Topics";
	public static String THUMB_URL = "Thumbnail Url";
	public static String BANNER_URL = "Banner Url";
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "All information provided on this form has been reviewed by me and " +
			"I hereby authorize Addosser Microfinance Bank Limited to verify the information provided on this form " +
			"as to my credit and employment history.";
	
	public static int OBLIGATION_IDX = 0;
	
	
	public final static String[] GENDER_LIST = {"Male", "Female"};
	public static String[][] BANKING_TABLE_CFG = {{"Bank", "Account No.", "Account Type", "Balance"},{"30", "30", "15", "25"}, {"bank account", "bank accounts"}, {TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.TEXT.toString(), TableMessageContent.NUMBER.toString()}};
	
	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> ORDER_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EMPLOYMENT_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_LOAN_VALIDATORS = new HashMap<String, FormValidator[]>();

	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] dateParsesOk = {FormValidator.DATE_OLD_ENOUGH};
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};		
    	
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(PRIMARY_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	
    	ORDER_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(PRIMARY_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(MOBILE_NO, phoneOnly);
    	NPMB_PERSONAL_VALIDATORS.put(BDAY, dateParsesOk);
    	NPMB_PERSONAL_VALIDATORS.put(ALTERNATE_EMAIL, emailOnly);
   	}
	
	//old style validation
    //data validation constants
    public final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    public final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    public final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";	
}
