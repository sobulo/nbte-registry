/**
 * 
 */
package com.fertiletech.nbte.shared;


/**
 * @author Segun Razaq Sobulo
 *
 */
public class FormHTMLDisplayBuilder {
	StringBuilder html;
	
	private final static String FORM_BEGINS_STYLE = "padding: 10px;text-align:left; width:95%; border: 1px solid gray; background-color:white; opacity:0.85";
	private final static String FORM_BEGINS = "<div style='" + FORM_BEGINS_STYLE + "'>";
	private final static String FORM_ENDS = "</div>\n";
	private final static String HEADER_STYLE = "font-weight:bold; clear:both; margin-bottom:15px";
	public final static String HEADER_BEGINS = "<div style='" + HEADER_STYLE + "'>";
	public final static String HEADER_ENDS = "</div>\n";
	private final static String LINE_STYLE = "margin-bottom: 15px; clear:both";
	private final static String RIGHT_STYLE = "clear:both; float:right;";
	private final static String LINE_BEGINS = "<div style='" + LINE_STYLE + "'>";
	private final static String LINE_RIGHT = "<span style='" + RIGHT_STYLE + "'>";
	private final static String LINE_ENDS = "</div>\n";
	private final static String LABEL_STYLE = "float:left; margin-right: 5px;";
	private final static String LABEL_BEGINS = "<span style='" + LABEL_STYLE + "'>";
	private final static String LABEL_ENDS = "</span>\n";
	private final String VALUE_STYLE;
	private final String VALUE_BEGINS;
	private final static String VALUE_ENDS = "</span>";
	private final static String SECTION_ENDS = "</div>";
	
	public FormHTMLDisplayBuilder()
	{
		this(true);
	}
	public FormHTMLDisplayBuilder(boolean darkTheme)
	{
		String prefix = "float:left;border-bottom:1px dotted black; margin-right: 15px; color: ";
		html = new StringBuilder();
		if(darkTheme)
			VALUE_STYLE	= prefix + "black;";
		else
			VALUE_STYLE = prefix + "white;";
		VALUE_BEGINS =  "<span style='" + VALUE_STYLE + "'>";
	}

	public FormHTMLDisplayBuilder formBegins()
	{
		html.append(FORM_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder formEnds()
	{
		html.append(FORM_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder lineBegins()
	{
		html.append(LINE_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder lineBeginsRight()
	{
		html.append(LINE_RIGHT);
		return this;
	}	
	
	public FormHTMLDisplayBuilder lineEndsRight()
	{
		html.append("</span>");
		return this;
	}	
	
	public FormHTMLDisplayBuilder lineEnds()
	{
		html.append(LINE_ENDS);
		return this;
	}

	public FormHTMLDisplayBuilder headerBegins()
	{
		html.append(HEADER_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder headerEnds()
	{
		html.append(HEADER_ENDS);
		return this;
	}
	
	boolean oddColor = false;
	public FormHTMLDisplayBuilder sectionBegins()
	{
		String color = "#99FFCC";
		if(oddColor)
			color = "#99CCFF";
		html.append("<div style='margin: 5px; padding: 5px; width: 95%; border-bottom: 1px solid black;").
			append("background-color:").append(color).append("'>");
		return this;
	}
	
	public FormHTMLDisplayBuilder sectionEnds()
	{
		html.append(SECTION_ENDS);
		oddColor = !oddColor;
		return this;
	}
	
	public FormHTMLDisplayBuilder labelBegins()
	{
		html.append(LABEL_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder labelEnds()
	{
		html.append(':').append(LABEL_ENDS);
		return this;
	}	
	
	public FormHTMLDisplayBuilder valueBegins()
	{
		html.append(VALUE_BEGINS);
		return this;
	}
	
	public FormHTMLDisplayBuilder valueEnds()
	{
		html.append(VALUE_ENDS);
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String txt)
	{
		html.append(txt);
		return this;
	}
	
	public FormHTMLDisplayBuilder blankLine()
	{
		html.append("<br/>");
		return this;
	}	
	
	public FormHTMLDisplayBuilder appendTextBody(String[] txts, String delimter)
	{
		for(int i = 0; i < txts.length - 1; i++)
			html.append(txts[i]).append(delimter).append(" ");
		if(txts.length > 0)
			html.append(txts[txts.length - 1]);
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String label, String value, boolean line)
	{
		if(line)
			lineBegins();
		
		labelBegins().appendTextBody(label).labelEnds().valueBegins().appendTextBody(value).valueEnds();
		
		if(line)
			lineEnds();
		
		return this;
	}
	
	public FormHTMLDisplayBuilder appendTextBody(String label, String value)
	{
		appendTextBody(label, value, false);
		return this;
	}
	
	public String toString()
	{
		return html.toString();
	}
}
