/**
 * 
 */
package com.fertiletech.nbte.shared;

import java.io.Serializable;

/**
 * @author Segun Razaq Sobulo
 *
 */
public enum LoginRoles implements Serializable
{
	    ROLE_SCHOOL_ADMIN_EDIT, ROLE_SCHOOL_ADMIN, ROLE_SCHOOL_STUDENT, ROLE_SCHOOL_STUDENT_NOT_IN_SESSION, 
	    ROLE_SCHOOL_STUDENT_UNKNOWN, ROLE_SCHOOL_PUBLIC ;	
}
