package com.fertiletech.nbte.shared;

import java.util.Date;
import java.util.HashMap;

import com.fertiletech.nbte.client.GUIConstants;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

interface Validators
{
	boolean validate(String key, String val, HashMap<String, String> messages, boolean isSubmit);
}

public enum FormValidator implements Validators
{
	EMAIL
	{
		@Override
		public boolean validate(String key, String val,
				HashMap<String, String> messages, boolean isSubmit) 
		{
			if(val != null && val.length() > 0 && !ValidationUtils.isValidEmail(val))
			{
				addError(messages, key, "invalid address");
				return false;
			}
			return true;
		}
	},
	PHONE
	{
		@Override
		public boolean validate(String key, String val,
				HashMap<String, String> messages, boolean isSubmit) {
			if(val != null && val.length() > 0 && !ValidationUtils.isValidNum(val))
			{
				addError(messages, key, "invalid phone number, e.g. use 01-123-4567 or 0801-234-5678");
				return false;
			}
			return true;			}
	},
	PARSES_INTEGER
	{
		@Override
		public boolean validate(String key, String val,
				HashMap<String, String> messages, boolean isSubmit) {
			
			if(val == null || val.length() == 0) 
				return true;
			
			try
			{
				Integer x = Integer.valueOf(val);
			}
			catch(NumberFormatException ex)
			{
				addError(messages, key, "Enter a whole number. No letters or fractions/decimals allowed");
				return false;
			}
			return true;
		}
	},
	PARSES_DOUBLE
	{
		@Override
		public boolean validate(String key, String val,
				HashMap<String, String> messages, boolean isSubmit) {
			if(val == null || val.length() == 0) 
				return true;
			
			try
			{
				Double x = Double.valueOf(val);
			}
			catch(NumberFormatException ex)
			{
				addError(messages, key, "Enter a valid number. No letters or symbols allowed");
				return false;
			}
			return true;
		}
	},
	DATE_OLD_ENOUGH
	{
		@Override
		public boolean validate(String key, String val,
				HashMap<String, String> messages, boolean isSubmit) 
		{
			if(val == null || val.length() == 0) 
				return true;				
			
			Date d = stringToDate(val);
			if( d == null)
			{
				addError(messages, key, "badly formatted date");
				return false;
			}
			Date twentyYearsAgo = new Date();
			CalendarUtil.addMonthsToDate(twentyYearsAgo, -12 * 15);
			if(d.after(twentyYearsAgo))
			{
				addError(messages, key, "Younger than 15? Contact us to confirm this");
				return false;
			}
			return true;
		}
	},
	MANDATORY
	{

		@Override
		public boolean validate(String key, String val, HashMap<String, String> errorMessage, boolean isSubmit) {
			if(isSubmit && (val == null || val.equals("")))
			{
				addError(errorMessage, key, "Enter a value");
				return false;
			}
			return true;
		}
	};

	public HashMap<String, String> mapToBufferHelper = new HashMap<String, String>();
	public boolean validate(String key, String val, StringBuilder errorMessage, boolean isSubmit)
	{
		boolean result = validate(key, val, mapToBufferHelper, isSubmit);
		if(!result)
			addError(errorMessage, mapToBufferHelper.get(key));
		return result;
	}
	
	public void addError(StringBuilder errorBuffer, String msg)
	{
		errorBuffer.append("<li>").append(msg).append("</li>");
	}
	
	public void addError(HashMap<String, String> errorMap, String key, String msg)
	{
		if(errorMap == null)
			return;
		errorMap.put(key, key + " (" + msg + ")");
	}
	
	public static Date stringToDate(String val)
	{
		DateBox db = new DateBox();
		setDateParameters(db);
		Date d = GUIConstants.DEFAULT_DATEBOX_FORMAT.parse(db, val, false);
		return d;
	}

	public static void setDateParameters(DateBox db) {
		Date d = new Date();
        db.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		db.setValue(null);
	}	
}
