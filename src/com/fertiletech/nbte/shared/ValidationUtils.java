/**
 * 
 */
package com.fertiletech.nbte.shared;


import com.google.gwt.regexp.shared.RegExp;

/**
 * @author Segun Razaq Sobulo
 */
public class ValidationUtils {
	
    public final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    public final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    private static final RegExp EMAIL_MATCHER = RegExp.compile(REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(REGEX_NUMS_ONLY);
    public final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";
    
    public static boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public static boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(REGEX_PHONE_REPLACE, "")) != null;
    }
    
    
}
