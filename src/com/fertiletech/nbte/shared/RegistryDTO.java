package com.fertiletech.nbte.shared;

public class RegistryDTO {
	
	public static final int LAST_NAME_IDX = 0;
	public static final int FIRST_NAME_IDX = 1;
	public static final int STUD_DEPT_IDX = 2;
	public static final int STUD_YEAR_IDX = 3;
	public static final int STUD_DIPL_IDX = 4;
	public static final int	MATRIC_NO_IDX = 5;
	public static final int JAMB_NO_IDX = 6;
	public static final int ACADEMIC_YEAR = 7;
	public static final int EMAIL_IDX = 8;
	
	public static final int COURSE_CODE_IDX = 0;
	public static final int COURSE_NAME_IDX = 1;
	public static final int COURSE_DEPT_IDX = 2;
	public static final int COURSE_DIPLOMA_IDX = 3;
	public static final int COURSE_YEAR_IDX = 4;
	public static final int COURSE_SEMESTER_IDX = 5;
	public static final int COURSE_ACADEMIC_YEAR_IDX = 6;
	public static final int COURSE_FEATURE_IDX =  7;
	public static final int COURSE_DESC_IDX = 8;
	public static final int COURSE_NOTES_IDX = 9;
	public static final int COURSE_THUMB_IDX = 10;
	public static final int COURSE_BANN_IDX = 11;
	public static final int COURSE_SCHEDULE_IDX = 12;
	public static final int COURSE_UNIT_IDX = 0; 
	
	
	/**School Sessions**/
	public final static int SESS_SCHOOL_ACADEMIC_YEAR_IDX = 0;
	public final static int SESS_SCHOOL_SEMESTER_IDX = 1;
	public final static int SESS_SCHOOL_CURRENT_IDX = 2;
	
	/**Student Sessions**/
	public final static int STUD_SESS_ACADEMIC_YEAR_IDX = 0;
	public final static int STUD_SESS_SEMESTER_IDX = 1;
	public final static int STUD_SESS_DIPL_IDX = 2;
	public final static int STUD_SESS_DEPT_IDX = 3;
	public final static int STUD_SESS_STATUS_IDX = 4;
	public final static int STUD_SESS_MESSAGE_IDX = 5;
	
	//bio-admin fields
	public final static int STUD_SESS_SURNAME_IDX = 6;
	public final static int STUD_SESS_OTHER_NAMES_IDX = 7;
	public final static int STUD_SESS_YEAR_IDX = 8;
	public final static int STUD_SESS_MATRIC_IDX = 9;
	public final static int STUD_SESS_DATE_IDX = 0;

}
