package com.fertiletech.nbte.shared;

import java.util.Date;
import java.util.List;

public class DrivePickerUtils {
    public final static int ATTACH_TITLE_IDX = 0;
    public final static int ATTACH_TYPE_IDX = 1; 
    public final static int ATTACH_USR_IDX = 2;
    public final static int ATTACH_VIEW_IDX = 3;
    public final static int ATTACH_EDIT_IDX = 4;
    public final static int ATTACH_PARENT_IDX = 5;
    public final static int ATTACH_THUMB_IDX = 6;
    public final static int ATTACH_LONG_IDX = 0;
    public final static int ATTACH_LAT_IDX = 1;
    public final static int ATTACH_DATE_IDX = 0;
    public final static int ATTACH_LINKDATE_IDX = 1;
    
    public static String getAttachmentDescription(List<TableMessage> attachments)
    {
    	TableMessageHeader h = (TableMessageHeader) attachments.get(0);
    	StringBuilder result = new StringBuilder("<table border='0' cellspacing='10' cellpadding='5'>\n")
    	                           .append("<tr><th colspan='2' style='background-color:black; color:white;'>")
    	                           .append(h.getCaption()).append("</th></tr>\n");
    	for(int i = 1; i < attachments.size(); i++)
    	{
    		getAttachmentDescription(result, attachments.get(i));
    	}
    	result.append("</table>");
    	return result.toString();
    }
    
    private static String replaceNull(String val, String defaultVal)
    {
    	return val == null?defaultVal:val;
    }
    
    public static String getAttachmentDescription(TableMessage attachment, String title)
    {
    	StringBuilder result = new StringBuilder("<table border='0' cellspacing='10' cellpadding='5'>\n")
        .append("<tr><th colspan='2' style='background-color:#d3d3d3; color:black; font-weight:bold'>")
        .append(title).append(attachment.getText(ATTACH_TYPE_IDX))
        .append("/").append(attachment.getText(ATTACH_TITLE_IDX)).append("</th></tr>\n");
    	getAttachmentDescription(result, attachment);
    	result.append("</table>");
    	return result.toString();
    }
    
    private static void getAttachmentDescription(StringBuilder output, TableMessage attachment)
    {
    	String thumbUrl = attachment.getText(ATTACH_THUMB_IDX);
    	String viewUrl = attachment.getText(ATTACH_VIEW_IDX);
    	String type = attachment.getText(ATTACH_TYPE_IDX);
    	String editUrl = attachment.getText(ATTACH_EDIT_IDX);
    	String title = attachment.getText(ATTACH_TITLE_IDX);
    	String parentFolder = attachment.getText(ATTACH_PARENT_IDX);
    	output.append("<tr style='border-bottom:1px solid purple'><td>");
    	if( thumbUrl != null)
    	{
    		String url = viewUrl == null ? thumbUrl : viewUrl;
    		output.append("<a target='_blank' href='").append(url).append("'>").append("<img src='").append(thumbUrl)
    		      .append("' style='style='max-height: 200px; max-width:200px;'/></a>");
    	}
    	else
    		output.append("no thumbnail");
    	output.append("</td><td><div style='background-color:purple;color:white;margin-right:auto;margin-left:auto;'>")
    	       .append(replaceNull(type, "FILE")).append(":  ").append(replaceNull(title, "untitled")).append("</div>\n");
    	getAttachmentDescription(output, viewUrl, editUrl, attachment.getText(ATTACH_PARENT_IDX));
    	output.append("<div style='font-size: smaller'>Attached by ").append(attachment.getText(ATTACH_USR_IDX))
    		  .append("</div></td></tr>\n");
    }
    
    private static void getAttachmentDescription(StringBuilder output, String viewUrl, String editUrl, String parentID)
    {
    	output.append("<div>");
    	output.append(replaceNullUrl(viewUrl, "View File", "No View Option"));
    	output.append(" OR ").append(replaceNullUrl(editUrl, "Edit File", "No Edit Option"));
    	output.append(getParentFolderUrl(parentID));
    	output.append("</div>\n");
    }
    
    private static String replaceNullUrl(String href, String text, String defaultText)
    {
    	if(href == null || href.length() == 0) return defaultText;
    	return "<a target='_blank' href='" + href + "'>" + text + "</a>" ;
    	
    }
    
    public static String getParentFolderUrl(String parentID)
    {
    	if(parentID == null || parentID.length() == 0) return "";
    	return " <a target='_blank' href='https://drive.google.com/folderview?id=" + parentID + "'>(Parent Folder)</a>" ;
    }
    
    public static TableMessage getUserMapAttachment(String title, Double lat, Double lng, String userName)
    {
    	if(lat == null || lng == null) return null;
    	String mapUrl = "http://maps.google.com/?q="+lat+","+lng;
		TableMessage info = new TableMessage(7, 2, 2);
		info.setText(DrivePickerUtils.ATTACH_TITLE_IDX, title);
		info.setText(DrivePickerUtils.ATTACH_VIEW_IDX, mapUrl);
		info.setText(DrivePickerUtils.ATTACH_EDIT_IDX, null);
		info.setText(DrivePickerUtils.ATTACH_THUMB_IDX,
				"https://sites.google.com/a/fertiletech.com/zero-finance-assets/oauthimages/user-map.png");
		info.setText(DrivePickerUtils.ATTACH_TYPE_IDX, "MAPS");
		info.setText(DrivePickerUtils.ATTACH_USR_IDX, userName);			
		info.setDate(DrivePickerUtils.ATTACH_LINKDATE_IDX, new Date());
		info.setNumber(DrivePickerUtils.ATTACH_LAT_IDX, lat);
		info.setNumber(DrivePickerUtils.ATTACH_LONG_IDX, lng);
		return info;
    }
}
