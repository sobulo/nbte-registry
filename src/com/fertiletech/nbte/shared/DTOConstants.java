package com.fertiletech.nbte.shared;

import java.util.HashMap;


public class DTOConstants {
	public final static String COMPANY_NAME = "FCAHPT IB";
	public final static String GOOGLE_CLOUD_LOGOUT = "http://accounts.google.com/Logout";
    public final static String YAHOO_CLOUD_LOGOUT = "http://login.yahoo.com/config/login?logout=1";	
    public final static String GCS_LOAN_ID_PARAM = "com.fertiletech.gcsloan";
    
    public final static String APP_PARAM_ADMINS = "user-admin";
    public final static String APP_PARAM_EDIT_COURSES = "has-edit-course";  //edit student details and course info
    public final static String APP_PARAM_EDIT_PAY_STATUS = "has-edit-pay-status"; //modify payment status
    public final static String APP_PARAM_EDIT_REGISTRY = "has-edit-registry"; //modify payment status
    public final static String APP_PARAM_EDIT_STUDENTS = "has-edit-student"; //modify payment status
    public final static String[] APP_PARAM_EDITORS = {APP_PARAM_EDIT_PAY_STATUS, APP_PARAM_EDIT_REGISTRY,
    												  APP_PARAM_EDIT_STUDENTS, APP_PARAM_EDIT_COURSES};

    public final static String APP_PARAM_MAP_TITLES = "map-titles";	
	
    /*attach idices?*/
	public final static int CATEGORY_IDX = 0;
	public final static int DETAIL_IDX = 1;
	public final static int LAT_IDX = 0;
	public final static int LNG_IDX = 1;
	
	/**
	 * DO not change the values of the APP_PARAM constants below without thoroughly thinking it through
	 * reason being there could be objects persisted in the database already with those values
	 * thus changing the value makes it impossible to read/write the objects pointed to by the old values
	 */	
	public final static String PDF_REPORT_LOGO = "College Report Logo";
	public final static String PDF_NO_IMAGE_LOGO = "Affix Passport Image";
	public final static String PDF_COAT_OF_ARMS = "Coat of Arms Image";
	public final static String APP_PARAM_COMPANY_LIST_KEY = "resident-company-names";
	public final static String APP_PARAM_RES_ACCT_LIST_KEY = "resident-account-types";
	public final static String APP_PARAM_COMP_ACCT_TYPE_KEY = "company-account-types";
	public final static String APP_PARAM_RES_BILL_LIST_KEY = "resident-bill-types";	
	public final static String APP_PARAM_PRODUCT_CATEGORY_KEY = "inventory-product-types";
	public final static String APP_PARAM_VENDOR_CATEGORY_LIST_KEY = "vendor-category-names";
	public final static String APP_PARAM_VENDOR_COMPANY_LIST_KEY = "vendor-company-names";
	public final static String APP_PARAM_EMPLOYEE_LIST = "employee-names";	
	public final static String APP_PARAM_VENDORS = "vendor-admins";
	public final static String APP_PARAM_ACCTS = "accounts-admins";
	public final static String APP_PARAM_ADMIN_TYPES = "admin-group";
	/*public final static int TNT_BUILDING_IDX = 0;
	public final static int TNT_APT_IDX = 1;	
	public final static int TNT_FNAME_IDX = 2;
	public final static int TNT_LNAME_IDX = 3;	
	public final static int TNT_COMPANY_IDX = 4;
	public final static int TNT_PRIMARY_EMAIL_IDX = 5;
	public final static int TNT_PRIMARY_PHONE_IDX = 6;
	public final static int TNT_ADDRESS_IDX = 7;
	public final static int TNT_TITLE_IDX = 8;
	public final static int TNT_GENDER_IDX = 9;
	public final static int TNT_IS_ARCH_IDX = 10;
	public final static int TNT_IS_OWNR_IDX = 11;	
	public final static int TNT_APT_ID_IDX = 12;
	public final static int TNT_NUM_OF_PRESPECIFIED_TXT_FIELDS = 13; //must be the largest idx value above + 1
	public final static int TNT_OTHER_PHONE_START_IDX = 0;
	public final static int TNT_OTHER_PHONE_END_IDX = 1;
	public final static int TNT_OTHER_EMAIL_START_IDX = 2;
	public final static int TNT_OTHER_EMAIL_END_IDX = 3;
	public final static int TNT_DATE_OF_BIRTH_IDX = 0;
	public final static String TNT_NO_COMPANY_VAL = "None Specified";		

	/public final static int VND_CATEGORY_IDX = TNT_APT_IDX; //vendors don't reside in apartments, good idx to use for vendor category
	public final static int RLT_TYPE_IDX = TNT_APT_IDX; //relationships don't need to know apartments either, just the parent obj
	public final static int REP_OFFSET = TNT_FNAME_IDX;*/
	
	public final static int APT_OCCUPIED_IDX = 0;
	public final static int APT_VACANT_IDX = 1;
	
	public final static int CURRENCY_LENGTH = 3;
	
    public final static int COMPANY_INFO_NAME_IDX = 0;
    public final static int COMPANY_INFO_ADDR_IDX = 1;
    public final static int COMPANY_INFO_NUMS_IDX = 2;
    public final static int COMPANY_INFO_EMAIL_IDX = 3;
    public final static int COMPANY_INFO_WEB_IDX = 4;
    public final static int COMPANY_INFO_ACCR_IDX = 5;
    public final static int COMPANY_INFO_FAX_IDX = 6;
    
    public final static int PRODUCT_MANUFACTURER = 0;
    public final static int PRODUCT_MODEL = 1;
    public final static int PRODUCT_CLASSIFICATION = 2;
    public final static int PRODUCT_DESCRIPTION = 3;
    
    public final static int INV_PRICE_IDX = 0;
    public final static int INV_QUANTITY_IDX = 1;    
    public final static int INV_SERIAL_START_IDX = 2;
    public final static int INV_SERIAL_END_IDX = 3;
    public final static int INV_BUILDING_IDX = 0;
    public final static int INV_PRODUCT_IDX = 1;
    public final static int INV_NUM_OF_PRESPECIFIED_TXT_FIELDS = 2;
    
    public final static int ALC_SERIAL_IDX = 0;
    public final static int ALC_FROM_IDX = 1;    
    public final static int ALC_TO_IDX = 2;
    public final static int ALC_USER_IDX = 3;
    public final static int ALC_PRODUCT_IDX = 4;
    public final static int ALC_DATE_IDX = 0;
    public final static int ALC_CRT_DATE_IDX = 1;
    public final static int ALC_PRICE_IDX = 0;
    public final static int ALC_ID_IDX = 1;
    public final static int ALC_TICKET_IDX = 2;
    
    public final static int TM_DIFF_HEADER = 0;
    public final static int TM_DIFF_VALS = 1;
    public final static int TM_DIFF_POS = 2;

    public final static int RSD_ACCT_TYPE_IDX = 0;
    public final static int RSD_ACCT_CURR_IDX = 1;
    public final static int RSD_ACCT_OVER_IDX = 2;
    public final static int RSD_ACCT_BLD_IDX = 3;     
    public final static int RSD_ACCT_APT_IDX = 4;    
    public final static int RSD_ACCT_COMPANY_IDX = 5;
    public final static int RSD_ACCT_ATTN_IDX = 6;
    public final static int RSD_ACCT_ATTN_ID_IDX = 7;
    public final static int RSD_ACCT_ATTN_ADDR_IDX = 8;
    public final static int RSD_ACCT_ARCHIVE_IDX = 9;
    public final static int RSD_ACCT_AMT_IDX = 0;
    public final static int RSD_ACCT_LEV_IDX = 1;
    public final static int RSD_ACCT_TXN_IDX = 2;
    public final static int RSD_ACCT_MOD_DT_IDX = 0;
    public final static int RSD_ACCT_CRT_DT_IDX = 1;
    
    public final static int CMP_ACCT_NAME_IDX = 0;
    public final static int CMP_ACCT_NUM_IDX = 1;
    public final static int CMP_ACCT_BNK_IDX = 2;
    public final static int CMP_ACCT_SRT_IDX = 3;
    public final static int CMP_ACCT_CURR_IDX = 4;
    public final static int CMP_ACCT_TYPE_IDX = 5;
    
    public final static int BT_TITLE_IDX = 0;
    public final static int BT_CCY_IDX = 1;
    public final static int BT_DIP_IDX = 2;
    public final static int BT_YR_IDX = 3;
    public final static int BT_TYPE_IDX = 4;
    public final static int BT_AMOUNT_IDX = 0;
    public final static int BT_START_IDX = 0;
    public final static int BT_END_IDX = 1;
    public final static int BT_DUE_IDX = 2;
    
    
    public final static String[] COMPANY_INFO = new String[7]; 
    
    public static String[] getCompanyInfo()
    {
    	COMPANY_INFO[COMPANY_INFO_NAME_IDX] = "Federal College of Animal Health and Production Technology";
    	COMPANY_INFO[COMPANY_INFO_ADDR_IDX] = "Moor Plantation, P.M.B. 5029, Ibadan, Oyo State, Nigeria.";
    	COMPANY_INFO[COMPANY_INFO_NUMS_IDX] = "+234-805-217-9585";
    	COMPANY_INFO[COMPANY_INFO_FAX_IDX] = "+234-805-217-9585";
    	COMPANY_INFO[COMPANY_INFO_EMAIL_IDX] = "college@fcahptib.edu.ng";
    	COMPANY_INFO[COMPANY_INFO_ACCR_IDX] = "FCAHPT";
    	COMPANY_INFO[COMPANY_INFO_WEB_IDX] = "http://www.fcahptib.edu.ng";
    	
    	return COMPANY_INFO;
    }
    
    //number fields
	public final static int MRS_TICKET_IDX = 0;
	//text fields
	public final static int MRS_SITE_NAME_IDX = 0;	
	public final static int MRS_ISSUE_IDX = 1;
	public final static int MRS_TITLE_IDX = 2;
	public final static int MRS_PRIORITY_IDX = 3;	
	public final static int MRS_STATUS_IDX = 4;
	public final static int MRS_STAFF_IDX = 5;
	public final static int MRS_DETAILS_IDX = 6;
	public final static int MRS_SITE_IDX = 7;	
	//create date
	public final static int MRS_REQUEST_IDX = 0;
	public final static int MRS_CREATE_IDX = 1;		
	public final static String[] MRS_TEXT_FIELD_DESC = {"SITE NAME", "ISSUE", "TITLE", "PRIORITY", "STATUS", "ASSIGNED STAFF", "DETAILS", "SITE ID"}; 
	public final static String[] MRS_NUM_FIELD_DESC = {"TICKET NO"};
	public final static String[] MRS_DATE_FIELD_DESC = {"CREATION DATE", "REQUEST DATE"};

	public final static String[] MRS_ISSUES = {"ADHOC ELECTRICAL", "ADHOC PLUMBING", "ADHOC REPAIR", "ADHOC OTHER",
	"SCHEDULED ELECTRICAL", "SCHEDULED PLUMBING", "SCHEDULED REPAIR", "SCHEDULED GENERATOR", "SCHEDULED OTHER"};
	public final static String MRS_STATUS_COMPLETE = "Completed";
	public final static String[] MRS_STATUSES = {"Started", MRS_STATUS_COMPLETE, "Cancelled", "Suspended", "Pending Feedback"};
	public final static String[] MRS_PRIORITIES = {"LOW", "MEDIUM", "HIGH", "CRITICAL"};
	
	public final static int LEG_OWNR_IDX = 1;
	public final static int LEG_BLD_IDX = 2;	
	public final static int LEG_APT_IDX = 3;
	public final static int LEG_UPDT_USER_USR = 4;
	public final static int LEG_STAFF_IDX = 5;	
	public final static int LEG_CLNT_FLDR_IDX = 6;
	public final static int LEG_CCY_RT_IDX = 0;
	public final static int LEG_CCY_AG_IDX = 7;
	public final static int LEG_CCY_LG_IDX = 8;
	public final static int LEG_DT_CRT_IDX = 0;
	public final static int LEG_DT_STR_IDX = 1;
	public final static int LEG_DT_END_IDX = 2;
	public final static int LEG_DT_AG_IDX = 3;
	public final static int LEG_DT_RT_IDX = 4;
	public final static int LEG_DT_LG_IDX = 5;
	public final static int LEG_FEE_RT_IDX = 0;
	public final static int LEG_FEE_AG_IDX = 1;
	public final static int LEG_FEE_LG_IDX = 2;

	public final static String DEFAULT_CURRENCY = "NGN";
	public final static HashMap<String, String> CURRENCY_MAP_NAME = new HashMap<String, String>();
	static
	{
		//CURRENCY_MAP_NAME.put("USD", "Dollars");
		//CURRENCY_MAP_NAME.put("GBP", "Pounds");
		//CURRENCY_MAP_NAME.put("EUR", "Euros");
		CURRENCY_MAP_NAME.put("NGN", "Naira");
	}
	

	public final static HashMap<String, String> CURRENCY_MAP_SIGN = new HashMap<String, String>();
	static
	{
		//CURRENCY_MAP_SIGN.put("USD", "\u0024");
		//CURRENCY_MAP_SIGN.put("GBP", "\u00A3");
		//CURRENCY_MAP_SIGN.put("EUR", "\u20AC");
		CURRENCY_MAP_SIGN.put("NGN", "\u20A6");
	}
}
