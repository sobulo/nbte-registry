package com.fertiletech.nbte.campusclient;


import com.fertiletech.nbte.campusclient.utils.JavaScriptInjector;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.ScriptElement;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;

public class StudentApp implements EntryPoint{
	
	boolean showPortal = false;
	private void setupCustomerApplyArea(final Panel rootPanel)
	{
		rootPanel.add(new CustomerWebApp());
		additionalResourceSetups();		
	}
	
	private void additionalResourceSetups()
	{
		try
		{
        Document doc = Document.get();
        ScriptElement script = doc.createScriptElement();
        script.setSrc("https://apis.google.com/js/plusone.js");
        script.setType("text/javascript");
        script.setLang("javascript");
        doc.getBody().appendChild(script);

        JavaScriptInjector
                .inject("!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=\"//platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");");
        JavaScriptInjector
                .inject("(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));");
		}
		catch(Exception e)
		{
			GWT.log(e.getMessage());
		}
		
	}	

	@Override
	public void onModuleLoad() {
		Panel rootPanel = RootPanel.get();
		setupCustomerApplyArea(rootPanel);
	}
}
