package com.fertiletech.nbte.campusclient;

public enum ArgumentType {
	ORDER_ARG, LOAN_ARG, LOAN_PARENT_ARG, NO_ARG, NOT_SPECIFIED
}
