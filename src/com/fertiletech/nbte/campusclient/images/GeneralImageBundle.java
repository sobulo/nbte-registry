/**
 * 
 */
package com.fertiletech.nbte.campusclient.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{

	//home page images
	@Source("logo.png")
	ImageResource logoSmall();
	
	ImageResource reghistory();
	ImageResource regstatus();
	ImageResource thankban();
	ImageResource thankside();
	ImageResource register();
	
	ImageResource grades();
	ImageResource collegewebsite();
	ImageResource postutme();
	ImageResource servingyou();
}
