/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.campusclient;

import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public interface HyperlinkedPanel {
    public Hyperlink getLink();
    public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args);
    public ArgumentType getArgumentType();
}
