package com.fertiletech.nbte.campusclient.utils;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.xtras.BSLoanFieldValidator;
import com.fertiletech.nbte.campusclient.utils.xtras.BSTextWidget;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.FormValidator;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MailingListItem{

	PopupPanel panel = new PopupPanel(true);
	public MailingListItem(final boolean isLoggedIn)
	{
		panel.addStyleName("cartPanel");
		VerticalPanel container = new VerticalPanel();
		HorizontalPanel fields = new HorizontalPanel();
		fields.setSpacing(10);
		final BSTextWidget<String> emailBox = new BSTextWidget<String>();
		emailBox.setLabel("Email");
		emailBox.setHelp("Email Address");
		final BSTextWidget<String> nameBox = new BSTextWidget<String>();
        nameBox.setLabel("Full Name");
		nameBox.setHelp("Enter Name");
		final Button subscribe = new Button("SUBSCRIBE");
		fields.add(nameBox);
		fields.add(emailBox);
		container.add(fields);
		container.add(subscribe);
		subscribe.setBlock(true);
		subscribe.setMarginTop(10);
		panel.add(container);
		subscribe.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				HashMap<String, String> errorMap = new HashMap<String, String>();
				BSTextWidget<String>[] fields = new BSTextWidget[2];
				fields[0] = nameBox;
				fields[1] = emailBox;
				if(isLoggedIn)
				{
					BankUserCookie uc = BankUserCookie.getCookie();
					emailBox.setValue(uc.getEmail());
					nameBox.setValue(uc.getUserName());
				}
				String[] formFields = {ApplicationFormConstants.SURNAME, ApplicationFormConstants.EMAIL};
				for(int i = 0; i < formFields.length; i++)
				{
					String formKey = formFields[i];
					FormValidator[] oldStyleValidator = ApplicationFormConstants.ORDER_VALIDATORS.get(formKey);
					BSLoanFieldValidator validator = new BSLoanFieldValidator(oldStyleValidator, fields[i], formKey, errorMap, true);
					validator.markErrors();
				}
				if(errorMap.size() > 0)
				{
					CustomerAppHelper.showErrorMessage(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found. See highlighted fields.");
					return;
				}
				subscribe.setEnabled(false);
				/*CustomerAppHelper.SHOP_SERVICE.subscribeToMailingList(nameBox.getValue(), emailBox.getValue(), new AsyncCallback<String>() {
					
					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						panel.hide();
						nameBox.clear();
						emailBox.clear();
						subscribe.setEnabled(true);
					}
					
					@Override
					public void onFailure(Throwable caught) {
						subscribe.setEnabled(true);
						CustomerAppHelper.showErrorMessage("Unable to check subscription status. Please try again later.<br/> " + caught.getMessage());
					}
				});*/
			}
		});
	}
	
	public void show(UIObject target)
	{
		panel.showRelativeTo(target);
	}

}
