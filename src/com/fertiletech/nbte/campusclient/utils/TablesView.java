package com.fertiletech.nbte.campusclient.utils;

/*
 * #%L
 * GwtBootstrap3
 * %%
 * Copyright (C) 2013 GwtBootstrap3
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Date;
import java.util.List;

import org.gwtbootstrap3.client.ui.AnchorListItem;
import org.gwtbootstrap3.client.ui.Pagination;
import org.gwtbootstrap3.client.ui.gwt.CellTable;

import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.NumberCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.AbstractCellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.RangeChangeEvent;

/**
 * @author Joshua Godi
 */
public class TablesView extends Composite{
    @UiField(provided = true)
    CellTable<TableMessage> cellTable = new CellTable<TableMessage>(10);
    @UiField
    Pagination cellTablePagination;

    boolean headerInitialized = false;
    
    interface Binder extends UiBinder<Widget, TablesView> {
    }

    private SimplePager cellTablePager = new SimplePager();
    private ListDataProvider<TableMessage> cellTableProvider = new ListDataProvider<TableMessage>();
	private Binder uiBinder = GWT.create(Binder.class);

    public TablesView() {
        initWidget(uiBinder .createAndBindUi(this));
    }

    TableMessageHeader header = null;
    public void showTable(List<TableMessage> data) {
    	List<TableMessage> list = cellTableProvider.getList();
    	list.clear();
    	if(data.get(0) instanceof TableMessageHeader)
    	{
    		header = (TableMessageHeader) data.remove(0);
    		if(!headerInitialized)
    		{
    			initTable(cellTable, cellTablePager, cellTablePagination, cellTableProvider, header);
    			headerInitialized = true;
    		}
    	}
    	
    	list.addAll(data);
        cellTableProvider.flush();
        rebuildPager(cellTablePagination, cellTablePager);
    }
    
    public List<TableMessage> getTableData()
    {
    	return cellTableProvider.getList();
    }
    
	public void setEmptyWidget(Widget w) {
		cellTable.setEmptyTableWidget(w);
	}    

    public TableMessageHeader getTableHeader()
    {
    	return header;
    }
    
    public boolean isInitialized()
    {
    	return headerInitialized;
    }
    

    
    private void initTable(AbstractCellTable<TableMessage> grid, final SimplePager pager, final Pagination pagination, ListDataProvider<TableMessage> dataProvider, TableMessageHeader header) {
    	initTableColumns(header, grid);
        grid.addRangeChangeHandler(new RangeChangeEvent.Handler() {

            @Override
            public void onRangeChange(RangeChangeEvent event) {
                rebuildPager(pagination, pager);
            }
        });

        pager.setDisplay(grid);
        pagination.clear();
        if(pager.getPageCount() <= 1)
        	pagination.setVisible(false);
        else
        	pagination.setVisible(true);
        dataProvider.addDataDisplay(grid);
    }

	public static void initTableColumns(TableMessageHeader header,
			final AbstractCellTable<TableMessage> cellTable) {
		int dateIdx, textIdx, numIdx;
		dateIdx = textIdx = numIdx = 0;
		for (int i = 0; i < header.getNumberOfHeaders(); i++) {
			TableMessageContent headerType = header.getHeaderType(i);
			GWT.log("Header: " + i + " Type: "
					+ (headerType == null ? null : headerType) + " Name: "
					+ (header.getText(i) == null ? null : header.getText(i)));
			if (headerType.equals(TableMessageContent.TEXT)) {
				final int messageIndex = textIdx++;
				Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
						new TextCell()) {
					@Override
					public String getValue(TableMessage object) {
						return object.getText(messageIndex);
					}
				};
				//textColumn.setSortable(true);
				//textColumn.setDefaultSortAscending(false);
				//sortHandler.setComparator(textColumn, TableMessage
				//		.getMessageComparator(messageIndex, headerType));
				cellTable.addColumn(textColumn, header.getText(i));
				// cellTable.setColumnWidth(addressColumn, 60, Unit.PCT);
			} else if (headerType.equals(TableMessageContent.NUMBER)) {
				String fmtOption = header.getFormatOption(i);
				if(fmtOption == null)
					fmtOption = "#,##0.00";
				final NumberFormat nf = NumberFormat.getFormat(fmtOption);

				final int messageIndex = numIdx++;
				Column<TableMessage, ?> columnToAdd; 
				if (header.isEditable(i)) {
					final EditTextCell ec = new EditTextCell();
					Column<TableMessage, String> textColumn = new Column<TableMessage, String>(
							ec) {

						@Override
						public String getValue(TableMessage object) {
							Double num = object.getNumber(messageIndex);
							if(num == null) return "edit";
							String numStr = nf.format(num);
							return numStr;
						}
					};
					textColumn
							.setFieldUpdater(new FieldUpdater<TableMessage, String>() {

								@Override
								public void update(int index,
										TableMessage object, String value) {
									try {
										Double newNumber = nf.parse(value);
										object.setNumber(messageIndex, newNumber);
									} catch (NumberFormatException ex) {
										Window.alert("Ignoring entry [" + value +  "]. Invalid number specified");
										object.setNumber(messageIndex, null);
									}	
									ec.clearViewData(object.getMessageId());
									cellTable.redraw();
								}
							});
					columnToAdd = textColumn;
				} else {
					Column<TableMessage, Number> numberColumn = new Column<TableMessage, Number>(
							new NumberCell(nf)) {
						@Override
						public Number getValue(TableMessage object) {
							return object.getNumber(messageIndex);
						}
					};
					columnToAdd = numberColumn;
				}
				/*columnToAdd.setSortable(true);
				columnToAdd.setDefaultSortAscending(false);
				sortHandler.setComparator(columnToAdd, TableMessage
						.getMessageComparator(messageIndex, headerType));*/
				cellTable.addColumn(columnToAdd, header.getText(i));
			} else if (headerType.equals(TableMessageContent.DATE)) {
				final int messageIndex = dateIdx++;
				Column<TableMessage, Date> dateColumn = new Column<TableMessage, Date>(
						new DateCell(GUIConstants.DEFAULT_DATE_FORMAT)) {
					@Override
					public Date getValue(TableMessage object) {
						return object.getDate(messageIndex);
					}
				};
				/*dateColumn.setSortable(true);
				dateColumn.setDefaultSortAscending(false);
				sortHandler.setComparator(dateColumn, TableMessage
						.getMessageComparator(messageIndex, headerType));*/
				cellTable.addColumn(dateColumn, header.getText(i));
			}
			/*
			 * else if(headerType.equals(TableMessageContent.HTML)) { final int
			 * messageIndex = textIdx++; Column<TableMessage, SafeHtml>
			 * htmlColumn = new Column<TableMessage, SafeHtml>(new
			 * SafeHtmlCell()) {
			 * 
			 * @Override public SafeHtml getValue(TableMessage object) { return
			 * new
			 * SafeHtmlBuilder().appendEscaped(object.getText(messageIndex)).
			 * toSafeHtml(); } }; htmlColumn.setSortable(true);
			 * htmlColumn.setDefaultSortAscending(false);
			 * sortHandler.setComparator(htmlColumn,
			 * TableMessage.getMessageComparator(messageIndex, headerType));
			 * cellTable.addColumn(htmlColumn, header.getText(i)); }
			 */
			else
				continue;
		}
	}
    
    

    private void rebuildPager(final Pagination pagination, final SimplePager pager) {
        pagination.clear();

        if (pager.getPageCount() == 0) {
            return;
        }

        AnchorListItem prev = pagination.addPreviousLink();
        prev.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                pager.previousPage();
            }
        });
        prev.setEnabled(pager.hasPreviousPage());

        for (int i = 0; i < pager.getPageCount(); i++) {
            final int display = i + 1;
            AnchorListItem page = new AnchorListItem(String.valueOf(display));
            page.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    pager.setPage(display - 1);
                }
            });

            if (i == pager.getPage()) {
                page.setActive(true);
            }

            pagination.add(page);
        }

        AnchorListItem next = pagination.addNextLink();
        next.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                pager.nextPage();
            }
        });
        next.setEnabled(pager.hasNextPage());
    }
    
	public void addColumn(Column<TableMessage, ?> col, String title) {
		cellTable.addColumn(col, title);
	}    
    
    public void clear()
    {
    	cellTableProvider.getList().clear();
    }
}
