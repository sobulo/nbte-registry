package com.fertiletech.nbte.campusclient.utils.xtras;

import java.util.Date;

import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateEvent;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateHandler;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasValue;

public class BSDatePicker extends DateTimePicker implements HasValue<Date>{

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Date> handler) {
		ChangeDateHandler dh = new ChangeDateHandler() {
			
			@Override
			public void onChangeDate(ChangeDateEvent evt) {
				ValueChangeEvent.fire(BSDatePicker.this, getValue());
			}
		};
		this.addChangeDateHandler(dh);
		return addHandler(handler, ValueChangeEvent.getType());
	}

}
