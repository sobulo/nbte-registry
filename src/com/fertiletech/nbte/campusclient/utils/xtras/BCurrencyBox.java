package com.fertiletech.nbte.campusclient.utils.xtras;

import org.gwtbootstrap3.client.ui.TextBox;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasValue;

public class BCurrencyBox extends Composite implements HasValue<String>, HasEnabled{
	final static String CURRENCY_FORMAT_STR = "#,##0.00";
	protected static String acceptableCharset = getAcceptedCharset();

	private static String getAcceptedCharset() {
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("0123456789.,");
		return strbuf.toString();
	}

	protected final TextBox amountBox;
	protected final NumberFormat formatter;

	public BCurrencyBox() {
		formatter = NumberFormat.getFormat(CURRENCY_FORMAT_STR);
		amountBox = new TextBox();
		initWidget(amountBox);

		amountBox.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				amountBox.setText(reformatContent());
			}
		});

		amountBox.addKeyDownHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER)
					amountBox.setText(reformatContent());
			}
		});

		amountBox.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.isAnyModifierKeyDown())
					amountBox.cancelKey();

				if (Character.isLetterOrDigit(event.getCharCode())
						&& acceptableCharset.indexOf(event.getCharCode()) == -1)
					amountBox.cancelKey();

				if (Character.isLetterOrDigit(event.getCharCode())
						&& amountBox.getText().length() >= 16)
					amountBox.cancelKey();

			}
		});

	}

	public void setEnabled(boolean enabled)
	{
		amountBox.setEnabled(enabled);
	}
	
	/*
	 * This method is used to retrieve the amount value user inputed. @return
	 * user inputed value in double.
	 */
	public Double getAmount() {
		try {
			return formatter.parse(amountBox.getText());
		} catch (NumberFormatException ex) {
			return null;
		}
	}
	
	public String getAmountString()
	{
		Double amount = getAmount();
		if(amount == null) return null;
		return amount.toString();
	}
	
	public void setAmountString(String amount)
	{
		if(amount == null) amountBox.setValue(null);
		amountBox.setValue(amount);
	}

	public void setAmount(Double amt) {
		if (amt == null)
			amountBox.setValue("");
		else {
			amountBox.setValue(String.valueOf(amt));
			amountBox.setValue(reformatContent());
		}
	}

	protected String reformatContent() {
		String str = amountBox.getText();
		double amount;
		try {
			amount = formatter.parse(str);
		} catch (NumberFormatException e) {
			return "";
		}
		return formatter.format(amount);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return amountBox.addValueChangeHandler(handler);
	}

	@Override
	public String getValue() {
		Double amount = getAmount();
		if(amount == null) return null;
		return String.valueOf(amount);
	}

	@Override
	public void setValue(String value) {
		Double amount = ((value == null || "".equals(value.trim()))? null : Double.valueOf(value));
		setAmount(amount);
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		amountBox.setValue(value, fireEvents);
	}

	@Override
	public boolean isEnabled() {
		return amountBox.isEnabled();
	}
}
