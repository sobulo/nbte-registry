package com.fertiletech.nbte.campusclient.utils.xtras;

import org.gwtbootstrap3.client.ui.constants.ValidationState;



public abstract class BSControlValidator<T>{
	final boolean isError;
	protected String text;
	protected abstract boolean validate();
	protected BSTextWidget<T> controlWidget;
	
	public BSControlValidator(BSTextWidget<T> widget, boolean isErrorType, String helpMessage)
	{
		isError = isErrorType;
		text = helpMessage;
		this.controlWidget = widget;
	}
	
	public boolean markErrors()
	{
		ValidationState result = ValidationState.NONE;
		
			if(validate())
				result = ValidationState.NONE; //ideally should be success, TODO ensure fields without validators get marked success as well
			else
			{
				String prefix = "Error: ";
				if(isError)
					result = ValidationState.ERROR;
				else
				{
					result = ValidationState.WARNING;
					prefix = "Warning: ";
				}
				controlWidget.setHelp(prefix + text); 
			}
		controlWidget.setType(result);
		return (result != ValidationState.ERROR);
	}
	
	public void clearErrors()
	{
		controlWidget.clear(false);
	}
}
