package com.fertiletech.nbte.campusclient.utils.xtras;

public class BSTextNotEmptyValidator<T> extends BSControlValidator<T>
{

	public BSTextNotEmptyValidator(BSTextWidget<T> widget) {
		super(widget, true, "Should not be empty, please enter a value");
	}

	@Override
	protected boolean validate() {
		if(controlWidget.getValue() == null)
			return false;
		if(controlWidget.getValue() instanceof String)
		{
			if(((String) controlWidget.getValue()).trim().length() == 0)
				return false;
		}
		return true;
	}	
}