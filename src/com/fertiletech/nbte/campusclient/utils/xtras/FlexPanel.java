package com.fertiletech.nbte.campusclient.utils.xtras;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.gwtbootstrap3.client.ui.ListGroupItem;

import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Widget;

public class FlexPanel extends Composite implements HasEnabled{

	@UiField
	BSListBox rowCount;
	
	@UiField
	FlexTable contentSlot;
	
	@UiField
	ListGroupItem label;
	
	TableMessageHeader header;
	HashMap<Integer, String> columnIndices;
	
	//int[] componentWidths;
	
	int MAX_NO_ROWS = 10;
	private static FlexPanelUiBinder uiBinder = GWT
			.create(FlexPanelUiBinder.class);

	interface FlexPanelUiBinder extends UiBinder<Widget, FlexPanel> {
	}

	public FlexPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		GWT.log("Created Binder");
				
		rowCount.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				int numOfRows = Integer.valueOf(rowCount.getValue());
				int currentNumOfRows = contentSlot.getRowCount() - 1; //subtract 1 for header
				
				if(numOfRows == currentNumOfRows)
					return;
				
				int numOfOperations = Math.abs(numOfRows - currentNumOfRows);
				for(int i = 0; i < numOfOperations; i++)
				{
					if(numOfRows > currentNumOfRows)
					{
						if(contentSlot.getRowCount() == 1)
							contentSlot.getRowFormatter().setVisible(0, true);						
						addRow(new HashMap<String, String>());
					}
					else
					{
						removeRow();
						if(contentSlot.getRowCount() == 1)
							contentSlot.getRowFormatter().setVisible(0, false);
					}
				}
			}
		});
	}
	
	public void clear()
	{
		int currentNumOfRows = contentSlot.getRowCount() - 1; 
		for(int i = currentNumOfRows; i > 0; i--)
			removeRow();
	}
	
	public void initHeader(TableMessageHeader h, String singular, String plural, Integer noRows)
	{
		if(noRows == null)
			noRows = MAX_NO_ROWS;
		for(int i = 0; i < noRows; i++)
		{
			String appendix = i == 1 ? singular : plural;
			rowCount.addItem(String.valueOf(i) + " " + appendix,String.valueOf(i));
		}
		header = h;
		columnIndices = new HashMap<Integer, String>();
		//label.setText(h.getCaption());
		contentSlot.setWidth("100%");
		contentSlot.setCellPadding(2);
		for(int i = 0; i < h.getNumberOfTextFields(); i++)
		{
			GWT.log("Adding heading to: " + i + " " + h.getText(i));
			contentSlot.setHTML(0, i, "<b>" + h.getText(i) + "</b>");
			columnIndices.put(i, h.getText(i));
			double width = h.getFormatOption(i) == null ? (100.0/h.getNumberOfTextFields()-5) : Integer.valueOf(h.getFormatOption(i));
			contentSlot.getColumnFormatter().setWidth(i, width + "%");
		}
		contentSlot.getRowFormatter().setVisible(0, false);
	}
	
	public void setLabel(String label)
	{
		this.label.setText(label);
	}
	
	public void initRows(ArrayList<HashMap<String, String>> rows)
	{
		if(rows.size() > 0)
			contentSlot.getRowFormatter().setVisible(0, true);						
		for(HashMap<String, String> r : rows)
			addRow(r);
		rowCount.setValue(String.valueOf(rows.size()), false);
	}
	
	private void addRow(HashMap<String, String> values)
	{
		int newIndex = contentSlot.getRowCount();
		for(int i = 0; i < header.getNumberOfTextFields(); i++)
		{
			BSTextWidget cell;
			if(header.getHeaderType(i).equals(TableMessageContent.DATE))
				cell = new BSTextWidget<Date>();		
			else
				cell = new BSTextWidget<String>();
			if(!rowCount.isEnabled())
				cell.setEnabled(false);

			cell.setWidth("95%");
			String valKey = columnIndices.get(i);
			if(valKey != null)
				cell.setValue(values.get(valKey));
			contentSlot.setWidget(newIndex, i, cell);
		}
	}
	
	public ArrayList<HashMap<String, String>> getAllRows()
	{
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String,String>>();
		for(int row = 1; row < contentSlot.getRowCount(); row++)
		{
			HashMap<String, String> rowContent = new HashMap<String, String>();
			for(int col = 0; col < header.getNumberOfTextFields(); col++)
			{
				BSTextWidget cell = (BSTextWidget) contentSlot.getWidget(row, col);
				rowContent.put(columnIndices.get(col), cell.getValue().toString());
			}
			result.add(rowContent);
		}
		return result;
	}
	
	private void removeRow()
	{
		contentSlot.removeRow(contentSlot.getRowCount() - 1);
	}

	@Override
	public boolean isEnabled() {
		return rowCount.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		rowCount.setEnabled(enabled);
		for(int row = 1; row < contentSlot.getRowCount(); row++)
		{
			for(int col = 0; col < header.getNumberOfTextFields(); col++)
			{
				BSTextWidget cell = (BSTextWidget) contentSlot.getWidget(row, col);
				cell.setEnabled(enabled);
			}
		}
	}
	

}
