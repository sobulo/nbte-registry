package com.fertiletech.nbte.campusclient.utils.xtras;

public class BSChainedValidator<T> extends BSControlValidator<T>{

	BSControlValidator<T>[] validators;
	public BSChainedValidator(BSControlValidator<T>[] validators) {
		super(validators[0].controlWidget, true, null);
		this.validators = validators;
	}

	@Override
	protected boolean validate() {
		boolean ok = true;
		int i;
		for(i = 0; i < validators.length; i++)
		{
			ok = ok && validators[i].validate();
			if(!ok)
				break;
		}
		if(!ok)
			this.text = validators[i].text;
		return ok;
	}

}
