package com.fertiletech.nbte.campusclient.utils.xtras;


import org.gwtbootstrap3.client.ui.FormGroup;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.TextArea;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.Tooltip;
import org.gwtbootstrap3.client.ui.base.HasPlaceholder;
import org.gwtbootstrap3.client.ui.base.ValueBoxBase;
import org.gwtbootstrap3.client.ui.constants.ValidationState;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class BSTextWidget<T> extends Composite implements HasValue<T>, HasEnabled{

	private static BSTextWidgetUiBinder uiBinder = GWT
			.create(BSTextWidgetUiBinder.class);

	interface BSTextWidgetUiBinder extends UiBinder<Widget, BSTextWidget> {
	}
	
	@UiField
	FormLabel label;
	@UiField
	SimplePanel fieldSlot;
	
	public HasValue<T> field;
	
	@UiField
	Tooltip help;
	
	@UiField
	FormGroup container;
	
	@UiField(provided = true)
	final String fieldId = Document.get().createUniqueId();
	
	public BSTextWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		if(fieldSlot.getWidget() == null)
		{
			GWT.log("Default initialization used");
			addFieldWidget(new TextBox());
		}		
		clear();
	}
	
	public void setType(ValidationState type)
	{
		container.setValidationState(type);
	}
	
	public void setEnabled(boolean enabled)
	{
		((HasEnabled)field).setEnabled(enabled);
		container.setValidationState(ValidationState.NONE);
	}
	
	public void setLabel(String text) {
		if(text == null)
			label.setText("");
		else
			label.setText(text);
	}
	
	public String getLabel()
	{
		return label.getText();
	}

	public void setTempDisplay(String placeHolder)
	{
		if(field instanceof ValueBoxBase)
			((ValueBoxBase) field).setPlaceholder(placeHolder);
	}
	
	public String getTempDisplay()
	{
		if(field instanceof ValueBoxBase)
			return ((ValueBoxBase) field).getPlaceholder();
		return null;		
	}

	public String getHelp() {
		return help.getTitle();
	}

	public void setHelp(String helpVal) {	
		try
		{
			
			if(field instanceof HasPlaceholder)
				((HasPlaceholder) field).setPlaceholder(helpVal.split(".")[0]);
			help.setText(helpVal);
			help.reconfigure();
		}
		catch(Exception e){}
	}
		
	void clear(boolean clearValue)
	{
		if(clearValue)
			field.setValue(null);
		help.setText(getTempDisplay()==null?"":getTempDisplay());
		container.setValidationState(ValidationState.NONE);
	}
	
	public void clear()
	{
		clear(true);
	}
	
	public String getDisplayText()
	{
		if(field instanceof BSDatePicker)
			return ((BSDatePicker) field).getTextBox().getText();
		else if(getValue() instanceof String)
			return (String) getValue();
		else
			return String.valueOf(getValue());
	}

	public void setDisplayText(String text)
	{
		if(field instanceof BSDatePicker)
			((BSDatePicker) field).getTextBox().setText(text);
		else if(field instanceof HasText)
			((HasText) field).setText(text);
		else
			setValue((T) text);
	}	

	@UiChild (limit=1)
	public void addFieldWidget(IsWidget w) {
		GWT.log("Custom initialization");
		field = (HasValue<T>) w;
		
		if(field instanceof ValueBoxBase)
		{
			((ValueBoxBase) field).setId(fieldId);
		}
		
		if(field instanceof TextBox)
		{
			((TextBox) field).setMaxLength(36);
		}
		else if(field instanceof TextArea)
		{
			((TextArea) field).setMaxLength(106);
		}
		
		fieldSlot.clear();
		fieldSlot.add(w);
		/*field.addFocusHandler(new FocusHandler() {
			
			@Override
			public void onFocus(FocusEvent event) {
				clear(false);
			}
		});*/
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<T> handler) {
		return field.addValueChangeHandler(handler);
	}

	@Override
	public T getValue() {
		return field.getValue();
	}

	@Override
	public void setValue(T value) {
		field.setValue(value);
	}

	@Override
	public void setValue(T value, boolean fireEvents) {
		field.setValue(value, fireEvents);
	}

	@Override
	public boolean isEnabled() {
		return ((HasEnabled) field).isEnabled();
	}
	
	public HasValue<T> getFieldWidget()
	{
		return field;
	}
	
	String span = null;
	public void setSpan(String span)
	{
		if(field != null)
		{
			((Widget) field).addStyleName(span);
		}
		else
			this.span = span;
	}
	
	public void markRequired()
	{
		label.setHTML(label.getText() + "<span style='margin-left:3px; color:navy'>**</span>");
	}
}
