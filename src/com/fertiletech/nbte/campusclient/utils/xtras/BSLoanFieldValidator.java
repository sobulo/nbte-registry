package com.fertiletech.nbte.campusclient.utils.xtras;

import java.util.Date;
import java.util.HashMap;

import com.fertiletech.nbte.shared.FormValidator;
import com.google.gwt.core.shared.GWT;

public class BSLoanFieldValidator<T> extends BSControlValidator<T>{

	String formKey;
	HashMap<String, String> errorMap;
	boolean isSubmit;
	FormValidator[] oldStyleValidators;
	
	public BSLoanFieldValidator(FormValidator[] validators, BSTextWidget<T> widget, String formKey, HashMap<String, String> errorMap, boolean isSubmit) {
		super(widget, true, null);
		this.formKey = formKey;
		this.errorMap = errorMap;
		this.isSubmit = isSubmit;
		this.oldStyleValidators = validators;
	}

	@Override
	protected boolean validate() {
		
		boolean ok = true;
		for(FormValidator v : oldStyleValidators)
		{
			T val = controlWidget.getValue();
			String strVal = null;
			if(val != null)
			{
				if(val instanceof String)
					strVal = (String) val;
				else if(val instanceof Date)
				{
					BSDatePicker cwDateBox = ((BSDatePicker) controlWidget.field);
					strVal = cwDateBox.getTextBox().getText();
					GWT.log("CDOB: " + strVal);
					GWT.log("CDATE: " + cwDateBox.getValue());
				}
				else
					strVal = val.toString();
					
			}
			ok = ok && v.validate(formKey, strVal, errorMap, isSubmit);
			if(!ok)
				break;
		}
		if(!ok)
			text = errorMap.get(formKey);
		return ok;
	}

}
