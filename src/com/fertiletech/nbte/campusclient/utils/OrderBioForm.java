package com.fertiletech.nbte.campusclient.utils;

import java.util.HashMap;

import com.fertiletech.nbte.campusclient.utils.xtras.BSTextWidget;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class OrderBioForm extends Composite{
	@UiField
	BSTextWidget<String> surname;
	@UiField
	BSTextWidget<String> otherNames;
	@UiField
	BSTextWidget<String> department;
	@UiField
	BSTextWidget<String> matric;
	
	private static GetStartedFormUiBinder uiBinder = GWT
			.create(GetStartedFormUiBinder.class);

	interface GetStartedFormUiBinder extends UiBinder<Widget, OrderBioForm> {
	}

	
	public OrderBioForm() {
		initWidget(uiBinder.createAndBindUi(this));
		enableMiniApplyForm(false);
		
	}
	
	public void enableMiniApplyForm(boolean enabled)
	{
		enabled = false;
		surname.setEnabled(enabled);
		otherNames.setEnabled(enabled);
		matric.setEnabled(enabled);
		department.setEnabled(enabled);
		
	}
	
	public void clear()
	{
		enableMiniApplyForm(true);
		setFormData(new HashMap<String, String>());
	}
	
	public void setFormData(HashMap<String, String> data)
	{
		surname.setValue(data.get(ApplicationFormConstants.SURNAME));
		otherNames.setValue(data.get(ApplicationFormConstants.FIRST_NAME));
		department.setValue(data.get(ApplicationFormConstants.DEPARTMENT));
		matric.setValue(data.get(ApplicationFormConstants.MATRIC_NO));

	}
	
	public String[] getNameParts(String fullName)
	{
		String[] parts = fullName.split(" ");
		for(int i = 0; i < parts.length; i++)
			parts[i] = parts[i].trim();
		String[] result = new String[3];
		if(parts.length == 1)
			result[0] = parts[0];
		else if( parts.length == 2)
		{
			result[0] = parts[1];
			result[1] = parts[0];
		}
		else if(parts.length > 2)
		{
			result[0] = parts[2];
			result[1] = parts[0];
			result[2] = parts[1];
		}
		return result;
	}
}
