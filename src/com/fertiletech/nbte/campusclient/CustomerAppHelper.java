/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.nbte.campusclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.IconStack;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Styles;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.extras.growl.client.ui.Growl;
import org.gwtbootstrap3.extras.growl.client.ui.GrowlHelper;
import org.gwtbootstrap3.extras.growl.client.ui.GrowlOptions;

import com.fertiletech.nbte.campusclient.images.GeneralImageBundle;
import com.fertiletech.nbte.campusclient.panels.LeaseCart;
import com.fertiletech.nbte.campusclient.panels.MortgageDownloadPanel;
import com.fertiletech.nbte.campusclient.panels.MyOrderStatus;
import com.fertiletech.nbte.campusclient.panels.OrderBookedPanel;
import com.fertiletech.nbte.campusclient.panels.OrderHistory;
import com.fertiletech.nbte.campusclient.panels.ProductListing;
import com.fertiletech.nbte.campusclient.panels.ViewDetails;
import com.fertiletech.nbte.campusclient.panels.bio.FileIploadPanel;
import com.fertiletech.nbte.campusclient.panels.bio.StudentDetailsPanel;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.client.MessagingManager;
import com.fertiletech.nbte.client.MessagingManagerAsync;
import com.fertiletech.nbte.client.RegistryService;
import com.fertiletech.nbte.client.RegistryServiceAsync;
import com.fertiletech.nbte.client.TicketService;
import com.fertiletech.nbte.client.TicketServiceAsync;
import com.fertiletech.nbte.client.UtilsService;
import com.fertiletech.nbte.client.UtilsServiceAsync;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Geolocation.PositionOptions;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 * IMPORTANT: only use package access or private access in this class. This is to prevent
 * unnecessary loading of certain gwt files in parts of the app that are public
 * 
 * TODO run gwt code splitting analytic tools to explicitly confirm which classes get loaded by public 
 * sections of the app
 */
public class CustomerAppHelper {
    
	public final static String TOKEN_COOKIE = "ng.edu.fcahptib.pubapparg";
    public final static RegistryServiceAsync REGISTRATION_SERVICE = GWT.create(RegistryService.class);
    public final static MessagingManagerAsync MESSAGING_SERVICE = GWT.create(MessagingManager.class);
    public final static TicketServiceAsync TICKET_SERVICE = GWT.create(TicketService.class);
    public final static UtilsServiceAsync READ_SERVICE = GWT.create(UtilsService.class);
    public final static GeneralImageBundle generalImages = GWT.create(GeneralImageBundle.class);
    private final static String APP_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
    final static String APP_PAGE_URL;
	final static int TOKEN_ARGS_PARAM_IDX = 0;
	final static int TOKEN_ARGS_RECENTLOAN_IDX = 1;
	final static int TOKEN_ARGS_EMAIL_IDX = 2;  
	final static int TOKEN_ARGS_OTHER_PARAM_IDX = 3;
    
    static
    {
    	if(!GWT.isProdMode()) //true if dev mode
    		APP_PAGE_URL = APP_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
    	else
    		APP_PAGE_URL = APP_PAGE_URL_PREFIX;
    }

    static ViewDetails detailPanel;
    static HashMap<String, IconType> CATEGORIES = new HashMap<String, IconType>();
    
    public static HashMap<String, IconType> getCategoryIconMap()
    {
    	if(CATEGORIES.size() == 0)
    	{
	        CATEGORIES.put(NameTokens.ALT_FSH, IconType.TINT);
	        CATEGORIES.put(NameTokens.ALT_SLT, IconType.FLASK);
	        CATEGORIES.put(NameTokens.ALT_STA, IconType.BAR_CHART_O);
	        CATEGORIES.put(NameTokens.ALT_AEM, IconType.TREE);
	        CATEGORIES.put(NameTokens.ALT_ANH, IconType.MEDKIT);
	        CATEGORIES.put(NameTokens.ALT_APT, IconType.LINUX);
	        CATEGORIES.put(NameTokens.ALT_AHP, IconType.GITHUB_ALT);
	        CATEGORIES.put(NameTokens.ALT_CS, IconType.LAPTOP);
    	}
        return CATEGORIES;
    }
    
    static ViewDetails getDetailPanel()
    {
    	if(detailPanel == null)
    		detailPanel = new ViewDetails();
    	return detailPanel;
    }
    
    static HyperlinkedPanel shoppingCart = null;
    static HyperlinkedPanel getShoppingCart()
    {
    	if(shoppingCart != null)
    		return shoppingCart;
		shoppingCart = new HyperlinkedPanel() {
			LeaseCart cartPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(cartPanel == null)
					cartPanel = new LeaseCart();
				else
					cartPanel.refresh();
				return cartPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Register Now", NameTokens.CART);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
    	return shoppingCart;
    }

    static HyperlinkedPanel myOrderStatus = null;
    static HyperlinkedPanel getOrderPanel()
    {
    	if(myOrderStatus != null)
    		return myOrderStatus;
    	
		myOrderStatus = new HyperlinkedPanel() {
			MyOrderStatus orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new MyOrderStatus(isLoggedIn);
				orderPanel.fetchOrderDetails(args[TOKEN_ARGS_PARAM_IDX], isLoggedIn);
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Order Status", NameTokens.MYORDER);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
		
    	return myOrderStatus;
    }

    static HyperlinkedPanel uploadPanel = null;
    static HyperlinkedPanel getUploadPanel()
    {
    	if(uploadPanel != null)
    		return uploadPanel;
    	
		uploadPanel = new HyperlinkedPanel() {
			FileIploadPanel orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				BankUserCookie uc = BankUserCookie.getCookie();
				LoginRoles role = uc.getRole();
				if(role.equals(LoginRoles.ROLE_SCHOOL_PUBLIC))
				{
					return (new Paragraph("You must be logged in to use the upload file panel"));
				
				}
				else if(role.ordinal() >= LoginRoles.ROLE_SCHOOL_STUDENT.ordinal())
				{
					if(orderPanel == null)
						orderPanel = new FileIploadPanel();
					orderPanel.setLoanID(uc.getLoanID());
					return orderPanel;
				}
				else
					return (new Paragraph("No bios available for staff"));
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Upload Files", NameTokens.UPLOADS);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
		
    	return uploadPanel;
    }
    
    
    static HyperlinkedPanel bookpanel = null;
    static HyperlinkedPanel getOrderBookedPanel()
    {
    	if(bookpanel != null)
    		return bookpanel;
    	
		bookpanel = new HyperlinkedPanel() {
			OrderBookedPanel orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new OrderBookedPanel(isLoggedIn, isOps, args[TOKEN_ARGS_PARAM_IDX]);
				orderPanel.setOrderIDValue(args[TOKEN_ARGS_PARAM_IDX]);
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Thank You", NameTokens.THANKORDER);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
		
    	return bookpanel;
    }
    
    
    static HyperlinkedPanel orderHistory = null;
    static HyperlinkedPanel getOrderHistoryPanel()
    {
    	if(orderHistory != null)
    		return orderHistory;
    	
		orderHistory = new HyperlinkedPanel() {
			OrderHistory orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(orderPanel == null)
					orderPanel = new OrderHistory(isLoggedIn);
					return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("View Registration", NameTokens.ORDER_HISTORY);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
    	return orderHistory;
    }       

    static HyperlinkedPanel studBio = null;
    static HyperlinkedPanel getStudBioPanel()
    {
    	if(studBio != null)
    		return studBio;
    	
		studBio = new HyperlinkedPanel() {
			StudentDetailsPanel orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				if(!isLoggedIn)
					return new Heading(HeadingSize.H4, "You must be logged in to view your student bio");
				if(isOps)
					return new Heading(HeadingSize.H4, "You are currently logged in as staff. Bio available only to logged in students");
				if(orderPanel == null)
					orderPanel = new StudentDetailsPanel();
				orderPanel.refresh();
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Student Bio", NameTokens.STUD_BIO);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_PARENT_ARG;
			}
		};
    	return studBio;
    }

    static HyperlinkedPanel downloadPanel = null;
	private final static String NO_ID_MESSAGE = "You must be a currently enrolled student to use the downloads panel";
	private final static String NO_ID_OPS_MESSAGE = "This is a student only module. Use the staff module to generate bulk downloads (e.g. for an entire department)";

    static HyperlinkedPanel getDownloadPanel()
    {
    	if(downloadPanel != null)
    		return downloadPanel;
    	
		downloadPanel = new HyperlinkedPanel() {
			MortgageDownloadPanel orderPanel;
			Hyperlink link;
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				BankUserCookie uc = BankUserCookie.getCookie();
				LoginRoles role = uc.getRole();
				isLoggedIn = role.equals(LoginRoles.ROLE_SCHOOL_STUDENT);
				isOps = role.ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal();
				if(isOps)
				{
					HorizontalPanel h = new HorizontalPanel();
					IconStack ic = new IconStack();
					Icon base = new Icon(IconType.USER);
					base.setSize(IconSize.TIMES5);
					ic.add(base, true);
					base = new Icon(IconType.BAN);
					base.setSize(IconSize.TIMES5);
					//base.setSpin(true);
					ic.add(base, false);
					h.setSpacing(20);
					h.add(ic);
					h.add(new Paragraph(NO_ID_OPS_MESSAGE));
					return h;
				}
				
				if(!isLoggedIn)
				{
					HorizontalPanel h = new HorizontalPanel();
					IconStack ic = new IconStack();
					Icon base = new Icon(IconType.USER);
					base.setSize(IconSize.TIMES5);
					ic.add(base, true);
					base = new Icon(IconType.BAN);
					base.setSize(IconSize.TIMES5);
					//base.setSpin(true);
					ic.add(base, false);
					h.setSpacing(20);
					h.add(ic);
					h.add(new Paragraph(NO_ID_MESSAGE));
					return h;
				}			
				
				if(orderPanel == null)
					orderPanel = new MortgageDownloadPanel();
				orderPanel.setLoanID();
				return orderPanel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Download/Print", NameTokens.DOWNLOADS);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.LOAN_ARG;
			}
		};
    	return downloadPanel;
    }    
    
	static List<HyperlinkedPanel> initializePanels() {
        GWT.log("loading static block");
       
        List<HyperlinkedPanel> result = new ArrayList<HyperlinkedPanel>();
        
        HyperlinkedPanel detailLinkPanel = new HyperlinkedPanel() {
        	Hyperlink link;
			
			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
					String[] args) {
				String arg = args[TOKEN_ARGS_PARAM_IDX];
				ViewDetails panel = getDetailPanel();
				panel.displayProduct(arg);
				return panel;
			}
			
			@Override
			public Hyperlink getLink() {
				if(link == null)
					link = new Hyperlink("Course Detail", NameTokens.DETAILS);
				return link;
			}

			@Override
			public ArgumentType getArgumentType() {
				return ArgumentType.ORDER_ARG;
			}
		};
				
        result.add(detailLinkPanel);
        for(String altDiploma : NameTokens.DIPLM_DEPTS.keySet())
        {
        	final String diploma = altDiploma.equals(NameTokens.ALT_ND)?NameTokens.ND:NameTokens.HND;
        	HashSet<String> departmentList = NameTokens.DIPLM_DEPTS.get(altDiploma);
        	for(String dept : departmentList)
        	{
	        	final String department = dept;
	        	HyperlinkedPanel panel = new HyperlinkedPanel() {
	        		ProductListing widget;
					Hyperlink link;
					
					@Override
					public Widget getPanelWidget(boolean isLoggedIn, boolean isOps,
							String[] args) {
						if(widget == null)
						{
							widget = new ProductListing(department, diploma);
							widget.addValueChangeHandler(getDetailPanel());
						}
						return widget;
					}
					
					@Override
					public Hyperlink getLink() {
						if(link == null)
							link = new Hyperlink(department, NameTokens.getNameTargetMap().get(department));
						return link;
					}
	
					@Override
					public ArgumentType getArgumentType() {
						return ArgumentType.NO_ARG;
					}
				};
				result.add(panel);
        	}
        }
        result.add(getShoppingCart());
        result.add(getOrderPanel());
        result.add(getOrderHistoryPanel());
        result.add(getOrderBookedPanel());
        result.add(getStudBioPanel());
        result.add(getUploadPanel());
        result.add(getDownloadPanel());
        return result;
    }

	
  
    
    
	static Icon ico = new Icon();
	static Modal infoBox = new Modal();
	static HTML message = new HTML();
	static
	{
		ico.setSize(IconSize.TIMES3);
		HorizontalPanel temp = new HorizontalPanel();
		temp.setSpacing(10);
		temp.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		temp.add(ico);
		temp.add(message);
		temp.setSpacing(20);
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(10);
		Button dismiss = new Button("OK");
		dismiss.setType(ButtonType.PRIMARY);
		dismiss.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				infoBox.hide();
			}
		});
		dismiss.setBlock(true);
		container.add(temp);
		container.add(dismiss);
		infoBox.add(container);				
	}
	
	private static GrowlOptions goGood, goBad; 
	static
	{
		goGood = GrowlHelper.getNewOptions();
		goBad = GrowlHelper.getNewOptions();
		goGood.setSuccessType();
		goBad.setDangerType();
	}
	
	public static void showErrorMessage(String error)
	{
		  Growl.growl("ERROR",error,Styles.FONT_AWESOME_BASE + " " + IconType.WARNING.getCssName(),goBad);		
	}	

	public static void showInfoMessage(String info)
	{
		Growl.growl("INFO", info, Styles.FONT_AWESOME_BASE + " " + IconType.INFO_CIRCLE.getCssName(), goGood);
	}
	
	
	public static void setupCompanyOracle(SuggestBox suggestBox)
	{
		String[] companyListVals = {};
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
		for (int counter = 0; counter < companyListVals.length; counter++) {
			oracle.add(companyListVals[counter]);
		}		
	}
	
	public static void updateLatLngVals(final String loanId)
	{
		if(ClientUtils.BankUserCookie.getCookie().getRole().equals(LoginRoles.ROLE_SCHOOL_ADMIN)) return; //ensure only applicant positions are tracked
		if(Geolocation.isSupported())
		{
			PositionOptions opts = new Geolocation.PositionOptions();
			opts.setHighAccuracyEnabled(true);
			opts.setTimeout(15 * 1000); //wait 15 seconds
			Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() { 
				@Override
				public void onSuccess(Position result) {
					if(result == null || result.getCoordinates() == null)
						onFailure(null);
					else
						CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId,
							result.getCoordinates().getLatitude(), result.getCoordinates().getLongitude(), null);
				}
				
				@Override
				public void onFailure(PositionError reason) 
				{
					String message = reason==null?"":reason.getMessage();
					CustomerAppHelper.showErrorMessage("Position request failed/denied. " + message);
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
				}
			}, opts);
		}
		else
		{
			CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
		}
	}
	
	public static String getLoanID(String[] args)
	{
		return (args[TOKEN_ARGS_PARAM_IDX] == null?
				args[TOKEN_ARGS_RECENTLOAN_IDX] : args[TOKEN_ARGS_PARAM_IDX]);		
	}
	
}
