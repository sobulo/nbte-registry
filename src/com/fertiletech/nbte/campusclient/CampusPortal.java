package com.fertiletech.nbte.campusclient;

import org.gwtbootstrap3.client.ui.Container;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class CampusPortal implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		Container x = new Container();
		x.add(new WelcomePage().getPanelWidget(false, false, null));
		RootPanel.get().add(new ScrollPanel(x));
	}
}
