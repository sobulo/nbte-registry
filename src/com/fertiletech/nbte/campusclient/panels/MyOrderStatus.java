package com.fertiletech.nbte.campusclient.panels;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;

import com.fertiletech.nbte.campusclient.ArgumentType;
import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.OrderBioForm;
import com.fertiletech.nbte.campusclient.utils.TablesView;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MyOrderStatus extends Composite{
	@UiField
	TablesView cartList;
	@UiField
	OrderBioForm bioForm;
	@UiField
	Label dateCreated;
	@UiField
	Label orderId;
	@UiField
	Small message;
	@UiField
	Column  headerSlot;
	
	private static MyOrderStatusUiBinder uiBinder = GWT
			.create(MyOrderStatusUiBinder.class);

	
	//public static boolean isInitialized = false;
	interface MyOrderStatusUiBinder extends UiBinder<Widget, MyOrderStatus> {
	}

	//String loanID = "";
	public MyOrderStatus(boolean isLoggedIn) {
		initWidget(uiBinder.createAndBindUi(this));
		Image headerPic = new Image(CustomerAppHelper.generalImages.regstatus());
		headerPic.addStyleName("img-responsive");
		headerSlot.add(headerPic);
		headerSlot.addStyleName("productListingHeader");
		bioForm.enableMiniApplyForm(false);
		cartList.setEmptyWidget(new Paragraph("You are NOT yet registered for any courses this session"));
	}


	
	String savedId = null;	
	public void fetchOrderDetails(final String sessionID, final boolean isLoggedIn)
	{
		savedId = sessionID;
		GWT.log("running refresh");
		cartList.clear();
		Cookies.setCookie(ArgumentType.LOAN_ARG+CustomerAppHelper.TOKEN_COOKIE, null);
		
		if(!isLoggedIn)
		{
			cartList.setEmptyWidget(new Paragraph("Course information request cancelled. You must be logged in"));
			message.setHTML("Login to view your registration status");
			return;
		}
		
		if(sessionID == null || sessionID.trim().length() == 0) return;						
		CustomerAppHelper.REGISTRATION_SERVICE.getSessionInfo(sessionID, new AsyncCallback<List<TableMessage>>() {
			
			@Override
			public void onSuccess(List<TableMessage> result) {
				TableMessage summary = result.remove(0);
				updateSummaryInfo(summary, isLoggedIn);
				cartList.showTable(result);
				Cookies.setCookie(ArgumentType.LOAN_ARG+CustomerAppHelper.TOKEN_COOKIE, summary.getMessageId());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to retrieve order status, try refreshing your browser." +
						"\n<BR/> Error Msg: " + caught.getMessage());
				
			}
		});
	}
	
	public void updateSummaryInfo(TableMessage m, boolean isLoggedIn)
	{			
		
		HashMap<String, String> data = new HashMap<String, String>();
		data.put(ApplicationFormConstants.SURNAME, m.getText(RegistryDTO.STUD_SESS_SURNAME_IDX));
		data.put(ApplicationFormConstants.FIRST_NAME, m.getText(RegistryDTO.STUD_SESS_OTHER_NAMES_IDX));
		data.put(ApplicationFormConstants.MATRIC_NO, m.getText(RegistryDTO.STUD_SESS_MATRIC_IDX));
		data.put(ApplicationFormConstants.DEPARTMENT, m.getText(RegistryDTO.STUD_SESS_DIPL_IDX) + " - " + m.getText(RegistryDTO.STUD_SESS_DEPT_IDX));
		bioForm.setFormData(data);
		orderId.setHTML("<i style='color:gold'>Session:</i> " + m.getText(RegistryDTO.STUD_SESS_ACADEMIC_YEAR_IDX)); 
		dateCreated.setHTML("<i style='color:gold'>Semester:</i> " + m.getText(RegistryDTO.STUD_SESS_SEMESTER_IDX));
		StringBuilder status = new StringBuilder("<div>");
		status.append("<span style='color:orange;font-weight:bold'>Status: ").append(m.getText(RegistryDTO.STUD_SESS_STATUS_IDX)).append("</span><br/>");
		String msg = m.getText(RegistryDTO.STUD_SESS_MESSAGE_IDX);
		if(msg != null && msg.length() > 0)
			status.append("<div style='font-size:xx-small;color:red'>").append(msg).append("<hr/></div>");
		status.append("<p style='float:right;font-size:smaller'>").append(GUIConstants.DEFAULT_DATE_FORMAT.
				format(m.getDate(RegistryDTO.STUD_SESS_DATE_IDX))).append("</p>");			
		status.append("</div>");
		message.setHTML(status.toString());
		
	}
}
 