package com.fertiletech.nbte.campusclient.panels.bio;

import java.util.List;

import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.TablesView;
import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Widget;

public class FileIploadPanel extends Composite implements ClickHandler,
		FormPanel.SubmitCompleteHandler {

	@UiField
	Button submit;
	@UiField
	Button refresh;
	@UiField
	FileUpload selectFile;
	@UiField
	FormPanel uploadForm;
	//@UiField
	//HTML status;
	@UiField
	TablesView fileList;
	@UiField
	Hidden loanID;
	@UiField
	Paragraph finePrint;
	@UiField
	org.gwtbootstrap3.client.ui.Button goback;
	@UiField
	Small limitMessage;
	private static FileIploadPanelUiBinder uiBinder = GWT
			.create(FileIploadPanelUiBinder.class);

	interface FileIploadPanelUiBinder extends UiBinder<Widget, FileIploadPanel> {
	}

	AsyncCallback<List<TableMessage>> refreshCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper
					.showErrorMessage("File list refresh failed. Error was: "
							+ caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			boolean alreadyInitialized = fileList.isInitialized();
			fileList.showTable(result);
			if (!alreadyInitialized) {
				fileList.addColumn(getColumn(), "View");
				fileList.addColumn(getDeleteColumn(), "Delete");
			}
			if(result.size() >= 1)
			{
				enableAll(false);
				limitMessage.setHTML("Delete current image to upload another one." +
						" For unlimited online storage of all your files/documents, visit " +
						"<a href='http://drive.fcahptib.edu.ng'>drive.fcahptib.edu.ng</a>");
			}
			else
			{
				limitMessage.setHTML("You are yet to upload an image for your profile");
				enableAll(true);
			}
			CustomerAppHelper
					.showInfoMessage("File list refreshed. You have " + result.size() + " uploaded files");
		}
	};	
	
	public FileIploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		submit.addClickHandler(this);
		uploadForm.addSubmitCompleteHandler(this);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		selectFile.setName("chooseFile");
		goback.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.back();
			}
		});
		enableAll(false);
		
		refresh.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
				{
					CustomerAppHelper.showErrorMessage("No application selected. Aborting refresh");
					return;
				}				
				CustomerAppHelper.READ_SERVICE.getUploads(null, refreshCallback);
			}
		});
		
	}

	private void enableAll(boolean enabled) {
		selectFile.setEnabled(enabled);
		refresh.setEnabled(enabled);
		submit.setEnabled(enabled);
	}

	private void clear() {
		fileList.clear();
		loanID.setName("dummy");
		loanID.setValue("");
		enableAll(false);
	}

	private void setFinePrint(String style, String message)
	{
		finePrint.setHTML("<span style='" + "'>" + message + "</span>");		
	}
	
	public void setLoanID(String newLoanID) {
		clear();		
		/*if (newLoanID == null || newLoanID.trim().length() == 0)
		{
			String msg = "Uploads disabled. " + (isOps? "Select an application on the staff portal" : " Try refreshing your browser");
			CustomerAppHelper.showErrorMessage(msg);
			setFinePrint("color:red", msg);
			return;
		}
		
		if(isOps)
		{
			setFinePrint("background-color: purple; color:white;", "NOTEWORTHY: Uploads done here are visible to the applicant." +
					" Use the Drive picker on the staff portal to attach files that are private. " +
					" You can also choose to share such files with any other college staff and/or student");
		}*/
	
		loanID.setName(DTOConstants.GCS_LOAN_ID_PARAM);
		loanID.setValue(newLoanID);
		CustomerAppHelper.READ_SERVICE.getUploads(null, refreshCallback);
	}

	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		CustomerAppHelper.showInfoMessage("Upload Successful! " + event.getResults());
		CustomerAppHelper.READ_SERVICE.getUploads(null, refreshCallback);
	}

	@Override
	public void onClick(ClickEvent event) {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper
						.showErrorMessage("Upload failed, error message is: "
								+ caught.getMessage());
				enableAll(true);
			}

			@Override
			public void onSuccess(String result) {
				if (result == null)
					return;
				enableAll(true);
				uploadForm.setAction(result);
				uploadForm.submit();
			}
		};
		if(selectFile.getFilename() == null || selectFile.getFilename().trim().length()==0)
		{
			CustomerAppHelper.showErrorMessage("No file selected! Aborting upload.");
			return;
		}
		if(!selectFile.getFilename().toLowerCase().endsWith("jpg") && !selectFile.getFilename().toLowerCase().endsWith("jpeg"))
		{
			CustomerAppHelper.showErrorMessage("Only JPG picture format supported. Aborting upload.");
			return;
		}		
		enableAll(false);
		if(fileList.getTableData().size() > 0)
		{
			CustomerAppHelper.showErrorMessage("File limit of 1 reached. You must delete the previously " +
					"uploaded file before you can upload another one");
			return;
		}
		if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
		{
			CustomerAppHelper.showErrorMessage("No application selected. Aborting upload.");
			return;
		}
		CustomerAppHelper.READ_SERVICE.getUploadUrl(callback);
	}

	private Column<TableMessage, SafeHtml> getColumn() {
		SafeHtmlCell cell = new SafeHtmlCell();
		Column<TableMessage, SafeHtml> urlColumn = new Column<TableMessage, SafeHtml>(
				cell) {

			@Override
			public SafeHtml getValue(TableMessage object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				String url = object.getMessageId();
				sb.appendHtmlConstant("<a target='_blank' href='" + url
						+ "'>Download</a>");
				return sb.toSafeHtml();
			}
		};
		return urlColumn;
	}
	
	private Column<TableMessage, String> getDeleteColumn()
	{
		ButtonCell removeAttachment = new ButtonCell();
		Column<TableMessage, String> invCol = new Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "Delete File";
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {
				final String[] parts = object.getText(1).split("/", 2);
				
				final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Looks like attachment removal failed. " 
									+ caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						CustomerAppHelper.READ_SERVICE.getUploads(parts[0], refreshCallback);

					}

					@Override
					protected void callService(AsyncCallback<String> cb) {
						enableWarning(false);
						CustomerAppHelper.READ_SERVICE.deleteUpload(object.getText(1), cb);
					}
				};
				editAttachCallback.go("Deleting file " + parts[1]);				
			}
		});
		return invCol;
	}
}
