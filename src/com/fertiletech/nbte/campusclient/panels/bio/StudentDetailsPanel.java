package com.fertiletech.nbte.campusclient.panels.bio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.ThumbnailLink;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.xtras.BSControlValidator;
import com.fertiletech.nbte.campusclient.utils.xtras.BSDatePicker;
import com.fertiletech.nbte.campusclient.utils.xtras.BSLoanFieldValidator;
import com.fertiletech.nbte.campusclient.utils.xtras.BSTextWidget;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.FormValidator;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;

public class StudentDetailsPanel extends Composite{

	private static StudentDetailsPanelUiBinder uiBinder = GWT
			.create(StudentDetailsPanelUiBinder.class);

	interface StudentDetailsPanelUiBinder extends
			UiBinder<Widget, StudentDetailsPanel> {
	}

	public StudentDetailsPanel() {
		if(!LoginRoles.ROLE_SCHOOL_STUDENT.equals(BankUserCookie.getCookie().getRole()))
		{
			CustomerAppHelper.showErrorMessage("Only currently enrolled students may use this module");
		}		
		initWidget(uiBinder.createAndBindUi(this));
		setupValidators();
		setupDateBox(dob, 21);
		setupDateBox(dateModified, 4);
		enableBoxes(false);
		CustomerAppHelper.REGISTRATION_SERVICE.getBio(new AsyncCallback<HashMap<String,String>>() {
			
			@Override
			public void onSuccess(HashMap<String, String> result) {
				enableBoxes(true);
				setValues(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to fetch bio: " + caught.getMessage());
			}
		});
		uploadPic.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				CustomerAppHelper.showInfoMessage("Hit the back button on your browser when you are done uploading");
				History.newItem(NameTokens.UPLOADS);
			}
		});
		save.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
 				if(!LoginRoles.ROLE_SCHOOL_STUDENT.equals(BankUserCookie.getCookie().getRole()))
				{
					CustomerAppHelper.showErrorMessage("Only currently enrolled students may use this module");
					return;
				}
				
 				if(validateFields())
 				{
					CustomerAppHelper.REGISTRATION_SERVICE.saveBio(null, getValues(), new AsyncCallback<HashMap<String,String>>() {
	
						@Override
						public void onFailure(Throwable caught) {
							CustomerAppHelper.showErrorMessage("Error saving bio " + caught.getMessage());
							enableBoxes(true);
						}
	
						@Override
						public void onSuccess(HashMap<String, String> result) {
							setValues(result);
							enableBoxes(true);
							CustomerAppHelper.showInfoMessage("Succesfully saved college profile for "
							+ result.get(ApplicationFormConstants.SURNAME));
						}
					});
 				}
			}
		});
	}
	
	@UiField Button uploadPic;
	@UiField Button save;
	@UiField BSTextWidget<String> firstName;
	@UiField BSTextWidget<String> surname;
	@UiField BSTextWidget<String> alternateEmail;
	@UiField BSTextWidget<String> phone;
	@UiField BSTextWidget<String> alternateNo;
	@UiField BSTextWidget<String> gender;
	@UiField BSTextWidget<String> address;
	@UiField BSTextWidget<String> email;
	@UiField BSTextWidget<String> department;
	@UiField BSTextWidget<String> diploma;
	@UiField BSTextWidget<String> year;
	@UiField BSTextWidget<String> matricNo;
	@UiField BSTextWidget<String> jambNo;
	@UiField BSTextWidget<Date> dob;
	@UiField BSTextWidget<Date> dateModified;
	@UiField ThumbnailLink profilePic;
	
	public HashMap<String, String> getValues()
	{
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(ApplicationFormConstants.FIRST_NAME, firstName.getValue());
		result.put(ApplicationFormConstants.SURNAME, surname.getValue());
		result.put(ApplicationFormConstants.ALTERNATE_EMAIL, alternateEmail.getValue());
		result.put(ApplicationFormConstants.PRIMARY_NO, phone.getValue());
		result.put(ApplicationFormConstants.MOBILE_NO, alternateNo.getValue());
		result.put(ApplicationFormConstants.GENDER, gender.getValue());
		result.put(ApplicationFormConstants.ADDRESS, address.getValue());
		result.put(ApplicationFormConstants.EMAIL, email.getValue());
		result.put(ApplicationFormConstants.DEPARTMENT, department.getValue());
		result.put(ApplicationFormConstants.DIPLOMA, diploma.getValue());
		result.put(ApplicationFormConstants.YEAR, year.getValue());
		result.put(ApplicationFormConstants.MATRIC_NO, matricNo.getValue());
		result.put(ApplicationFormConstants.JAMB_NO, jambNo.getValue());
		result.put(ApplicationFormConstants.BDAY, dob.getDisplayText());
		result.put(ApplicationFormConstants.BDAY, dob.getDisplayText());
		return result;
	}
	
	public void setupDateBox(BSTextWidget<Date> datePicker, int years)
	{
		BSDatePicker dobBox = (BSDatePicker) datePicker.getFieldWidget();
		dobBox.setStartView(DateTimePickerView.DECADE);
		dobBox.setMinView(DateTimePickerView.MONTH);
		dobBox.setFormat("yyyy M d");
		dobBox.setValue(null);
		Date startDate = new Date();
		CalendarUtil.addDaysToDate(startDate, -(365 * years));
		CalendarUtil.setToFirstDayOfMonth(startDate);
		CalendarUtil.resetTime(startDate);
		dobBox.setStartDate(startDate);
		dobBox.setAutoClose(true);
		dobBox.reload();

	}	
	
	public void refresh()
	{
		CustomerAppHelper.READ_SERVICE.getUploads(null, new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Error fetching profile pic " + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				if(result.get(0) instanceof TableMessageHeader)
					result.remove(0);
				profilePic.clear();
				
				if(result.size() == 0)
				{
					Icon icon = new Icon(IconType.USER);
					icon.setSize(IconSize.TIMES5);
					icon.setMuted(true);
					icon.addStyleName("img-responsive");
					profilePic.add(icon);
					return;
				}
				
				if(result.size() >  1)
					CustomerAppHelper.showErrorMessage("Warning: found more than 1 image file on record." +
							" Choosing the first file found");
				
				Image m = new Image(result.get(0).getMessageId());
				m.addStyleName("img-responsive");
				profilePic.add(m);
				profilePic.setTargetHistoryToken(NameTokens.UPLOADS);
				
			}
		});
	}
	
	public void setValues(HashMap<String, String> vals)
	{
		firstName.setValue(vals.get(ApplicationFormConstants.FIRST_NAME));
		surname.setValue(vals.get(ApplicationFormConstants.SURNAME));
		alternateEmail.setValue(vals.get(ApplicationFormConstants.ALTERNATE_EMAIL));
		phone.setValue(vals.get(ApplicationFormConstants.PRIMARY_NO));
		alternateNo.setValue(vals.get(ApplicationFormConstants.MOBILE_NO));
		gender.setValue(vals.get(ApplicationFormConstants.GENDER));
		address.setValue(vals.get(ApplicationFormConstants.ADDRESS));
		email.setValue(vals.get(ApplicationFormConstants.EMAIL));
		department.setValue(vals.get(ApplicationFormConstants.DEPARTMENT));
		diploma.setValue(vals.get(ApplicationFormConstants.DIPLOMA));
		year.setValue(vals.get(ApplicationFormConstants.YEAR));
		matricNo.setValue(vals.get(ApplicationFormConstants.MATRIC_NO));
		jambNo.setValue(vals.get(ApplicationFormConstants.JAMB_NO));
		dob.setDisplayText(vals.get(ApplicationFormConstants.BDAY));
		dateModified.setDisplayText(vals.get(ApplicationFormConstants.BDAY));
	}
	
	public void enableBoxes(boolean enabled)
	{
		address.setEnabled(enabled);
		gender.setEnabled(enabled);
		alternateNo.setEnabled(enabled);
		phone.setEnabled(enabled);
		alternateEmail.setEnabled(enabled);
		dob.setEnabled(enabled);
		uploadPic.setEnabled(enabled);
		save.setEnabled(enabled);
		
		enabled=false;
		surname.setEnabled(enabled);
		firstName.setEnabled(enabled);
		email.setEnabled(enabled);
		department.setEnabled(enabled);
		diploma.setEnabled(enabled);
		year.setEnabled(enabled);
		matricNo.setEnabled(enabled);
		jambNo.setEnabled(enabled);
		dateModified.setEnabled(enabled);
	}
	
	HashMap<String, String> errorMap = new HashMap<String, String>();
	ArrayList<BSControlValidator> personalValidators = new ArrayList<BSControlValidator>();
	private void setupValidators()
	{
		HashMap<String, BSTextWidget> controlWidgetsMap = new HashMap<String, BSTextWidget>();
		//sanity check
    	controlWidgetsMap.put(ApplicationFormConstants.EMAIL, email);
    	controlWidgetsMap.put(ApplicationFormConstants.SURNAME, surname);
    	controlWidgetsMap.put(ApplicationFormConstants.FIRST_NAME, firstName);
    	
    	//use editable fields
    	controlWidgetsMap.put(ApplicationFormConstants.PRIMARY_NO, phone);
    	controlWidgetsMap.put(ApplicationFormConstants.ADDRESS, address);
    	controlWidgetsMap.put(ApplicationFormConstants.BDAY, dob);
    	controlWidgetsMap.put(ApplicationFormConstants.MOBILE_NO, alternateNo);
    	controlWidgetsMap.put(ApplicationFormConstants.ALTERNATE_EMAIL, alternateEmail);
    	
    	
    	HashMap<String, FormValidator[]> valiMap = new HashMap<String, FormValidator[]>();

    	setupValidator(controlWidgetsMap, ApplicationFormConstants.NPMB_PERSONAL_VALIDATORS, personalValidators);
	}

	private void setupValidator(HashMap<String, BSTextWidget> controlWidgetsMap, HashMap<String, FormValidator[]> validationSet, ArrayList<BSControlValidator> targetValidatorList)
	{
		for(String formKey : validationSet.keySet())
		{
			FormValidator[] fieldValidator = validationSet.get(formKey);
			BSTextWidget inputField = controlWidgetsMap.get(formKey);
			for(FormValidator v : fieldValidator)
				if(v.equals(FormValidator.MANDATORY))
					inputField.markRequired();
			BSLoanFieldValidator submitValidator = new BSLoanFieldValidator(fieldValidator,inputField, formKey, errorMap, true);
			targetValidatorList.add(submitValidator);
		}
	}

	private boolean validateFields()
	{
		ArrayList<BSControlValidator> validators = personalValidators;
		errorMap.clear();
		
		for(BSControlValidator v : validators)
			v.markErrors();
		
		if(errorMap.size() > 0)
		{
			StringBuilder b = new StringBuilder(errorMap.size() + (errorMap.size()==1? " error" : " errors") + " found.<ul>");
			for(String val : errorMap.values())
				b.append("<li>").append(val).append("</li>");
			b.append("</ul>");
			CustomerAppHelper.showErrorMessage(b.toString());
		}
		return errorMap.size() == 0;
	}	

}
