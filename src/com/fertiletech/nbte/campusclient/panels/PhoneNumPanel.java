package com.fertiletech.nbte.campusclient.panels;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class PhoneNumPanel extends Composite{

	private static PhoneNumPanelUiBinder uiBinder = GWT
			.create(PhoneNumPanelUiBinder.class);

	interface PhoneNumPanelUiBinder extends UiBinder<Widget, PhoneNumPanel> {
	}

	public PhoneNumPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
