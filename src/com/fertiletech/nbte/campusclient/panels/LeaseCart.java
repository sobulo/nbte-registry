package com.fertiletech.nbte.campusclient.panels;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.OrderBioForm;
import com.fertiletech.nbte.campusclient.utils.TablesView;
import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.client.OAuthLoginService;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class LeaseCart extends Composite{
	
	
	@UiField
	TablesView cartList;
	
	@UiField
	Button placeOrder;

	@UiField
	Button viewDept;
	
	@UiField
	OrderBioForm bioForm;
	
	@UiField
	Button addCourses;

	private static LeaseCartUiBinder uiBinder = GWT
			.create(LeaseCartUiBinder.class);

	interface LeaseCartUiBinder extends UiBinder<Widget, LeaseCart> {
	}
	
	public LeaseCart() {
		initWidget(uiBinder.createAndBindUi(this));
		cartList.setEmptyWidget(new Paragraph("No courses selected"));
		placeOrder.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				AsyncCallback<String> callback = new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						placeOrder.setEnabled(true);
						if(caught instanceof DuplicateEntitiesException)
						{
							CustomerAppHelper.showInfoMessage("<p>Looks like you have already registered some of the selected courses:</p>" + caught.getMessage());
							History.newItem(NameTokens.MYORDER + "/" + BankUserCookie.getCookie().getLoanID());
						}
						else
							CustomerAppHelper.showErrorMessage("Error registering, " +
									"try refreshing your browser. " + caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage("Courses registered. " +
								"An email will be sent to your college email account once payment verification is completed");
						String token = NameTokens.THANKORDER + "/" + result;
						placeOrder.setEnabled(true);
						History.newItem(token);
					}
				};
				if(cartList.getTableData().size() == 0)
				{
					CustomerAppHelper.showInfoMessage("No courses selected! Registration request cancelled.");
					return;
				}
				placeOrder.setEnabled(false);
				CustomerAppHelper.REGISTRATION_SERVICE.registerSelectedCourses(callback);
			}
		});
		refresh();
		viewDept.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				List<TableMessage> courses = cartList.getTableData();
				if(courses.size() == 0)
				{
					BankUserCookie uc = BankUserCookie.getCookie();
					if(uc.getRole().equals(LoginRoles.ROLE_SCHOOL_STUDENT))
					{
						String target = NameTokens.getNameTargetMap().get(uc.getDepartment());
						History.newItem(target);
						return;
					}
					CustomerAppHelper.showInfoMessage("Login to register for your courses");
					History.newItem(NameTokens.WELCOME);
					return;
				}
				History.newItem(courses.get(courses.size()-1).getText(RegistryDTO.COURSE_DEPT_IDX));
			}
		});
		addCourses.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(!ViewDetails.checkLoginEligibility())
					return;
				
				addCourses.setEnabled(false);
				CustomerAppHelper.REGISTRATION_SERVICE.updateCart(new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("ERROR adding all courses. " + caught.getMessage());
						addCourses.setEnabled(true);
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						addCourses.setEnabled(true);
						refresh();
					}
				});				
			}
		});
		
		BankUserCookie uc = BankUserCookie.getCookie();
		if(uc.getRole().equals(LoginRoles.ROLE_SCHOOL_STUDENT))
		{
			CustomerAppHelper.REGISTRATION_SERVICE.getSessionInfo(uc.getLoanID(), new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					CustomerAppHelper.showErrorMessage("unable to retrieve bio information. " + caught.getMessage());
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					HashMap<String, String> data = new HashMap<String, String>();
					TableMessage m = result.remove(0);
					data.put(ApplicationFormConstants.SURNAME, m.getText(RegistryDTO.STUD_SESS_SURNAME_IDX));
					data.put(ApplicationFormConstants.FIRST_NAME, m.getText(RegistryDTO.STUD_SESS_OTHER_NAMES_IDX));
					data.put(ApplicationFormConstants.MATRIC_NO, m.getText(RegistryDTO.STUD_SESS_MATRIC_IDX));
					data.put(ApplicationFormConstants.DEPARTMENT, m.getText(RegistryDTO.STUD_SESS_DIPL_IDX) + " - " + m.getText(RegistryDTO.STUD_SESS_DEPT_IDX));
					bioForm.setFormData(data);	
				}
			});			
		}
		
	}	
	
	
	public void refresh()
	{
		cartList.clear();		
		CustomerAppHelper.REGISTRATION_SERVICE.getCart(new AsyncCallback<List<TableMessage>>() {			
			@Override
			public void onSuccess(List<TableMessage> result) {

				boolean initialized = cartList.isInitialized();
				cartList.showTable(result);
				if(!initialized)
				{
					cartList.addColumn(getViewColumn(), "Details");
					cartList.addColumn(getEditorColumn(), "Cancel");
				}
				
				if(cartList.getTableData().size() == 0)
				{
					BankUserCookie uc = BankUserCookie.getCookie();
					LoginRoles role = uc.getRole();
					String department = uc.getDepartment();
					if(role.equals(LoginRoles.ROLE_SCHOOL_STUDENT))
					{
						String target = NameTokens.getNameTargetMap().get(department);
						Hyperlink h = new Hyperlink("Select courses from " + 
										department + " registration page", target);
						cartList.setEmptyWidget(h);
					}
					else if(role.ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal())
					{
						String href = GWT.getHostPageBaseURL() + "staffzone/";
						Anchor a = new Anchor("Use the staffzone to administer the registry", href);
						cartList.setEmptyWidget(a);
					}
					else if(role.ordinal() < LoginRoles.ROLE_SCHOOL_PUBLIC.ordinal())
					{
						String body = "Hello Helpdesk,\n\nPlease setup my student college email account " + uc.getEmail() + " for the current academic session.";
						String href = "https://mail.google.com/mail/u/0/?view=cm&tf=0&to="+NameTokens.HELP_ADDRESS+"&cc="+uc.getEmail()+
								"&su=Registration Setup Request&body="+body;
						Anchor a = new Anchor("Oops, looks like you're not setup in the database for registration." +
								" If you intend to register this semester, click here to mail 9jedu-helpdesk@fcahptib.edu.ng and request access.", href);
						cartList.setEmptyWidget(a);
					}
					else
					{
						Anchor a = new Anchor();
						a.setText("You must login with your college email account to enable Registration");
						a.setHref(Window.Location.getHref());
						a.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								final int authProvider = ClientUtils.getAuthProvider("Google");
								OAuthLoginService.Util.getAuthorizationUrl(authProvider);
							}
						});
						cartList.setEmptyWidget(a);
					}
				}				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to fetch list of courses selected for registration. Try refreshing your browser. <br/>" + caught.getMessage());
			}
		});		
	}					
	
	private com.google.gwt.user.cellview.client.Column<TableMessage, String> getEditorColumn()
	{
		
		ButtonCell removeAttachment = new ButtonCell();
		
		com.google.gwt.user.cellview.client.Column<TableMessage, String> invCol = new com.google.gwt.user.cellview.client.Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "Remove " + object.getText(RegistryDTO.COURSE_CODE_IDX);
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {				
				final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Course removal failed. Try a browser refresh. <br/>" 
									+ caught.getMessage());
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						refresh();
					}

					@Override
					protected void callService(AsyncCallback<String> cb) {
						enableWarning(false);
						CustomerAppHelper.REGISTRATION_SERVICE.
							updateCart(object.getMessageId(), false, cb);
					}
				};
				editAttachCallback.go("Removing " + object.getText(RegistryDTO.COURSE_CODE_IDX) 
						+ " from list of courses to be registered");
			}
		});
		return invCol;
	}

	private com.google.gwt.user.cellview.client.Column<TableMessage, String> getViewColumn()
	{
		
		ButtonCell removeAttachment = new ButtonCell();
		
		com.google.gwt.user.cellview.client.Column<TableMessage, String> invCol = new com.google.gwt.user.cellview.client.Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "View " + object.getText(RegistryDTO.COURSE_CODE_IDX);
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {
				History.newItem(NameTokens.DETAILS + "/" + object.getMessageId());
			}
		});
		return invCol;
	}
	
}
