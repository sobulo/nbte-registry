package com.fertiletech.nbte.campusclient.panels;

import org.gwtbootstrap3.client.ui.AnchorButton;

import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.NameTokens;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class OrderBookedPanel extends Composite{
	
	@UiField AnchorButton orderLink;
	@UiField AnchorButton downloadpage;
	
	String orderId;
	private static OrderBookedPanelUiBinder uiBinder = GWT
			.create(OrderBookedPanelUiBinder.class);

	interface OrderBookedPanelUiBinder extends
			UiBinder<Widget, OrderBookedPanel> {
	}

	public OrderBookedPanel(final boolean isLoggedIn, final boolean isOps, final String sessionId) {
		initWidget(uiBinder.createAndBindUi(this));
		orderId = sessionId;
		orderLink.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String arg = orderId;
				if(arg == null)
				{
					BankUserCookie uc = BankUserCookie.getCookie();
					arg = uc.getLoanID();
				}
				if(arg == null)
					arg = "";
				History.newItem(NameTokens.MYORDER + "/" + arg);
			}
		});
		downloadpage.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.newItem(NameTokens.DOWNLOADS);
			}
		});
		 
	}
	
	public void setOrderIDValue(String id)
	{
		orderId = id;
	}
}
