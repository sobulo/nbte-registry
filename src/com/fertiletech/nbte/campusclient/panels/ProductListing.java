package com.fertiletech.nbte.campusclient.panels;

import java.util.HashMap;
import java.util.List;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.ThumbnailPanel;
import org.gwtbootstrap3.client.ui.Tooltip;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.client.eventboxes.UniqueListBox;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

public class ProductListing extends Composite implements HasValueChangeHandlers<TableMessage>{

	Container content;
	String dept;
	String dipl;
	ListBox courseYear;
	ListBox academicYear;
	ListBox semester;
	Row header;
	Row searchBar;
	Button search;
	
	public final static HashMap<String, Image> deptImageMap = new HashMap<String, Image>();
	static Image getImageMap(String deptName)
	{
		if(!deptImageMap.containsKey(deptName))
		{
			Image headerImage = new Image(NameTokens.getDepartmentListingImageUrl(deptName, true));
			deptImageMap.put(deptName, headerImage);
		}
		return deptImageMap.get(deptName);
	}
	
	public ProductListing(final String dept, final String dipl)
	{
		content = new Container();
		initWidget(content);
		content.setFluid(true);
		Image pic = getImageMap(dept);
		pic.setResponsive(true);		
		Column picWrapper = new Column(ColumnSize.XS_12,  pic);
		picWrapper.addStyleName("redPageHeader");
		picWrapper.addStyleName("headerMargin");
		picWrapper.add(pic);
		header = new Row();
		header.add(picWrapper);
		this.dept = dept;
		this.dipl = dipl;
		setupSearchBar(new Row());
	}
	
	public void setupSearchBar(Row row)
	{
		searchBar = row;
		searchBar.addStyleName("headerMargin");
		academicYear = new ListBox();
		courseYear = new ListBox();
		semester = new ListBox();
		search = new Button("View Available Courses");
		search.setBlock(true);
		search.setType(ButtonType.WARNING);
		
		new UniqueListBox(academicYear, NameTokens.getSampleAcademicYears());
		new UniqueListBox(semester, NameTokens.getSampleSemesters());
		new UniqueListBox(courseYear, NameTokens.YEARS);
		Widget[] widgs = {academicYear, courseYear, semester, search};
		String[] labels = {"Select session/academic year", "Select " + dipl + " level", "Select semester", 
						   "Click to retrieve course list for " + dept};
		
		for(int i = 0; i < widgs.length; i++)
		{
			Column col = getDisplayColumn();
			Tooltip tip = new Tooltip();
			tip.add(widgs[i]);
			tip.setText(labels[i]);
			col.add(tip);
			searchBar.add(col);
		}
		
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				search.setEnabled(false);
				searchCourses();	
			}
		});		
		content.add(header);
		content.add(searchBar);
	}
	
	public void searchCourses()
	{
		//fetch listings
		MyAsyncCallback<List<TableMessage>> fetchListing = new MyAsyncCallback<List<TableMessage>>() {
		
			Icon[] icons = new Icon[4];
			{
				for(int i =0; i < icons.length; i++)
					icons[i] = new Icon(IconType.SPINNER);
			}
			
			public String getDisplayedCode(TableMessage m)
			{
				long units = Math.round(m.getNumber(RegistryDTO.COURSE_UNIT_IDX));
				String append = units > 1 ? " units)" : " unit)";
				return m.getText(RegistryDTO.COURSE_CODE_IDX) + " (" + units + append;
			}

			@Override
			public void onFailure(Throwable caught) {
				search.setEnabled(true);
				String message = "Oops! Unable to fetch course catalogue. Try refereshing your browser.";
				CustomerAppHelper.showErrorMessage(message);
				setIcons(false, IconType.WARNING, message + " " + caught.getMessage());

			}

			void setIcons(boolean spin, IconType type, String title)
			{
				for(Icon icon : icons)
				{
					icon.setSpin(spin);
					icon.setType(type);
					icon.setTitle(title); 
				}				
			}
			@Override
			public void onSuccess(List<TableMessage> result) {
				content.clear();
				//add header image
				content.add(header);
				//add search bar
				content.add(searchBar);
				search.setEnabled(true);

				result.remove(0);//dump data header				
				CustomerAppHelper.showInfoMessage("Found " + result.size() + " items");
				if(result.size() == 0)
				{
					Row row = new Row();
					HorizontalPanel zeroResults = new HorizontalPanel();
					zeroResults.setSpacing(10);
					zeroResults.add(icons[3]);
					zeroResults.add(new Heading(HeadingSize.H4, "No results found!"));
					Column col = new Column(ColumnSize.XS_12, zeroResults);
					icons[3].setSpin(false);
					icons[3].setType(IconType.BAN);
					icons[3].setTitle("No courses found");
					row.add(col);
					content.add(row);
					return;					
				}				
				Row currentRow = null; //new Row();
				/*Column[] cols = new Column[4];
				for(int i = 0; i < cols.length; i++)
				{
					cols[i] = getDisplayColumn();
					currentRow.add(cols[i]);
				}*/
				LeaseInfoHandler handler = new LeaseInfoHandler();
				HashMap<String, IconType> iconMap = CustomerAppHelper.getCategoryIconMap();
				for(int i = 0; i < result.size(); i++)
				{
					if( i % 4 == 0)
					{
						if(currentRow != null)
							content.add(currentRow);
						currentRow = new Row();
					}
					final TableMessage m = result.get(i);
					GWT.log("IS MESSAGE NULL: " + (m==null));
					GWT.log("TXT: " + m.getNumberOfTextFields() + " NUM: " + m.getNumberOfDoubleFields() + " DAT: " +m.getNumberOfDateFields());
					
					String imageUrl = m.getText(RegistryDTO.COURSE_THUMB_IDX);
					Image pic = new Image(imageUrl);
					pic.setResponsive(true);
					ThumbnailPanel listing = new ThumbnailPanel();
					listing.add(pic);
					
					StringBuilder display = 
							new StringBuilder("<div style='width:100%;margin-top:0px;background-color:black;" +
									"color:white;font-size:smaller;text-align:right;'>").
							append(m.getText(RegistryDTO.COURSE_DEPT_IDX)).append("</div>");
					display.append("<p>").append(getDisplayedCode(m)).append("</p><p style='font-size:small'>").
							append(m.getText(RegistryDTO.COURSE_NAME_IDX)).append("<br/>").
						    append( m.getText(RegistryDTO.COURSE_DIPLOMA_IDX)).append(" - ").
						    append(m.getText(RegistryDTO.COURSE_YEAR_IDX)).append("</p>");
					Paragraph p = new Paragraph(display.toString());
					listing.add(p);
					
					Button b = new Button("Course Details");
					b.setBlock(true);
					b.setIcon(IconType.INFO_CIRCLE);
					handler.registerMaapping(b, m);
					b.addClickHandler(handler);
					listing.add(b);
					
					//int colIdx = i%4;
					Column col = getDisplayColumn(); //cols[colIdx];
					col.add(listing);
					currentRow.add(col);
				}
				content.add(currentRow);
			}

			@Override
			protected void callService(AsyncCallback<List<TableMessage>> cb) {
				//loading symbol
				Row row = new Row();
				IconSize[] gearSizes = {IconSize.TIMES2, IconSize.TIMES3, IconSize.TIMES4, IconSize.TIMES5};
				for(int i = 0; i < gearSizes.length; i++)
				{
					Column col = getDisplayColumn();
					icons[i].setSpin(true);
					icons[i].setSize(gearSizes[i]);
					col.add(icons[i]);
					row.add(col);					
				}
				content.add(row);
				CustomerAppHelper.REGISTRATION_SERVICE.getDepartmentCourseTemplates(dept,
						dipl, getValue(courseYear), Integer.valueOf(getValue(semester)), Integer.valueOf(getValue(academicYear)), cb);
			}
			
			String getValue(ListBox lb)
			{
				if(lb.getItemCount() == 0)
					return "";
				return lb.getValue(lb.getSelectedIndex());
			}
		};
		fetchListing.go("Loading...");
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TableMessage> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	Column getDisplayColumn()
	{
		Column col = new Column(ColumnSize.MD_3, ColumnSize.SM_6);
		return col;
	}

	class LeaseInfoHandler implements ClickHandler
	{
		HashMap<Button, TableMessage> buttonMessageMap = new HashMap<Button, TableMessage>();
		@Override
		public void onClick(ClickEvent event) {
			Button source = (Button) event.getSource();
			TableMessage m = buttonMessageMap.get(source);
			if(m != null)
				ValueChangeEvent.fire(ProductListing.this, m);
			
		}
		
		void registerMaapping(Button b, TableMessage m)
		{
			buttonMessageMap.put(b, m);
		}
		
	}
}
