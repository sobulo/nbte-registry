package com.fertiletech.nbte.campusclient.panels;

import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Heading;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.constants.HeadingSize;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ViewDetails extends Composite implements ValueChangeHandler<TableMessage>{

	@UiField
	Button leaseNow;
	@UiField
	Button reportIssue;
	@UiField
	Button viewDept;
	@UiField
	Heading price;
	@UiField
	Heading title;
	@UiField
	Paragraph contentArea;
	@UiField
	Paragraph supplementArea;	
	@UiField
	SimplePanel headerWrapper;	
	@UiField
	Button co;
	@UiField
	Paragraph schedule;
	
	public static HashMap<String, Image> imageCache = new HashMap<String, Image>();
	
	private static ViewDetailsUiBinder uiBinder = GWT
			.create(ViewDetailsUiBinder.class);

	interface ViewDetailsUiBinder extends UiBinder<Widget, ViewDetails> {
	}
	
	TableMessage selectedProduct = null;
	
	public ViewDetails() {
		initWidget(uiBinder.createAndBindUi(this));
		ClickHandler temp = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				CustomerAppHelper.showInfoMessage("Coming Soon! Allows feedback to be passed anonymously" +
						" to the office of the registrar via your HODs office. <b style='color:#7f0000'>We are looking for practical " +
						"suggestions on how we can improve your experience at FCAHPT</b>" );
			}
		};
		
		co.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(checkLoginEligibility())
				{
					co.setEnabled(false);
					leaseNow.setEnabled(false);
					CustomerAppHelper.REGISTRATION_SERVICE.updateCart(leaseCallBack);
				}
			}
		});
		
		reportIssue.addClickHandler(temp);
		
		viewDept.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(selectedProduct == null)
				{
					BankUserCookie uc = ClientUtils.BankUserCookie.getCookie();
					if(uc.getRole().equals(LoginRoles.ROLE_SCHOOL_STUDENT))
					{
						String target = NameTokens.getNameTargetMap().get(uc.getDepartment());
						History.newItem(target);
						return;
					}
					CustomerAppHelper.showInfoMessage("No department selected previously. " +
							"Redirecting to registration home page");
					History.newItem(NameTokens.WELCOME);
					return;
				}
				String target = NameTokens.getNameTargetMap().get(selectedProduct.
						getText(RegistryDTO.COURSE_DEPT_IDX));
				History.newItem(target);
			}
		});
		
		leaseNow.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(enableCourseRegistration(selectedProduct))
				{
					leaseNow.setEnabled(false);
					co.setEnabled(false);
					CustomerAppHelper.REGISTRATION_SERVICE.updateCart(selectedProduct.getMessageId(), true, leaseCallBack );
				}
			}
		});		
		reportIssue.setTitle("Provide anonymous feedback to the registrar's office as well as your department head");
	}

	protected AsyncCallback<String> leaseCallBack = new AsyncCallback<String>() {

		@Override
		public void onFailure(Throwable caught) {
			co.setEnabled(true);
			leaseNow.setEnabled(true);
			CustomerAppHelper.showErrorMessage("Course registration selection failed. Error was: " + caught.getMessage());
		}

		@Override
		public void onSuccess(String result) {
			co.setEnabled(true);
			leaseNow.setEnabled(true);
			CustomerAppHelper.showInfoMessage(result); 
			History.newItem(NameTokens.CART);
		}
	};
	
	void updateRegistrationSideBar()
	{
		if(selectedProduct == null) return; //sanity check
		price.setText(selectedProduct.getText(RegistryDTO.COURSE_DEPT_IDX));
		title.setText(selectedProduct.getText(RegistryDTO.COURSE_YEAR_IDX) + " - " + selectedProduct.getText(RegistryDTO.COURSE_DIPLOMA_IDX));
		
		String leaseMsg = ("Add " + selectedProduct.getText(RegistryDTO.COURSE_CODE_IDX));
		leaseNow.setText(leaseMsg);
		leaseMsg = "Add all " + selectedProduct.getText(RegistryDTO.COURSE_SEMESTER_IDX) + " courses";
		co.setText(leaseMsg);
		
		String schedInfo = selectedProduct.getText(RegistryDTO.COURSE_SCHEDULE_IDX);
		if(schedInfo != null)
		{
			boolean loggedIn = !BankUserCookie.getCookie().getRole().equals(LoginRoles.ROLE_SCHOOL_PUBLIC);
			if(!loggedIn)
				schedInfo = "Login to view schedule";
		}
		else
			schedInfo = "<font color='red'>No schedule available</font>";
		schedule.setHTML(schedInfo);
	}
	
	static boolean checkLoginEligibility()
	{
		BankUserCookie uc = ClientUtils.BankUserCookie.getCookie();
		LoginRoles role = uc.getRole();
		if(!role.equals(LoginRoles.ROLE_SCHOOL_STUDENT))
		{
			if(role.ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal())
				CustomerAppHelper.showInfoMessage("Ignoring registration as " + uc.getEmail() 
						+ " is not registered as a student email account. Use the staff zone to adminster student registrations");
			else
				CustomerAppHelper.showInfoMessage("Ignoring registration, as account " + uc.getEmail() + 
					" database records show you're not currently enrolled in the college. Contact " + NameTokens.HELP_ADDRESS + 
					" if this should not be the case.");
			return false;
		}
		return true;
		
	}
	
	/**
	 * enable buttons only if the following are true. Order matters in terms of usability/user experience
	 * 1. is a logged in student //i.e. only registered students allowed
	 * 2. student matches course department and diploma
	 * 3. student matches academic year and semester course is in
	 * 4. student is in a level greater than course level
	 * 
	 * 
	 * By locking it at the cart level, this should prevent any bad data from
	 * making it into the database, TODO: stronger server side checks still recommended
	 */
	static boolean enableCourseRegistration(TableMessage  course)
	{ 
		if(course == null)
		{
			CustomerAppHelper.showInfoMessage("No course selected. Use the add all button to register all your" +
					" courses for the current semester. Alternatively visit your department's registration page " +
					" to select individual courses");
			return false;
		}
		BankUserCookie uc = ClientUtils.BankUserCookie.getCookie();		
		//1. Check this is a current student, i.e. only registered students allowed
		checkLoginEligibility();
		//2. student matches course department and diploma
		String department = uc.getDepartment();
		if(!department.equals(course.getText(RegistryDTO.COURSE_DEPT_IDX)))
		{
			String anchor = "<a href='#" + NameTokens.getNameTargetMap().get(department) + "'>" + department + 
					        "</a>";
			CustomerAppHelper.showInfoMessage("Ignoring registration. Visit the " + anchor + " registration page" +
					" to select your courses");
			return false;
		}
		String diploma = uc.getDiploma(); //sanity check, moor has no department with both hnd and nd
		if(!diploma.equals(course.getText(RegistryDTO.COURSE_DIPLOMA_IDX)))
		{
			CustomerAppHelper.showInfoMessage("Ignoring registration. This course is a " +
					course.getText(RegistryDTO.COURSE_DIPLOMA_IDX) + " course but database records reflect" +
							" you are a " + diploma + " student");
			return false;
		}		
		
		//3. student matches academic year and semester course is in
		String year = uc.getAcademicYear();
		String term = uc.getAcademicTerm();
		if(!year.equals(course.getText(RegistryDTO.COURSE_ACADEMIC_YEAR_IDX)))
		{
			CustomerAppHelper.showInfoMessage("Ignoring registration. This course is for " + course.getText(RegistryDTO.COURSE_ACADEMIC_YEAR_IDX)
					+ " and database records show you are currently in " + year);
			return false;
		}
		if(!term.equals(course.getText(RegistryDTO.COURSE_SEMESTER_IDX)))
		{
			CustomerAppHelper.showInfoMessage("Ignoring registration. This course is for " + course.getText(RegistryDTO.COURSE_SEMESTER_IDX)
					+ " and database records show you are currently in " + term);
			return false;			
		}
		
		//4. student is in a level greater than course level
		String level = uc.getLevelYear();
		if(!level.equals(course.getText(RegistryDTO.COURSE_YEAR_IDX)))
		{
			if(level.equals(NameTokens.ALT_YEAR2))
				CustomerAppHelper.showInfoMessage("Click register only if you are carrying over the course! This is a " + course.getText(RegistryDTO.COURSE_YEAR_IDX)
						                          + " course and database records show you are already a " + level + " student.");
			else
			{
				CustomerAppHelper.showInfoMessage("You are not yet eligible to take " + course.getText(RegistryDTO.COURSE_YEAR_IDX) + " courses");
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void onValueChange(ValueChangeEvent<TableMessage> event) {
		TableMessage m = event.getValue();
		updateView(m);
		History.newItem(NameTokens.DETAILS);
	}
	
	public void displayProduct(String productID)
	{
		if(productID == null || productID.trim().length() == 0)
			return;
		AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to load product information. Try refreshing your browser");
			}

			@Override
			public void onSuccess(TableMessage result) {
				updateView(result);
			}
		};
		CustomerAppHelper.REGISTRATION_SERVICE.getCourseTemplateInfo(productID, callback );
	}
	
	private void updateView(TableMessage product)
	{
		selectedProduct = product;
		headerWrapper.clear();
		if(selectedProduct == null)
		{
			Icon i = new Icon(IconType.QUESTION_CIRCLE);
			i.setSize(IconSize.TIMES5);
			Heading h = new Heading(HeadingSize.H5,"No course selected. Visit your department page to select a course or use the" +
					" add all button to register all your courses for this semester");
			FlowPanel f = new FlowPanel();
			f.add(i);
			f.add(h);
			headerWrapper.add(f);
			return;
		}
		
		final String url = product.getText(RegistryDTO.COURSE_BANN_IDX);
		Image productPicture = imageCache.get(url);
		if(productPicture == null)
		{
			final Icon loadingIcon = new Icon(IconType.CIRCLE_O_NOTCH);
			loadingIcon.setSize(IconSize.TIMES5);
			loadingIcon.setSpin(true);
			final HorizontalPanel p = new HorizontalPanel();
			p.add(loadingIcon);
			//imageWrapper.add(p);
			final Image pic = new Image(url);
			/*pic.addLoadHandler(new LoadHandler() {
				
				@Override
				public void onLoad(LoadEvent event) {
					p.remove(loadingIcon);
					pic.setResponsive(true);
					pic.setType(ImageType.THUMBNAIL);
					pic.setVisible(true);		
					imageCache.put(url, pic);
				}
			});*/
			pic.setResponsive(true);
			//pic.setType(ImageType.THUMBNAIL);
			//pic.setVisible(false);
			headerWrapper.add(pic);
		}
		else
			headerWrapper.add(productPicture);
		String feat = product.getText(RegistryDTO.COURSE_FEATURE_IDX);
		feat = feat == null?"":feat;
		StringBuilder details = new StringBuilder("<h4>").append(product.getText(RegistryDTO.COURSE_NAME_IDX)).append("</h4>").
				append("<div><b>").append(getDisplayedCode(product)).append("</b><br/>").
				append(feat).append("</div>");
		contentArea.setHTML(details.toString());
		details = new StringBuilder();
		details.append("<div><b>").append(product.getText(RegistryDTO.COURSE_DIPLOMA_IDX)).
				append("</b><br/>").
				append(getHTMLUrl("Syllabus", product.getText(RegistryDTO.COURSE_DESC_IDX))).
				append(getHTMLUrl("Notes", product.getText(RegistryDTO.COURSE_NOTES_IDX))).
				append("</div>");
		supplementArea.setHTML(details.toString());
		updateRegistrationSideBar();		
	}
	
	public String getDisplayedCode(TableMessage m)
	{
		long units = Math.round(m.getNumber(RegistryDTO.COURSE_UNIT_IDX));
		String append = units > 1 ? " units)" : " unit)";
		return m.getText(RegistryDTO.COURSE_CODE_IDX) + " (" + units + append;
	}	
	
	public String getHTMLUrl(String name, String url)
	{
		if(url == null || url.trim().length() == 0) return "";
		return "<p><a href='" + url + "'>" + name + "</a></p>"; 
	}
}
