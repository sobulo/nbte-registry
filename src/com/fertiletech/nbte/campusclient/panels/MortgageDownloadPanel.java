package com.fertiletech.nbte.campusclient.panels;

import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.Button;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MortgageDownloadPanel extends Composite {

	@UiField
	Button courseButton;
	@UiField
	Button invoiceButton;
	@UiField
	Button accountButton;
	@UiField
	Anchor linkDisplay;
	
	private static MortgageDownloadPanelUiBinder uiBinder = GWT
			.create(MortgageDownloadPanelUiBinder.class);

	interface MortgageDownloadPanelUiBinder extends
			UiBinder<Widget, MortgageDownloadPanel> {
	}
	
	HandlerRegistration appWarnRegistration = null;
	private final String WAIT_MESSAGE = "Fetching link, wait a few seconds and try again";
	private final String NOT_READY_MESSAGE = "We have not yet received your clearance notifcation. Please ask your department to forward your name to " +
			"the office of the registrar";
	private final String ERROR_MESSAGE = "Failed to fetch download link. Try refreshing your browser.<br/>";
	
	String COURSE_TXT = "Click here to download your course registration form";
	String ACCOUNTS_TXT = "Click here to download your account statement";
	String INVOICE_TXT = "Click here to download your invoice/fee breakdowns";
	private AsyncCallback<String[]> appDownloadLinkCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage(ERROR_MESSAGE + caught.getMessage());
			enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
			if(result[0] != null)
			{
				linkDisplay.setText(result[0]);
				linkDisplay.setHref(result[1]);
				linkDisplay.setTarget("_blank");
				Timer t = new Timer() {
					
					@Override
					public void run() {
						clear();
					}
				};
				t.schedule(20 * 1000);
			}		
			if(result[2].length() > 0)
			{
				CustomerAppHelper.showInfoMessage(result[2]);
				CustomerAppHelper.showErrorMessage("DOWNLOAD FAILED!!");
			}
			enableButtons(true);
		}
	};
	
	public void enableButtons(boolean enabled)
	{
		courseButton.setEnabled(enabled);
		accountButton.setEnabled(enabled);
		invoiceButton.setEnabled(enabled);
	}
	
	public MortgageDownloadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		ClickHandler warningHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				enableButtons(false);
				String sessionID = BankUserCookie.getCookie().getLoanID();
				if(event.getSource() == courseButton)
				{
					CustomerAppHelper.REGISTRATION_SERVICE.
					getCourseRegistrationDownloadLink(sessionID, appDownloadLinkCallback);		
				}
				else if(event.getSource() == accountButton)
				{
					CustomerAppHelper.REGISTRATION_SERVICE.
					getAccountStatementLink(sessionID, appDownloadLinkCallback);					
				}
				else if(event.getSource() == invoiceButton)
				{
					CustomerAppHelper.REGISTRATION_SERVICE.
					getItemizedTuitionDownloadLink(sessionID, appDownloadLinkCallback);	
				}
			}
		};
		courseButton.addClickHandler(warningHandler);
		accountButton.addClickHandler(warningHandler);
		invoiceButton.addClickHandler(warningHandler);
	}
	
	private void clear()
	{
		String disableLink = "javascript:;";
		linkDisplay.setText("Click any of the buttons above to generate a pdf for download");
		linkDisplay.setHref(disableLink);
	}
	
	//TODO below needs to be uncommented to support multiple sessions but server side needs changes
	//anyways so best to disable here until those changes are supported. UPDATE code moved to click handler
	public void setLoanID(/*String id*/)
	{
		clear();
	}
}
