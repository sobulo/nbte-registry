package com.fertiletech.nbte.campusclient.panels;

import java.util.List;

import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.Legend;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.gwt.FlowPanel;
import org.gwtbootstrap3.client.ui.html.Paragraph;

import com.fertiletech.nbte.campusclient.CustomerAppHelper;
import com.fertiletech.nbte.campusclient.utils.TablesView;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;

public class OrderHistory extends Composite{
	Container content = new Container();

	TablesView orders = new TablesView();
	public OrderHistory(boolean isLoggedIn)
	{
		initWidget(content);
		FlowPanel f = new FlowPanel();
		Legend l = new Legend("Please Log In");
		if(!isLoggedIn)
		{
			Paragraph p = new Paragraph("You must be logged in before you can access your registration history. " +
					"If you're already logged in, please try refreshing your browser");
			f.add(l);
			f.add(p);
			content.add(f);
			return;
		}
		content.setFluid(true);
		content.clear();
		Image pic = new Image(CustomerAppHelper.generalImages.reghistory());
		pic.setResponsive(true);
		Row header = new Row();
		Column picWrapper = new Column(ColumnSize.XS_12,  pic);
		picWrapper.addStyleName("productListingHeader");
		picWrapper.add(pic);
		header.add(picWrapper);
		content.add(header);
		Row contentRow = new Row();
		Column tableView = new Column(ColumnSize.XS_12, f);
		BankUserCookie uc = BankUserCookie.getCookie();
		String title = "Listed below are registration requests for " + uc.getUserName() + " (" + uc.getEmail() + ")"; 
		l.setHTML(title);
		f.add(l);
		f.add(orders);
		Paragraph x = new Paragraph("You have not yet registered");
		orders.setEmptyWidget(x);
		contentRow.add(tableView);
		content.add(contentRow);
		CustomerAppHelper.REGISTRATION_SERVICE.getMySessions(new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper.showErrorMessage("Unable to retrieve your registration history. " +
						"Try refreshing your browser.<br/> Error was: " + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				boolean isInit = orders.isInitialized();
				orders.showTable(result);
				if(!isInit)
					orders.addColumn(getEditorColumn(), "Action");
			}
		});
		
	}
	
	private com.google.gwt.user.cellview.client.Column<TableMessage, String> getEditorColumn()
	{	
		ButtonCell removeAttachment = new ButtonCell();
		
		com.google.gwt.user.cellview.client.Column<TableMessage, String> invCol = new com.google.gwt.user.cellview.client.Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "View";
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {				
				History.newItem(NameTokens.MYORDER + "/" + object.getMessageId());
			}
		});
		return invCol;
	}
	
}
