package com.fertiletech.nbte.campusclient;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TreeMap;

import org.gwtbootstrap3.client.ui.AnchorButton;
import org.gwtbootstrap3.client.ui.AnchorListItem;
import org.gwtbootstrap3.client.ui.DropDownMenu;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.ListDropDown;
import org.gwtbootstrap3.client.ui.NavbarNav;
import org.gwtbootstrap3.client.ui.constants.IconPosition;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Toggle;

import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.client.MyRefreshCallback;
import com.fertiletech.nbte.client.OAuthLoginService;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class CustomerWebApp extends Composite implements ValueChangeHandler<String> {

	@UiField
	NavbarNav nav;

	@UiField
	SimplePanel contentWidget;

	@UiField
	DropDownMenu loginDropdown;

	@UiField
	AnchorButton loginDropHeader;

	@UiField
	Image logo;

	private static CustomerWebAppUiBinder uiBinder = GWT
			.create(CustomerWebAppUiBinder.class);

	interface CustomerWebAppUiBinder extends UiBinder<Widget, CustomerWebApp> {
	}

	// different views
	HyperlinkedPanel currentContent; // keeps track of view currently displayed
	List<HyperlinkedPanel> appPanels;
	WelcomePage homePage;
	private boolean loggedInMenuInitialized;
	private boolean loggedOutMenuInitialized;

	private void fetchUser() {
		
		if(!ClientUtils.alreadyLoggedIn())
		{
			//Window.alert("Not logged in, updating the app");
			updateWebApp();
			return;
		}
		
		final MyAsyncCallback<SocialUser> ensureLoginCallback = new MyAsyncCallback<SocialUser>() {

			@Override
			public void onSuccess(SocialUser result) {
				//Window.alert("Fetch returned, making a call to update the screen");
				updateWebApp();
			}

			@Override
			public void onFailure(Throwable caught) {
				//Window.alert("Failure");
				String displayMessage = "Login credential check failed."
						+ " Error was: [" + caught.getMessage() + "]<br/>";
				CustomerAppHelper.showErrorMessage(displayMessage);
			}

			@Override
			protected void callService(AsyncCallback<SocialUser> cb) {
				super.enableWarning(false);
				OAuthLoginService.Util.getInstance().fetchMe(
						ClientUtils.getSessionIdFromCookie(), cb);
			}
		};
		//Window.alert("Making call to verify user credentials");
		ensureLoginCallback.go("Verifying user credentials ...");
	}
	
	private void updateWebApp()
	{
		GWT.log("Updating web app");
		boolean isOps = false;
		String[] tokenArgument = new String[3];
		String tokArg = Cookies.getCookie(currentContent.getArgumentType().toString()+TOKEN_COOKIE);
		if(tokArg != null && (tokArg.equals("null") || tokArg.equals("undefined"))) 
			tokArg = null;

		tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX] = tokArg;
		//CustomerAppHelper.showInfoMessage("Type: " + currentContent.getArgumentType() + " Arg: " + tokArg);
		
		//Window.alert("Token Arg: " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);
		if (ClientUtils.alreadyLoggedIn()) 
		{
			GWT.log("Updating web app for logged in user");
			//Window.alert("Fetching user cookie");
			BankUserCookie usercookie = ClientUtils.BankUserCookie.getCookie();
			tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] = usercookie.getLoanID();
			tokenArgument[CustomerAppHelper.TOKEN_ARGS_EMAIL_IDX] = usercookie.getEmail();
			isOps = usercookie.getRole().equals(LoginRoles.ROLE_SCHOOL_ADMIN);
			//Window.alert("Is OPS: " + isOps);
			if (!loggedInMenuInitialized) {
				setupLoggedInMenu(usercookie.getEmail());
				loggedInMenuInitialized = true;
				loggedOutMenuInitialized = false;
			}
			//Window.alert("IN: " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] + 
			//		" AND " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);			
		} else 
		{
			GWT.log("Updating web app for logged out user");
			tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] = null;
			tokenArgument[CustomerAppHelper.TOKEN_ARGS_EMAIL_IDX] = null;			
			if (!loggedOutMenuInitialized) {
				setupLoggedOutMenu();
				loggedOutMenuInitialized = true;
				loggedInMenuInitialized = false;
			}
			//Window.alert("OUT: " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_RECENTLOAN_IDX] + 
			//		" AND " + tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX]);						
			//Window.alert("Not logged in, so nothing to do");
		}
		GWT.log("Loading panel");
		loadCurrentDisplay(tokenArgument, isOps, ClientUtils.alreadyLoggedIn());
	}

	public CustomerWebApp() {
		initWidget(uiBinder.createAndBindUi(this));
		logo.setResource(CustomerAppHelper.generalImages.logoSmall());
		logo.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				History.newItem(NameTokens.WELCOME);
			}
		});
		homePage = new WelcomePage();//(WelcomePage) contentWidget.getWidget();
		appPanels = CustomerAppHelper.initializePanels();
		appPanels.add(homePage);
		currentContent = homePage;
		
		OAuthLoginService.Util.handleRedirect(new MyRefreshCallback() {
			
			@Override
			public void updateScreen() {
				setupHistoryHandler();
				setupMenu();
				updateWebApp();
				sayHello();
				String token = History.getToken();
				if (token.equals("")) {
					String homeToken = NameTokens.WELCOME;
					History.newItem(homeToken);
				}
				else
					History.fireCurrentHistoryState();	
			}
		});
	}
	
	private ListDropDown getCategoryLinks(TreeMap<String, HashSet<String>> categoriesMap, String menuTitle) {
		ListDropDown result = new ListDropDown();
		AnchorButton header = new AnchorButton();
		header.setDataToggle(Toggle.DROPDOWN);
		header.setText(menuTitle);
		header.setIconPosition(IconPosition.RIGHT);
		result.add(header);
		DropDownMenu menuContent = new DropDownMenu();

		HashMap<String, String> map = NameTokens.getNameTargetMap();
		HashMap<String, IconType> icons = CustomerAppHelper.getCategoryIconMap();
		for(String cat : categoriesMap.get(menuTitle))
		{
			String destination = map.get(cat);
			if(destination == null) 
				destination = NameTokens.WELCOME;
			AnchorListItem link = getLink(cat, destination, true);
			link.setIcon(icons.get(cat));
			menuContent.add(link);
		}
		//menuContent.add(dLink);
		//menuContent.add(uLink);
		result.add(menuContent);
		
		//menuContent.setWidth("500px");
		return result;	
	}

	private ListDropDown getRegistrationLinks() {
		ListDropDown result = new ListDropDown();
		AnchorButton header = new AnchorButton();
		header.setDataToggle(Toggle.DROPDOWN);
		header.setText("Registration");
		header.setIconPosition(IconPosition.RIGHT);
		result.add(header);
		DropDownMenu menuContent = new DropDownMenu();

		AnchorListItem regLink = getLink(CustomerAppHelper.getShoppingCart().getLink());
		regLink.setIcon(IconType.PENCIL_SQUARE_O);
		AnchorListItem histLink = getLink(CustomerAppHelper.getOrderHistoryPanel().getLink());
		histLink.setIcon(IconType.BOOK);
		AnchorListItem downLink = getLink(CustomerAppHelper.getDownloadPanel().getLink());
		downLink.setIcon(IconType.PRINT);
		//AnchorListItem upLink = new AnchorListItem("Upload/Review");
		//upLink.setIcon(IconType.THUMBS_O_UP);
		AnchorListItem bioLink = getLink(CustomerAppHelper.getStudBioPanel().getLink());
		bioLink.setIcon(IconType.GRADUATION_CAP);

		
		menuContent.add(regLink);
		menuContent.add(histLink);
		menuContent.add(downLink);
		menuContent.add(bioLink);
		
		result.add(menuContent);
		
		//menuContent.setWidth("500px");
		return result;	
	}	
	

	
	private void sayHello()
	{
		String name = "";
		String suffix = "<a href='http://www.fcahptib.edu.ng/contact-us'>Contact us</a>" +
				" if you need help using the registration portal";
		if(ClientUtils.alreadyLoggedIn())
		{
			BankUserCookie uc = ClientUtils.BankUserCookie.getCookie();
			name = uc.getUserName();
			name = " " + name;
			LoginRoles role = uc.getRole();
			if(role.equals(LoginRoles.ROLE_SCHOOL_ADMIN))
				suffix = "Visit the <a href='http://www.fcahptib.edu.ng/staffzone/'>staff only portion of the portal</a> to manage student registrations";
			else if(role.equals(LoginRoles.ROLE_SCHOOL_PUBLIC))
				suffix = "<br/><hr/><b style='color:red'>Registry features disabled as no student records were found for you</b>" +
						" Send an email to records@fcahptib.edu.ng if you are enrolled for the current school session and request" +
						" for access to the system";
		}
		CustomerAppHelper.showInfoMessage("Welcome <b><i>" + name + "</i></b>. <span style='font-size:smaller'> " 
		+ suffix + "</span>");				
	}
		
	private void setupHistoryHandler()
	{
		History.addValueChangeHandler(this);	
	}
	
	private ListDropDown getDocumentLinks(Hyperlink downloadLink, Hyperlink uploadLink) {
		ListDropDown result = new ListDropDown();
		AnchorButton header = new AnchorButton();
		header.setDataToggle(Toggle.DROPDOWN);
		header.setText("Documents");
		result.add(header);
		DropDownMenu menuContent = new DropDownMenu();

		AnchorListItem dlink = getLink(downloadLink.getText(), downloadLink.getTargetHistoryToken(), true);
		dlink.setIcon(IconType.PRINT);
		AnchorListItem ulink = getLink(uploadLink.getText(), uploadLink.getTargetHistoryToken(), true);
		ulink.setIcon(IconType.THUMBS_UP);
		menuContent.add(dlink);
		menuContent.add(ulink);
		result.add(menuContent);
		return result;	
	}	
	
	private ListDropDown getApplyLinks(Hyperlink apply, AnchorListItem requirements) {
		ListDropDown result = new ListDropDown();
		AnchorButton header = new AnchorButton();
		header.setDataToggle(Toggle.DROPDOWN);
		header.setText("Apply");
		result.add(header);
		DropDownMenu menuContent = new DropDownMenu();

		AnchorListItem ulink = getLink(apply.getText(), apply.getTargetHistoryToken(), true);
		ulink.setIcon(IconType.LAPTOP);
		menuContent.add(ulink);
		menuContent.add(requirements);
		result.add(menuContent);
		return result;
	}

	private AnchorListItem getLink(String text, String href, boolean useTarget) {
		AnchorListItem result = new AnchorListItem();
		result.setText(text);
		if (useTarget)
			result.setTargetHistoryToken(href);
		else
			result.setHref(href);
		return result;
	}

	private void setupLoggedOutMenu() {
		loginDropdown.clear();
		loginDropHeader.setText("Sign in");
		loginDropHeader.setIcon(IconType.SIGN_IN);
		loginDropHeader.setIconPosition(IconPosition.RIGHT);

		final String[] providerNames = {"Google"};
		IconType[] iconTypes = { IconType.GOOGLE_PLUS_SQUARE,
				IconType.HACKER_NEWS, IconType.FACEBOOK_SQUARE };
		for (int i = 0; i < providerNames.length; i++) {
			final String name = providerNames[i];
			AnchorListItem link = new AnchorListItem(name);
			link.setIcon(iconTypes[i]);
			link.setIconPosition(IconPosition.RIGHT);
			link.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					final int authProvider = ClientUtils.getAuthProvider(name);
					OAuthLoginService.Util.getAuthorizationUrl(authProvider);
				}
			});
			loginDropdown.add(link);
		}
	}

	private void setupMenu() {
		nav.clear();
		for(String diploma : NameTokens.DIPLM_DEPTS.keySet())
			nav.add(getCategoryLinks(NameTokens.DIPLM_DEPTS, diploma));		
		nav.add(getRegistrationLinks());

		//nav.add(getApplyLinks(menuItems[CustomerAppHelper.APPLICATION_FORM_IDX], getGeneralRequirements()));	
		//nav.add(getDocumentLinks(menuItems[CustomerAppHelper.PRINT_FORM_IDX], menuItems[CustomerAppHelper.UPLOAD_IDX]));
		//nav.add(getLink(menuItems[CustomerAppHelper.STATUS_TABLE_IDX]));
		//nav.add(getPropertyLinks());
		//nav.add(getAboutLinks());
	}
	
	AnchorListItem getLink(Hyperlink item)
	{
		return getLink(item.getText(), item.getTargetHistoryToken(), true);
	}

	private void setupLoggedInMenu(final String id) 
	{
		loginDropdown.clear();
		loginDropHeader.setText(id);
		loginDropHeader.setIcon(IconType.SIGN_OUT);
		loginDropHeader.setIconPosition(IconPosition.RIGHT);

		// setupMenu();

		// add link for logout
		AnchorListItem logoutButton = new AnchorListItem();
		String providerName = ClientUtils.getAuthProviderNameFromCookie();
		logoutButton.setText(providerName + " sign out");
		final String[] url = new String[1];
		if (providerName.equals("Google")) {
			logoutButton.setIcon(IconType.GOOGLE_PLUS);
			url[0] = DTOConstants.GOOGLE_CLOUD_LOGOUT;
		} else if(providerName.equals(DTOConstants.COMPANY_NAME))
		{
			logoutButton.setIcon(IconType.USER);
			logoutButton.setText("Staff sign out");
			url[0] = DTOConstants.GOOGLE_CLOUD_LOGOUT;
		}
		else if (providerName.equals("Facebook")) {
			url[0] = "";
			logoutButton.setIcon(IconType.FACEBOOK);
		} else if (providerName.equals("Yahoo!")) {
			url[0] = DTOConstants.YAHOO_CLOUD_LOGOUT;
			logoutButton.setIcon(IconType.YAHOO);
		} else {
			url[0] = null;
			logoutButton.setIcon(IconType.WARNING);
		}

		logoutButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (url[0] == null)
					return;

				Frame f = new Frame(url[0]);
				f.addLoadHandler(new LoadHandler() {
					@Override
					public void onLoad(LoadEvent event) {
						new Timer() {

							@Override
							public void run() {
								OAuthLoginService.Util.logout();
								if (url[0].equals(""))
									fbLogout();
								Window.open(url[0], "_blank", "");
							}
						}.schedule(1000 * 2); // allow network 2 seconds to log
												// out b4 we log out of sang
					}
				});
				GWT.log("F: " + f);
				f.setSize("15px", "15px");
				loginDropdown.clear();
				loginDropHeader.setText("Logging out...");
				loginDropdown.add(f);
				new Timer() {
					@Override
					public void run() {
						OAuthLoginService.Util.logout();
						if (url[0].equals(""))
							fbLogout();
						Window.open(url[0], "_blank", "");
					}
				}.schedule(1000 * 20); // if we're not logged out 20 seconds,
										// try alternate option (opens an
										// additional window)
			}
		});
		loginDropdown.add(logoutButton);
		// loginWidgetSlot.add(logoutButton);
		// navcontent.setSpacing(GUIConstants.MENU_SPACING);
	}

	public static native void fbLogout() /*-{
		FB.logout();
	}-*/;

	String TOKEN_COOKIE = CustomerAppHelper.TOKEN_COOKIE;
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		GWT.log("value change event received: " + event.getValue());
		GWT.log("HISTORY: " + event.getValue());
		String[] urlParams = event.getValue().split("/");
		String token = urlParams[0];
		String args = null;
		if (urlParams.length == 2 && urlParams[1].trim().length() > 0)
			args = urlParams[1].trim();
		GWT.log("TOKEN: " + token + " Args: " + args);
		//tokenArgument[CustomerAppHelper.TOKEN_ARGS_PARAM_IDX] = args;
		long expiryTime = new Date().getTime(); //now
		expiryTime += 30 * (60 * 1000); //30 minutes from now in milliseconds
		//Window.alert("Just set cookie with: " + Cookies.getCookie(TOKEN_COOKIE));
		Window.addCloseHandler(new CloseHandler<Window>() {
			
			@Override
			public void onClose(CloseEvent<Window> event) {
				//Window.alert("Close Handler Called, wiping token arg");
				for(ArgumentType type : ArgumentType.values())
					Cookies.setCookie(type.toString()+TOKEN_COOKIE, null);
			}
		});
		Window.addWindowClosingHandler(new ClosingHandler() {
			
			@Override
			public void onWindowClosing(ClosingEvent event) {
				//Window.alert("Close handler called, wiping token arg");
				for(ArgumentType type : ArgumentType.values())
					Cookies.setCookie(type.toString()+TOKEN_COOKIE, null);
			}
		});
		GWT.log("History fired for: " + token);
		ArgumentType type = shouldLoadNewPanel(token);
		if (type != null) {
			if(args != null && args.length() > 0)
				Cookies.setCookie(type.toString()+TOKEN_COOKIE, args, new Date(expiryTime));
			fetchUser();
		}
	}

	private void loadCurrentDisplay(String[] args, boolean isOps,
			boolean isLoggedIn) {
		GWT.log("Attempting to set current content");
		Widget w = currentContent.getPanelWidget(isLoggedIn, isOps, args);
		if (w == null || contentWidget.getWidget() == w)
			return;
		GWT.log("Made it this far");
		ArgumentType type = null;
		try
		{
			type = currentContent.getArgumentType();
			contentWidget.clear();
		}
		catch(Exception e){} //firefox hack

		if(w instanceof WelcomePage)
		{
			RootPanel.getBodyElement().removeClassName("xmas");
			RootPanel.getBodyElement().addClassName("christmas");
		}
		else
		{
			RootPanel.getBodyElement().removeClassName("christmas");
			if(ArgumentType.ORDER_ARG.equals(type))
				RootPanel.getBodyElement().addClassName("xmas");
			else
				RootPanel.getBodyElement().removeClassName("xmas");
		}
		
		GWT.log("Setting the widget");
		contentWidget.setWidget(w);
		GWT.log("widget set ok");
	}

	private ArgumentType shouldLoadNewPanel(String newToken) {

		// String newToken = lastRequestedPage;
		HyperlinkedPanel newContent = null;

			// determine what widget user link points to
			for (HyperlinkedPanel cursorPanel : appPanels) {
				if (newToken.equals(cursorPanel.getLink()
						.getTargetHistoryToken())) {
					GWT.log("Other panel requested");
					newContent = cursorPanel;
					break;
				}
			}

		// update current widget to widget specified by user above
		if (newContent != null && currentContent != newContent) {
			currentContent = newContent;
			GWT.log("Current content setup: "
					+ currentContent.getLink().getTargetHistoryToken());
			return currentContent.getArgumentType();
		}
		return ArgumentType.NOT_SPECIFIED;
	}
}
