package com.fertiletech.nbte.campusclient;

import org.gwtbootstrap3.client.ui.CarouselSlide;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Image;
import org.gwtbootstrap3.client.ui.html.Div;

import com.fertiletech.nbte.campusclient.utils.MailingListItem;
import com.fertiletech.nbte.shared.NameTokens;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

public class WelcomePage extends Composite implements HyperlinkedPanel, ClickHandler{

	@UiField
	CarouselSlide bannr1;
	@UiField
	CarouselSlide bannr2;
	@UiField
	CarouselSlide bannr3;
	@UiField
	CarouselSlide bannr5;	
	@UiField
	CarouselSlide bannr4;
	@UiField
	CarouselSlide bannr6;
	@UiField
	CarouselSlide bannr7;
	@UiField
	CarouselSlide bannr8;	

	@UiField
	Column thumb1;
	@UiField
	Column thumb2;
	@UiField
	Column thumb3;
	@UiField
	Column thumb4;
	@UiField
	Column thumb5;
	@UiField
	Column thumb6;
	@UiField
	Column thumb7;
	@UiField
	Column thumb8;
	
	@UiField
	Image visitWeb;
	@UiField
	Image checkGrades;
	@UiField
	Image postUtme;
	
	MailingListItem mailPanel;
	final static int NUM_SLIDE_IMAGES = 8;
	
	
	private static WelcomePageUiBinder uiBinder = GWT
			.create(WelcomePageUiBinder.class);	
	
	interface WelcomePageUiBinder extends UiBinder<Widget, WelcomePage> {
	}
	
	void loadBannerImages(boolean isLoggedIn)
	{
		Div[] departmentSliders = {bannr1, bannr2, bannr3, bannr4, bannr5, bannr6, bannr7, bannr8};
		
		for(int i = 0; i < departmentSliders.length; i++)
		{
			departmentSliders[i].clear();
			String url = NameTokens.getPublicSlideUrl(departmentSliders[i].getTitle());
			if(isLoggedIn)
				url = NameTokens.getGASlideUrl(departmentSliders[i].getTitle());
			Image slidePic = new Image(url);
			slidePic.addStyleName("img-responsive");
			slidePic.addStyleName("showAnchorHand");
			slidePic.setAltText(departmentSliders[i].getTitle());
			departmentSliders[i].add(slidePic);
			slidePic.addClickHandler(this);
		}
	}
	
	void loadThumbImages()
	{
		Div[] departmentSliders = {thumb1, thumb2, thumb3, thumb4, thumb5, thumb6, thumb7, thumb8};
		
		for(int i = 0; i < departmentSliders.length; i++)
		{
			departmentSliders[i].clear();
			String url = NameTokens.getDepartmentListingImageUrl(departmentSliders[i].getTitle(), false);
			Image slidePic = new Image(url);
			slidePic.addStyleName("img-responsive");
			slidePic.addStyleName("showAnchorHand");
			slidePic.setAltText(departmentSliders[i].getTitle());
			departmentSliders[i].add(slidePic);
			slidePic.addClickHandler(this);
		}
	}
	

	public WelcomePage() {
		initWidget(uiBinder.createAndBindUi(this));				
		loadThumbImages();
		postUtme.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				//mailPanel.show(postUtme);
				Window.open("http://www.fcahptib.edu.ng/admission", "_blank", "");
			}
		});
		visitWeb.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Window.open("http://www.fcahptib.edu.ng", "_blank", "");
				
			}
		});
		checkGrades.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Window.open("http://grades.fcahptib.edu.ng", "_blank", "");
				
			}
		});		
	}
		
	@Override
	public Hyperlink getLink() {
		return new Hyperlink("Welcome", NameTokens.WELCOME);
	}

	@Override
	public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
		if(mailPanel == null)
			mailPanel = new MailingListItem(isLoggedIn);
		loadBannerImages(isLoggedIn);
		return this;
	}

	@Override
	public void onClick(ClickEvent event) {
		Image source  = (Image) event.getSource();
		String imageName = source.getAltText();
		String imageTarget = NameTokens.getNameTargetMap().get(imageName);
		History.newItem(imageTarget);	
	}


	@Override
	public ArgumentType getArgumentType() {
		return ArgumentType.NO_ARG;
	} 
}
