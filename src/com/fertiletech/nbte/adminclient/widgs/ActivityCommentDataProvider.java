package com.fertiletech.nbte.adminclient.widgs;

import java.util.List;

import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;

public class ActivityCommentDataProvider extends AsyncDataProvider<TableMessage> {
	/**
	 * {@link #onRangeChanged(HasData)} is called when the table requests a new
	 * range of data. You can push data back to the displays using
	 * {@link #updateRowData(int, List)}.
	 */
	//String noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");
	@Override
	protected void onRangeChanged(HasData<TableMessage> display) {
		// Get the new range.
		final Range range = display.getVisibleRange();
		GWT.log("Range change detected: " + range.getStart() + " - " + range.getLength());
		/*
		 * Query the data asynchronously. If you are using a database, you can
		 * make an RPC call here. We'll use a Timer to simulate a delay.
		 */
		
		//TODO, this is where you'd normally figure out how to do a real-time feed type thing.
	}
}
