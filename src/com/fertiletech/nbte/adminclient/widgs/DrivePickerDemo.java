package com.fertiletech.nbte.adminclient.widgs;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTableFilter;
import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.client.OAuthLoginService;
import com.fertiletech.nbte.server.login.oauth.OurOAuthParams;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.floreysoft.gwt.picker.client.callback.AbstractPickerCallback;
import com.floreysoft.gwt.picker.client.domain.DocsUploadView;
import com.floreysoft.gwt.picker.client.domain.DocsView;
import com.floreysoft.gwt.picker.client.domain.Feature;
import com.floreysoft.gwt.picker.client.domain.Picker;
import com.floreysoft.gwt.picker.client.domain.PickerBuilder;
import com.floreysoft.gwt.picker.client.domain.ViewGroup;
import com.floreysoft.gwt.picker.client.domain.ViewId;
import com.floreysoft.gwt.picker.client.domain.result.BaseResult;
import com.floreysoft.gwt.picker.client.domain.result.DocumentResult;
import com.floreysoft.gwt.picker.client.domain.result.MapResult;
import com.floreysoft.gwt.picker.client.domain.result.ViewToken;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DrivePickerDemo extends ResizeComposite {

	ResultPrinter resultPrinter;
	Button saveAttachment, fileChooser, cancelAttachment;
	String accessToken = null;
	final String APP_ID = OurOAuthParams.GOOGLE_API_KEY;
	ShowcaseTable attachments;
	String loanID = null;
	private ListBox mapTypes = new ListBox();
	private SimplePanel mapTitleSlot = new SimplePanel();
	PopupPanel popupTitleBox = new PopupPanel();
	

	private MyAsyncCallback<List<TableMessage>> displayAttachmentsCallback = new MyAsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show("Error message: "
					+ caught.getMessage());
			resultPrinter.clearOutput();
			fileChooser.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			boolean alreadyInitialized = attachments.isInitialized();
			loanID = result.get(0).getMessageId();
			attachments.showTable(result);
			resultPrinter.clearOutput();
			fileChooser.setEnabled(true);
			if (!alreadyInitialized) {
				final ButtonCell viewAttachment = new ButtonCell();
				Column<TableMessage, String> viewCol = new Column<TableMessage, String>(
						viewAttachment) {

					@Override
					public String getValue(TableMessage object) {
						return "preview";
					}
				};
				viewCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
										

					@Override
					public void update(int index, final TableMessage object,
							String value) {
						resultPrinter.showPreview(object, attachments);
					}
				});
				attachments.addColumn(viewCol, "Preview");
				
				ButtonCell removeAttachment = new ButtonCell();
				Column<TableMessage, String> invCol = new Column<TableMessage, String>(
						removeAttachment) {

					@Override
					public String getValue(TableMessage object) {
						return "Remove Attachment";
					}
				};
				invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
										

					@Override
					public void update(int index, final TableMessage object,
							String value) {
						final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

							@Override
							public void onFailure(Throwable caught) {
								PanelUtilities.errorBox.show("Looks like attachment removal failed. " 
											+ caught.getMessage());
								fileChooser.setEnabled(true);
							}

							@Override
							public void onSuccess(String result) {
								PanelUtilities.infoBox.show(result);
								displayAttachmentsCallback.go("Refereshing attachment list after recent removal. please wait...");
							}

							@Override
							protected void callService(AsyncCallback<String> cb) {
								enableWarning(false);
								PanelUtilities.READ_SERVICE.deleteAttachment(object.getMessageId(), cb);
							}
						};						
						final YesNoDialog confirmRemoval = new YesNoDialog(
								"Confirm Removal");
						confirmRemoval.setClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								confirmRemoval.hide();
								clearPickerAll(false, true);
								editAttachCallback.go("Removing attachment");
							}
						});
						confirmRemoval.show("This will remove the link stored on M.A.P (original " +
								"copy on Google Drive will not be modified/deleted). Proceed?");
					}
				});
				attachments.addColumn(invCol, "Remove");
				attachments.applyFilter(ShowcaseTableFilter.NOT_EQ, 1, "MAPS");
			}
		}

		@Override
		protected void callService(AsyncCallback<List<TableMessage>> cb) {
			
			PanelUtilities.READ_SERVICE.getAttachments(loanID, cb);
		}
	};

	DrivePickerDemo() {
		DockLayoutPanel content = new DockLayoutPanel(Unit.PX);
		// setup selection bar
		HorizontalPanel selectBar = new HorizontalPanel();
		resultPrinter = new ResultPrinter();
		fileChooser = new Button("Secure Attachments");
		saveAttachment = new Button("Attach");
		cancelAttachment = new Button("Cancel");
		selectBar.add(fileChooser);
		selectBar.add(resultPrinter);
		selectBar.add(saveAttachment);
		selectBar.add(cancelAttachment);
		
		// setup list of attachments
		attachments = new ShowcaseTable(null, true, false);
		// setup display
		content.addNorth(selectBar, 50);
		content.add(attachments);
		onPickerLoaded();
		update(null, false);
		setupTitlePopup();
		PanelUtilities.READ_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_MAP_TITLES, new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error loading map titles. " +
						"Staff map attachments will be labeled untitled. Details: " + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				result.remove(0); //get rid of the header
				for(TableMessage m : result)
					mapTypes.addItem(m.getText(0));
			}
		});
		initWidget(content);
	}
	
	private void setupTitlePopup()
	{

		Grid container = new Grid(1, 2);
		container.setWidget(0, 0, mapTypes);
		container.setWidget(0, 1, mapTitleSlot);
		popupTitleBox.setGlassEnabled(true);
		popupTitleBox.setStyleName("busy-PopupPanel");
		popupTitleBox.add(container);
	}

	public void update(final String loanID, boolean checkToken) {
		clearPickerAll(true, true);
		if(loanID == null) return;
		if(!checkToken) return;
		if (ClientUtils.alreadyLoggedIn()) {
			MyAsyncCallback<String> fetchToken = new MyAsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox
							.show("<b>Add/Remove attachments disabled!!</b> An error occured while requesting "
									+ "access to your cloud data <br/>"
									+ caught.getMessage());
				}

				@Override
				public void onSuccess(String result) {
					accessToken = result;
					DrivePickerDemo.this.loanID = loanID;
					displayAttachmentsCallback.go("Drive access verified, fetching attachments for selected application");
				}

				@Override
				protected void callService(AsyncCallback<String> cb) {
					enableWarning(false);
					OAuthLoginService.Util.getInstance().getAccessToken(
							ClientUtils.getSessionIdFromCookie(), cb);
				}
			};
			fetchToken.go("Requesting security token to access your Google Drive");
		}
	}

	private void onPickerLoaded() {
		saveAttachment.addClickHandler(new ClickHandler() {
			MyAsyncCallback<String> saveCallback = new MyAsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Failed to link attachments to M.A.P. Error was: "
												  + caught.getMessage());
					fileChooser.setEnabled(true);
					
				}

				@Override
				public void onSuccess(String result) {
					PanelUtilities.infoBox.show(result);
					displayAttachmentsCallback.go("Refreshing attachment list after recent save, please wait");
				}

				@Override
				protected void callService(AsyncCallback<String> cb) {
					enableWarning(false);
					clearPickerAll(false, false);
					PanelUtilities.READ_SERVICE.saveAttachments(loanID, resultPrinter.getValues(), cb);
					
				}
			};
			@Override
			public void onClick(ClickEvent event) {
				if(loanID == null)
				{
					PanelUtilities.errorBox.show("You must first select an application");
					return;
				}
				saveCallback.go("Linking attachments to Addoser's cloud database");
			}
		});
		cancelAttachment.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				resultPrinter.clearOutput();
				cancelAttachment.setEnabled(false);
				saveAttachment.setEnabled(false);
				fileChooser.setEnabled(true);
			}
		});
		
		fileChooser.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent clickEvent) {
				
				if(accessToken == null)
				{
					PanelUtilities.errorBox
							.show("Looks like your session has expired. Redirecting back to login page");
					OAuthLoginService.Util.getAuthorizationUrl(ClientUtils.BANK);
					return;
				}	
				
				DocsView folderView = DocsView.create(ViewId.FOLDERS);
				folderView.setSelectFolderEnabled(true);	
				final ViewGroup docsGroup = ViewGroup.create(ViewId.DOCS);
				docsGroup.addView(ViewId.DOCUMENTS);
				docsGroup.addView(ViewId.PDFS);
				docsGroup.addView(ViewId.SPREADSHEETS);
				docsGroup.addLabel("grouped by");
				docsGroup.addView(ViewId.FOLDERS);
				//docsGroup.addLabel("selectable");
				//docsGroup.addView(folderView);
				
				final ViewGroup mediaGroup = ViewGroup.create(ViewId.DOCS_IMAGES);
				mediaGroup.addView(ViewId.PHOTOS);
				mediaGroup.addLabel("public media");
				mediaGroup.addView(ViewId.IMAGE_SEARCH);
				mediaGroup.addView(ViewId.VIDEO_SEARCH);
				mediaGroup.addView(ViewId.YOUTUBE);
				
				DocsUploadView uploadView = DocsUploadView.create();
				uploadView.setIncludeFolders(true);
				final ViewGroup uploadGroup = ViewGroup.create(uploadView);
				uploadGroup.addLabel("web album");
				uploadGroup.addView(ViewId.PHOTO_UPLOAD);
				
				

				PickerBuilder builder = PickerBuilder.create()
						.addViewGroup(docsGroup)
						.addViewGroup(mediaGroup)
						//.addView(folderView)
						.addViewGroup(uploadGroup)
						.addView(ViewId.MAPS)
						.setTitle("Addosser Private Google Cloud and Public Media Searches");
				builder.enableFeature(Feature.MULTISELECT_ENABLED);
				builder.setAppId(APP_ID);
				builder.setOAuthToken(accessToken);
				builder.addCallback(buildPickerCallback());	
				//builder.addCallback(buildPickerCallback(ViewId.MAPS));
				Picker picker = builder.build();
				
				
				picker.setVisible(true);
			}
		}

		);
	}

	private AbstractPickerCallback buildPickerCallback() {
		return new AbstractPickerCallback() {
			@Override
			public void onPicked(final ViewToken viewToken, final BaseResult result) {
				ViewId id = result.getViewToken().getViewId();
				if(id != null && id.equals(ViewId.MAPS))
				{
					Button save = new Button("Select Map Title");
					mapTitleSlot.clear();
					mapTitleSlot.add(save);
					save.addClickHandler(new ClickHandler() {
						
						@Override
						public void onClick(ClickEvent event) {
							String mapTitle = "Untitled";
							mapTitle = mapTypes.getValue(mapTypes.getSelectedIndex());
							resultPrinter.setAttached(viewToken, result.<MapResult> cast(), mapTitle);
							popupTitleBox.hide();
						}
					});
					popupTitleBox.showRelativeTo(saveAttachment);
					
				}
				else
					resultPrinter.setAttached(viewToken, result.<DocumentResult> cast());

				fileChooser.setEnabled(false);
				cancelAttachment.setEnabled(true);
				saveAttachment.setEnabled(true);
			}

			@Override
			public void onCanceled() {
				resultPrinter.clearOutput();
				fileChooser.setEnabled(true);
				cancelAttachment.setEnabled(false);
				saveAttachment.setEnabled(false);
			}
		};
	}
	
	private void clearPickerAll(boolean clearTable, boolean clearChosen)
	{
		if(clearChosen)
			resultPrinter.clearOutput();
		fileChooser.setEnabled(false);
		cancelAttachment.setEnabled(false);
		saveAttachment.setEnabled(false);
		if(clearTable)
		{
			loanID = null;
			attachments.clear();
		}
	}

}
