package com.fertiletech.nbte.adminclient.widgs;


import java.util.HashMap;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.ValidationUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class SearchEmailBox extends Composite implements HasClickHandlers, HasEnabled {

	@UiField
	TextBox idBox;
	@UiField
	Button search;
	@UiField
	Button newApp;
	
	private static SearchEmailBoxUiBinder uiBinder = GWT
			.create(SearchEmailBoxUiBinder.class);

	interface SearchEmailBoxUiBinder extends UiBinder<Widget, SearchEmailBox> {
	}

	public SearchEmailBox() {
		initWidget(uiBinder.createAndBindUi(this));
		newApp.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Error creating new application. " + caught.getMessage());
				}

				@Override
				public void onSuccess(String[] result) {
					Element elem = search.getElement();
					clickElement(elem);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				HashMap<String,String> appData = new HashMap<String, String>();
				String email = getEmailAddress();
				if(email == null) return;
				appData.put(ApplicationFormConstants.EMAIL, email);
				//PanelUtilities.getLoanMktService().startLoanApplication(appData, callback );
				//TODO
				Window.alert("TODO");
			}
		});
		newApp.setEnabled(false);
	}

	public static native void clickElement(Element elem) /*-{
    	elem.click();
	}-*/;

	@Override
	public boolean isEnabled() {
		return search.isEnabled();
	}
	
	public String getEmailAddress()
	{
		String val = idBox.getValue();
		if(val == null || val.trim().length() == 0)
		{
			PanelUtilities.errorBox.show("You must specify an email address");
			return null;			
		}
		
		if(!ValidationUtils.isValidEmail(val))
		{
			PanelUtilities.errorBox.show("Invalid email address specified: " + val);
			return null;
		}
		
		return val.trim().toLowerCase();
	}

	@Override
	public void setEnabled(boolean enabled) {
		search.setEnabled(enabled);
		idBox.setEnabled(enabled);
		if(!enabled)
			newApp.setEnabled(enabled);
	}
	
	public void enableNewApps(boolean enabled)
	{
		newApp.setEnabled(enabled);
		if(enabled)
			newApp.setTitle("Click to create a new application form");
		else
			newApp.setTitle("Application creation disabled. To create a new applications, all prior applications from this user must " +
					"either have been cancelled, approved or the application must be over a year old and denied");
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return search.addClickHandler(handler);
	}
}
