package com.fertiletech.nbte.adminclient.widgs;

import java.util.Date;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

public class SearchDateBox extends Composite implements HasClickHandlers, HasEnabled {
	@UiField
	DateBox startDate;
	@UiField
	DateBox endDate;
	@UiField
	Button search;
	
	private final int DEFAULT_DAYS_TO_LOOK_BACK = 28; //4 weeks


	private static SearchDateBoxUiBinder uiBinder = GWT
			.create(SearchDateBoxUiBinder.class);

	interface SearchDateBoxUiBinder extends UiBinder<Widget, SearchDateBox> {
	}

	public SearchDateBox() {
		initWidget(uiBinder.createAndBindUi(this));
		Date startVal = new Date();
		Date endVal = new Date();
		CalendarUtil.addDaysToDate(startVal, -DEFAULT_DAYS_TO_LOOK_BACK);
		startDate.setValue(startVal);
		endDate.setValue(endVal);
		
	}
	
	public Date[] getSearchDates()
	{
		Date[] result = new Date[2];
		result[0] = startDate.getValue();
		result[1] = endDate.getValue();
		if(result[0] == null || result[1] == null)
		{
			PanelUtilities.errorBox.show("Please select a valid date for both start and end date fields");
			return null;
		}
		if(result[0].after(result[1]))
		{
			PanelUtilities.errorBox.show("Start date specified is a later date than specified end date.");
			return null;
		}
		return result;
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return search.addClickHandler(handler);
	}


	@Override
	public boolean isEnabled() {
		return search.isEnabled();
	}


	@Override
	public void setEnabled(boolean enabled) {
		search.setEnabled(enabled);
		startDate.setEnabled(enabled);
		endDate.setEnabled(enabled);
	}

}
