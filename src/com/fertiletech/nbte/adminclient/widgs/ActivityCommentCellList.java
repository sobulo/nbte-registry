package com.fertiletech.nbte.adminclient.widgs;


import java.util.Date;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.TableMessageKeyProvider;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class ActivityCommentCellList extends ResizeComposite{
	 ListDataProvider<TableMessage> dataProvider;
	 HTML lastRefresh;
	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource notice();
	}

	/**
	 * The Cell used to render
	 */

	public static class NoticeCell extends AbstractCell<TableMessage> {

		/**
		 * The html of the image used for contacts.
		 */
		private final String imageHtml;

		static Images images = GWT.create(Images.class);
		
		public NoticeCell(ImageResource image) {
			this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}
		
		public String getImageHTML(){ return imageHtml; }
		
		public NoticeCell()
		{
			this(images.notice());
		}

		@Override
		public void render(Context context, TableMessage value,
				SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}

			sb.appendHtmlConstant("<table style='font-size:smaller;border-bottom:1px solid grey'>");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td rowspan='3' valign='top'>");
			sb.appendHtmlConstant(imageHtml);
			sb.appendHtmlConstant("</td>");

			String user = value.getText(0);
			String msg = value.getText(1).replaceAll("\\<.*?\\>", "");
			msg = msg.substring(0, Math.min(100, msg.length())) + "...";
			String date = GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(value.getDate(0));
			String fullName = user + " on " + date;
			// Add the name and address.
			sb.appendHtmlConstant("<td>");
			sb.appendEscaped(fullName);
			sb.appendHtmlConstant("</td></tr><tr><td>");
			sb.appendEscaped(msg);
			sb.appendHtmlConstant("</td></tr></table>");
		}
	}

	private AsyncCallback<List<TableMessage>> commentsCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			TableMessage m = new TableMessage(2, 0, 1);
			m.setText(0, "FAILURE ALERT");
			m.setText(1, caught.getMessage());
			m.setDate(0, new Date());
			lastRefresh.setHTML(DIV_START + "Refreshed on " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(new Date())+ "</div>");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() == 0)
			{
				lastRefresh.setHTML(DIV_START + "!!Activity Feed Empty!!</div>");
				return;
			}
			lastRefresh.setHTML(DIV_START + "Refreshed on " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(new Date())+ "</div>");
			dataProvider.getList().addAll(result);
		}
	};
	
	final static String DIV_START = "<div style='font-size:smaller;width:100%;background-color:#005500;color:white'>";
	public ActivityCommentCellList()
	{

	    CellList<TableMessage> cellList = new CellList<TableMessage>(new NoticeCell());
	    lastRefresh = new HTML(DIV_START + "Loading ..." + "</div>");
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		final SingleSelectionModel<TableMessage> selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					PopupPanel panel = new PopupPanel();
					HTML contentArea = new HTML();
					{
						panel.setAnimationEnabled(true);
						panel.setAutoHideEnabled(true);
						panel.setWidth("400px");
						panel.setHeight("300px");
						panel.add(new ScrollPanel(contentArea));
					}
					public void onSelectionChange(SelectionChangeEvent event) {
						TableMessage m = selectionModel.getSelectedObject();
						if(m == null) return;
						contentArea.setHTML(m.getText(1));
						panel.center();
					}
				});
	    // Create a data provider.
	    dataProvider = new ListDataProvider<TableMessage>();

	    // Add the cellList to the dataProvider.
	    dataProvider.addDataDisplay(cellList);

	    // Create paging controls.
	    ShowMorePagerPanel pager = new ShowMorePagerPanel();
	    pager.setDisplay(cellList);
	    DockLayoutPanel widget = new DockLayoutPanel(Unit.PX);
	    widget.addNorth(lastRefresh, 20);
	    Button b = new Button("Refresh");
	    b.setWidth("100%");
	    widget.addSouth(b, 40);
	    widget.add(pager);
	    initWidget(widget);
	    b.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				refresh();
			}
		});
	}
	
	public void refresh()
	{
		dataProvider.getList().clear();
		lastRefresh.setHTML(DIV_START + "Loading ..." + "</div>");
		PanelUtilities.READ_SERVICE.getRecentActivityComments(commentsCallback );
	}
}
