package com.fertiletech.nbte.adminclient.contentwrappers;

import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class EditorCoursesCW extends CWEditParamsBase{

	public EditorCoursesCW() {
		super("Courses Admins", "Unique ID should be the email address of staff in question. " +
				"Adding an email account grants that account user rights for editing course information");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_EDIT_COURSES;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(EditorCoursesCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
