package com.fertiletech.nbte.adminclient.contentwrappers;

import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class EditorStudentsCW extends CWEditParamsBase{

	public EditorStudentsCW() {
		super("Students Admins", "Unique ID should be the email address of staff in question. " +
				"Adding an email account grants that account user rights for editing student records");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_EDIT_STUDENTS;
	}
	
	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(EditorStudentsCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}
}
