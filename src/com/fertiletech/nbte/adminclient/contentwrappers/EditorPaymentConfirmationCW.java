package com.fertiletech.nbte.adminclient.contentwrappers;

import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class EditorPaymentConfirmationCW extends CWEditParamsBase{

	public EditorPaymentConfirmationCW() {
		super("Payments Admins", "Add email address of staff you would like to" +
				" grant the ability to confirm the status of student payments");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_EDIT_PAY_STATUS;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(EditorPaymentConfirmationCW.class, super.getAsyncCall(callback));	
	}
	
	@Override
	protected boolean uniqueAsLowerCase() {
		return true;
	}

}
