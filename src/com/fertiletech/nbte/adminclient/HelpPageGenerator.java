package com.fertiletech.nbte.adminclient;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;

public class HelpPageGenerator {
	//MODULE NAMES
	public final static String HELP_WELCOME_URL = "http://cs.fcahptib.edu.ng/";
	public final static String HELP_ADMIN_URL = HELP_WELCOME_URL + "/admin";
	
	static HTML help;
	static
	{
		//setup help
		help = new HTML();
		help.setSize("95%", "500px");
		help.addStyleName("helpWatermark");
	}
	
	
	public static HTML getHelpWidget(String baseUrl, String moduleName)
	{
		Exception e = new Exception();
		StringBuffer b = new StringBuffer();
		for(StackTraceElement s : e.getStackTrace())
			b.append(s.getClassName() + " Method: " + s.getMethodName() + " Line: " + s.getLineNumber() + "***");
		GWT.log("Requested help page for: " + moduleName);
		GWT.log("Stack Trace: " + b.toString());
		String url = baseUrl + "#" + moduleName.replace(" ", "").toLowerCase();
		openNewWindow(moduleName, url);
		help.setHTML("<p>If you do not see the help window it may be due to your browser preventing popups.<br/> You can visit the help page on Addosser's Intranet directly at: <a href='" + url + "'>" + url + "</a></p>");
		return help;
	}
	
	private static void openNewWindow(String name, String url) {
	    Window.open(url, name.replace(" ", "_"),
	           "menubar=no," + 
	           "location=false," + 
	           "resizable=yes," + 
	           "scrollbars=yes," + 
	           "status=no," + 
	           "dependent=true");
	}
	
	public static String getHistoryTokenWithArgs(Class<? extends ContentWidget> cw, String args)
	{
		String token = Showcase.getContentWidgetToken(cw) + "/" + args;
		return token;
	}
}
