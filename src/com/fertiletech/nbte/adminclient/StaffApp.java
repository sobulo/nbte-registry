package com.fertiletech.nbte.adminclient;

import com.fertiletech.nbte.server.login.oauth.OurOAuthParams;
import com.floreysoft.gwt.picker.client.utils.PickerLoader;
import com.google.gwt.core.client.EntryPoint;

public class StaffApp implements EntryPoint {

	@Override
	public void onModuleLoad() {
	    PickerLoader.loadApi(OurOAuthParams.GOOGLE_API_SECRET, new Runnable() {
	        public void run() {
	        	//RootLayoutPanel.get().add(new AddBillCW().onInitialize());
	        	new Showcase();
	        }
	      });		
	}

}
