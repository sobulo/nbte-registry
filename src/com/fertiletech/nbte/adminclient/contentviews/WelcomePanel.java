/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.nbte.adminclient.contentviews;

import com.fertiletech.nbte.adminclient.widgs.ActivityCommentCellList;
import com.fertiletech.nbte.client.OAuthLoginService;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.LoginRoles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WelcomePanel extends ResizeComposite
{	
	@UiField
	HTML displayMessage;
	
	@UiField
	SimplePanel anchorSlot;
	@UiField ActivityCommentCellList activityFeed;
	@UiField DockLayoutPanel container;
	Anchor logOutAnchor;
	Anchor loginAnchor;
	final int FEED_WIDTH = 410;

    
    private static WelcomePanelUiBinder uiBinder = GWT.create(WelcomePanelUiBinder.class);
    
    interface WelcomePanelUiBinder extends UiBinder<Widget, WelcomePanel> {
    }
    
    
    public WelcomePanel() {
        initWidget(uiBinder.createAndBindUi(this));
        logOutAnchor = new Anchor(true);
		logOutAnchor.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) 
			{
				OAuthLoginService.Util.logout();
			}
		});
		loginAnchor = new Anchor(true);
		loginAnchor.setText("Click here to login with your college email account");
		loginAnchor.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
	               final int authProvider = ClientUtils.BANK;
	               OAuthLoginService.Util.getAuthorizationUrl(authProvider);
			}
		});
    }
    
    public void updateScreen()
    {
    	anchorSlot.clear();
    	String message = "<p>Hello";
    	refreshFeed(false);
    	if(ClientUtils.alreadyLoggedIn())
    	{
    		BankUserCookie userCookie = ClientUtils.BankUserCookie.getCookie();
    		message+= " " + userCookie.getUserName() + ",</p>";
    		logOutAnchor.setText("Click here to logout of your " + userCookie.getEmail() + " " + ClientUtils.getAuthProviderNameFromCookie() + " account");
    		anchorSlot.add(logOutAnchor);
    		boolean isOps = userCookie.getRole().ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal();
    		if(isOps)
    			refreshFeed(true);
    	}
    	else
    	{
    		message += ", </p>";
    		anchorSlot.add(loginAnchor);
    	}
    	displayMessage.setHTML(message);
    }
    
	public void refreshFeed(boolean show)
	{
		if(show)
		{
			activityFeed.refresh();
			container.setWidgetSize(activityFeed, FEED_WIDTH);
		}
		else
			container.setWidgetSize(activityFeed, 0);
		container.animate(1500);
	}    
}