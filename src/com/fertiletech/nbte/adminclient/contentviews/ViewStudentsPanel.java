package com.fertiletech.nbte.adminclient.contentviews;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.client.eventboxes.DiplDeptLB;
import com.fertiletech.nbte.client.eventboxes.UniqueListBox;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

public class ViewStudentsPanel extends Composite{
	final static int DIPL_IDX = 0;
	final static int DEPT_IDX = 1;
	final static int YEAR_IDX = 2;
	final static int ACADEMIC_YEAR_IDX = 3;
	
	public ViewStudentsPanel() {
		DockLayoutPanel panel = new DockLayoutPanel(Unit.PX);
		Grid searchBar = new Grid(2, 5);
		String[] searchLabels = {"Diploma", "Department", "Year", "Session"};
		final ListBox[] searchBoxes = new ListBox[4];
		for(int i = 0; i < searchBoxes.length; i++)
		{
			searchBar.setWidget(0, i, new Label(searchLabels[i]));
			ListBox lb = new ListBox();
			searchBoxes[i] = lb;
			searchBar.setWidget(1, i, lb);
			switch (i) {
			case DEPT_IDX:
				new DiplDeptLB(searchBoxes[DIPL_IDX], searchBoxes[DEPT_IDX]);
				break;
			case YEAR_IDX:
				new UniqueListBox(lb, NameTokens.YEARS);
				break;
			case ACADEMIC_YEAR_IDX:
				new UniqueListBox(lb, NameTokens.getSampleAcademicYears());
				break;
			}
		}
		final Button search = new Button("View Roster");
		searchBar.setWidget(1, 4, search);
		final ShowcaseTable table = new ShowcaseTable();
		search.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Failed to load course information: " + caught.getMessage());
					search.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					table.showTable(result);
					search.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				search.setEnabled(false);
				PanelUtilities.REGISTRATION_SERVICE.getDepartmentRoster(getValue(searchBoxes[DEPT_IDX]), getValue(searchBoxes[DIPL_IDX]), 
						getValue(searchBoxes[YEAR_IDX]), Integer.valueOf(getValue(searchBoxes[ACADEMIC_YEAR_IDX])), callback);
			}
		});
		panel.addNorth(searchBar, 60);
		panel.add(table);
		initWidget(panel);
	}
	
	String getValue(ListBox lb)
	{
		if(lb.getItemCount() == 0)
			return "";
		return lb.getValue(lb.getSelectedIndex());
	}
}
