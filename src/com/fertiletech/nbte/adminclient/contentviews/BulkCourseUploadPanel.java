/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.nbte.adminclient.contentviews;



import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
/**
 *
 * @author Administrator
 */
public class BulkCourseUploadPanel extends Composite implements 
        ClickHandler, FormPanel.SubmitCompleteHandler
{
    
    private final static String LOAD_PARAM_NAME = "loadData";
    private final static String GOOD_DATA_PREFIX = "<i></i>";
    public final static String INIT_DISPLAY = "Please choose a spreadsheet " +
            "and hit submit to upload, ensure spreadsheet has the following " +
            "columns only<b>MODEL,MANUFACTURER,CATEGORY,TITLE,FEATURES,DESCRIPTION</b>";
    
    
    @UiField FileUpload selectFile;
    @UiField Button submitFile;
    @UiField FormPanel vendorUploadForm;
    @UiField HTML formResponse;
    @UiField SimplePanel topLevelPanel;

    FormPanel confirmationForm;
    Button confirmData;
    Button resetData;


    private static BulkVendorUploadPanelUiBinder uiBinder = GWT.create(BulkVendorUploadPanelUiBinder.class);

    interface BulkVendorUploadPanelUiBinder extends UiBinder<Widget, BulkCourseUploadPanel> {
    }


    public void resetPanel()
    {
        vendorUploadForm.reset();
        confirmationForm.reset();
        formResponse.setHTML(INIT_DISPLAY);
        topLevelPanel.remove(topLevelPanel.getWidget());
        topLevelPanel.add(vendorUploadForm);
    }
    
    public void onClick(ClickEvent event)
    {
        if(event.getSource().equals(submitFile))
        {
        	if(!selectFile.getFilename().endsWith("xls"))
        	{
        		Window.alert("Please select an excel 97-2003 worksheet (.xls extension)");
        		return;
        	}
            formResponse.setHTML("<b>Sending file to server ...</b>");
            vendorUploadForm.submit();
        }
        else if(event.getSource().equals(confirmData))
        {
            //confirmDisplay.setHTML("<b>Saving confirmed data ...</b>");
        	formResponse.setHTML("<b>Saving confirmed data ...</b>");
            confirmationForm.submit();
        }
        else if(event.getSource().equals(resetData))
        {
            resetPanel();
        }
    }

    public void onSubmitComplete(SubmitCompleteEvent event)
    {
        String results = event.getResults();
        //confirmDisplay.setHTML(event.getResults());
        formResponse.setHTML(event.getResults());
        if(results.startsWith(GOOD_DATA_PREFIX)) //hacky way 2 determine state
            confirmData.setVisible(true);
        else
            confirmData.setVisible(false);
        if(topLevelPanel.getWidget().equals(vendorUploadForm))
        {
            topLevelPanel.remove(vendorUploadForm);
            topLevelPanel.add(confirmationForm);
        }
    }

    public BulkCourseUploadPanel()
    {
		// TODO Auto-generated method stub
		initWidget(uiBinder.createAndBindUi(this));
		final String ACTION_URL = "/upload/courses";
        selectFile.setName("chooseFile");
        submitFile.addClickHandler(this);
        vendorUploadForm.setAction(ACTION_URL);
        vendorUploadForm.addSubmitCompleteHandler(this);
        vendorUploadForm.setMethod("post");
        vendorUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);

        confirmationForm = new FormPanel();
        FlowPanel formComponents = new FlowPanel();
        confirmationForm.setAction(ACTION_URL);
        confirmationForm.setMethod("post");
        confirmationForm.addSubmitCompleteHandler(this);
        confirmData = new Button("Save Loaded Data");
        resetData = new Button("Load new File");
        confirmData.addClickHandler(this);
        resetData.addClickHandler(this);
        //confirmDisplay = new HTML();
        formComponents.add(new Hidden(LOAD_PARAM_NAME));
        //formComponents.add(confirmDisplay);
        formComponents.add(confirmData);
        formComponents.add(resetData);
        confirmationForm.add(formComponents);
	}


}