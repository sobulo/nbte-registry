package com.fertiletech.nbte.adminclient.contentviews;



import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.utils.CommentsPanel;
import com.fertiletech.nbte.adminclient.utils.ValuesListBox;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class EditAppParameters extends Composite {
	@UiField
	ShowcaseTable display;
	
	@UiField
	Button newItem;

	@UiField
	Button saveItems;	
	
	@UiField
	Button removeItem;

	@UiField
	TabLayoutPanel container;
	
	@UiField
	CommentsPanel generalComments;
	
	
	private boolean commentsLoaded;
	
	final static int COMMENT_TAB_IDX = 1;	
	SimpleDialog infoBox = null;
	
	private TextBox key;
	private HasValue<String> value;
	boolean showValues;
	Button add = new Button("Add");
	Button edit = new Button("Edit");
	SimplePanel buttonSlot;
	PopupPanel editPanelContainer;
	String parameterID;
	boolean lowerCaseIDs;
	String appID;

	private AsyncCallback<List<TableMessage>> loadCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
				infoBox.show("Unable to fetch parameters for " + parameterID + " error was: <br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			appID = result.get(0).getMessageId();
			display.showTable(result);
			newItem.setEnabled(true);
			saveItems.setEnabled(true);
		}
	};

	private static EditAppParametersUiBinder uiBinder = GWT
			.create(EditAppParametersUiBinder.class);

	interface EditAppParametersUiBinder extends
			UiBinder<Widget, EditAppParameters> {
	}

	public EditAppParameters(String parameterID, boolean showValues, boolean uniqueAsLowerCase, String dropDownID) {
		initWidget(uiBinder.createAndBindUi(this));
		this.parameterID = parameterID;
		this.showValues = showValues;
		this.lowerCaseIDs = uniqueAsLowerCase;
		infoBox = new SimpleDialog("INFO");
		buttonSlot = new SimplePanel();
		newItem.setEnabled(false);
		saveItems.setEnabled(false);
		setupContentArea(dropDownID);
		container.setAnimationVertical(true);
		container.setAnimationDuration(800);
		container.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			@Override
			public void onBeforeSelection(
					BeforeSelectionEvent<Integer> event) 
			{
				if (event.getItem() > 0 && appID == null) {
					infoBox.show("Please wait for table to load before viewing comments. Try back shortly");
					event.cancel();
					return;
				}				
				else if(event.getItem() == COMMENT_TAB_IDX && !commentsLoaded)
				{
					generalComments.setCommentID(appID);
					commentsLoaded = true;
				}
			}
		});
		
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event == null) return;
				showEditRowPanel(edit);
			}
		});
		newItem.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				display.selectNone();
				showEditRowPanel(add);	
			}
		});
		add.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//check if key is non-empty
				if(key.getValue().trim().length() == 0)
				{
					infoBox.show("Enter a unique id for this entry");
					return;
				}

				if(EditAppParameters.this.showValues && value.getValue().trim().length() == 0)
				{
					infoBox.show("Enter a description for this entry");
					return;
				}
				
				//check if entry exists already
				List<TableMessage> list = display.getDataList();
				boolean foundKey = false;
				boolean foundValue = false;
				for(TableMessage l : list)
				{
					if(l.getMessageId().trim().equalsIgnoreCase(key.getValue().trim()))
						foundKey = true;
					if(l.getText(1).trim().equalsIgnoreCase(value.getValue().trim()))
						foundValue = true;
				}
				
				if(foundKey)
				{
					infoBox.show("An entry with this unique id exists already, click cancel and edit the entry " +
							"instead of creating a new one");
					return;
				}
				
				if(EditAppParameters.this.showValues && foundValue)
				{
					infoBox.show("This configuration panel requires descriptions be unique as well and an existing entry was found" +
							" with this value. Click cancel and edit the entry instead");
					return;
				}
				
				//add new entry
				TableMessage m = new TableMessage(2, 0, 0);
				m.setText(0, lowerCaseIDs? key.getValue().trim().toLowerCase() : key.getValue().trim());
				m.setText(1, value.getValue().trim());
				m.setMessageId(m.getText(0));
				list.add(m);
				key.setValue(null);
				value.setValue(null);
				display.highlightRow(m);
				editPanelContainer.hide();
			}
		});

		edit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				//check if key is non-empty
				if(key.getValue().trim().length() == 0)
				{
					infoBox.show("Enter a unique id for this entry");
					return;
				}

				if(EditAppParameters.this.showValues && value.getValue().trim().length() == 0)
				{
					infoBox.show("Enter a description for this entry");
					return;
				}
				
				//get object to edit
				TableMessage m = display.getSelectedRow();
				if(m == null)
				{
					//this is a sanity check, no logical reason why m should be null
					infoBox.show("Local table state is corrupted, hit cancel and try again");
					return;
				}
				//edit the entry
				m.setText(0, lowerCaseIDs? key.getValue().trim().toLowerCase() : key.getValue().trim());
				m.setText(1, value.getValue().trim());
				m.setMessageId(m.getText(0));
				display.refresh(m);
				display.highlightRow(m);
				display.selectNone();
				key.setValue(null);
				value.setValue(null);
				editPanelContainer.hide();
			}
		});
		removeItem.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				TableMessage m = display.getSelectedRow();
				if(m == null) //nothing selected, nothing to do
					return;
				display.getDataList().remove(m);
			}
		});
		saveItems.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<Void> callback = new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
					infoBox.show("Failed to save changes on database. Error was: " + caught.getMessage());
					saveItems.setEnabled(true);
				}

				@Override
				public void onSuccess(Void result) {
					commentsLoaded = false;
					infoBox.show("All changes in table persisted successfully on database");
					saveItems.setEnabled(true);
					display.clearHighlights();
					display.refreshAll();
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				List<TableMessage> contents = display.getDataList();
				HashMap<String, String> val = new HashMap<String, String>(contents.size());
				for(TableMessage m : contents)
					val.put(m.getText(0), m.getText(1));
				saveItems.setEnabled(false);
				PanelUtilities.READ_SERVICE.saveApplicationParameter(EditAppParameters.this.parameterID, val, callback );
			}
		});
		PanelUtilities.READ_SERVICE.getApplicationParameter(parameterID, loadCallback );
	}
	
	private void setupContentArea(String dropDownID)
	{
		editPanelContainer = new PopupPanel();
		editPanelContainer.setAnimationEnabled(true);
		editPanelContainer.setGlassEnabled(true);
		key = new TextBox();
		String width = "180px";
		key.setWidth(width);
		TextArea descr = new TextArea();
		descr.setWidth(width);
		descr.setVisibleLines(5);
		value = (dropDownID == null) ?  descr: new ValuesListBox().loadBox(dropDownID);
		Grid content = new Grid(3, 2);
		Button c = new Button("Cancel");
		c.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				key.setValue(null);
				value.setValue(null);
				editPanelContainer.hide();
			}
		});
		content.setWidget(0, 0, new Label("Unique ID"));
		content.setWidget(0, 1, key);
		content.setWidget(1, 0, new Label("Description"));
		content.setWidget(1, 1, (Widget) value);
		content.setWidget(2, 0, c);
		content.setWidget(2, 1, buttonSlot);
		editPanelContainer.add(content);
	}
	
	private void showEditRowPanel(Button b)
	{
		TableMessage m = display.getSelectedRow();
		if(m != null)
		{
			key.setValue(m.getText(0));
			value.setValue(m.getText(1));
		}
		buttonSlot.clear();
		buttonSlot.add(b);
		editPanelContainer.center();
	}
	
	public void enableAddingItems()
	{
		newItem.setEnabled(true);
		saveItems.setEnabled(true);
	}
}

