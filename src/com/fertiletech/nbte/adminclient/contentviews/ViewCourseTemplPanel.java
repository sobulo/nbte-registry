package com.fertiletech.nbte.adminclient.contentviews;



import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.client.eventboxes.DiplDeptLB;
import com.fertiletech.nbte.client.eventboxes.UniqueListBox;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;

public class ViewCourseTemplPanel extends Composite{

	final static int DIPL_IDX = 0;
	final static int DEPT_IDX = 1;
	final static int YEAR_IDX = 2;
	final static int SEMESTER_IDX = 3;
	final static int ACADEMIC_YEAR_IDX = 4;
	public ViewCourseTemplPanel() {
		DockLayoutPanel panel = new DockLayoutPanel(Unit.PX);
		Grid searchBar = new Grid(2, 5);
		String[] searchLabels = {"Diploma", "Department", "Year", "Semester"};
		final ListBox[] searchBoxes = new ListBox[5];
		final Button search = new Button("Search Session");
		for(int i = 0; i < searchBoxes.length; i++)
		{
			if(i == ACADEMIC_YEAR_IDX)
				searchBar.setWidget(1, i, search);
			else
				searchBar.setWidget(1, i, new Label(searchLabels[i]));
			ListBox lb = new ListBox();
			searchBoxes[i] = lb;
			searchBar.setWidget(0, i, lb);
			switch (i) {
			case DEPT_IDX:
				new DiplDeptLB(searchBoxes[DIPL_IDX], searchBoxes[DEPT_IDX]);
				break;
			case YEAR_IDX:
				new UniqueListBox(lb, NameTokens.YEARS);
				break;
			case SEMESTER_IDX:
				new UniqueListBox(lb, NameTokens.getSampleSemesters());
				break;
			case ACADEMIC_YEAR_IDX:
				new UniqueListBox(lb, NameTokens.getSampleAcademicYears());
				break;
			}
		}
		final ShowcaseTable table = new ShowcaseTable();
		table.setEmptyWidget(new HTML("No courses found"));
		search.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					PanelUtilities.errorBox.show("Failed to load course information: " + caught.getMessage());
					search.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					table.showTable(result);
					search.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				Window.alert("I got called");
				search.setEnabled(false);
				String dept = getValue(searchBoxes[DEPT_IDX]);
				String dipl = getValue(searchBoxes[DIPL_IDX]);
				String year = getValue(searchBoxes[YEAR_IDX]);
				int semester = Integer.valueOf(getValue(searchBoxes[SEMESTER_IDX]));
				int academicYear = Integer.valueOf(getValue(searchBoxes[ACADEMIC_YEAR_IDX]));
				GWT.log("DEPT: [" + dept + "] DIPLOMA: [" + dipl + "] YEAR: [" + year +
						"] ACADEMIC [" + academicYear + "] SEMESTER [" + semester +"]");
				PanelUtilities.REGISTRATION_SERVICE.getDepartmentCourseTemplates(dept, dipl,
						year, semester, academicYear, callback );
			}
		});
		panel.addNorth(searchBar, 60);
		panel.add(table);
		initWidget(panel);
	}
	
	String getValue(ListBox lb)
	{
		if(lb.getItemCount() == 0)
			return "";
		return lb.getValue(lb.getSelectedIndex());
	}
}
