package com.fertiletech.nbte.adminclient;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class AccessDeniedDisplay extends Composite {

	private static AccessDeniedDisplayUiBinder uiBinder = GWT
			.create(AccessDeniedDisplayUiBinder.class);

	interface AccessDeniedDisplayUiBinder extends
			UiBinder<Widget, AccessDeniedDisplay> {
	}

	public AccessDeniedDisplay() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
