/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fertiletech.nbte.adminclient.accounts.AddBillCW;
import com.fertiletech.nbte.adminclient.accounts.AddBillTemplate;
import com.fertiletech.nbte.adminclient.accounts.AddTenantPayment;
import com.fertiletech.nbte.adminclient.accounts.CWAddTenantDeposit;
import com.fertiletech.nbte.adminclient.accounts.CWEditResidentAccount;
import com.fertiletech.nbte.adminclient.accounts.CWRABlotter;
import com.fertiletech.nbte.adminclient.accounts.CWResidentAccount;
import com.fertiletech.nbte.adminclient.accounts.CWSearchInvoice;
import com.fertiletech.nbte.adminclient.accounts.CWViewDepositBlotter;
import com.fertiletech.nbte.adminclient.accounts.ViewBillBlotter;
import com.fertiletech.nbte.adminclient.accounts.admin.CWCompanyBankAccount;
import com.fertiletech.nbte.adminclient.accounts.admin.CWEditResidentAccountTypes;
import com.fertiletech.nbte.adminclient.accounts.admin.CWEditResidentBillTypes;
import com.fertiletech.nbte.adminclient.accounts.print.CWDepositStatements;
import com.fertiletech.nbte.adminclient.accounts.print.CWPrintDepositRequest;
import com.fertiletech.nbte.adminclient.accounts.print.PrintInvoices;
import com.fertiletech.nbte.adminclient.contentwrappers.AdminCW;
import com.fertiletech.nbte.adminclient.contentwrappers.BulkCoursesCW;
import com.fertiletech.nbte.adminclient.contentwrappers.BulkDepositCW;
import com.fertiletech.nbte.adminclient.contentwrappers.BulkStudentCW;
import com.fertiletech.nbte.adminclient.contentwrappers.BulkTimeTableCW;
import com.fertiletech.nbte.adminclient.contentwrappers.CWLogoUpload;
import com.fertiletech.nbte.adminclient.contentwrappers.EditorCoursesCW;
import com.fertiletech.nbte.adminclient.contentwrappers.EditorPaymentConfirmationCW;
import com.fertiletech.nbte.adminclient.contentwrappers.EditorRegistrationCW;
import com.fertiletech.nbte.adminclient.contentwrappers.EditorStudentsCW;
import com.fertiletech.nbte.adminclient.contentwrappers.ViewCoursesCW;
import com.fertiletech.nbte.adminclient.contentwrappers.ViewStudentsCW;
import com.fertiletech.nbte.adminclient.contentwrappers.WelcomePanelCW;
import com.fertiletech.nbte.adminclient.tickets.CWUnAttached;
import com.fertiletech.nbte.adminclient.tickets.CWViewTickets;
import com.fertiletech.nbte.adminclient.tickets.EditTicket;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.LoginRoles;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.prefetch.RunAsyncCode;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;

/**
 * The {@link TreeViewModel} used by the main menu.
 */
public class MainMenuTreeViewModel implements TreeViewModel {

  /**
   * The cell used to render categories.
   */
  private static class CategoryCell extends AbstractCell<Category> {
    @Override
    public void render(Context context, Category value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * The cell used to render examples.
   */
  private static class ContentWidgetCell extends AbstractCell<ContentWidget> {
    @Override
    public void render(Context context, ContentWidget value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendEscaped(value.getName());
      }
    }
  }

  /**
   * A top level category in the tree.
   */
  public class Category {

    private final ListDataProvider<ContentWidget> examples =
        new ListDataProvider<ContentWidget>();
    private final String name;
    private NodeInfo<ContentWidget> nodeInfo;
    private final List<RunAsyncCode> splitPoints =
        new ArrayList<RunAsyncCode>();

    public Category(String name) {
      this.name = name;
    }

    public void addExample(ContentWidget example, String style, RunAsyncCode splitPoint) {
    	if(style != null)
    		example.addStyleName(style);
      examples.getList().add(example);
      if (splitPoint != null) {
        splitPoints.add(splitPoint);
      }
      contentCategory.put(example, this);
      contentToken.put(Showcase.getContentWidgetToken(example), example);
    }

    public String getName() {
      return name;
    }

    /**
     * Get the node info for the examples under this category.
     * 
     * @return the node info
     */
    public NodeInfo<ContentWidget> getNodeInfo() {
      if (nodeInfo == null) {
        nodeInfo = new DefaultNodeInfo<ContentWidget>(examples,
            contentWidgetCell, selectionModel, null);
      }
      return nodeInfo;
    }

    /**
     * Get the list of split points to prefetch for this category.
     * 
     * @return the list of classes in this category
     */
    public Iterable<RunAsyncCode> getSplitPoints() {
      return splitPoints;
    }
  }

  /**
   * The top level categories.
   */
  private final ListDataProvider<Category> categories = new ListDataProvider<Category>();

  /**
   * A mapping of {@link ContentWidget}s to their associated categories.
   */
  private final Map<ContentWidget, Category> contentCategory = new HashMap<ContentWidget, Category>();

  /**
   * The cell used to render examples.
   */
  private final ContentWidgetCell contentWidgetCell = new ContentWidgetCell();

  /**
   * A mapping of history tokens to their associated {@link ContentWidget}.
   */
  private final Map<String, ContentWidget> contentToken = new HashMap<String, ContentWidget>();

  /**
   * The selection model used to select examples.
   */
  private final SelectionModel<ContentWidget> selectionModel;

  public MainMenuTreeViewModel(SelectionModel<ContentWidget> selectionModel) {
    this.selectionModel = selectionModel;
    initializeTree();
  }

  /**
   * Get the {@link Category} associated with a widget.
   * 
   * @param widget the {@link ContentWidget}
   * @return the associated {@link Category}
   */
  public Category getCategoryForContentWidget(ContentWidget widget) {
    return contentCategory.get(widget);
  }

  /**
   * Get the content widget associated with the specified history token.
   * 
   * @param token the history token
   * @return the associated {@link ContentWidget}
   */
  public ContentWidget getContentWidgetForToken(String token) {
    return contentToken.get(token);
  }

  public <T> NodeInfo<?> getNodeInfo(T value) {
    if (value == null) {
      // Return the top level categories.
      return new DefaultNodeInfo<Category>(categories, new CategoryCell());
    } else if (value instanceof Category) {
      // Return the examples within the category.
      Category category = (Category) value;
      return category.getNodeInfo();
    }
    return null;
  }

  public boolean isLeaf(Object value) {
    return value != null && !(value instanceof Category);
  }

  /**
   * Get the set of all {@link ContentWidget}s used in the model.
   * 
   * @return the {@link ContentWidget}s
   */
  Set<ContentWidget> getAllContentWidgets() {
    Set<ContentWidget> widgets = new HashSet<ContentWidget>();
    for (Category category : categories.getList()) {
      for (ContentWidget example : category.examples.getList()) {
        widgets.add(example);
      }
    }
    return widgets;
  }

  /**
   * Initialize the top level categories in the tree.
   */
  private void initializeTree() {
    List<Category> catList = categories.getList();
   
    //Welcome
    {
      Category category = new Category("Welcome");
      catList.add(category);
      // CwCheckBox is the default example, so don't prefetch it.
      category.addExample(new WelcomePanelCW(),"datermark", null);
    }

    //prospective
    {
    	//credit apps
    	Category category = new Category("Courses");
        category.addExample(new ViewCoursesCW(), "dwatermark",
                RunAsyncCode.runAsyncCode(ViewCoursesCW.class));
    	category.addExample(new BulkCoursesCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(BulkCoursesCW.class));
    	category.addExample(new BulkTimeTableCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(BulkTimeTableCW.class));              	
    	catList.add(category);
    	
    	//orders
    	category = new Category("Students");
        category.addExample(new ViewStudentsCW(), "dwatermark",
                RunAsyncCode.runAsyncCode(ViewStudentsCW.class));
    	category.addExample(new BulkStudentCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(BulkStudentCW.class));         	
    	catList.add(category);
    }
       
    //prospective
    {
    	Category category = new Category("Registration");
       	catList.add(category);
       	
       	//orders
       	category = new Category("Payments Confirmation");
    	catList.add(category);
        category.addExample(new CWRABlotter(), "dwatermark",
                RunAsyncCode.runAsyncCode(CWRABlotter.class));          
        category.addExample(new CWDepositStatements(), "dwatermark",
                RunAsyncCode.runAsyncCode(CWDepositStatements.class));
    	category.addExample(new CWAddTenantDeposit(), "dwatermark",
    			RunAsyncCode.runAsyncCode(CWAddTenantDeposit.class));    	    	
    	category.addExample(new BulkDepositCW(), "adminWatermark",
                RunAsyncCode.runAsyncCode(BulkDepositCW.class));       	
    }
    
    
    //Resident Accounts
    {
    	Category category = new Category("Payments Reports");
    	catList.add(category);	          	
        category.addExample(new CWViewDepositBlotter(), "dwatermark",
                RunAsyncCode.runAsyncCode(CWViewDepositBlotter.class));             
        category.addExample(new CWSearchInvoice(), "dwatermark", RunAsyncCode.runAsyncCode(CWSearchInvoice.class));
        category.addExample(new ViewBillBlotter(), "dwatermark",
                RunAsyncCode.runAsyncCode(ViewBillBlotter.class));      
        category.addExample(new PrintInvoices(), "dwatermark",
                RunAsyncCode.runAsyncCode(PrintInvoices.class));
    }
    
    BankUserCookie uc = BankUserCookie.getCookie();
    if(LoginRoles.ROLE_SCHOOL_ADMIN_EDIT.equals(uc.getRole()))
    {
	    //Accounts Config
	    {
	    	Category category = new Category("Accounts Configuration ");
	    	catList.add(category);	
	        category.addExample(new AddBillTemplate(), "dwatermark",
	                RunAsyncCode.runAsyncCode(AddBillTemplate.class));
	        category.addExample(new AddBillCW(), "dwatermark",
	                RunAsyncCode.runAsyncCode(AddBillCW.class));
	        category.addExample(new AddTenantPayment(), "dwatermark",
	                RunAsyncCode.runAsyncCode(AddTenantPayment.class));
	    	category.addExample(new CWResidentAccount(), "dwatermark",
	    			RunAsyncCode.runAsyncCode(CWResidentAccount.class));      	
	    	category.addExample(new CWEditResidentAccount(), "dwatermark",
	    			RunAsyncCode.runAsyncCode(CWEditResidentAccount.class));      	 	    	
		    category.addExample(new CWEditResidentAccountTypes(), "dwatermark",
		              RunAsyncCode.runAsyncCode(CWEditResidentAccountTypes.class));
		    category.addExample(new CWEditResidentBillTypes(), "dwatermark",
		              RunAsyncCode.runAsyncCode(CWEditResidentBillTypes.class));	    
		    category.addExample(new CWCompanyBankAccount(), "dwatermark",
		              RunAsyncCode.runAsyncCode(CWCompanyBankAccount.class));	    
	        category.addExample(new CWLogoUpload(), "dwatermark",
	                RunAsyncCode.runAsyncCode(CWLogoUpload.class));		    
	        category.addExample(new CWPrintDepositRequest(), "dwatermark", RunAsyncCode.runAsyncCode(CWPrintDepositRequest.class));
	    }
	    
	    //MRS
	    {
		    Category category = new Category("Requests - TBD");
		    catList.add(category);
		    category.addExample(new EditTicket(), "maintenanceWatermark",
		              RunAsyncCode.runAsyncCode(EditTicket.class));	    
		    category.addExample(new CWViewTickets(), "maintenanceWatermark",
		              RunAsyncCode.runAsyncCode(CWViewTickets.class));
		    category.addExample(new CWUnAttached(), "maintenanceWatermark",
		              RunAsyncCode.runAsyncCode(CWUnAttached.class));
	    }    
	    
	    //prospective
	    {
	    	Category category = new Category("Admin Roles");
	    	catList.add(category);
	        category.addExample(new EditorCoursesCW(), "adminWatermark",
	                RunAsyncCode.runAsyncCode(EditorCoursesCW.class));     
	        category.addExample(new EditorStudentsCW(), "adminWatermark",
	                RunAsyncCode.runAsyncCode(EditorStudentsCW.class));     
	        category.addExample(new EditorRegistrationCW(), "adminWatermark",
	                RunAsyncCode.runAsyncCode(EditorRegistrationCW.class));        
	        category.addExample(new EditorPaymentConfirmationCW(), "adminWatermark",
	                RunAsyncCode.runAsyncCode(EditorPaymentConfirmationCW.class));
	        category.addExample(new AdminCW(), "adminWatermark",
	                RunAsyncCode.runAsyncCode(AdminCW.class));        
	    }
    }        
  }
  
  public ContentWidget setBackgroundStyle(ContentWidget w, String styl)
  {
	  w.addStyleName(styl);
	  return w;
  }
}
