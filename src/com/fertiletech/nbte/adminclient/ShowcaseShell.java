/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * HMM ... some help notes
 * MainMenuTreeViewModel builds the content widgets, this is where they screens are instantiated (e.g. uibinder etc)
 * This is done by calling a class that extends ContentWidget
 * Async call is then made where widget created in onloadinitialize is passed in when async calls are made
 * Why this many levels of indirection ... well gwt code splitting seems to be the reason
 * Also of note is that to password protect etc these displays, hook you'll need will be inside showExample()
 * Don't think there is any harm in allowing help screens be viewed
 */
package com.fertiletech.nbte.adminclient;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.TreeViewModel;

/**
 * Application shell for Showcase sample.
 */
public class ShowcaseShell extends ResizeComposite {

  interface ShowcaseShellUiBinder extends UiBinder<Widget, ShowcaseShell> {
  }

  /**
   * The text color of the selected tab.
   */
  private static final String SELECTED_TAB_COLOR = "#333333";

  /**
   * The unique ID assigned to the next callback.
   */
  private static int nextCallbackId = 0;

  private static ShowcaseShellUiBinder uiBinder = GWT.create(
      ShowcaseShellUiBinder.class);

  /**
   * The panel that holds the content.
   */
  @UiField
  SimpleLayoutPanel contentPanel;

  /**
   * The main menu used to navigate to examples.
   */
  @UiField(provided = true)
  CellTree mainMenu;

  /**
   * The button used to show the example.
   */
  @UiField
  Anchor tabPanelDisplay;

  /**
   * The button used to show the source code.
   */
  @UiField
  Anchor tabHelpDisplay;

  /**
   * The current {@link ContentWidget} being displayed.
   */
  private ContentWidget content;

  /**
   * The widget that holds help display.
   */
  //private SimpleLayoutPanel contentHelp = new SimpleLayoutPanel();

  /**
   * The html used to show a loading icon.
   */
  private final Image loadingImage;
  private final FlowPanel loadingImageContainer; 

  /**
   * Construct the {@link ShowcaseShell}.
   *
   * @param treeModel the treeModel that backs the main menu
   */
  public ShowcaseShell(TreeViewModel treeModel) {
    AbstractImagePrototype proto = AbstractImagePrototype.create(
        Showcase.images.loading());
    loadingImage = proto.createImage();
    loadingImage.addStyleName("loadingSpacer");
    loadingImageContainer = new FlowPanel();
    HTML label = new HTML();
    label.setWidth("500px");
    label.setHTML("Loading ...");
    loadingImageContainer.add(loadingImage);
    loadingImageContainer.add(label);
    loadingImageContainer.addStyleName("loadingSpacerContainer");

    // Create the cell tree.
    mainMenu = new CellTree(treeModel, null);
    mainMenu.setAnimationEnabled(true);
    mainMenu.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
    mainMenu.ensureDebugId("mainMenu");

    // Initialize the ui binder.
    initWidget(uiBinder.createAndBindUi(this));
    /*contentHelp.getElement().getStyle().setBackgroundColor("#eee");
    contentHelp.getElement().getStyle().setMargin(10.0, Unit.PX);
    contentHelp.getElement().getStyle().setProperty(
        "border", "1px solid #c3c3c3");
    contentHelp.getElement().getStyle().setProperty("padding", "10px 2px");*/

    // Handle events from the tabs.
    tabPanelDisplay.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        showPanel();
      }
    });
    tabHelpDisplay.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
    	  showHelp();
      }
    });
    // Default to no content.
    contentPanel.ensureDebugId("contentPanel");
    setContent(null);
  }

  public void showLoading()
  {
	  contentPanel.setWidget(loadingImageContainer);
  }
  
  static Widget denialScreen = null;
  public void showAdminScreen()
  {
	  if(denialScreen == null)
		  denialScreen = new AccessDeniedDisplay();
	  contentPanel.setWidget(denialScreen);
  }
  
  /**
   * Returns the currently displayed content. (Used by tests.)
   */
  public ContentWidget getContent() {
    return content;
  }

  /**
   * Get the main menu used to select examples.
   *
   * @return the main menu
   */
  public CellTree getMainMenu() {
    return mainMenu;
  }

  /**
   * Set the content to display.
   *
   * @param content the content
   */
  public void setContent(final ContentWidget content) {
    this.content = content;
    if (content == null) {
      tabPanelDisplay.setVisible(false);
      tabHelpDisplay.setVisible(false);
      contentPanel.setWidget(null);
      return;
    }

    // Setup the options bar.
    tabPanelDisplay.setVisible(true);
    tabHelpDisplay.setVisible(true);

    // Show the widget.
    showPanel();
  }

  /**
   * Show a example.
   */
  private void showPanel() {
    if (content == null) {
      return;
    }

    // Set the highlighted tab.
    tabPanelDisplay.getElement().getStyle().setColor(SELECTED_TAB_COLOR);
    tabHelpDisplay.getElement().getStyle().clearColor();

    contentPanel.setWidget(content);
  }
  
  /**
   * Show the source CSS style.
   */
  private void showHelp() {
    if (content == null) {
      return;
    }

    // Set the highlighted tab.
    tabPanelDisplay.getElement().getStyle().clearColor();
    tabHelpDisplay.getElement().getStyle().setColor(SELECTED_TAB_COLOR);

   // contentHelp.setWidget(content.getHelpPage());
   // contentPanel.setWidget(new ScrollPanel(contentHelp));
    contentPanel.setWidget(content.getHelpPage());

  }
}  

