package com.fertiletech.nbte.adminclient.tickets;

import com.fertiletech.nbte.adminclient.CWArguments;
import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.client.GUIConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.Widget;

public class EditTicket extends ContentWidget implements ClickHandler, CWArguments{

	@UiField
	LongBox ticketNumber;
	@UiField
	SupportRequest requestPanel;
	
	@UiField
	Button newTicket;
	
	@UiField
	Button lookup;
	
	String savedId = null;
	
	private static EditTicketUiBinder uiBinder = GWT
			.create(EditTicketUiBinder.class);

	interface EditTicketUiBinder extends UiBinder<Widget, EditTicket> {
	}

	public EditTicket() {
		super("Edit Ticket", "Use this module to create or edit maintanance requests. Comments tab maintains an audit trail of all edits made to the ticket");
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	public Widget onInitialize() {
		Widget w = uiBinder.createAndBindUi(this);
		if(savedId != null)
			requestPanel.lookupTicket(savedId);
		lookup.setEnabled(false);
		ticketNumber.addKeyPressHandler(new KeyPressHandler() {
			
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(ticketNumber.getValue() != null)
					lookup.setEnabled(true);
				else
					lookup.setEnabled(false);
			}
		});
		lookup.addClickHandler(this);
		newTicket.addClickHandler(this);
		return w;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(EditTicket.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	@Override
	public void onClick(ClickEvent event) {
		if(event.getSource() == lookup)
		{
			if(ticketNumber.getValue() != null)
				requestPanel.lookupTicket(ticketNumber.getValue());
		}
		else
		{
			ticketNumber.setValue(null);
			requestPanel.clear();
		}
	}


	@Override
	public void setParameterValues(String args) {
		if(requestPanel != null)
		{
			if(args.startsWith(GUIConstants.TICKET_ID_PREFIX))
			{
				String numStr = args.substring(GUIConstants.TICKET_ID_PREFIX.length());
				try
				{
					int ticketID = Integer.valueOf(numStr);
					requestPanel.lookupTicket(ticketID);
				}
				catch(NumberFormatException ex)
				{
					Window.alert("Specify a valid number for ticket request. Received: " + numStr);
					return;
				}
			}
			else
				requestPanel.lookupTicket(args);
		}
		else
			savedId = args;
	}

}
