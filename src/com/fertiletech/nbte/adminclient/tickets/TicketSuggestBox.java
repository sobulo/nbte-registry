package com.fertiletech.nbte.adminclient.tickets;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.Showcase;
import com.fertiletech.nbte.adminclient.utils.UserSuggestBox;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Hyperlink;

public class TicketSuggestBox extends UserSuggestBox{
	
	public TicketSuggestBox()
	{
		super(new Hyperlink("Ticket", History.getToken()));
		final Hyperlink ticketLabel = (Hyperlink) getDisplayWidget();
		ticketLabel.setTitle("Enter {Ticket ID}. Ensure to start with { and end with }. Link clickable once ticket selected");
		this.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				ticketLabel.setTargetHistoryToken(getTokenForSelected());
			}
		});
	}

	@Override
	protected void populateSuggestBox() 
	{
		PanelUtilities.TICKET_SERVICE.getAllTickets(getPopulateCallBack());
	}

	@Override
	public String getRole() 
	{
		return "mrs-ticket";
	}
	
	public String getTokenForSelected()
	{
		if(getSelectedUser() == null) return History.getToken();
		return Showcase.getContentWidgetToken(EditTicket.class) + "/" + getSelectedUser();
	}

}
