package com.fertiletech.nbte.adminclient.tickets;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWViewTickets extends ContentWidget{
	
	public CWViewTickets() {
		super("View Tickets", "Use this module to view tickets for the maintenance request system");
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}	

	@Override
	public Widget onInitialize() {
		return new ViewTickets();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWViewTickets.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });	
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}
