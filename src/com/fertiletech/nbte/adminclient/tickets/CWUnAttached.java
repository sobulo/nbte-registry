package com.fertiletech.nbte.adminclient.tickets;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWUnAttached extends ContentWidget{

	public CWUnAttached() {
		super("Unattached", "Use this module to view student bills/invoices not associated with a request ticket. " +
				"You may also click on any displayed row to attach it to a ticket");
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}		
	
	@Override
	public Widget onInitialize() {
		return new UnAttached();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWUnAttached.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL  ;
	}

}
