package com.fertiletech.nbte.adminclient.tickets;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class ViewTickets extends Composite {

	@UiField
	ShowcaseTable display;
	
	@UiField
	DateBox startDate;
	
	@UiField
	DateBox endDate;
	
	@UiField
	Button search;

	SimpleDialog infoBox;
	
	protected AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("<b>Error fetching ticket list as table<br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
		}
	};
	
	private static ViewTicketsUiBinder uiBinder = GWT
			.create(ViewTicketsUiBinder.class);

	interface ViewTicketsUiBinder extends UiBinder<Widget, ViewTickets> {
	}

	public ViewTickets() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				PanelUtilities.TICKET_SERVICE.getTickets(startDate.getValue(), endDate.getValue(), callback);	
			}
		});
	}

}
