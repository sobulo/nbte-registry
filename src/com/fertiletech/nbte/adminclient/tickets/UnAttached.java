package com.fertiletech.nbte.adminclient.tickets;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.MessageListToGrid;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class UnAttached extends Composite {
	@UiField
	RadioButton invoiceSelected;
	
	@UiField
	RadioButton inventorySelected;
	
	@UiField
	Button search;
	
	@UiField
	SimpleLayoutPanel displaySlot;

	SimpleDialog infoBox;
	
	ShowcaseTable displayInvoice, displayInventory;
	

	protected AsyncCallback<List<TableMessage>> invoiceCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Unable to fetch invoices: <br/>" + caught.getMessage());
			search.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(!displayInvoice.isInitialized())
				displayInvoice.setEmptyWidget(new HTML("No orphaned inventory transfers/allocations found"));			
			displaySlot.clear();
			displayInvoice.showTable(result);
			displaySlot.add(displayInvoice);
			search.setEnabled(true);
		}
	};
	
	private AsyncCallback<List<TableMessage>> inventoryCallBack = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Unable to fetch inventory: <br/>" + caught.getMessage());
			search.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(!displayInventory.isInitialized())
				displayInventory.setEmptyWidget(new HTML("No orphaned inventory allocations/transfers found"));
			displaySlot.clear();
			displayInventory.showTable(result);
			displaySlot.add(displayInventory);
			search.setEnabled(true);
		}
	};
	
	private static UnAttachedUiBinder uiBinder = GWT
			.create(UnAttachedUiBinder.class);

	interface UnAttachedUiBinder extends UiBinder<Widget, UnAttached> {
	}

	public UnAttached() {
		initWidget(uiBinder.createAndBindUi(this));
		invoiceSelected.setName("mrs-selection");
		inventorySelected.setName("mrs-selection");
		ValueChangeHandler<Boolean> mrsSelectionHandler = new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
					displaySlot.clear();
					if(invoiceSelected.getValue())
						displaySlot.add(displayInvoice);
					else if(inventorySelected.getValue())
						displaySlot.add(displayInventory);
			}
		};
		invoiceSelected.addValueChangeHandler(mrsSelectionHandler);
		inventorySelected.addValueChangeHandler(mrsSelectionHandler);
		infoBox = new SimpleDialog("INFO");
		displayInvoice = new ShowcaseTable();
		displayInvoice.setEmptyWidget(new HTML("Use search button to refresh display"));
		displayInventory = new ShowcaseTable();
		displayInventory.setEmptyWidget(new HTML("Use search button to refresh display"));		
		
		search.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(inventorySelected.getValue())
				{
					search.setEnabled(false);
					displayInventory.clear();
					PanelUtilities.TICKET_SERVICE.getAllocations(null, inventoryCallBack);
				}
				else if(invoiceSelected.getValue())
				{
					search.setEnabled(false);
					displayInvoice.clear();
					PanelUtilities.TICKET_SERVICE.getInvoices(null, invoiceCallback);
				}
				else
					infoBox.show("Select inventory or invoice search option");
			}
		});

		
		//setup popup panel
		final PopupPanel popPanel = new PopupPanel();
		popPanel.setWidth("300px");
		popPanel.setAutoHideEnabled(true);
		popPanel.setAnimationEnabled(true);
		
		//setup ticket assignment popup content
		final TicketSuggestBox ticketBox = new TicketSuggestBox();
		final LongBox id = new LongBox();
		id.setEnabled(false);
		final Button assignToTicket = new Button("Assign Ticket");
		final FlowPanel container = new FlowPanel();
		container.add(id);
		container.add(ticketBox);
		container.add(assignToTicket);
		final MessageListToGrid grid = new MessageListToGrid();

		
		//setup grid display popup content
		final HTML headerDisplay = new HTML();
		final FlowPanel gridContainer = new FlowPanel();
		gridContainer.add(headerDisplay);
		gridContainer.add(grid);
		

		final AsyncCallback<List<TableMessage>> ticketAddcallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				search.setEnabled(true);
				enableRadioButtons(true);
				infoBox.show("Ticket assignment failed:<br/>" + caught.getMessage());	
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				search.setEnabled(true);
				//attempt to clear
				ShowcaseTable disp = displayInventory;
				if(invoiceSelected.getValue())
					disp = displayInvoice;
				for(TableMessage m : result)
				{
					if(disp.getDataList().remove(m))
						break;
				}
				enableRadioButtons(true);
				grid.populateTable(result);
				popPanel.clear();
				popPanel.add(new ScrollPanel(gridContainer));
				popPanel.center();
			}
		};		
		
		assignToTicket.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				String ticketID = ticketBox.getSelectedUser();
				popPanel.hide();
				if(ticketID == null)
				{
					infoBox.show("Select a valid ticket from suggest box. Type in ticket title or use format {123} to specify ticket id");
					return;
				}
				Long idVal = null;
				if(displaySlot.equals(displayInventory.getParent()))
				{
					String idStr = id.getName();
					search.setEnabled(false);
					enableRadioButtons(false);
					headerDisplay.setHTML("<div style='background-color:black; color: white;margin-bottom:5px; width:100%'> Inventory transfer attachments for ticket: " + ticketBox.getSelectedUserDisplay() + "</div><br/><hr/>");
					PanelUtilities.TICKET_SERVICE.addAllocationID(ticketID, idStr, true, ticketAddcallback);
				}
				else if(displaySlot.equals(displayInvoice.getParent()))
				{
					idVal = id.getValue();
					search.setEnabled(false);
					enableRadioButtons(false);
					headerDisplay.setHTML("<div style='background-color:black; color: white;margin-bottom:5px; width:100%'> Invoice attachments for ticket: " + ticketBox.getSelectedUserDisplay() + "</div><br/><hr/>");
					PanelUtilities.TICKET_SERVICE.addInvoiceID(ticketID, idVal, true, ticketAddcallback);
				}
				else
				{
					infoBox.show("Invalid parameters, please contact IT");
				}
			}
		});
		
		displayInventory.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				TableMessage m = event.getValue();
				if(m == null || m.getNumber(DTOConstants.ALC_ID_IDX) == null) return;
				long allocID = Math.round(m.getNumber(DTOConstants.ALC_ID_IDX));
				id.setValue(allocID);
				id.setName(m.getMessageId());
				ticketBox.clear();
				popPanel.clear();
				popPanel.add(container);
				popPanel.center();
			}
		});
		
		displayInvoice.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				TableMessage m = event.getValue();
				if(m == null || m.getNumber(2) == null) return;
				long billID = Math.round(m.getNumber(2));
				id.setValue(billID);
				ticketBox.clear();
				popPanel.clear();
				popPanel.add(container);
				popPanel.center();
			}
		});
	}
	
	public void enableRadioButtons(boolean enabled)
	{
		inventorySelected.setEnabled(false); //permanently disable, inventory not currently supported
		invoiceSelected.setEnabled(enabled);
	}
	

}
