/**
 * 
 */
package com.fertiletech.nbte.adminclient.tickets;


import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.utils.CommentsPanel;
import com.fertiletech.nbte.adminclient.utils.CustomRichTextArea;
import com.fertiletech.nbte.adminclient.utils.StudentSuggestBox;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.event.logical.shared.BeforeSelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SupportRequest extends Composite{
	
	@UiField
	ListBox priority;
	
	@UiField
	ListBox status;
	
	@UiField
	StudentSuggestBox studentBox;
	
	@UiField
	ListBox assignTo;
			
	@UiField
	Button save;
		
	@UiField
	Label jobnum;
	
	@UiField
	CustomRichTextArea description;
	
	@UiField
	TextBox title;
	
	@UiField
	DateBox requestDate;
	
	@UiField
	ListBox type;
	
	@UiField
	TabLayoutPanel ticketContainer;
	
	@UiField
	CommentsPanel generalComments;
	
	@UiField
	TicketAttachments attachments;

	TableMessage lastSavedTicket;
	
	SimpleDialog infoBox;
	boolean commentsLoaded, attachmentsLoaded;
	public final static int COMMENT_TAB_IDX = 1;
	public final static int ATTACH_TAB_IDX = 2;
	public final static int ATTACH_INV_TAB_IDX = 3;
	final YesNoDialog confirmBox;	
	private AsyncCallback<TableMessage> saveCallBack = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error occured<br/>" + caught.getMessage());
			enablePanel(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			lastSavedTicket = getValues(result);
			long ticketNum = Math.round(result.getNumber(DTOConstants.MRS_TICKET_IDX));
			final String display = "Saved request ticket: " + ticketNum;
			jobnum.setText(String.valueOf(ticketNum));
			generalComments.setCommentID(result.getMessageId());
			if(isTicketCompletion())
			{
				AsyncCallback<Void> commentPersistedCallback = new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						infoBox.show(display + "<br/> However failed to create an audit trail. You may" +
								" ignore this message or enter a comment manually indicating you've completed the ticket. Audit trail error was: +<br/>" + caught.getMessage());
					}

					@Override
					public void onSuccess(Void result) {
						infoBox.show(display + "<br/> Also flagged ticket as completed.");
						
					}
				};
				PanelUtilities.READ_SERVICE.saveActivityComment(lastSavedTicket.getMessageId(), 
						lastServerMessage, false, commentPersistedCallback );
			}
			else
				infoBox.show(display);
			enablePanel(true);
		}
	};
	
	
	private static SupportRequestUiBinder uiBinder = GWT
			.create(SupportRequestUiBinder.class);

	interface SupportRequestUiBinder extends UiBinder<Widget, SupportRequest> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SupportRequest() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		for(String i : DTOConstants.MRS_ISSUES)
			type.addItem(i);
		for(String p : DTOConstants.MRS_PRIORITIES)
			priority.addItem(p);
		for(String s : DTOConstants.MRS_STATUSES)
			status.addItem(s);
		confirmBox = new YesNoDialog("Confirm Edit Details", "Continue Save", "Cancel", "           ");
		
		
		save.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if(!validateTicket()) return;
				TableMessage m = getValues();
				
				if(lastSavedTicket == null)
				{
					if(isTicketCompletion())
					{
						infoBox.show("Illegal operation. You must first create a ticket before you can flag it as " + DTOConstants.MRS_STATUS_COMPLETE);
						return;
					}
					GWT.log("about to create ticket");
					enablePanel(false);
					PanelUtilities.TICKET_SERVICE.createTicket(m, saveCallBack);
				}
				else
				{
					GWT.log("About to call getDiffs");
					final TableMessage[] details = getTicketDiffs(m);					
					GWT.log("Got the diffs Diffs");
					confirmBox.setClickHandler(new ClickHandler() {						
						@Override
						public void onClick(ClickEvent event) {
							GWT.log("About to make remote function call");
							enablePanel(false);
							PanelUtilities.TICKET_SERVICE.editTicket(details, saveCallBack);
							confirmBox.hide();
						}
					});
					
					if(isTicketCompletion())
						validateTicketCompletion();
					else
						confirmBox.show("Do you want to save the changes you made to this maintenance request ticket?");
				}
			}
		});
		ticketContainer
		.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
			@Override
			public void onBeforeSelection(
					BeforeSelectionEvent<Integer> event) 
			{
				if (event.getItem() > 0 && lastSavedTicket == null) {
					infoBox.show("This is a new ticket. You must save it before you can access other tabs");
					event.cancel();
					return;
				}				
				else if(event.getItem() == COMMENT_TAB_IDX && !commentsLoaded)
				{
					generalComments.setCommentID(lastSavedTicket.getMessageId());
					commentsLoaded = true;
				}
				else if(event.getItem() == ATTACH_TAB_IDX && !attachmentsLoaded)
				{
					attachments.loadAttachments(lastSavedTicket.getMessageId());
					attachmentsLoaded = true;
				}
			}
		});
		AsyncCallback<List<TableMessage>> staffcallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				infoBox.show("Unable to populate staff dropdown. Error is: <br/>" + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				result.remove(0);
				GWT.log("Assign to: " + result.size());
				for(TableMessage m : result)
					assignTo.addItem(m.getText(1), m.getText(0).toLowerCase());
			}
		};
		PanelUtilities.READ_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_EMPLOYEE_LIST, staffcallback);
		
		ticketContainer.setAnimationDuration(600);
		ticketContainer.setAnimationVertical(true);
	}
	
	private TableMessage[] getTicketDiffs(TableMessage m)
	{
		TableMessage[] details = null;
		try
		{
		GWT.log("deep message compare happening");
		details = TableMessageHeader.deepTableMessageCompare(lastSavedTicket, m, DTOConstants.MRS_TEXT_FIELD_DESC, 
				DTOConstants.MRS_NUM_FIELD_DESC, DTOConstants.MRS_DATE_FIELD_DESC);
		GWT.log("updating message id");
		for(int i = 0; i < details.length; i++)
			details[i].setMessageId(lastSavedTicket.getMessageId());
		}
		catch(RuntimeException ex)
		{
			GWT.log("Exception occured: " + ex.toString());
			throw(ex);
		}
		GWT.log("returning diffs to caller");
		
		
		return details;
	}
	
	AsyncCallback<TableMessage> fetchTixCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error fetching ticket:<br/>" + caught.getMessage());
			enablePanel(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			if(result == null)
			{
				infoBox.show("<b>Unable to find ticket number.</b><br/>Confirm you have the right ticket number, alternatively you may fill in details and" +
						" then click save to create a NEW ticket");
			}
			else
			{
				lastSavedTicket = getValues(result);
				setValues(result);
			}
			enablePanel(true);
		}
	};
	
	public void lookupTicket(final long ticketID)
	{
		clear();
		enablePanel(false);
		PanelUtilities.TICKET_SERVICE.getTicket(ticketID, fetchTixCallback);
	}
	
	public void lookupTicket(String ticketID)
	{
		clear();
		enablePanel(false);
		PanelUtilities.TICKET_SERVICE.getTicket(ticketID, fetchTixCallback);
	}	

	private void enablePanel(boolean enabled)
	{
		priority.setEnabled(enabled);
		status.setEnabled(enabled);
		studentBox.setEnabled(enabled);
		assignTo.setEnabled(enabled);
		save.setEnabled(enabled);
		description.enable(enabled);
		title.setEnabled(enabled);
		requestDate.setEnabled(enabled);
		type.setEnabled(enabled);
	}

	public void clear()
	{
		lastSavedTicket = null;
		commentsLoaded = false;
		attachmentsLoaded = false;
		priority.setSelectedIndex(0);
		status.setSelectedIndex(0);
		assignTo.setSelectedIndex(0);
		description.setValue("");
		title.setValue(null);
		requestDate.setValue(null);
		type.setSelectedIndex(0);
		//tenant.setText("");
		jobnum.setText("");
	}
	
	private void setValues(TableMessage m)
	{
		setListBoxValue(priority, m.getText(DTOConstants.MRS_PRIORITY_IDX));
		setListBoxValue(status, m.getText(DTOConstants.MRS_STATUS_IDX));
		for(int i = 0; i < assignTo.getItemCount(); i++)
			if(assignTo.getValue(i).equalsIgnoreCase(m.getText(DTOConstants.MRS_STAFF_IDX)))
			{
				assignTo.setSelectedIndex(i);
				break;
			}
		description.setValue(m.getText(DTOConstants.MRS_DETAILS_IDX));
		title.setValue(m.getText(DTOConstants.MRS_TITLE_IDX));
		requestDate.setValue(m.getDate(DTOConstants.MRS_REQUEST_IDX));
		setListBoxValue(type, m.getText(DTOConstants.MRS_ISSUE_IDX));
		jobnum.setText(String.valueOf(Math.round(m.getNumber(DTOConstants.MRS_TICKET_IDX))));
	}
	
	private TableMessage getValues()
	{
		TableMessage m = new TableMessage(8, 0, 1);
		m.setText(DTOConstants.MRS_PRIORITY_IDX, priority.getValue(priority.getSelectedIndex()));
		m.setText(DTOConstants.MRS_STATUS_IDX, status.getValue(status.getSelectedIndex()));
		m.setText(DTOConstants.MRS_STAFF_IDX, assignTo.getValue(assignTo.getSelectedIndex()));
		m.setText(DTOConstants.MRS_DETAILS_IDX, description.getValue());
		m.setText(DTOConstants.MRS_TITLE_IDX, title.getValue());
		m.setDate(DTOConstants.MRS_REQUEST_IDX, requestDate.getValue());
		m.setText(DTOConstants.MRS_ISSUE_IDX, type.getValue(type.getSelectedIndex()));
		return m;
	}
	
	private boolean validateTicket()
	{
		StringBuilder errors = new StringBuilder();
		if(title.getValue().trim().length() == 0)
			errors.append("<li>You must specify a ticket title</li>");
		if(errors.length() > 0)
		{
			infoBox.show("Please fix issues below:<br/><ul>" + errors.toString() + "</ul>");
			return false;
		}
		return true;
	}
	
	private boolean isTicketCompletion()
	{
		return status.getValue(status.getSelectedIndex()).equalsIgnoreCase(DTOConstants.MRS_STATUS_COMPLETE);
	}
	
	//TODO, move this to server side, i.e. pass a flag into save indicating failure?
	String lastServerMessage = null;
	private void validateTicketCompletion()
	{		
		AsyncCallback<List<TableMessage>> invoiceCallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				infoBox.show("Unable to complete invoice attachment check. " +
						"Please try refreshing your browser.<br/>" + caught.getMessage());
			}

			@Override
			public void onSuccess(final List<TableMessage> invoiceResult) {

						StringBuilder attachmentsBypassed = new StringBuilder();
						StringBuilder totals = new StringBuilder();

						if(invoiceResult.size() == 1) //only got header
							attachmentsBypassed.append("<p>No invoice attached</p>");
						else
							totals.append("<p>No. of attached Invoices: " + invoiceResult.size()).append("</p>");
						
					
						
						String style = "font-weight: bold; color: red; text-decoration: underline;";
						String addtionalServerStyle = "font-size: large";
						String styleBegin = "<div style='";
						String styleFinish = "'>";
						String serverComment = styleBegin + style + addtionalServerStyle + styleFinish;
						String closeTitle = "<b style='color:green'>Ticket Closed</b><br/>";
						String clientComment = styleBegin + style + styleFinish;
						String totalMsg = totals.toString();
						if(attachmentsBypassed.length() > 0)
						{
							String msg = attachmentsBypassed.toString() + "</div>" + totalMsg;
							lastServerMessage = closeTitle + serverComment + msg;
							confirmBox.show(clientComment + msg + "<br/> Proceed?");					
						}
						else
						{
							lastServerMessage = closeTitle + totalMsg;
							confirmBox.show(totalMsg + "<br/> Proceed?");
						}
					}
		};
		attachments.loadAttachments(lastSavedTicket.getMessageId(), invoiceCallback);
	}
	
	private TableMessage getValues(TableMessage rawData)
	{
		TableMessage m = new TableMessage(8, 0, 1);
		m.setMessageId(rawData.getMessageId());
		m.setText(DTOConstants.MRS_PRIORITY_IDX, rawData.getText(DTOConstants.MRS_PRIORITY_IDX));
		m.setText(DTOConstants.MRS_STATUS_IDX, rawData.getText(DTOConstants.MRS_STATUS_IDX));
		m.setText(DTOConstants.MRS_SITE_IDX, rawData.getText(DTOConstants.MRS_SITE_IDX));
		m.setText(DTOConstants.MRS_SITE_NAME_IDX, rawData.getText(DTOConstants.MRS_SITE_NAME_IDX));
		m.setText(DTOConstants.MRS_STAFF_IDX, rawData.getText(DTOConstants.MRS_STAFF_IDX));
		m.setText(DTOConstants.MRS_DETAILS_IDX, rawData.getText(DTOConstants.MRS_DETAILS_IDX));
		m.setText(DTOConstants.MRS_TITLE_IDX, rawData.getText(DTOConstants.MRS_TITLE_IDX));
		m.setDate(DTOConstants.MRS_REQUEST_IDX, rawData.getDate(DTOConstants.MRS_REQUEST_IDX));
		m.setText(DTOConstants.MRS_ISSUE_IDX, rawData.getText(DTOConstants.MRS_ISSUE_IDX));
		return m;	
	}
	
	private void setListBoxValue(ListBox lb, String val)
	{
		if(val == null)
			return;
		
		for(int i=0; i<lb.getItemCount(); i++)
			if(lb.getValue(i).equals(val))
			{
				lb.setSelectedIndex(i);
				break;
			}
	}	
}
