/**
 * 
 */
package com.fertiletech.nbte.adminclient;

import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.AccountManager;
import com.fertiletech.nbte.client.AccountManagerAsync;
import com.fertiletech.nbte.client.MessagingManager;
import com.fertiletech.nbte.client.MessagingManagerAsync;
import com.fertiletech.nbte.client.RegistryService;
import com.fertiletech.nbte.client.RegistryServiceAsync;
import com.fertiletech.nbte.client.TicketService;
import com.fertiletech.nbte.client.TicketServiceAsync;
import com.fertiletech.nbte.client.UtilsService;
import com.fertiletech.nbte.client.UtilsServiceAsync;
import com.google.gwt.core.client.GWT;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

    public final static UtilsServiceAsync READ_SERVICE = GWT.create(UtilsService.class);
    public final static RegistryServiceAsync REGISTRATION_SERVICE = GWT.create(RegistryService.class);
    public final static MessagingManagerAsync MESSAGING_SERVICE = GWT.create(MessagingManager.class);
    public final static TicketServiceAsync TICKET_SERVICE = GWT.create(TicketService.class);
    public final static AccountManagerAsync ACCT_SERVICE = GWT.create(AccountManager.class);

}
