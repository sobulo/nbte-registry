package com.fertiletech.nbte.adminclient;

import com.fertiletech.nbte.adminclient.accounts.AddBillCW;
import com.fertiletech.nbte.adminclient.accounts.AddTenantDeposit;
import com.fertiletech.nbte.adminclient.contentviews.ViewCourseTemplPanel;
import com.fertiletech.nbte.adminclient.contentviews.ViewStudentsPanel;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class StaffPortal implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		History.addValueChangeHandler(new ValueChangeHandler<String>() {
			final Widget coursePanel = new AddBillCW().onInitialize();
			final Widget studPanel = new AddTenantDeposit();
			final ViewCourseTemplPanel courseView = new ViewCourseTemplPanel();
			final ViewStudentsPanel studentsView = new ViewStudentsPanel();
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				RootLayoutPanel.get().clear();
				if(event.getValue().equals("courses"))
					RootLayoutPanel.get().add(coursePanel);
				else if(event.getValue().equals("students"))
					RootLayoutPanel.get().add(studPanel);
				else if(event.getValue().equals("vs"))
					RootLayoutPanel.get().add(studentsView);
				else if(event.getValue().equals("vc"))
					RootLayoutPanel.get().add(courseView);				
				else
					RootLayoutPanel.get().add(new HTML("You miss road?"));
				Window.alert("Hmm...");
			}
		});
		RootLayoutPanel.get().add(new HTML("<h1>Mo fe Tako fuun eye towa lori twitter ...</h1><br/><marquee>Story for the Gods niyen o," +
				" to dethrone the social media beast known as twitter no be yam</marquee>"));
	}
}
