package com.fertiletech.nbte.adminclient.tables;


import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.view.client.ProvidesKey;

public class TableMessageKeyProvider implements ProvidesKey<TableMessage>{

    /**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<TableMessage> KEY_PROVIDER = new TableMessageKeyProvider();

	@Override
	public Object getKey(TableMessage item) {
		return item == null ? null : item.getMessageId();
	}
}
