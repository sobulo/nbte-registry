package com.fertiletech.nbte.adminclient.tables;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;

public class FilteredListDataProvider<T> extends ListDataProvider<T> {

	private boolean filterEnabled;

	public final IFilter<T> filter;

	public FilteredListDataProvider(IFilter<T> filter) {
		GWT.log("Filter initialized as " + filter);
		this.filter = filter;
	}

	public boolean isFilterEnabled() {
		return filterEnabled;
	}

	public void enableFilter(boolean enabled) {
		this.filterEnabled = enabled;
		refresh();
	}

	@Override
	protected void updateRowData(HasData<T> display, int start, List<T> values) {
		//GWT.log("F call: " + filterEnabled + " filter: " + (filter==null));
		if (!filterEnabled || filter == null) { // we don't need to filter, so
												// call base class
			super.updateRowData(display, start, values);
			//display.setRowCount(values.size());
			GWT.log("F call 2 + size: " + values.size());
		} else {
			GWT.log("F call 3");
			int end = start + values.size();
			List<T> resulted = new ArrayList<T>(values.size());
			for (int i = start; i < end; i++) {
				GWT.log("Cheecking filter");
				if (filter.isValid((T) values.get(i))) 
				{
					GWT.log("@#@#@#@filter entered");
					resulted.add((T) values.get(i));
				}
				GWT.log("F call 4");
			}
			display.setRowData(start, resulted);
			display.setRowCount(resulted.size());
			GWT.log("F: " + resulted.size() + " N: " + values.size());
		}
	}
}

interface IFilter<T> {
	boolean isValid(T value);
}