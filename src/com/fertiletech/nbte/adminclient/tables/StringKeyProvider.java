package com.fertiletech.nbte.adminclient.tables;


import com.google.gwt.view.client.ProvidesKey;

public class StringKeyProvider implements ProvidesKey<String>{

    /**
     * The key provider that provides the unique ID of a contact.
     */
    public static final ProvidesKey<String> KEY_PROVIDER = new StringKeyProvider();

	@Override
	public Object getKey(String item) {
		return item;
	}
}
