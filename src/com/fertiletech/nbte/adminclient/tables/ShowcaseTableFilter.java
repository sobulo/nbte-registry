package com.fertiletech.nbte.adminclient.tables;

import java.util.Date;

import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DoubleBox;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

public class ShowcaseTableFilter extends Composite implements IFilter<TableMessage>{
	
	@UiField
	CheckBox enableFilter;
	@UiField
	Button doFilter;
	@UiField
	SimplePanel filterBoxSlot;
	@UiField
	ListBox colName;
	@UiField
	ListBox operation;
	
	DoubleBox numberFilterBox;
	TextBox textFilterBox;
	DateBox dateFilterBox;
	
	FilteredListDataProvider<TableMessage> dataProvider;
	TableMessageHeader header;
	int[] colMappings;

	private final static Date BASELINE_DATE = DateTimeFormat.getFormat("yyyyMMdd").parse("19790418");
	private static ShowcaseTableFilterUiBinder uiBinder = GWT
			.create(ShowcaseTableFilterUiBinder.class);

	interface ShowcaseTableFilterUiBinder extends
			UiBinder<Widget, ShowcaseTableFilter> {
	}
	
	public final static String EQUAL = " = ";
	public final static String GREATER = " > ";
	public final static String LESS = " < ";
	public final static String AT = " @ ";
	public final static String NOT_EQ = " \u2260 ";
	final static String[] OPS = {EQUAL, GREATER, LESS, NOT_EQ, AT};
	public ShowcaseTableFilter() {
		initWidget(uiBinder.createAndBindUi(this));
		enableFilter.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if(event.getValue() == null)
					return;
				enableFilterComponents(event.getValue());
			}
		});
		doFilter.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				GWT.log("Event called to change filter");
				if(dataProvider == null) return;
				dataProvider.enableFilter(true);
				GWT.log("filter applied");
			}
		});
		enableFilterComponents(false);	
	}
	
	public void init(TableMessageHeader h, FilteredListDataProvider<TableMessage> dp)
	{
		this.header = h;
		this.dataProvider = dp;
		colMappings = new int[header.getNumberOfHeaders()];
		int numIdx, txtIdx, dateIdx;
		numIdx = txtIdx = dateIdx = 0;
		for(int i = 0; i < header.getNumberOfHeaders(); i++)
		{
			colName.addItem(header.getText(i));
			switch(header.getHeaderType(i))
			{
			case DATE:
				colMappings[i] = dateIdx++;
				break;
			case NUMBER:
				colMappings[i] = numIdx++;
				break;
			case TEXT:
				colMappings[i] = txtIdx++;
				break;
			default:
				break;	
			}
		}
		selectFilterBox();
		colName.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				selectFilterBox();
			}
		});
	}
	
	private void updateOpsList(TableMessageHeader.TableMessageContent headerType)
	{
		operation.clear();
		int endIdx = OPS.length;
		if(!headerType.equals(TableMessageHeader.TableMessageContent.TEXT))
			endIdx = OPS.length - 1;
		for(int i = 0; i < endIdx; i++)
			operation.addItem(OPS[i]);
	}
	
	private void selectFilterBox()
	{
		Widget fw = null;
		TableMessageContent ht = header.getHeaderType(colName.getSelectedIndex());
		switch(ht)
		{
		case DATE:
			if(dateFilterBox == null)
			{
				dateFilterBox = new DateBox();
			}
			fw = dateFilterBox;
			break;
		case NUMBER:
			if(numberFilterBox == null) numberFilterBox = new DoubleBox();
			fw = numberFilterBox;
			break;
		case TEXT:
			if(textFilterBox == null) textFilterBox = new TextBox();
			fw = textFilterBox;
			break;
		default:
			break;
		}
		if(fw == null) return;
		if(fw == filterBoxSlot.getWidget()) return;
		filterBoxSlot.clear();
		filterBoxSlot.add(fw);
		updateOpsList(ht);
	}
	
	public void enableFilterComponents(boolean enable)
	{
		doFilter.setEnabled(enable);
		colName.setEnabled(enable);
		operation.setEnabled(enable);
		Widget w = filterBoxSlot.getWidget();
		if(w != null)
		{
			if(w == dateFilterBox)
			{
				dateFilterBox.setEnabled(enable);
				dateFilterBox.setValue(null);
			}
			else if(w == textFilterBox)
			{
				textFilterBox.setEnabled(enable);
				textFilterBox.setValue(null);
			}
			else if(w == numberFilterBox)
			{
				numberFilterBox.setEnabled(enable);
				numberFilterBox.setValue(null);
			}
		}
		//disable filter when widget disabled, note that enabling widget not enough to enable filter,
		//need to click apply filter button on widget
		if(!enable && dataProvider != null)
			dataProvider.enableFilter(false); 
	}

	@Override
	public boolean isValid(TableMessage value) {
		int col = colName.getSelectedIndex();
		if(col < 0) return true; //nothing to filter on

		Comparable tableColVal = null;
		Comparable filterVal = null;
		switch(header.getHeaderType(col))
		{
		case DATE:
			tableColVal = standardizeDate(value.getDate(colMappings[col]), BASELINE_DATE);
			filterVal = standardizeDate(dateFilterBox.getValue(), BASELINE_DATE);			
			break;
		case NUMBER:
			tableColVal = value.getNumber(colMappings[col]);
			filterVal = numberFilterBox.getValue();
			break;
		case TEXT:
			tableColVal = standardizeString(value.getText(colMappings[col]));
			filterVal = standardizeString(textFilterBox.getValue());			
			break;
		default:
			break;
		}
		
		Boolean returnVal = null;
		String op = operation.getValue(operation.getSelectedIndex());
		if(op.equals(EQUAL))
		{
			if(filterVal == tableColVal) returnVal = true;
			if(filterVal == null) returnVal = false;
			if(tableColVal == null) returnVal = false;
			returnVal = (tableColVal.compareTo(filterVal) == 0);
		}
		else if(op.equals(GREATER))
		{
			if(filterVal == tableColVal) returnVal = false;
			if(filterVal == null) returnVal = false;
			if(tableColVal == null) returnVal = true;
			returnVal = (tableColVal.compareTo(filterVal) > 0);
		}
		else if(op.equals(LESS))
		{
			if(filterVal == tableColVal) returnVal = false;
			if(filterVal == null) returnVal = true;
			if(tableColVal == null) returnVal = false;
			returnVal = (tableColVal.compareTo(filterVal) < 0);
		}
		else if(op.equals(AT))
		{
			if(filterVal == null || tableColVal == null) returnVal = false;
			returnVal = ((String) tableColVal).contains((String) filterVal);
		}
		else if(op.equals(NOT_EQ))
		{
			if(filterVal == tableColVal) returnVal = false;
			if(filterVal == null) returnVal = true;
			if(tableColVal == null) returnVal = true;
			returnVal = (tableColVal.compareTo(filterVal) != 0);			
		}
		GWT.log("Compare of filter " + filterVal + " with col val " + tableColVal + " yielded " + returnVal);
		if(returnVal == null) return true; //bug? op got selected that we're currently not testing for;
		return returnVal;
	}
	
	public void applyFilter(String op, int col, String val)
	{
		//TODO, this method should support date/numeric initialization of filters as well
		boolean found = false;
		for(int i = 0; i < operation.getItemCount(); i++)
		{
			if(operation.getValue(i).equals(op))
			{
				operation.setSelectedIndex(i);
				found = true;
				break;
			}
		}
		if(!found) return;
		colName.setSelectedIndex(col);
		if(dataProvider == null) return;
		enableFilter.setValue(true, true);
		selectFilterBox();
		textFilterBox.setValue(val);
		dataProvider.enableFilter(true);
	}
	
	private Integer standardizeDate(Date d, Date baseline)
	{
		if(d == null) return null;
		return CalendarUtil.getDaysBetween(d, baseline);
	}
	
	private String standardizeString(String val)
	{
		if(val == null) return null;
		return val.toLowerCase().trim();
	}

}
