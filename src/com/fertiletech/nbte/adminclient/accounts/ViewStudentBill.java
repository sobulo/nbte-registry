/**
 * 
 */
package com.fertiletech.nbte.adminclient.accounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.MessageListToGrid;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.AccountManagerAsync;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class ViewStudentBill extends Composite implements HasValueChangeHandlers<Boolean>{
	@UiField Label billName;
	@UiField TextBox billYear;
	@UiField TextBox billTerm;
	@UiField TextBox billAmount;
	@UiField TextBox billDate;	
	@UiField MessageListToGrid itemizedSlot;
	
	@UiField TextBox priorPayments;
	@UiField TextBox balance;
	@UiField MessageListToGrid paySlot; 
	
	@UiField SimplePanel selectorSlot;
	@UiField Label currency;
		
	private String billID;
	private SimpleDialog infoBox;
	private static HashMap<String, List<TableMessage>> cachedBillData = new HashMap<String, List<TableMessage>>();
	private static AccountManagerAsync accountService = PanelUtilities.ACCT_SERVICE;
	
	String lastSelectedBill;
	private static ViewStudentBillUiBinder uiBinder = GWT
			.create(ViewStudentBillUiBinder.class);

	interface ViewStudentBillUiBinder extends UiBinder<Widget, ViewStudentBill> {
	}
	
	final AsyncCallback<List<TableMessage>> viewBillCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error loading invoice. Error was<br/>" + caught.getMessage());
			lastSelectedBill = null;
			ValueChangeEvent.fire(ViewStudentBill.this, false);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				infoBox.show("Loaded billing data succesfully");
				cachedBillData.put(billID, result);
				setupDetails(result);
			}
			else
			{
				infoBox.show("No data found for selected student invoice!!");				
			}
			billID = lastSelectedBill;
			lastSelectedBill = null;
			ValueChangeEvent.fire(ViewStudentBill.this, true);
		}
	};	
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ViewStudentBill() {
		initWidget(uiBinder.createAndBindUi(this));
		//billName.setEnabled(false);
		billYear.setEnabled(false);
		billTerm.setEnabled(false);
		billAmount.setEnabled(false);
		billDate.setEnabled(false);
		priorPayments.setEnabled(false);
		balance.setEnabled(false);
		infoBox = new SimpleDialog("INFO");
	}

	public void addSelector(Widget w)
	{
		selectorSlot.add(w);
	}
	
	private void setupDetails(List<TableMessage> info)
	{
		ArrayList<TableMessage> itemizedCopy = new ArrayList<TableMessage>(info.size());
		ArrayList<TableMessage> historyCopy = new ArrayList<TableMessage>(info.size());
	    TableMessage studBillInfo = info.get(0); //settlement info
	    double totalCheck = studBillInfo.getNumber(0);
	    double priorPayAmount = studBillInfo.getNumber(1);
		setupSummaryInfo(info.get(1), priorPayAmount, totalCheck); //bill template info
		itemizedCopy.add(info.get(2));
		boolean switchToHistory = false;
		for(int i = 3; i < info.size(); i++)
		{
			TableMessage m = info.get(i);
			if(m instanceof TableMessageHeader)
				switchToHistory = true;
			if(switchToHistory)
				historyCopy.add(m);
			else
				itemizedCopy.add(m);
		}
		itemizedSlot.populateTable(itemizedCopy);
		paySlot.populateTable(historyCopy);
	}
	
	private void setupSummaryInfo(TableMessage m, double priorPayAmount, double totalCheck)
	{
	
		billName.setText(m.getText(0));
		currency.setText(m.getText(1));
		billYear.setText(m.getDate(1)==null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(1)));
		billTerm.setText(m.getDate(2)==null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(2)));
		billAmount.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0)));
		billDate.setText(m.getDate(0)==null?"":GUIConstants.DEFAULT_DATE_FORMAT.format(m.getDate(0)));
		priorPayments.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(priorPayAmount));
		balance.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(m.getNumber(0) - priorPayAmount));
		if(Math.abs(m.getNumber(0) - totalCheck) > 0.000001 )
		{
			infoBox.show("Total for bill doesn't match its template. " +
					"This should not be the case, please alert your administrator about the discrepancy");
		}
	}	
	
	public void clearFields()
	{
		billName.setText("");
		billYear.setText("");
		billTerm.setText("");
		billAmount.setText("");
		billDate.setValue(null);
		itemizedSlot.clear();
		priorPayments.setText("");
		balance.setText("");
		paySlot.clear();
		billID = null;
		lastSelectedBill = null;
	}
	
	public void loadBill(String invoiceID)
	{
		clearFields();
		lastSelectedBill = invoiceID;
		List<TableMessage> cachedVal = cachedBillData.get(invoiceID);
		if(cachedVal == null)
			accountService.getStudentBillAndPayment(invoiceID, viewBillCallBack);
		else
			setupDetails(cachedVal);
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Boolean> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
