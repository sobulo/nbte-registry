package com.fertiletech.nbte.adminclient.accounts;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public abstract class CWBillViewBase extends ContentWidget{
	public abstract ValueChangeHandler<Boolean> getViewerLoadedAlert();
	public abstract Widget getBillSelector();
	
	ViewStudentBill viewer;
	public CWBillViewBase(String description) {
		super("View Invoice", description);
	}

	@Override
	public Widget onInitialize() {
		viewer = new ViewStudentBill();
		viewer.addValueChangeHandler(getViewerLoadedAlert());
		viewer.addSelector(getBillSelector());
		return viewer;
	}
	
	protected void loadViewer(String id)
	{
		viewer.loadBill(id);
	}

	protected RunAsyncCallback getAsyncCallBack(final AsyncCallback<Widget> callback) {
		return new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      };
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

}
