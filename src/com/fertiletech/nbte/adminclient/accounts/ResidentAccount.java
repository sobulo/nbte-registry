package com.fertiletech.nbte.adminclient.accounts;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.utils.CurrencyBox;
import com.fertiletech.nbte.adminclient.utils.MoneySignBox;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;

public class ResidentAccount extends Composite {

	@UiField
	TenantSelectionList tenantSelector;
	@UiField
	ListBox accountTypes;
	@UiField
	Label accountBalance;
	@UiField
	Label txnCount;
	@UiField
	Button create;
	@UiField
	CurrencyBox depositLevel;
	@UiField MoneySignBox currency;
	@UiField RadioButton overdraftAllowed;
	@UiField RadioButton otherDraftButton;
	@UiField Label typeLabel;
	
	boolean isEdit;
	
	SimpleDialog infoBox;
	private AsyncCallback<List<TableMessage>> typesCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error loading account types, try refreshing <br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			String headerId = result.remove(0).getMessageId();  //remove header
			if(isEdit)
			{
				populateCache(result, headerId);
				populateResidentAccounts(residentAccListCache.get(headerId));
				enableSaveFields(true);
			}
			else
			{
				for(TableMessage m : result)
					accountTypes.addItem(m.getText(0));
				enableSaveFields(true);
			}
		}
	};
	
	protected AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Save failed <br/>" + caught.getMessage());
			enableSaveFields(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			if(isEdit)
			{
				accountsCache.put(result.getMessageId(), result);
				enableSaveFields(true);
			}
			else
			{
				if(accountsCache.containsKey(result.getMessageId()))
				{
					String idToRemove = null;
					for(String tenantID : residentAccListCache.keySet())
					{
						for(String acctID : residentAccListCache.get(tenantID).keySet())
						{
							if(acctID.equals(result.getMessageId()))
							{
								idToRemove = tenantID;
								break;
							}
						}
						if(idToRemove != null) break;
					}
					if(idToRemove == null)
						PanelUtilities.infoBox.show("Cache stale, please refresh browser");
					else
						residentAccListCache.remove(idToRemove);
				}
			}
			infoBox.show("Updated student account " + GUIConstants.getAcctDisplayName(result.getText(DTOConstants.RSD_ACCT_TYPE_IDX), 
					result.getText(DTOConstants.RSD_ACCT_CURR_IDX)) + " succesfully");
			populateSingleAccountsInfo(result);
		}
	};
	
	
	private AsyncCallback<TableMessage> infoCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Lookup failed. Try refreshing browser <br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(TableMessage result) {
			GWT.log("Result returned with: " + result);
			if(result == null)
			{
				accountBalance.setText("Click Save to Create");
				txnCount.setText("N/A");
				enableSaveFields(true);
			}
			else
			{
				populateSingleAccountsInfo(result);	
			}
		} 
	};
	
	
	private static ResidentAccountUiBinder uiBinder = GWT
			.create(ResidentAccountUiBinder.class);

	interface ResidentAccountUiBinder extends UiBinder<Widget, ResidentAccount> {
	}
	
	public ResidentAccount()
	{
		this(false);
	}

	public ResidentAccount(boolean editMode) {
		isEdit = editMode;
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		enableSaveFields(false); //clicking on a resident will fix this
		if(isEdit)//fetch 
			typeLabel.setText("Select Account");		
		else
		{
			currency.addUserChangeHander(new ChangeHandler() {
				
				@Override
				public void onChange(ChangeEvent event) {
					lookupAcctDetails();
				}
			});
			typeLabel.setText("Account Type");
			PanelUtilities.READ_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_RES_ACCT_LIST_KEY, typesCallback);
		}
		create.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(!validateSaveArgs(true)) return;
				enableSaveFields(false);

				if(isEdit)
					PanelUtilities.ACCT_SERVICE.updateResidentAccount(accountTypes.getValue(accountTypes.getSelectedIndex()), 
							overdraftAllowed.getValue(), (int) Math.round(depositLevel.getAmount()), saveCallback);
				else
					PanelUtilities.ACCT_SERVICE.createResidentAccount(tenantSelector.getSelectedTenant().getMessageId(), accountTypes.getValue(accountTypes.getSelectedIndex()), 
							overdraftAllowed.getValue(), currency.getValue(),(int) Math.round(depositLevel.getAmount()), saveCallback );
			}
		});
		accountTypes.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				if(isEdit)
				{
					TableMessage m = accountsCache.get(accountTypes.getValue(accountTypes.getSelectedIndex()));
					populateSingleAccountsInfo(m);
				}
				else
					lookupAcctDetails();
			}
		});
		tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(isEdit)
				{
					TableMessage tenantObj = tenantSelector.getSelectedTenant();
					if(tenantObj == null) return;
					HashMap<String, String> acctTypesMap = residentAccListCache.get(tenantObj.getMessageId());
					if(acctTypesMap != null)
						populateResidentAccounts(acctTypesMap);
					else
					{
						enableSaveFields(false);
						PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(tenantObj.getMessageId(), typesCallback);
					}
				}
				else
					lookupAcctDetails();
			}
		});
	}
	
	//contains actual account details so no need to do a lookup if we've done so before // what about a create version?
	public static HashMap<String, TableMessage> accountsCache = new HashMap<String, TableMessage>();
	
	//stores entries teantid -> MAP[ acct id -- > display ]
	public static HashMap<String, HashMap<String, String>> residentAccListCache = new HashMap<String, HashMap<String,String>>();		
	void populateCache(List<TableMessage> result, String tenantID)
	{
		if(residentAccListCache.get(tenantID) == null)
		{
			residentAccListCache.put(tenantID, new HashMap<String, String>());
		}

		HashMap<String, String> typesCache = residentAccListCache.get(tenantID);

		for(TableMessage m : result)
		{
			accountsCache.put(m.getMessageId(), m);
			typesCache.put(m.getMessageId(), GUIConstants.getAcctDisplayName(m.getText(DTOConstants.RSD_ACCT_TYPE_IDX), 
					m.getText(DTOConstants.RSD_ACCT_CURR_IDX)));
		}
	}
	
	
	public void populateResidentAccounts(HashMap<String, String> accounts)
	{
		accountTypes.clear();
		HashSet<String> ensureUnique = new HashSet<String>();
		HashSet<String> duplicates = new HashSet<String>(); 
		
		for(String acctID : accounts.keySet())
		{
			if(ensureUnique.contains(accounts.get(acctID)))
				duplicates.add(acctID);
			else
				ensureUnique.add(accounts.get(acctID));
			accountTypes.addItem(accounts.get(acctID), acctID);
		}
		if(duplicates.size() > 0)
		{
			PanelUtilities.errorBox.show("Database integrity check failed. " + duplicates.toString() + ". Please send full text of this error message IT" );
			return;
		}
		
		if(accountTypes.getItemCount() > 0)
			populateSingleAccountsInfo(accountsCache.get(accountTypes.getValue(accountTypes.getSelectedIndex())));
		else
		{
			accountBalance.setText("No accounts have been created for this student");
			txnCount.setText("No accounts have been created for this student");
			enableSaveFields(false);
		}
	}
	
	public void populateSingleAccountsInfo(TableMessage result)
	{
		clearFields();
		accountTypes.setEnabled(true);
		currency.enableBox(true && !isEdit);
		accountBalance.setText(GUIConstants.DEFAULT_NUMBER_FORMAT.format(result.getNumber(DTOConstants.RSD_ACCT_AMT_IDX)));
		txnCount.setText(GUIConstants.DEFAULT_INTEGER_FORMAT.format(result.getNumber(DTOConstants.RSD_ACCT_TXN_IDX)));
		depositLevel.setAmount(result.getNumber(DTOConstants.RSD_ACCT_LEV_IDX));
		boolean overDraft = Boolean.valueOf(result.getText(DTOConstants.RSD_ACCT_OVER_IDX));
		if(overDraft)
			overdraftAllowed.setValue(true);
		else
			otherDraftButton.setValue(true);
		String currencyVal = result.getText(DTOConstants.RSD_ACCT_CURR_IDX);
		currency.setVal(currencyVal);
		
	}

	private void lookupAcctDetails()
	{
		if(!validateSaveArgs(false)) return;
		enableSaveFields(false);
		String type = accountTypes.getValue(accountTypes.getSelectedIndex());
		String ccy = currency.getValue();
		String tenant = tenantSelector.getSelectedTenant().getMessageId();
		PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(tenant, type, ccy, infoCallback );		
	}
	
	private void enableSaveFields(boolean enable)
	{
		create.setEnabled(enable);
		accountTypes.setEnabled(enable);
		currency.enableBox(enable && !isEdit);
		depositLevel.setEnabled(enable);
		overdraftAllowed.setEnabled(enable);
		otherDraftButton.setEnabled(enable);
	}
	
	
	private void clearFields()
	{
		depositLevel.setAmount(null);
		overdraftAllowed.setValue(false);
	}	
	
	private boolean validateSaveArgs(boolean isSave)
	{
		TableMessage tenantObj = tenantSelector.getSelectedTenant();			
		
		if(tenantObj == null)
		{
			infoBox.show("Please select a student to create an account for");
			return false;
		}
		
		if(accountTypes.getItemCount() == 0)
		{
			PanelUtilities.errorBox.show("Stale state, please refresh browser. Contact IT if problem persists");
			return false;
		}
		
        if(isSave && depositLevel.getAmount() == null)
        	infoBox.show("Enter a valid amount for level at which system should trigger deposit low warnings");      
		
        return true;
	}
}
