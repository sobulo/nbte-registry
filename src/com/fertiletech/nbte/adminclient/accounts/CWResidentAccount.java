package com.fertiletech.nbte.adminclient.accounts;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWResidentAccount extends ContentWidget{

	public CWResidentAccount() {
		super("Create Student Account", "Use this module to create student expense accounts. " +
				" Expense accounts are folders/buckets used to track and/or organize various payments made by students");
	}

	@Override
	public Widget onInitialize() {
		return new ResidentAccount(false);
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWResidentAccount.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
