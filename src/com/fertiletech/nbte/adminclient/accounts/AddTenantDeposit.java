package com.fertiletech.nbte.adminclient.accounts;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.utils.AccountSelector;
import com.fertiletech.nbte.adminclient.utils.CurrencyBox;
import com.fertiletech.nbte.adminclient.utils.MultipleMessageDialog;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AddTenantDeposit extends Composite implements ValueChangeHandler<TableMessage>{

	@UiField CurrencyBox amount;
	@UiField TextBox referenceID;
	@UiField TextBox comments;
	@UiField AccountSelector accounts;
	@UiField ListBox type;
	@UiField DateBox depDate;
	@UiField TenantSelectionList tenantSelector;
	@UiField Button saveDeposit;
	@UiField Button payAll;
	@UiField CheckBox autoSettle;
	@UiField DateBox settleDate;
	final static String BROWSER_MESSAGE = "<p style='font-size:smaller'>" +
			". If you're done adding student deposits, refresh your browser to" +
			" sync accounting modules on your browser with the server</p>";
	HashMap<String, String> typeToAcctMap = null;
	boolean autoMode;
	private void enableRegularFields(boolean enabled)
	{
		amount.setEnabled(enabled);
		referenceID.setEnabled(enabled);
		comments.setEnabled(enabled);
		depDate.setEnabled(enabled);
		accounts.enableBox(enabled);
		type.setEnabled(enabled);
		saveDeposit.setEnabled(enabled);
	}

	public void clear()
	{
		amount.setAmount(null);
		referenceID.setValue(null);
		comments.setValue(null);
		depDate.setValue(null);
	}	
	
	private SimpleDialog infoBox;
	private MultipleMessageDialog errorBox;
	
	private static AddTenantDepositUiBinder uiBinder = GWT
			.create(AddTenantDepositUiBinder.class);

	interface AddTenantDepositUiBinder extends
			UiBinder<Widget, AddTenantDeposit> {
	}
	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show(caught.getMessage());
			enableFields(true);
		}

		@Override
		public void onSuccess(String result) {
			infoBox.show(result + BROWSER_MESSAGE);
			clear();
			enableFields(true);
		}
	};	

	void enableAutoFields(boolean enabled)
	{
		payAll.setEnabled(enabled);
		//autoSettle.setEnabled(enabled);
		settleDate.setEnabled(enabled); 
	}
	
	void enableFields(boolean enabled)
	{
		enableAutoFields(autoMode && enabled);
		enableRegularFields(!autoMode && enabled);
	}
	
	public AddTenantDeposit() {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		errorBox = new MultipleMessageDialog("ERRORS");
		saveDeposit.setEnabled(false);
		depDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		enableFields(false);
		autoSettle.setEnabled(false);
		autoSettle.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				autoMode = event.getValue();
				if(autoMode)
					settleDate.setValue(new Date());
				//tenantSelector.selectionModel.clear();
				boolean isTenantSelected = tenantSelector.getSelectedTenant() != null;
				enableFields(autoMode || isTenantSelected);
			}
		});		
		payAll.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				TableMessage t = tenantSelector.getSelectedTenant();
				Date sd = settleDate.getValue();
				
				if( t == null)
				{
					PanelUtilities.errorBox.show("Please select a student to auto clear");
					return;
				}
				
				if(sd == null)
				{
					PanelUtilities.errorBox.show("Must enter a value for deposit date");
					return;
				}
				
				enableFields(false);
				PanelUtilities.ACCT_SERVICE.createDeposits(t.getMessageId(), sd, new AsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						PanelUtilities.errorBox.show("Error settling student bills." +
								" Error: " + caught.getMessage());
						enableFields(true);
					}

					@Override
					public void onSuccess(String result) {
						PanelUtilities.infoBox.show(result + BROWSER_MESSAGE);
						enableFields(true);
						
					}
				});
			}
		});
		saveDeposit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(checkRegularErrors())
					return;
				enableFields(false);
				PanelUtilities.ACCT_SERVICE.createDeposit(accounts.getValue(accounts.getSelectedIndex()), depDate.getValue(), referenceID.getValue(), 
						comments.getValue(), type.getValue(type.getSelectedIndex()), amount.getAmount(), saveCallBack);
			}
		});
		tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			private AsyncCallback<List<TableMessage>> resAcctCallback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					errorBox.show("Unable to fetch account list<br/>" + caught.getMessage());
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					populateDepositTypes(result);
					if(type.getItemCount() > 0)
					{
						enableFields(true);
					}
				}
			};

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null) return;
				type.clear();
				clear();
				enableFields(false);
				TableMessage tenant = event.getValue();
				PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(tenant.getMessageId(), resAcctCallback );
			}
		});
		autoSettle.setValue(true, true);
	}
	
	public boolean checkRegularErrors()
	{
		if(autoMode) return true;
		ArrayList<String> errors = new ArrayList<String>(3);
		if(typeToAcctMap == null)
			errors.add("Still loading error checking objects, try again shortly");
		String billtype = typeToAcctMap.get(type.getItemText(type.getSelectedIndex()));
		String acctType = accounts.getAcctTypeID();
		if(!billtype.equals(acctType))
			errors.add("Type mismatch: Selected student account type [" + billtype +"] vs. [" + acctType +"]");
		if(amount.getAmount() == null)
			errors.add("Amount must be a valid number");
		if(referenceID.getValue().trim().length() == 0)
			errors.add("Reference ID field must be populated");
		if(depDate.getValue() == null)
			errors.add("Specify a valid date for when this amount was deposited at the bank");
		boolean errorsExist = errors.size() > 0;
		if(errorsExist)
			errorBox.show("Please fix issues below", errors);
		return errorsExist;
	}
	
	public void populateDepositTypes(List<TableMessage> acctTypes)
	{
		acctTypes.remove(0); //remove header
		typeToAcctMap = new HashMap<String, String>();
		for(TableMessage typeItem : acctTypes)
		{
			String t = typeItem.getText(DTOConstants.RSD_ACCT_TYPE_IDX);
			String c = typeItem.getText(DTOConstants.RSD_ACCT_CURR_IDX);
			String amt = NumberFormat.getDecimalFormat().format(typeItem.getNumber(DTOConstants.RSD_ACCT_AMT_IDX));
			String name = GUIConstants.getAcctDisplayName(t, c) + "/" + amt;
			typeToAcctMap.put(name, t);
			type.addItem(name, typeItem.getMessageId());
		}
		type.setTitle(typeToAcctMap.get(type.getItemText(type.getSelectedIndex())));
	}

	@Override
	public void onValueChange(ValueChangeEvent<TableMessage> event) {
		infoBox.show("Warning: Student changed to " + event.getValue().getText(RegistryDTO.MATRIC_NO_IDX)+ 
				". Clicking button 'save payment' will reflect a recent deposit by this student");
	}
	
	
}
