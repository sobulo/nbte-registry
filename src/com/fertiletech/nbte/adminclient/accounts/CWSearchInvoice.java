package com.fertiletech.nbte.adminclient.accounts;

import java.util.HashMap;

import com.fertiletech.nbte.adminclient.CWArguments;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.Widget;

public class CWSearchInvoice extends CWBillViewBase implements CWArguments
{
	public CWSearchInvoice() 
	{
		super("Enter an invoice number in the text box and then click search");
	}

	LongBox billSelector;
	Button search;
	HTML status;

	private String getBillID() {
		Long val = billSelector.getValue();
		
		if(val == null)
		{
			status.setHTML("<font color='red'>Invoice ID should only be numeric values</font>");
			return null;
		}
		
		String result = cachedIDs.get(val);
		
		if(result == null)
		{
			status.setHTML("<font color='red'>Invoice ID not found!!</font>");
			return null;			
		}
	
		return result;
	}

	@Override
	public ValueChangeHandler<Boolean> getViewerLoadedAlert() {
		return new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				billSelector.setEnabled(true);
				search.setEnabled(true);
				if(event.getValue())
					status.setHTML("<font color='green' style='font-size:smaller'>Invoice loaded</font>");
				else
					status.setHTML("<font color='red' style='font-size:smaller'>Failed to load invoice!</font>");
			}
		};
	}

	
	private void fetchBill()
	{
		String id = getBillID();
		if(id == null) return;
		search.setEnabled(false);
		billSelector.setEnabled(false);
		loadViewer(id);		
	}
	
	HashMap<Long, String> cachedIDs;
	@Override
	public Widget getBillSelector() {
		initBillSelector();
		search = new Button("Search");
		status = new HTML("Loading, please wait ...");
		status.setWidth("300px");
		search.setEnabled(false);
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				fetchBill();
			}
		});
		HorizontalPanel result = new HorizontalPanel();
		result.setSpacing(20);
		result.add(billSelector);
		result.add(search);
		result.add(status);
		AsyncCallback<HashMap<Long, String>> callback = new AsyncCallback<HashMap<Long,String>>() {

			@Override
			public void onFailure(Throwable caught) {
				status.setHTML("<marquee>Error on server side, try refreshing browser. Error was " + caught.getMessage() + "</marquee>");
			}

			@Override
			public void onSuccess(HashMap<Long, String> result) {
				if(result.size() == 0)
				{
					status.setHTML("<marquee>No invoices found in database</marquee>");
				}
				else
				{
					status.setHTML("Invoices loaded (" + result.size() + ")");
					cachedIDs = result;
				}		
				
				search.setEnabled(true);
				
				//race condition check, while @ server, client could have set an id
				Long val = billSelector.getValue();
				if(val != null)
					fetchBill();
			}
		};
		//go fetch bill IDs
		PanelUtilities.ACCT_SERVICE.getAllBillIDs(callback);
		return result;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWSearchInvoice.class, getAsyncCallBack(callback));
	}

	@Override
	public void setParameterValues(String id) {
		initBillSelector();
		if(id != null && id.trim().length() > 0)
		{
			billSelector.setText(id);
			if(cachedIDs != null) //race condition exists when this panel has never been loaded, this minimizes it but the race condition possibly still exists
				fetchBill();
		}
		else
			viewer.clearFields();		
	}
	
	void initBillSelector()
	{
		if(billSelector == null)
		{
			billSelector = new LongBox();
		}
	}

}
