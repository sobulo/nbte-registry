/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient.accounts;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import com.fertiletech.nbte.adminclient.utils.CurrencyBox;
import com.fertiletech.nbte.adminclient.utils.MultipleMessageDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Example file.
 */
public class BillTemplateEditor {
	/**
	 * The constants used in this Content Widget.
	 */

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	final FlexTable flexTable;
	final CurrencyBox amount;
	final TextBox name;
	final TextBox description;
	MultipleMessageDialog errorBox;
	double totalBillAmount = 0;
	HTML totalLabel;

	public BillTemplateEditor() {
		totalLabel = new HTML();
		errorBox = new MultipleMessageDialog("Error Adding Item");
		flexTable = new FlexTable();
		FlexCellFormatter cellFormatter = flexTable.getFlexCellFormatter();
		flexTable.addStyleName("cw-FlexTable");
		flexTable.setWidth("35em");
		flexTable.setCellSpacing(5);
		flexTable.setCellPadding(3);

		// setup panel to collect user input
		amount = new CurrencyBox();
		name = new TextBox();
		description = new TextBox();
		name.setMaxLength(40);
		description.setMaxLength(50);
		VerticalPanel inputPanel = new VerticalPanel();
		inputPanel.setSpacing(5);
		addToInputPanel(inputPanel, "Item", name);
		addToInputPanel(inputPanel, "Amount", amount);
		addToInputPanel(inputPanel, "Description", description);

		// Add user input panel to content area
		cellFormatter.setHorizontalAlignment(0, 1,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.setWidget(0, 0, inputPanel);
		cellFormatter.setColSpan(0, 0, 3);

		// Add a button that will add more rows to the table
		Button addRowButton = new Button("Add Item", new ClickHandler() {
			public void onClick(ClickEvent event) {
				Double newAddition = addRow();
				GWT.log("Add Row returned: " + newAddition);
				if(newAddition != null)
				{
					GWT.log("Amount before addition: " + totalBillAmount);
					totalBillAmount += newAddition;
					redrawTotalLabel();
					GWT.log("Amount after addition: " + totalBillAmount);
				}
			}
		});
		addRowButton.addStyleName("sc-FixedWidthButton");

		Button removeRowButton = new Button("Remove Item", new ClickHandler() {
			public void onClick(ClickEvent event) {
				totalBillAmount += removeRow();
				redrawTotalLabel();
			}
		});
		removeRowButton.addStyleName("sc-FixedWidthButton");
		VerticalPanel buttonPanel = new VerticalPanel();
		buttonPanel.setStyleName("cw-FlexTable-buttonPanel");
		buttonPanel.add(addRowButton);
		buttonPanel.add(removeRowButton);
		buttonPanel.add(totalLabel);
		flexTable.setWidget(0, 1, buttonPanel);
		cellFormatter
				.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_TOP);
		redrawTotalLabel();
	}

	private void redrawTotalLabel()
	{
		GWT.log("Redraw total label called with: " + totalBillAmount);
		totalLabel.setHTML("<div style='border:1px solid black;margin-top:20px'><b>Total:</b>" + GUIConstants.DEFAULT_NUMBER_FORMAT.format(totalBillAmount) + "</div>");
	}
	
	private void addToInputPanel(VerticalPanel inputPanel, String labelName,
			Widget w) {
		FlowPanel f = new FlowPanel();
		f.add(new Label(labelName));
		w.setWidth("80%");
		f.add(w);
		inputPanel.add(f);
	}
	
	private void clearInputPanel()
	{
		name.setValue(null);
		description.setValue(null);
		amount.setAmount(null);
		redrawTotalLabel();
	}

	private boolean validateInput() {
		ArrayList<String> errors = new ArrayList<String>(3);
		if(name.getText().trim().length() == 0)
			errors.add("Please enter a value for item name");
		if(description.getText().trim().length() == 0)
			errors.add("Please enter a value for description field");
		if(amount.getAmount() == null)
			errors.add("Please enter a valid amount");
		if(errors.size() > 0)
		{
			errorBox.show("Please fix issues below then click add item button again", errors);
			return false;
		}
		return true;
	}

	private void addHeader() {
		int numRows = flexTable.getRowCount();
		flexTable.setHTML(numRows, 0, "<b>Name</b>");
		flexTable.setHTML(numRows, 1, "<b>Description</b>");
		flexTable.setHTML(numRows, 2, "<b>Amount</b>");
	}	
	/**
	 * Add a row to the flex table.
	 */
	private Double addRow() {
		if(!validateInput())
			return null;
		int numRows = flexTable.getRowCount();
		if(numRows == 1)
		{
			addHeader();
			numRows++;
		}
		
		flexTable.setText(numRows, 0, name.getText());
		flexTable.setText(numRows, 1, description.getText());
		flexTable.setText(numRows, 2, amount.getAmountString());
		flexTable.getFlexCellFormatter().setRowSpan(0, 1, numRows + 1);
		double result = amount.getAmount();
		clearInputPanel();
		return result;
		
	}

	/**
	 * Remove a row from the flex table.
	 */
	private double removeRow() {
		int numRows = flexTable.getRowCount();
		double result = 0.0;
		if (numRows > 1) { //leaving leading header text intact
			String remove = flexTable.getText(numRows - 1, 2);
			GWT.log("Remove requested for: " + remove);
			if(numRows > 2)
				result = 0 - Double.valueOf(remove);
			flexTable.removeRow(numRows - 1);
			flexTable.getFlexCellFormatter().setRowSpan(0, 1, numRows - 1);
		}
		return result;
	}
	
	public LinkedHashSet<BillDescriptionItem> getBillItems(double divisor)
	{
		LinkedHashSet<BillDescriptionItem> result = new LinkedHashSet<BillDescriptionItem>();
		int numOfRows = flexTable.getRowCount();
		if(numOfRows > 2) //ignore leading header text as well as subsequent header columns
		{
			for(int i = 2; i < numOfRows; i++)
				result.add(new BillDescriptionItem(flexTable.getText(i, 0), flexTable.getText(i, 1),
						Double.valueOf(flexTable.getText(i, 2)) / divisor));
		}
		return result;
	}
	
	public Widget getEditorWidget()
	{
		return flexTable;
	}
	
	public void clearEditorWidget()
	{
		while(flexTable.getRowCount() > 1)
		{
			removeRow();
		}
		clearInputPanel();
		totalBillAmount = 0;
		redrawTotalLabel();
	}
}
