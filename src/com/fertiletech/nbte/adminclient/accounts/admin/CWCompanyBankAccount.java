package com.fertiletech.nbte.adminclient.accounts.admin;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWCompanyBankAccount extends ContentWidget{

	public CWCompanyBankAccount() {
		super("College Bank Accounts", "Warning! Do not use this module without being absolutely sure of what you're doing as this data " +
				"is exposed externally to comapny clients, e.g. when making deposit requests. This module allows you to specify bank accounts that students pay into.");
	}

	@Override
	public Widget onInitialize() {
		return new EditCompanyBankAccount();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWCompanyBankAccount.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});	
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}	

}
