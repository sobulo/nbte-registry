package com.fertiletech.nbte.adminclient.accounts.admin;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.utils.AccountSelector;
import com.fertiletech.nbte.adminclient.utils.MoneySignBox;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class EditCompanyBankAccount extends Composite {
	@UiField
	TextBox accountName;
	@UiField
	TextBox accountNum;
	@UiField
	TextBox bank;
	@UiField
	TextBox sortCode;
	@UiField
	AccountSelector acctSelector;
	@UiField
	RadioButton actionEdit;
	@UiField
	RadioButton actionNew;
	@UiField
	Button save;
	@UiField
	MoneySignBox currencyCode;

	private static EditCompanyBankAccountUiBinder uiBinder = GWT
			.create(EditCompanyBankAccountUiBinder.class);

	interface EditCompanyBankAccountUiBinder extends
			UiBinder<Widget, EditCompanyBankAccount> {
	}

	public EditCompanyBankAccount() {
		initWidget(uiBinder.createAndBindUi(this));
		acctSelector.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				clear();
				save.setEnabled(true);
				TableMessage m = acctSelector.getDTO(acctSelector
						.getSelectedIndex());
				setMessage(m);
				actionEdit.setValue(true);
			}
		});
		actionEdit.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				clear();
				if (acctSelector.getSelectedIndex() < 0) {
					Window.alert("Nothing to edit");
					return;
				}
				TableMessage m = acctSelector.getDTO(acctSelector
						.getSelectedIndex());
				setMessage(m);
				acctSelector.enableBox(true);
			}
		});

		actionNew.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				clear();
				acctSelector.enableBox(false);
			}
		});
		actionEdit.setValue(false);

		if (acctSelector.getItemCount() == 0)
			save.setEnabled(false);
		else
			actionEdit.setValue(true);

		save.addClickHandler(new ClickHandler() {

			private AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Save failed: " + caught.getMessage());
					save.setEnabled(true);
				}

				@Override
				public void onSuccess(TableMessage result) {
					acctSelector.setMessage(result);
					actionEdit.setValue(true);
					Window.alert("Stored account information succesfully");
					save.setEnabled(true);
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				String b = bank.getValue();
				String s = sortCode.getValue();
				String acctName = accountName.getValue();
				String acctNumber = accountNum.getValue();
				if (b == null || s == null || acctName == null
						|| acctNumber == null) {
					Window.alert("Complete all fields prior to clicking save");
					return;
				}

				if (actionEdit.getValue())
					PanelUtilities.ACCT_SERVICE.updateAccount(
							acctSelector.getValue(acctSelector
									.getSelectedIndex()), b, s, acctName,
							acctNumber, currencyCode.getValue(), callback);
				else
					PanelUtilities.ACCT_SERVICE.createAccount(b, s,
							acctName, acctNumber, currencyCode.getValue(),
							callback);
			}
		});
	}

	private void clear() {
		accountName.setValue(null);
		accountNum.setValue(null);
		bank.setValue(null);
		sortCode.setValue(null);
	}

	private void setMessage(TableMessage m) {
		accountName.setValue(m.getText(DTOConstants.CMP_ACCT_NAME_IDX));
		accountNum.setValue(m.getText(DTOConstants.CMP_ACCT_NUM_IDX));
		bank.setValue(m.getText(DTOConstants.CMP_ACCT_BNK_IDX));
		sortCode.setValue(m.getText(DTOConstants.CMP_ACCT_SRT_IDX));
		currencyCode.setVal(m.getText(DTOConstants.CMP_ACCT_CURR_IDX));
	}
}
