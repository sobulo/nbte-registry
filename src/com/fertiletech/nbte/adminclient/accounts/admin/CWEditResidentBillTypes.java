package com.fertiletech.nbte.adminclient.accounts.admin;

import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.contentwrappers.CWEditParamsBase;
import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditResidentBillTypes extends CWEditParamsBase{

	public CWEditResidentBillTypes() {
		super("Bill/Invoice Types", "Use this module to setup which options are available under type field of invoice creation module. Also select which account type the invoice type should be matched with");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_RES_BILL_LIST_KEY;
	}
	
	@Override
	protected String getDropDownID()
	{
		return DTOConstants.APP_PARAM_RES_ACCT_LIST_KEY;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditResidentBillTypes.class, super.getAsyncCall(callback));			
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}	
}
