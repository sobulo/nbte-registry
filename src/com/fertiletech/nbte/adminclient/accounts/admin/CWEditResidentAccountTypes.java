package com.fertiletech.nbte.adminclient.accounts.admin;

import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.contentwrappers.CWEditParamsBase;
import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditResidentAccountTypes extends CWEditParamsBase{

	public CWEditResidentAccountTypes() {
		super("Student Acct. Types", "Use this module to select student account" +
				" types that deposits as well as invoice withdrawals can be made on.");
	}

	@Override
	protected boolean getShowValues() {
		return false;
	}

	@Override
	protected String getParameterID() {
		return DTOConstants.APP_PARAM_RES_ACCT_LIST_KEY;
	}

	@Override
	protected void asyncOnInitialize(AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditResidentAccountTypes.class, super.getAsyncCall(callback));			
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}	
}
