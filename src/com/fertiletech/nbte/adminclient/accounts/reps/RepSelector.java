package com.fertiletech.nbte.adminclient.accounts.reps;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.accounts.ContactCell;
import com.fertiletech.nbte.adminclient.tables.TableMessageKeyProvider;
import com.fertiletech.nbte.adminclient.utils.DataProviderHelper;
import com.fertiletech.nbte.adminclient.utils.MultipleMessageDialog;
import com.fertiletech.nbte.adminclient.widgs.RangeLabelPager;
import com.fertiletech.nbte.adminclient.widgs.ShowMorePagerPanel;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SelectionChangeEvent.HasSelectionChangedHandlers;
import com.google.gwt.view.client.SingleSelectionModel;

public class RepSelector extends Composite implements HasSelectionChangedHandlers{
	@UiField
	ShowMorePagerPanel pagerPanel;

	@UiField
	RangeLabelPager rangeLabelPager;

	@UiField
	Label statusMessage;
	
	SimpleDialog infoBox;
	MultipleMessageDialog errorBox;
	private CellList<TableMessage> cellList;
	
	private static EditVendorUiBinder uiBinder = GWT
			.create(EditVendorUiBinder.class);

	interface EditVendorUiBinder extends UiBinder<Widget, RepSelector> {
	}

	ListDataProvider<TableMessage> dp = new ListDataProvider<TableMessage>();

    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> repsCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	List<TableMessage> dpList = dp.getList();
        	dpList.clear();
        	dpList.addAll(result);
        	statusMessage.setText(String.valueOf(dpList.size()) + " company rep(s)");
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to fetch company/billing reps list. Try refreshing your browser. " +
        			"Contact technology@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };
    

    final SingleSelectionModel<TableMessage> selectionModel;
	public RepSelector() {
		// Create a CellList.
		ContactCell contactCell = new RepCell();

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);
		HTML emptyMessage = new HTML("<marquee>No company reps found. Create one first</marquee>");
		emptyMessage.setWidth("200px");
		cellList.setEmptyListWidget(emptyMessage);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);
		errorBox = new MultipleMessageDialog("<font color='red'>Error Occured</font>", true);
		infoBox = new SimpleDialog("<font color='green'>INFO</font>", true);
		initWidget(uiBinder.createAndBindUi(this));
		pagerPanel.setDisplay(cellList);
		rangeLabelPager.setDisplay(cellList);
		dp.addDataDisplay(cellList);
		PanelUtilities.ACCT_SERVICE.getAllReps(repsCallback);
	}
	
	@Override
	public HandlerRegistration addSelectionChangeHandler(Handler handler) {
		return selectionModel.addSelectionChangeHandler(handler);
	}
	
	public TableMessage getSelectedRep()
	{
		return selectionModel.getSelectedObject();
	}
	
	public void addRepToList(TableMessage m)
	{
		dp.getList().add(m);
	}
	
	public void refreshRepList(TableMessage m)
	{
		DataProviderHelper.refreshSingleData(dp.getList(), m);
	}
}

class RepCell extends ContactCell
{
	@Override
	public void render(Context context, TableMessage value,
			SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
		
		/*String company = value.getText(DTOConstants.TNT_COMPANY_IDX);
		company = (company==null || company.length()==0) ? "ERROR - BLANK COMPANY" : company;
		String repName = value.getText(DTOConstants.TNT_FNAME_IDX) + " " + value.getText(DTOConstants.TNT_LNAME_IDX);
		repName = (repName==null || repName.length()==0) ? "No contact name specified" : repName; 

		sb.appendHtmlConstant("<table>");

		// Add the contact image.
		sb.appendHtmlConstant("<tr><td rowspan='3'>");
		sb.appendHtmlConstant(getImageHTML());
		sb.appendHtmlConstant("</td>");

		// Add the name and address.
		sb.appendHtmlConstant("<td style='font-size:95%;'>");
		sb.appendEscaped(company);
		sb.appendHtmlConstant("</td></tr><tr><td>");
		sb.appendEscaped(repName);
		sb.appendHtmlConstant("</td></tr></table>");*/
	}
}
