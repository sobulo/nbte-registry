package com.fertiletech.nbte.adminclient.accounts.reps;

import com.fertiletech.nbte.adminclient.utils.UserPanel;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;

public class EditRepsPanel extends Composite{

	@UiField
	Button update;
	@UiField
	Button clear;	
	@UiField
	RepSelector repSelector;
	@UiField
	UserPanel bioPanel;
	
	final AsyncCallback<TableMessage> saveCallback = new AsyncCallback<TableMessage>() {

		@Override
		public void onSuccess(TableMessage result) {
			bioPanel.setUserID(result.getMessageId());
			/*repSelector.infoBox.show("Updated contact info for "
					+ result.getText(DTOConstants.TNT_LNAME_IDX)
					+ " successfully");*/
			repSelector.refreshRepList(result);
			update.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			repSelector.errorBox.show("Unable to save values. Error was: "
					+ caught.getMessage());
			update.setEnabled(true);
		}
	};	
	
	private static EditVendorPanelUiBinder uiBinder = GWT
			.create(EditVendorPanelUiBinder.class);

	interface EditVendorPanelUiBinder extends UiBinder<Widget, EditRepsPanel> {
	}

	public EditRepsPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		repSelector.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				if(repSelector.getSelectedRep() == null) 
					bioPanel.clear();
				else
					bioPanel.setContactValues(repSelector.getSelectedRep());
			}
		});
		update.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

				@Override
				public void onFailure(Throwable caught) {
					repSelector.errorBox.show("Unable to create company rep. Error was: " + caught.getMessage());
				}

				@Override
				public void onSuccess(TableMessage result) {
					bioPanel.setUserID(result.getMessageId());
					repSelector.addRepToList(result);
					//repSelector.infoBox.show("Succesfully created billing rep " + result.getText(DTOConstants.TNT_FNAME_IDX) + " " + result.getText(DTOConstants.TNT_LNAME_IDX) + 
					//		" for " + result.getText(DTOConstants.TNT_COMPANY_IDX));
				}
			};

			@Override
			public void onClick(ClickEvent event) {
				/*if(bioPanel.getUserID() == null)
				{
					PanelUtilities.ACCT_SERVICE.createCompanyRep(bioPanel.getFirstName(), bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(), 
							bioPanel.getPhone(), null, bioPanel.getOtherMails(), null, bioPanel.getAddress(), bioPanel.isMale(), bioPanel.getSalutation(), callback);
				}
				else
				{
					LocalDataProvider.TENANT_SERVICE.updateContact(
							bioPanel.getUserID(), bioPanel.getFirstName(),
							bioPanel.getLastName(), bioPanel.getDateOfBirth(), bioPanel.getEmail(),
							bioPanel.getPhone(), bioPanel.getOtherNumbers(),
							bioPanel.getOtherMails(), bioPanel.getCompany(),
							bioPanel.getAddress(), bioPanel.isMale(),
							bioPanel.getSalutation(), null, saveCallback);
				}*/
			}
		});
		clear.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				bioPanel.clear();
			}
		});
	}
}
