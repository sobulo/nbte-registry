package com.fertiletech.nbte.adminclient.accounts.reps;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.utils.TenantSelectionTreePanel;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;

public class FileResidentWithRep extends Composite {

	@UiField
	Button add;
	@UiField
	Button remove;
	@UiField(provided=true)
	ShowcaseTable display;
	@UiField
	TenantSelectionTreePanel tenantSelector;
	@UiField
	RepSelector repSelector;
	YesNoDialog warningBox;
	SimpleDialog errorBox, infoBox;
	
	HashMap<String, List<TableMessage>> cachedDepositInfo = new HashMap<String, List<TableMessage>>();
	final AsyncCallback<List<TableMessage>> accountListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve resident list for selected company rep");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			String repID = result.get(0).getMessageId();
			cachedDepositInfo.put(repID, result);
			display.showTable(result);
			add.setEnabled(true);
			if(result.size() > 1)
				remove.setEnabled(true);
		}
	};
	
	final AsyncCallback<List<TableMessage>> updateCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve resident list for selected company rep");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			TableMessageHeader h = (TableMessageHeader) result.get(0);
			infoBox.show(h.getCaption());
			accountListCallBack.onSuccess(result);
		}
	};
	
	private static PrintDepositStatementUiBinder uiBinder = GWT
			.create(PrintDepositStatementUiBinder.class);

	interface PrintDepositStatementUiBinder extends
			UiBinder<Widget, FileResidentWithRep> {
	}

	public FileResidentWithRep() {
		display = new ShowcaseTable(true, false, false);
		initWidget(uiBinder.createAndBindUi(this));
		tenantSelector.hideAdditionalWidgets();
		warningBox = new YesNoDialog("Warning");
		warningBox.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				doAdd();
				warningBox.hide();
			}
		});
		add.setEnabled(false);
		remove.setEnabled(false);
		errorBox = new SimpleDialog("Error!");
		infoBox = new SimpleDialog("Info");
		repSelector.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				TableMessage m = repSelector.getSelectedRep();
				List<TableMessage> cachedList = cachedDepositInfo.get(m.getMessageId());
				add.setEnabled(false);
				remove.setEnabled(false);
				if(cachedList != null)
				{
					display.showTable(cachedList);
					add.setEnabled(true);
					if(cachedList.size() > 1 || (cachedList.size() == 1 && cachedList.get(0) instanceof TableMessage))
						remove.setEnabled(true);
					return;
				}
				PanelUtilities.ACCT_SERVICE.getCompanyTenants(m.getMessageId(), accountListCallBack);			
			}
		});
		
		add.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Set<TableMessage> selectedTenants = tenantSelector.getSelectedTenantObjects();
				if(selectedTenants.size() == 0)
				{
					errorBox.show("Please select residents on the right-side bar to add them to selected company-billing profile");
					return;
				}
				
				
				TableMessage m = repSelector.getSelectedRep();
				if(m == null)
				{
					errorBox.show("Please select a company billing representative on the left-side bar before attempting to add selected residents");
					return;
				}
				StringBuilder warningMsg = new StringBuilder();
				/*String repCompany = m.getText(DTOConstants.TNT_COMPANY_IDX);
				repCompany = (repCompany == null? "" : repCompany.trim());
				
				
				for(TableMessage t : selectedTenants)
				{
					String tntCompany = t.getText(DTOConstants.TNT_COMPANY_IDX);
					tntCompany = (tntCompany == null? "" : tntCompany.trim());
					if(!tntCompany.equalsIgnoreCase(repCompany))
						warningMsg.append("<li>").append(tntCompany).append(" vs ").append(repCompany).
							append(" mismatch for resident: ").append(t.getText(DTOConstants.TNT_LNAME_IDX)).append("</li>");
				}*/
				
				
				if(warningMsg.length() > 0)
					warningBox.show("<font color='red'>Warning!</font><br/> Below shows cases where selected residents work at a" +
							" company different from the selected rep. <br/><ul>" + warningMsg + "</ul><br/> Proceed with profile setup?");
				else
					doAdd();
			}
		});
		remove.addClickHandler(new ClickHandler() {
			

			@Override
			public void onClick(ClickEvent event) {
				Set<TableMessage> selectedTenants = display.getSelectedSet();
				if(selectedTenants.size() == 0)
				{
					errorBox.show("1 or more residents from table below before attempting to remove");
					return;
				}
				String[] tenantIDs = new String[selectedTenants.size()];
				int i = 0;
				for(TableMessage m : selectedTenants)
					tenantIDs[i++] = m.getMessageId();
				
				add.setEnabled(false);
				remove.setEnabled(false);
				PanelUtilities.ACCT_SERVICE.removeCompanyTenants(tenantIDs, repSelector.getSelectedRep().getMessageId(), updateCallBack);
			}
		});
	}
	
	public void doAdd()
	{
		String[] selectedTenants = tenantSelector.getSelectedTenants();
		TableMessage m = repSelector.getSelectedRep();		
		PanelUtilities.ACCT_SERVICE.saveCompanyTenants(selectedTenants, m.getMessageId(), updateCallBack);
	}
	
	private String getTenantName(TableMessage value)
	{
		String firstName = value.getText(RegistryDTO.FIRST_NAME_IDX);
		firstName = firstName.length() == 0 ? "" : firstName + " ";
		String fullName = firstName
				+ value.getText(RegistryDTO.LAST_NAME_IDX);
		return fullName;
	}
}
