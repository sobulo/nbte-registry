package com.fertiletech.nbte.adminclient.accounts.reps;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditCompanyRep extends ContentWidget{

	public CWEditCompanyRep() {
		super("Billing Rep", "Use this module to setup contact information of billing recipient. Useful in instances where an organization and/or guardian " +
				"(i.e. not the student) is responsible for funding the student's fees");
	}

	@Override
	public Widget onInitialize() {
		return new EditRepsPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditCompanyRep.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}