package com.fertiletech.nbte.adminclient.accounts.reps;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWEditCompanyResidents extends ContentWidget{

	public CWEditCompanyResidents() {
		super("Billing Profiles", "Use this module to create profiles that associate students with a billing rep");
	}

	@Override
	public Widget onInitialize() {
		return new FileResidentWithRep();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWEditCompanyResidents.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
