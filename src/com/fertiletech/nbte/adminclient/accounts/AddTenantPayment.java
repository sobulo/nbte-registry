/*
 * Copyright 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient.accounts;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.TableMessageKeyProvider;
import com.fertiletech.nbte.adminclient.utils.ArchiveButton;
import com.fertiletech.nbte.adminclient.utils.CurrencyBox;
import com.fertiletech.nbte.adminclient.utils.DataProviderHelper;
import com.fertiletech.nbte.adminclient.widgs.ShowMorePagerPanel;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.client.eventboxes.DiplDeptLB;
import com.fertiletech.nbte.client.eventboxes.UniqueListBox;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.sun.corba.se.pept.transport.ContactInfo;

public class AddTenantPayment extends ContentWidget implements ChangeHandler, ClickHandler {

	@UiField ListBox billList;
	
	@UiField TextBox totalAmount;
	@UiField TextBox priorPayment;
	@UiField TextBox balance;
	
	@UiField CurrencyBox paymentAmount;
	@UiField TextBox comments;
	@UiField DateBox paymentDate;
	
	@UiField Label description;
	@UiField CheckBox unpaid;
	@UiField ListBox accountList;
	@UiField ArchiveButton archive;
	@UiField Label ccy;
	
	@UiField Button savePayment;
	SingleSelectionModel<TableMessage> selectionModel;
	/**
	 * The pager used to change the range of data.
	 */

	@UiField
	ShowMorePagerPanel pagerPanel;

	/**
	 * The pager used to display the current range.
	 */

	//@UiField
	//RangeLabelPager rangeLabelPager;

	@UiField
	ListBox diploma;
	
	@UiField
	ListBox department;

	@UiField
	ListBox year;
	
	DataProviderHelper pc = new DataProviderHelper();
	
	private SimpleDialog errorBox, infoBox;
	
	private final static String DEFAULT_TITLE = "Please select a student bill to pay";
	private final static NumberFormat NUMBER_FORMAT = GUIConstants.DEFAULT_NUMBER_FORMAT;
	private static HashMap<String, List<TableMessage>> cachedBillInfo = new HashMap<String, List<TableMessage>>();
	private static HashMap<String, List<TableMessage>> cachedDepositInfo = new HashMap<String, List<TableMessage>>();
	
	private static HashMap<String, String> billAccountMap;
	private static HashMap<String, String> billIDAccountMap;
	private static HashMap<String, String> accountIDTypeMap;
	private AsyncCallback<List<TableMessage>> billAccountcallback = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Error loading values<br/>" + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			result.remove(0); //remove header
			billAccountMap = new HashMap<String, String>();
			billIDAccountMap = new HashMap<String, String>();
			accountIDTypeMap = new HashMap<String, String>();
			for(TableMessage m : result)
				billAccountMap.put(m.getText(0), m.getText(1));
			GWT.log("ACCT MAP SIZE: " + billAccountMap.size());
		}
	};
	
	final AsyncCallback<List<TableMessage>> billListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve billing data for " + selectionModel.getSelectedObject().getText(0));
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 0)
			{
				//tenant-id to bill list mapping
				cachedBillInfo.put(selectionModel.getSelectedObject().getMessageId(), result);
				populateBillBox(result, unpaid.getValue());
				
				//go fetch the account information
				String tenantID = selectionModel.getSelectedObject().getMessageId();
				PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(tenantID, accountListCallBack);
			}
			else
			{
				GWT.log("Selected student is: " + selectionModel.getSelectedObject().getMessageId());
				errorBox.show("No bills found for this student!!Please generate an invoice before attempting to settle payments.");
			}
		}
	};

	final AsyncCallback<List<TableMessage>> accountListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve billing data for " + selectionModel.getSelectedObject().getText(0));
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			if(result.size() > 1)
			{
				String tenantID = result.remove(0).getMessageId();
				cachedDepositInfo.put(tenantID, result);
				populateDepositBox(result);
				enableFields(true);
			}
			else
			{
				GWT.log("Selected tenant is: " + selectionModel.getSelectedObject().getMessageId());
				errorBox.show("No funded deposits found for this Tenant!!Please enter deposit info for the tenant first.");
			}
		}
	};	
	
	final AsyncCallback<String> saveCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			infoBox.show(caught.getMessage());
			savePayment.setEnabled(true);
		}

		@Override
		public void onSuccess(String result) {
			infoBox.show(result);
			cachedBillInfo.remove(selectionModel.getSelectedObject().getMessageId());
			cachedDepositInfo.remove(selectionModel.getSelectedObject().getMessageId());
			billList.clear();
			accountList.clear();			
			clearFields(false);
			PanelUtilities.ACCT_SERVICE.getStudentBills(selectionModel.getSelectedObject().getMessageId(), billListCallBack);
		}
	};	
	
	
	/**
	 * The UiBinder interface used by this example.
	 */
	interface Binder extends UiBinder<Widget, AddTenantPayment> {
	}


	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource contact();
	}

	/**
	 * The Cell used to render a {@link ContactInfo}.
	 */

	static class ContactCell extends AbstractCell<TableMessage> {

		/**
		 * The html of the image used for contacts.
		 */
		private final String imageHtml;

		public ContactCell(ImageResource image) {
			this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}

		@Override
		public void render(Context context, TableMessage value,
				SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}
            String archiveColorStyle = "";
			sb.appendHtmlConstant("<table" + archiveColorStyle + ">");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td rowspan='3'>");
			sb.appendHtmlConstant(imageHtml);
			sb.appendHtmlConstant("</td>");

			String firstName = value.getText(RegistryDTO.FIRST_NAME_IDX);
			firstName = firstName.length() == 0 ? "" : firstName + " ";
			String fullName = firstName
					+ value.getText(RegistryDTO.LAST_NAME_IDX);
			// Add the name and address.
			sb.appendHtmlConstant("<td style='font-size:95%;'>");
			sb.appendEscaped(fullName);
			sb.appendHtmlConstant("</td></tr><tr><td>");
			sb.appendEscaped(value.getText(RegistryDTO.STUD_DIPL_IDX) + " "
					+ value.getText(RegistryDTO.STUD_DEPT_IDX));
			sb.appendHtmlConstant("</td></tr><tr><td style='font-size:70%;'>");
			sb.appendEscaped("Matric: " + value.getText(RegistryDTO.MATRIC_NO_IDX));
			sb.appendHtmlConstant("</td></tr></table>");
		}
	}

	/**
	 * The CellList.
	 */
	
	void enableBoxes(boolean enabled)
	{
		department.setEnabled(enabled);
		diploma.setEnabled(enabled);
		year.setEnabled(enabled);
	}

	private CellList<TableMessage> cellList;
    
    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	TableMessageHeader header = (TableMessageHeader) result.remove(0);
        	String buildingID = header.getMessageId();
        	List<TableMessage> tenantList = pc.getTenantInfoList(buildingID + archive.isCurrent());
        	tenantList.addAll(result);
    		//add display to the new data source
    		pc.addViewToTenantListDataProvider(cellList, buildingID + archive.isCurrent());        	
        	enableBoxes(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve building names. Try refreshing your browser. " +
        			"Contact " + NameTokens.HELP_ADDRESS + " if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        }
    };      

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public AddTenantPayment() {
		super("Approve Charges", "Confirm student invoice/bill. When an invoice is created, " +
				"it doesn't get charged to a student account until the invoice has been approved/confirmed" +
				" via this module. This serves as a control measure.");
	}

	private void populateBillBox(List<TableMessage> billInfo, boolean filterUnpaid)
	{
		billList.clear();
		for(TableMessage m : billInfo)
			if(!(filterUnpaid && isPaid(m)))
			{
				GWT.log(m.getMessageId() + " and " + m.getText(1) + " and " + billAccountMap.get(m.getText(1)));
				billIDAccountMap.put(m.getMessageId(), billAccountMap.get(m.getText(1)));
				billList.addItem(m.getText(0), m.getMessageId());
			}
		displaySelectedBillInfo();
	}
	
	private void populateDepositBox(List<TableMessage> deposits)
	{
		accountList.clear();
		for(TableMessage m : deposits)
		{
			String type = m.getText(DTOConstants.RSD_ACCT_TYPE_IDX);
			String amt = NumberFormat.getDecimalFormat().format(m.getNumber(DTOConstants.RSD_ACCT_AMT_IDX));
			String ccy = m.getText(DTOConstants.RSD_ACCT_CURR_IDX);
			accountIDTypeMap.put(m.getMessageId(), type);			
			accountList.addItem(GUIConstants.getAcctDisplayName(type, ccy) + " / "  + amt , m.getMessageId());
		}
	}
	
	private TableMessage getBillInfoFromCache(String studentID, String billID, HashMap<String, List<TableMessage>> cache)
	{
		List<TableMessage> billInfoList = cache.get(studentID);
		for(TableMessage m : billInfoList)
			if(m.getMessageId().equals(billID))
				return m;
		return null;
	}
	
	
	private void clearFields(boolean enable)
	{
		totalAmount.setText("");
		priorPayment.setText("");
		balance.setText("");
		description.setText(DEFAULT_TITLE);
		
		paymentAmount.setAmount(null);
		paymentDate.setValue(new Date());
		comments.setText("");
		enableFields(enable);
	}	
	
	private void enableFields(boolean enabled)
	{
		paymentAmount.setEnabled(enabled);
		paymentDate.setEnabled(enabled);
		comments.setEnabled(enabled);
		savePayment.setEnabled(enabled);		
	}
	
	/**
	 * Initialize this example.
	 */

	@Override
	public Widget onInitialize() {
		Images images = GWT.create(Images.class);
		
		// Create a CellList.
		ContactCell contactCell = new ContactCell(images.contact());

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		//cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);

		errorBox = new SimpleDialog("<font color='red'>Error Occured</font>", true);
		infoBox = new SimpleDialog("<font color='green'>INFO</font>", true);
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);
		paymentDate.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
		// server side async calls
		year.addChangeHandler(this);


		// Add the CellList to the data provider in the database.

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellList);
		//rangeLabelPager.setDisplay(cellList);

		totalAmount.setEnabled(false);
		priorPayment.setEnabled(false);
		balance.setEnabled(false);
		paymentDate.setValue(new Date());
		savePayment.setEnabled(false);
		billList.addChangeHandler(this);
		archive.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				switchBuildingDisplay();
			}
		});
		savePayment.addClickHandler(this);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {			
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				onValueChange();
			}
		});

		DiplDeptLB dipdl = new DiplDeptLB(diploma, department);
		new UniqueListBox(year, NameTokens.YEARS);
		dipdl.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				switchBuildingDisplay();
			}
		});
		switchBuildingDisplay();
		
		PanelUtilities.READ_SERVICE.getApplicationParameter(DTOConstants.APP_PARAM_RES_BILL_LIST_KEY, billAccountcallback);
		return widget;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(AddTenantPayment.class, new RunAsyncCallback() {

			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	@Override
	public void onChange(ChangeEvent event) {
		GWT.log("");
		if(event.getSource() == year)
			switchBuildingDisplay();
		else
		{			
			clearFields(false);
			displaySelectedBillInfo();			
		}
	}
	

	public void onValueChange() 
	{
	
		String selectedTenant = selectionModel.getSelectedObject() == null? null : 
			selectionModel.getSelectedObject().getMessageId();
		
		if(selectedTenant == null)
		{
			GWT.log("selected tenant is null");
			return;
		}
		
		billList.clear();
		accountList.clear();
		List<TableMessage> billInfoList = cachedBillInfo.get(selectedTenant);
		List<TableMessage> depositList = cachedDepositInfo.get(selectedTenant);
		if(billInfoList == null)
		{
			clearFields(false);
			description.setText("Fetching list of student bills. Please wait ...");
			PanelUtilities.ACCT_SERVICE.getStudentBills(selectedTenant, billListCallBack);
		}
		else
		{
			clearFields(true);
			GWT.log("BillPayment cache hit!!!");
			populateBillBox(billInfoList, unpaid.getValue());
			if(depositList != null)
				populateDepositBox(depositList);
		}
	}	

	private void switchBuildingDisplay() {
		enableBoxes(false);
		//remove our display from its current data source
		pc.removeDisplayFromTenantMap(cellList);
		
		//fetch list for newly created building
		String dipl = diploma.getValue(diploma.getSelectedIndex());
		String dept = department.getItemText(department.getSelectedIndex());
		String yr = year.getValue(year.getSelectedIndex());
		String providerID = NameTokens.getProviderID(dipl, dept, yr);
		List<TableMessage> tenantInfoList = pc.getTenantInfoList(providerID + archive.isCurrent());
		if(tenantInfoList.size() == 0) //nothing found locally, check server to see if data is available
		{
			PanelUtilities.READ_SERVICE.getAllStudents(dipl, dept, yr, archive.isCurrent(), tenantListCallback);
		}
		else
		{
			//add display to the new data source
			pc.addViewToTenantListDataProvider(cellList, providerID + archive.isCurrent());
			enableBoxes(true);
		}
	}
	
	private void displaySelectedBillInfo()
	{	
		GWT.log("Entered Display Function");
		TableMessage billInfo = getBillInfoFromCache(selectionModel.getSelectedObject().getMessageId(), 
				billList.getValue(billList.getSelectedIndex()), cachedBillInfo);
		double total = billInfo.getNumber(0);
		double paid = billInfo.getNumber(1);
		totalAmount.setText(NUMBER_FORMAT.format(total));
		priorPayment.setText(NUMBER_FORMAT.format(paid));
		balance.setText(NUMBER_FORMAT.format(total-paid));
		ccy.setText(billInfo.getText(2));

		if(isPaid(billInfo))
		{
			description.setText("This bill has already been confirmed");
		}
		else
		{
			enableFields(true);
			description.setText("Enter an amount matching outstanding balance to mark this invoice as paid");
		}
		GWT.log("leaving display");
	}
	
	private boolean isPaid(TableMessage billInfo)
	{
		//fully paid already?
		return billInfo.getText(2).equals("True");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) 
	{
		if(event.getSource() == unpaid)
		{
			onValueChange();
			return;
		}
		final Date payDate = paymentDate.getValue();
		final String description = comments.getText();
		final String billKey = billList.getValue(billList.getSelectedIndex());
		final String depKey = accountList.getValue(accountList.getSelectedIndex());		
		final Double amount = paymentAmount.getAmount();
		
		if(amount == null)
		{
			errorBox.show("Enter a valid number for payment amount");
			return;
		}
		
		String billCategory = billIDAccountMap.get(billKey);
		String acctCategory = accountIDTypeMap.get(depKey);
		GWT.log("Bill: " + billCategory + " Acct: " + acctCategory);
		if(billCategory.equals(acctCategory))
			doSave(billKey, depKey, amount, payDate, description);
		else
		{
			final YesNoDialog confirmSave = new YesNoDialog("WARNING - Invoice and Account mismatch");
			confirmSave.setClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					doSave(billKey, depKey, amount, payDate, description);
					confirmSave.hide();
				}
			});
			confirmSave.show("Invoice should be charged to a " + billCategory + 
					" account.<br/>Are you sure you want to charge/deduct from account: " + acctCategory + "?");
		}
		
		
	}
	
	public void doSave(String billKey, String depKey, double amount, Date payDate, String desc)
	{
		enableFields(false);
		infoBox.show("Saving payment. Please wait ...");
		PanelUtilities.ACCT_SERVICE.savePayment(billKey, depKey, amount, payDate, desc, saveCallBack);				
	}
	
}
