package com.fertiletech.nbte.adminclient.accounts;

import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.sun.corba.se.pept.transport.ContactInfo;

/**
 * The Cell used to render a {@link ContactInfo}.
 */

public class ContactCell extends AbstractCell<TableMessage> {

	/**
	 * The html of the image used for contacts.
	 */
	private final String imageHtml;

	public ContactCell(ImageResource image) {
		this.imageHtml = AbstractImagePrototype.create(image).getHTML();
	}
	
	public ContactCell() {
		this.imageHtml = "?";
	}
	
	public String getImageHTML()
	{
		return imageHtml;
	}
	

	@Override
	public void render(Context context, TableMessage value,
			SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
        String archiveColorStyle = "";
        
        //archiveColorStyle = " style='color: red;'";
		sb.appendHtmlConstant("<table" + archiveColorStyle + ">");

		// Add the contact image.
		sb.appendHtmlConstant("<tr><td rowspan='3'>");
		sb.appendHtmlConstant(imageHtml);
		sb.appendHtmlConstant("</td>");

		String firstName = value.getText(RegistryDTO.FIRST_NAME_IDX);
		firstName = firstName.length() == 0 ? "" : firstName + " ";
		String fullName = firstName
				+ value.getText(RegistryDTO.LAST_NAME_IDX);
		// Add the name and address.
		sb.appendHtmlConstant("<td style='font-size:80%;'>");
		sb.appendEscaped(fullName);
		sb.appendHtmlConstant("</td></tr><tr><td>");
		sb.appendEscaped(value.getText(RegistryDTO.STUD_DEPT_IDX) + " " + value.getText(RegistryDTO.ACADEMIC_YEAR));
		sb.appendHtmlConstant("</td></tr></table>");
	}
}