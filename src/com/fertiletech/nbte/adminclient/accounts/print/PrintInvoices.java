/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient.accounts.print;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.tables.TableMessageKeyProvider;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;

//import com.fertiletech.fieldcodata.ui.client.gui.LocalDataProvider;

/**
 * Example file.
 */

public class PrintInvoices extends ContentWidget {

	/*@UiField
	Label typeDisplay;
	@UiField
	CheckBox searchByCreationDate;*/
	@UiField
	DateBox startDate;
	@UiField
	DateBox endDate;

	@UiField
	Button search;
	@UiField
	Button print;
	@UiField
	HTML displayLinkSlot;

	private SimpleDialog errorBox;
	private SimpleDialog infoBox;
	private final static HashMap<String, List<TableMessage>> cachedPayments = new HashMap<String, List<TableMessage>>();

	// Create an asynchronous callback to handle the result.
	private final AsyncCallback<List<TableMessage>> tenantSearchCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onSuccess(List<TableMessage> result) {
			GWT.log("print searcher returned with: " + result.size());
			cellTable.showTable(result);
			print.setEnabled(true);
			search.setEnabled(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve invoices. Try refreshing your browser. "
					+ "Contact " + NameTokens.HELP_ADDRESS + " if problems persist. <p> Error msg: <b>"
					+ caught.getMessage() + "</b></p>");
			search.setEnabled(true);
		}
	};

	final AsyncCallback<String> invoiceCallBack = new AsyncCallback<String>() {
		@Override
		public void onFailure(Throwable caught) {
			search.setEnabled(true);
			print.setEnabled(false);
		}

		@Override
		public void onSuccess(String result) {
			infoBox.show("Setup for invoice/bill generation completed. "
					+ "Please click on link in status box to start download");
			displayLinkSlot.setHTML(result);
			search.setEnabled(true);
			print.setEnabled(false);
		}
	};

	/**
	 * The UiBinder interface used by this example.
	 */

	interface Binder extends UiBinder<Widget, PrintInvoices> {
	}

	@UiField(provided = true)
	ShowcaseTable cellTable;


	/**
	 * An instance of the constants.
	 */
	// MultiSelectionModel<TableMessage> selectionModel;
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public PrintInvoices() {
		super("Print Invoices", "Use this module to print out an invoice. Useful in instances where you want to issue a student an invoice for a particular fee");
 	}

	@Override
	public boolean hasMargins() {
		return false;
	}

	/**
	 * Initialize this example.
	 */
	@Override
	public Widget onInitialize() {
		// Create a CellTable.
		cellTable = new ShowcaseTable(true, true, true);

		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);

		// add click handlers
		search.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				print.setEnabled(false);
				search.setEnabled(false);
				displayLinkSlot.setHTML("");
				PanelUtilities.ACCT_SERVICE.getBills(startDate.getValue(), endDate.getValue(), tenantSearchCallback);
			}
		});

		print.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Set<TableMessage> chosen = cellTable.getSelectedSet();
				
				if(chosen.size() == 0)
				{
					errorBox.show("You must first select an invoice. Try searching first, then select the invoices you'd like to print");
					return;
				}
				
				String[] tenantKeys = new String[chosen.size()];
				int i = 0;
				for (TableMessage m : chosen)
					tenantKeys[i++] = m.getMessageId();

				search.setEnabled(false);
				print.setEnabled(false);
				PanelUtilities.ACCT_SERVICE.getInvoiceDownloadLink(tenantKeys,invoiceCallBack);
			}
		});
		print.setEnabled(false);
		search.setEnabled(true);
		infoBox = new SimpleDialog("INFO");
		errorBox = new SimpleDialog("Error");
		return widget;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(PrintInvoices.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}
