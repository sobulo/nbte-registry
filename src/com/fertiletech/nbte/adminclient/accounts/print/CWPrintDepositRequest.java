package com.fertiletech.nbte.adminclient.accounts.print;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWPrintDepositRequest extends ContentWidget{
	public CWPrintDepositRequest() {
		super("Deposit Requests", "Use this module to generate pdf invoices requesting deposits for selected student accounts. " +
				"All requests addressed to the same recipient will be grouped together");
	}

	@Override
	public Widget onInitialize() {
		return new PrintDepositRequests();
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWPrintDepositRequest.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}
