package com.fertiletech.nbte.adminclient.accounts.print;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWDepositStatements extends ContentWidget{

	public CWDepositStatements() {
		super("Account Statements", "Use this module to generate a printable statement of accounts" +
				" showing debits/credits on selected student accounts.");
	}

	@Override
	public Widget onInitialize() {
		return new PrintDepositStatement();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(CWDepositStatements.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}
	
	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}
