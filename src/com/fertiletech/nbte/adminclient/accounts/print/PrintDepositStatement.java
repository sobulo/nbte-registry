package com.fertiletech.nbte.adminclient.accounts.print;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.accounts.TenantSelectionList;
import com.fertiletech.nbte.adminclient.accounts.reps.RepSelector;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.SelectionChangeEvent;

public class PrintDepositStatement extends Composite implements ClickHandler{

	@UiField
	Button search;
	@UiField
	Button fetch;
	@UiField
	HTML link;
	@UiField(provided=true)
	ShowcaseTable display;
	@UiField
	SimplePanel selectionSlot;
	@UiField
	RadioButton useTenantList;
	@UiField
	RadioButton useCompanyList;
	TenantSelectionList tenantSelector;
	RepSelector repSelector;
	SimpleDialog errorBox;
	PopupPanel fetchPanel;
	DateBox fetchStart;
	DateBox fetchEnd;
	HashMap<String, List<TableMessage>> cachedDepositInfo = new HashMap<String, List<TableMessage>>();
	final AsyncCallback<List<TableMessage>> accountListCallBack = new AsyncCallback<List<TableMessage>>() {
		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve deposits for selected student");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			String tenantID = result.get(0).getMessageId();
			cachedDepositInfo.put(tenantID, result);
			display.showTable(result);
			search.setEnabled(true);
			fetch.setEnabled(true);
		}
	};	
	
	
	private static PrintDepositStatementUiBinder uiBinder = GWT
			.create(PrintDepositStatementUiBinder.class);

	interface PrintDepositStatementUiBinder extends
			UiBinder<Widget, PrintDepositStatement> {
	}

	public PrintDepositStatement() {
		display = new ShowcaseTable(true);
		fetchEnd = new DateBox();
		fetchStart = new DateBox();
		Button goFetch = new Button("Generate");
		Button cancel = new Button("Cancel");		
		Grid datesContainer = new Grid(3, 3);
		datesContainer.setWidget(0, 0, new Label("Start Date"));
		datesContainer.setWidget(0, 1, fetchStart);
		datesContainer.setWidget(1, 0, new Label("End Date"));
		datesContainer.setWidget(1, 1, fetchEnd);
		datesContainer.setWidget(2, 0, goFetch);
		datesContainer.setWidget(2, 1, cancel);
		VerticalPanel fetchContainer = new VerticalPanel();
		fetchContainer.setSpacing(10);
		fetchContainer.add(new Label("Select start & end dates to generate pdf report for"));
		fetchContainer.add(datesContainer);
		cancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				fetchPanel.hide();
			}
		});
		goFetch.addClickHandler(new ClickHandler() {

			private AsyncCallback<String> callback = new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					errorBox.show("Unable to fetch download link. Try again later");
					search.setEnabled(true);
				}

				@Override
				public void onSuccess(String result) {
					GWT.log("Received link " + result);
					search.setEnabled(true);
					link.setHTML("<div style='background-color:yellow'>" + result + "</div>");
				}
			};			
			
			@Override
			public void onClick(ClickEvent event) {
				Set<TableMessage> selectedDeposits = display.getSelectedSet();
				if(selectedDeposits.size() == 0)
				{
					errorBox.show("Please select a deposit first");
					return;
				}
				String[] depositIDs = new String[selectedDeposits.size()];
				int i = 0;
				for(TableMessage m : selectedDeposits)
					depositIDs[i++] = m.getMessageId();
				
				Date start = fetchStart.getValue();
				Date end = fetchEnd.getValue();
				
				if(start == null || end == null)
				{
					errorBox.show("Please select an account first");
					return;
				}
				TableMessage selected = getSelectedContact();
				search.setEnabled(false);
				fetch.setEnabled(false);
				PanelUtilities.ACCT_SERVICE.getDepositStatementDownloadLink(selected.getMessageId(), depositIDs, start, end, callback );
				fetchPanel.hide();
			}
		});
		fetchPanel = new PopupPanel(true);
		fetchPanel.add(fetchContainer);
		initWidget(uiBinder.createAndBindUi(this));
		useTenantList.addClickHandler(this);
		useCompanyList.addClickHandler(this);
		fetch.setEnabled(false);
		errorBox = new SimpleDialog("Error!");
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				TableMessage m = getSelectedContact();
				if(m == null)
				{
					errorBox.show("Please select student before searching for deposits");
					return;
				}
				List<TableMessage> cachedList = cachedDepositInfo.get(m.getMessageId());
				if(cachedList != null)
				{
					display.showTable(cachedList);
					fetch.setEnabled(true);
					return;
				}
				search.setEnabled(false);
				fetch.setEnabled(false);
				PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(m.getMessageId(), accountListCallBack);
			}
		});
		fetch.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				fetchPanel.showRelativeTo(fetch);
			}
		});
		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(display.getSelectedSet().size() == 0)
				{
					fetch.setEnabled(false);
				}
				else
				{
					fetch.setEnabled(true);
					link.setHTML("You've modified selected account list. Click fetch to generate another statement");
				}
			}
		});
		useTenantList.setValue(true);
		onClick(null);
	}
	
	private TableMessage getSelectedContact()
	{
		TableMessage m = null;
		if(useTenantList.getValue())
			m = tenantSelector.getSelectedTenant();
		else
			m = repSelector.getSelectedRep();
		return m;
	}
	
	private String getTenantName(TableMessage value)
	{
		String firstName = value.getText(RegistryDTO.FIRST_NAME_IDX);
		firstName = firstName.length() == 0 ? "" : firstName + " ";
		String fullName = firstName
				+ value.getText(RegistryDTO.LAST_NAME_IDX);
		return fullName;
	}

	@Override
	public void onClick(ClickEvent x) {
		if(useTenantList.getValue())
		{
			selectionSlot.clear();
			if(tenantSelector == null)
			{
				tenantSelector = new TenantSelectionList();
				tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
					
					@Override
					public void onValueChange(ValueChangeEvent<TableMessage> event) {
						clearSearchResults();						
					}
				});
			}
			selectionSlot.add(tenantSelector);
		}
		else if(useCompanyList.getValue())
		{
			selectionSlot.clear();
			if(repSelector == null)
			{
				repSelector = new RepSelector();
				repSelector.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						clearSearchResults();
					}
				});
			}
			selectionSlot.add(repSelector);
		}
	}
	
	public void clearSearchResults()
	{
		display.clear();
		link.setHTML("Search for student accounts, then select 1 or more accounts and click fetch button");
	}
}
