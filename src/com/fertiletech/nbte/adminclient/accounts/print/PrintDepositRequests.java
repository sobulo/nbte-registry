package com.fertiletech.nbte.adminclient.accounts.print;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.utils.AccountSelector;
import com.fertiletech.nbte.client.eventboxes.DiplDeptLB;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class PrintDepositRequests extends Composite {

	@UiField
	ListBox diploma;
	@UiField
	ListBox department;
	@UiField(provided=true)
	ShowcaseTable display;
	@UiField
	Button generateRequests;
	@UiField
	HTML linkSlot;
	@UiField
	CheckBox includeArchive;
		
	private static PrintDepositRequestsUiBinder uiBinder = GWT
			.create(PrintDepositRequestsUiBinder.class);

	interface PrintDepositRequestsUiBinder extends
			UiBinder<Widget, PrintDepositRequests> {
	}
	
	List<TableMessage> archivedAccounts = new ArrayList<TableMessage>();

	
	DiplDeptLB dipldept = null;
	public PrintDepositRequests() {
		display = new ShowcaseTable(null); //disable selection
		initWidget(uiBinder.createAndBindUi(this));
		dipldept = new DiplDeptLB(diploma, department);
		dipldept.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Error loading student accounts for the selected department: " + caught.getMessage());
					dipldept.setEnabled(true);
				}

				@Override
				public void onSuccess(List<TableMessage> result) {
					GWT.log("Returned with:******* " + result.size());
					dipldept.enableSelector(true);
					includeArchive.setEnabled(true);
					display.showTable(result);
					List<TableMessage> displayedResult = display.getDataList();
					for(TableMessage m : displayedResult)
					{
						String archiveStr = m.getText(DTOConstants.RSD_ACCT_ARCHIVE_IDX);
						if(archiveStr == null) continue;
						if(Boolean.valueOf(archiveStr))
						{
							archivedAccounts.add(m);
							display.highlightRow(m);
						}
					}
					includeArchive.setValue(false);
					updateDisplayArchive(false);
				}
			};

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				display.clear();
				includeArchive.setEnabled(false);
				dipldept.enableSelector(false);
				archivedAccounts.clear();
				PanelUtilities.ACCT_SERVICE.getResidentAccountsByBuilding(event.getValue(), callback );
			}
		});
		includeArchive.setEnabled(false);
		includeArchive.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				updateDisplayArchive(event.getValue());
			}
		});
		generateRequests.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) 
			{
				List<TableMessage> requests = new ArrayList<TableMessage>();
				for(TableMessage m : display.getDataList())
				{
					Double num = m.getNumber(DTOConstants.RSD_ACCT_TXN_IDX);
					if(num != null)
						requests.add(m);
				}
				
				if(requests.size() == 0)
				{
					Window.alert("You must enter a valid amount for at least one student account");
					return;
				}
				showArgs(requests);
			}
		});
	}
	
	public void updateDisplayArchive(boolean includeArchive)
	{
		List<TableMessage> displayedResult = display.getDataList();
		if(includeArchive)
		{
			Window.alert("Included " + archivedAccounts.size() + " accounts that belonging to departed student(s)");
			displayedResult.addAll(0, archivedAccounts);
		}
		else
			displayedResult.removeAll(archivedAccounts);
	}
	
	PopupPanel args;
	DateBox dateBox;
	AccountSelector acctSelector;
	TextBox descriptionBox;
	private void showArgs(final List<TableMessage> requests)
	{
		final Button c = new Button("Cancel");
		c.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				args.hide();
			}
		});
		final Button s = new Button("Fetch Link");
		s.addClickHandler(new ClickHandler() {
			
			private AsyncCallback<String> callback = new AsyncCallback<String>() {

				@Override
				public void onFailure(Throwable caught) {
					args.hide();
					Window.alert("Error occurred  " + caught.getMessage());
					generateRequests.setEnabled(true);
				}

				@Override
				public void onSuccess(String result) {
					args.hide();
					Window.alert("Click on the link displayed to download request headers.");
					linkSlot.setHTML("<span style='background-color:yellow'>" + result + "</div>");
					generateRequests.setEnabled(true);
				}
			};
			
			@Override
			public void onClick(ClickEvent event) {
				List<TableMessage> selectedAccounts = acctSelector.getSelectedValues();
				if(selectedAccounts.size() == 0)
				{	
					Window.alert("Select a company account!!");
					return;
				}		
				Date invDate = dateBox.getValue();
				if(invDate == null)
				{	
					Window.alert("Select a due date!!");
					return;
				}				
				String description = descriptionBox.getValue().trim().length() == 0? null: descriptionBox.getValue();
				if(description == null || description.trim().length() == 0)
				{
					Window.alert("Enter a value in the description field!!");
					return;
				}
				//final check, compare currencies
				if(!compareCurrencies(requests, selectedAccounts)) return;
				String[] companyAccts = new String[selectedAccounts.size()];
				for(int i = 0; i < companyAccts.length; i++)
					companyAccts[i] = selectedAccounts.get(i).getMessageId();
				c.setEnabled(false);
				s.setEnabled(false);
				generateRequests.setEnabled(false);
				PanelUtilities.ACCT_SERVICE.getDepositRequestLink(companyAccts, invDate, description, requests, callback);
			}
		});

		if(args == null)
		{
			dateBox = new DateBox();
			descriptionBox = new TextBox();
			descriptionBox.setValue("Amount(s) payable:");
			acctSelector = new AccountSelector(true);
			args = new PopupPanel();			
			args.setGlassEnabled(true);
			args.setAnimationEnabled(true);
			args.setSize("300px", "300px");
			args.setAutoHideEnabled(true);
			Grid g = new Grid(4, 2);
			g.setWidget(0, 0, new HTML("<b>Due Date:</b>"));
			g.getCellFormatter().setAlignment(0, 0, HasHorizontalAlignment.ALIGN_RIGHT, HasVerticalAlignment.ALIGN_TOP);
			g.setWidget(0, 1, dateBox);
			g.getCellFormatter().setAlignment(0, 1, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_TOP);
			g.setWidget(1, 0, new HTML("<b>Request Comment:</b>"));
			g.getCellFormatter().setAlignment(1, 0, HasHorizontalAlignment.ALIGN_RIGHT, HasVerticalAlignment.ALIGN_TOP);
			g.setWidget(1, 1, descriptionBox);
			g.getCellFormatter().setAlignment(1, 1, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_TOP);
			g.setWidget(2, 0, new HTML("<b>Bank Acct:</b>"));
			g.getCellFormatter().setAlignment(2, 0, HasHorizontalAlignment.ALIGN_RIGHT, HasVerticalAlignment.ALIGN_TOP);
			g.setWidget(2, 1, acctSelector);
			g.getCellFormatter().setAlignment(2, 1, HasHorizontalAlignment.ALIGN_RIGHT, HasVerticalAlignment.ALIGN_MIDDLE);
			args.add(g);
		}
		Grid container = (Grid) args.getWidget();
		container.setWidget(3, 0, null);
		container.setWidget(3, 1, null);		
		container.setWidget(3, 0, c);
		container.getCellFormatter().setAlignment(3, 0, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_TOP);
		container.setWidget(3, 1, s);
		container.getCellFormatter().setAlignment(3, 1, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_TOP);
		args.center();
	}
	
	boolean compareCurrencies(List<TableMessage> requests, List<TableMessage> companyAccounts)
	{
		int initSize = DTOConstants.CURRENCY_MAP_NAME.size();
		HashSet<String> requestCurrencies = new HashSet<String>(initSize);
		HashSet<String> accountCurrencies = new HashSet<String>(initSize);
		for(TableMessage req : requests)
			requestCurrencies.add(req.getText(DTOConstants.RSD_ACCT_CURR_IDX));
		for(TableMessage acct : companyAccounts)
			accountCurrencies.add(acct.getText(DTOConstants.CMP_ACCT_CURR_IDX));
		if(accountCurrencies.size() != companyAccounts.size())
		{
			Window.alert("One or more company accounts selected with the same currency. " +
					"When selecting multiple company bank accounts, currency must be unique");
			return false;
		}
		if(!accountCurrencies.equals(requestCurrencies))
		{
			Window.alert("Currency on selected student accounts isn't an exact match" +
					" with currency on selected college accounts" + requestCurrencies.toString() + " vs " + accountCurrencies.toString());
			return false;
		}
		return true;
	}

}
