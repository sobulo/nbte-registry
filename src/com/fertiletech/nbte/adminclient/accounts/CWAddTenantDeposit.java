package com.fertiletech.nbte.adminclient.accounts;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class CWAddTenantDeposit extends ContentWidget{

	public CWAddTenantDeposit() {
		super("Confirm Payments", "Use this module for recording deposits made by or on behalf of a student. " +
				"Amounts should have been confirmed as being deposited at the bank before it is entered here");
	}

	@Override
	public Widget onInitialize() {
		return new AddTenantDeposit();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(CWAddTenantDeposit.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}