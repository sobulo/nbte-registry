package com.fertiletech.nbte.adminclient.accounts;

import java.util.List;

import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.utils.DepositSearchPanel;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class ViewDepositBlotter extends Composite{
	@UiField
	DepositSearchPanel depositSearcher;
	
	@UiField(provided=true)
	ShowcaseTable display;


	private static ViewDepositBlotterUiBinder uiBinder = GWT
			.create(ViewDepositBlotterUiBinder.class);

	interface ViewDepositBlotterUiBinder extends
			UiBinder<Widget, ViewDepositBlotter> {
	}

	public ViewDepositBlotter() {
		display = new ShowcaseTable(null);
		// create widget from binder
		initWidget(uiBinder.createAndBindUi(this));

		depositSearcher
				.addValueChangeHandler(new ValueChangeHandler<List<TableMessage>>() {

					@Override
					public void onValueChange(
							ValueChangeEvent<List<TableMessage>> event) {
						if(event.getValue() == null)
							display.clear();
						else
							display.showTable(event.getValue());
					}
				});
	}

}
