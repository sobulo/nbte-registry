package com.fertiletech.nbte.adminclient.accounts;

import java.util.ArrayList;
import java.util.LinkedHashSet;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.utils.MoneySignBox;
import com.fertiletech.nbte.adminclient.utils.MultipleMessageDialog;
import com.fertiletech.nbte.adminclient.utils.ValuesListBox;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class AddBillTemplate extends ContentWidget implements ClickHandler, ValueChangeHandler<Integer>{

	@UiField DateBox dueDate;
	@UiField DateBox startDate;
	@UiField DateBox endDate;
	@UiField ValuesListBox billTemplateName;
	@UiField Button saveBill;
	@UiField SimplePanel editorSlot;
	@UiField MoneySignBox currencySign;
	
	private BillTemplateEditor billEditor;
	private MultipleMessageDialog errorBox;
	private SimpleDialog infoBox, simpleErrorBox;
	private final static String DEFAULT_BOX_WIDTH = "150px";
	
	private static AddBillTemplateUiBinder uiBinder = GWT
			.create(AddBillTemplateUiBinder.class);
 
	interface AddBillTemplateUiBinder extends UiBinder<Widget, AddBillTemplate> {
	}
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> billTemplateCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
        	billTemplateName.setValue("");
        	dueDate.setValue(null);
        	startDate.setValue(null);
        	endDate.setValue(null);
        	saveBill.setEnabled(true);
        	billEditor.clearEditorWidget();
            infoBox.show("Created invoice(s) [" + result + "]" + " succesfully.");
        }

        @Override
        public void onFailure(Throwable caught) {
        	String msg = "Try refreshing your browser. User request failed. Error is: " + caught.getMessage();
        	simpleErrorBox.show(msg);
            
        }
    };	

	public AddBillTemplate() {
		super("Create Invoice Template", "Use this module to create an invoice that can be addressed to 1 or more students");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final ArrayList<String> errors = new ArrayList<String>();
		final String title = billTemplateName.getValue();
		if(dueDate.getValue() == null)
			errors.add("Must enter a value for date bill is due");

		final LinkedHashSet<BillDescriptionItem> billDescs;
						
		billDescs = billEditor.getBillItems(1);
		if(billDescs.size() == 0)
			errors.add("Bill template must have at least 1 billable ITEM added");				
				
		
		final YesNoDialog confirmSplit = new YesNoDialog("Confirm Itemized Invoice");
		confirmSplit.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {																	
				//rpc call to persist bill
				saveBill.setEnabled(false);
				infoBox.show("Sending save request to server, please wait ...");
				PanelUtilities.ACCT_SERVICE.createBillTemplate(billDescs, title, 
						startDate.getValue(), endDate.getValue(), dueDate.getValue(), 
						currencySign.getValue(), billTemplateCallback);
				confirmSplit.hide();
			}
		});
		
		if(errors.size() > 0)
		{
			errorBox.show("Save request aborted because:", errors);
			return;
		}
		
		StringBuilder displayMessage = new StringBuilder("Invoice template below will now be created. Note that " +
				" this is just a template, you will need to select it later under create invoices to generate " +
				" student invoices <b>");
		displayMessage.append("<center><table width='90%' style='margin:5px; border-collapse:collapse' border='1' cellspacing='5' cellpadding='2'><tr><th>Item</th><th>Amount</th></tr>");
		for(BillDescriptionItem b : billDescs)
			displayMessage.append("<tr><td>").append(b.getName()).append("</td><td>").
				append(GUIConstants.DEFAULT_NUMBER_FORMAT.format(b.getAmount())).append("</td></tr>");
		displayMessage.append("</center></table>");
		confirmSplit.show(displayMessage.toString());
	}

	HTML totalDisplay;
	
	@Override
	public Widget onInitialize() {
		Widget w = uiBinder.createAndBindUi(this);

		totalDisplay = new HTML("Select students to allocate this Bill to");
		totalDisplay.setWidth("100%");
        billTemplateName.setWidth(DEFAULT_BOX_WIDTH);
        billTemplateName.loadBox(DTOConstants.APP_PARAM_RES_BILL_LIST_KEY);
        dueDate.setWidth(DEFAULT_BOX_WIDTH);
		dueDate.setValue(null);
		errorBox = new MultipleMessageDialog("Error!!");
		simpleErrorBox = new SimpleDialog("<font color='red'>Error</font>", true);
		infoBox = new SimpleDialog("<font color='green'>Info</font>", true);
		billEditor = new BillTemplateEditor();
		editorSlot.add(billEditor.getEditorWidget());
		saveBill.addClickHandler(this);
    	DateBox.DefaultFormat df = new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM)); 
    	dueDate.setFormat(df);
    	startDate.setFormat(df);
    	endDate.setFormat(df);		
		return w;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(AddBillTemplate.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_ADMIN_URL;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Integer> event) {
		totalDisplay.setHTML("Selected " + event.getValue() + " students");
	}

}
