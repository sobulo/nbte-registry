package com.fertiletech.nbte.adminclient.accounts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tickets.TicketSuggestBox;
import com.fertiletech.nbte.adminclient.utils.BillTemplateSelector;
import com.fertiletech.nbte.adminclient.utils.MultipleMessageDialog;
import com.fertiletech.nbte.adminclient.utils.TenantSelectionTreePanel;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class AddBillCW extends ContentWidget implements ClickHandler, ValueChangeHandler<Integer>{

	@UiField BillTemplateSelector billTemplateName;
	@UiField Button saveBill;
	@UiField ScrollPanel tenantAssignmentPanel;
	@UiField TicketSuggestBox ticketBox;
	
	private MultipleMessageDialog errorBox;
	private SimpleDialog infoBox, simpleErrorBox;
	private final static String DEFAULT_BOX_WIDTH = "150px";
	
	private static AddBillTemplateUiBinder uiBinder = GWT
			.create(AddBillTemplateUiBinder.class);
 
	interface AddBillTemplateUiBinder extends UiBinder<Widget, AddBillCW> {
	}
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> billTemplateCallback = new AsyncCallback<String>() {
        @Override
        public void onSuccess(String result) {
        	billTemplateName.selectNone();
        	tenantSelector.selectNone();
            infoBox.show(result);
        }

        @Override
        public void onFailure(Throwable caught) {
        	String msg = "Try refreshing your browser. User request failed. Error is: " + caught.getMessage();
        	simpleErrorBox.show(msg);
            
        }
    };	

	public AddBillCW() {
		super("Assign Invoice", "Use this module to generate invoices (via a template) for 1 or more students");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event.dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
		final ArrayList<String> errors = new ArrayList<String>();

		final String[] selectedTenants = tenantSelector.getSelectedTenants();
						
		if(selectedTenants == null)
		{
			errors.add("You must select at least one student to assign invoice/bill to");
		}
		
		if(!validateTemplate(billTemplateName.getSelectedItems(), tenantSelector.getSelectedTenantObjects()))
		{
			errors.add("Template validation failed");
		}
		
		final YesNoDialog confirmSplit = new YesNoDialog("Confirm Split");
		confirmSplit.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {																	
				//rpc call to persist bill
				saveBill.setEnabled(false);
				infoBox.show("Sending save request to server, please wait ...");
				PanelUtilities.ACCT_SERVICE.createUserBill(selectedTenants, getTemplateNames(), true,
						billTemplateCallback);
				confirmSplit.hide();
			}
		});
		
		if(errors.size() > 0)
		{
			errorBox.show("Save request aborted because:", errors);
			return;
		}
		
		StringBuilder displayMessage = new StringBuilder("Selected invoice template will be applied to each of the <b>");
		displayMessage.append(selectedTenants.length).append(" selected students");
		confirmSplit.show(displayMessage.toString());
	}
	
	String[] getTemplateNames()
	{
		Set<TableMessage> templates = billTemplateName.getSelectedItems();
		String[] result = new String[templates.size()];
		int i = 0;
		for(TableMessage m : templates)
			result[i++] = m.getMessageId();
		return result;
	}

	private TenantSelectionTreePanel tenantSelector;
	HTML totalDisplay;
	
	@Override
	public Widget onInitialize() {
		Widget w = uiBinder.createAndBindUi(this);
		ticketBox.setEnabled(false);
		ticketBox.setTitle("Billable request tickets currently not supported for FCAHPT");
		totalDisplay = new HTML("Select students to address this Bill to");
		totalDisplay.setWidth("100%");
		errorBox = new MultipleMessageDialog("Error!!");
		simpleErrorBox = new SimpleDialog("<font color='red'>Error</font>", true);
		infoBox = new SimpleDialog("<font color='green'>Info</font>", true);
		saveBill.addClickHandler(this);
		tenantSelector = new TenantSelectionTreePanel();
		tenantAssignmentPanel.add(tenantSelector);
		tenantSelector.setViewPortContent(totalDisplay);
		tenantSelector.addValueChangeHandler(this);
 		return w;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
	    GWT.runAsync(AddBillCW.class, new RunAsyncCallback() {

	        public void onFailure(Throwable caught) {
	          callback.onFailure(caught);
	        }

	        public void onSuccess() {
	          callback.onSuccess(onInitialize());
	        }
	      });		
	}
	
	@Override
	public boolean hasScrollableContent()
	{
		return false;
	}

	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

	@Override
	public void onValueChange(ValueChangeEvent<Integer> event) {
		totalDisplay.setHTML("Selected " + event.getValue() + " student(s)");
	}
	
	boolean validateTemplate(Collection<TableMessage> templates, Collection<TableMessage> students)
	{
		StringBuilder error = new StringBuilder();
		HashMap<String, TableMessage> typeMap = new HashMap<String, TableMessage>();
		if(templates.size() == 0)
			addError(error, "You must select at least 1 bill type");
		for(TableMessage bt : templates)
		{
			String acctType = bt.getText(DTOConstants.BT_TYPE_IDX);
			String title = bt.getText(DTOConstants.BT_TITLE_IDX);
			if(typeMap.containsKey(acctType))
				addError(error, "More than one bill selected with type " + acctType + ": " + 
						typeMap.get(acctType).getText(DTOConstants.BT_TITLE_IDX) + " and " + 
						title);
			String dipl = bt.getText(DTOConstants.BT_DIP_IDX);
			String yr = bt.getText(DTOConstants.BT_YR_IDX);
			for(TableMessage stud : students)
			{
				String studDipl = stud.getText(RegistryDTO.STUD_DIPL_IDX);
				if(dipl != null)
				{
					if(dipl.equals(NameTokens.NDX))
					{
						if(studDipl.equals(NameTokens.ALT_ND) && studDipl.equals(NameTokens.ALT_YEAR1))
						{
							addError(error, "Selected template " + title + " requires ND-Year 2 or HND students. Yet you selected " +
									stud.getText(RegistryDTO.LAST_NAME_IDX) + " who is a " + studDipl + "-" + stud.getText(RegistryDTO.STUD_YEAR_IDX) + 
									" student");							
						}
					}
					else
					{
						if(!dipl.equals(studDipl))
							addError(error, "Selected template " + title + " requires " + dipl + " students. Yet you selected " +
									stud.getText(RegistryDTO.LAST_NAME_IDX) + " who is a " + stud.getText(RegistryDTO.STUD_DIPL_IDX) + 
									" student");
						if(yr != null && !yr.equals(stud.getText(RegistryDTO.STUD_YEAR_IDX)))
							addError(error, "Selected template " + title + " requires " + yr + " students. Yet you selected " +
									stud.getText(RegistryDTO.LAST_NAME_IDX) + " who is a " + stud.getText(RegistryDTO.STUD_YEAR_IDX) + 
									" student");
						}
						
					}
			}
		}
		
		if(error.length() > 0)
		{
			PanelUtilities.errorBox.show("<ul>" + error.toString() + "</ul>");
			return false;
		}
		return true;
	}
	
	StringBuilder addError(StringBuilder errorBuffer, String val)
	{
		errorBuffer.append("<li>").append(val).append("</li>");
		return errorBuffer;
	}

}
