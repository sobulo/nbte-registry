package com.fertiletech.nbte.adminclient.accounts;

import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.TableMessageKeyProvider;
import com.fertiletech.nbte.adminclient.utils.ArchiveButton;
import com.fertiletech.nbte.adminclient.utils.DataProviderHelper;
import com.fertiletech.nbte.adminclient.widgs.ShowMorePagerPanel;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.eventboxes.DiplDeptLB;
import com.fertiletech.nbte.client.eventboxes.UniqueListBox;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class TenantSelectionList extends Composite implements ChangeHandler,
		HasValueChangeHandlers<TableMessage> {

	@UiField
	ArchiveButton archive;
	private static TenantSelectionListUiBinder uiBinder = GWT
			.create(TenantSelectionListUiBinder.class);

	interface TenantSelectionListUiBinder extends
			UiBinder<Widget, TenantSelectionList> {
	}

	DataProviderHelper pc;

	public TenantSelectionList() {
		initWidget(uiBinder.createAndBindUi(this));
		pc = new DataProviderHelper();
		archive.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				switchBuildingDisplay();
			}
		});
		Images images = GWT.create(Images.class);

		// Create a CellList.
		ContactCell contactCell = new ContactCell(images.contact());

		// Set a key provider that provides a unique key for each contact. If
		// key is
		// used to identify contacts when fields (such as the name and address)
		// change.
		cellList = new CellList<TableMessage>(contactCell,
				TableMessageKeyProvider.KEY_PROVIDER);
		// cellList.setPageSize(30);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		// Add a selection model so we can select cells.
		selectionModel = new SingleSelectionModel<TableMessage>(
				TableMessageKeyProvider.KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);

		errorBox = new SimpleDialog("<font color='red'>Error Occured</font>",
				true);

		// server side async calls
		year.addChangeHandler(this);

		// Add the CellList to the data provider in the database.

		// Set the cellList as the display of the pagers. This example has two
		// pagers. pagerPanel is a scrollable pager that extends the range when
		// the
		// user scrolls to the bottom. rangeLabelPager is a pager that displays
		// the
		// current range, but does not have any controls to change the range.
		pagerPanel.setDisplay(cellList);
		// rangeLabelPager.setDisplay(cellList);

		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						fireTenantChangeEvent();
					}
				});
		new UniqueListBox(year, NameTokens.YEARS);
		DiplDeptLB dipdl = new DiplDeptLB(diploma, department);
		dipdl.addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				switchBuildingDisplay();
			}
		});
		switchBuildingDisplay();
	}

	public void fireTenantChangeEvent() {
		ValueChangeEvent.fire(this, selectionModel.getSelectedObject());
	}

	SingleSelectionModel<TableMessage> selectionModel;
	/**
	 * The pager used to change the range of data.
	 */

	@UiField
	ShowMorePagerPanel pagerPanel;

	/**
	 * The pager used to display the current range.
	 */

	// @UiField
	// RangeLabelPager rangeLabelPager;

	@UiField
	ListBox diploma;

	@UiField
	ListBox department;

	@UiField
	ListBox year;

	private SimpleDialog errorBox;

	/**
	 * The images used for this example.
	 */
	static interface Images extends ClientBundle {
		ImageResource contact();
	}

	/**
	 * The CellList.
	 */

	private CellList<TableMessage> cellList;

	public void enableBoxes(boolean enabled) {
		year.setEnabled(enabled);
		diploma.setEnabled(enabled);
		department.setEnabled(enabled);
		archive.enableBox(enabled);
	}

	// Create an asynchronous callback to handle the result.
	private final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onSuccess(List<TableMessage> result) {
			TableMessageHeader header = (TableMessageHeader) result.remove(0);
			String buildingID = header.getMessageId();
			List<TableMessage> tenantList = pc.getTenantInfoList(buildingID
					+ (ignoreArchiveFlag ? "" : archive.isCurrent()));
			tenantList.clear();
			tenantList.addAll(result);
			// add display to the new data source
			pc.addViewToTenantListDataProvider(cellList, buildingID
					+ (ignoreArchiveFlag ? "" : archive.isCurrent()));
			enableBoxes(true);
		}

		@Override
		public void onFailure(Throwable caught) {
			errorBox.show("Unable to retrieve student names. Try refreshing your browser. "
					+ "Contact " + NameTokens.HELP_ADDRESS + " if problems persist. <p> Error msg: <b>"
					+ caught.getMessage() + "</b></p>");
		}
	};

	public TableMessage getSelectedTenant() {
		return selectionModel.getSelectedObject();
	}

	private void switchBuildingDisplay() {
		try {
			enableBoxes(false);
			// remove our display from its current data source
			pc.removeDisplayFromTenantMap(cellList);
			// fetch list for newly created building
			String dept = department.getValue(department.getSelectedIndex());
			String yr = year.getItemText(year.getSelectedIndex());
			String dipl = diploma.getValue(diploma.getSelectedIndex());

			String providerID = NameTokens.getProviderID(dipl, dept, yr);
			List<TableMessage> tenantInfoList = pc.getTenantInfoList(providerID
					+ (ignoreArchiveFlag ? "" : archive.isCurrent()));
			if (tenantInfoList.size() == 0) // nothing found locally, check
											// server to see if data is
											// available
			{
				PanelUtilities.READ_SERVICE.getAllStudents(dipl, dept, yr,
						archive.isCurrent(), tenantListCallback);
			} else {
				enableBoxes(true);
				// add display to the new data source
				pc.addViewToTenantListDataProvider(cellList, providerID
						+ (ignoreArchiveFlag ? "" : archive.isCurrent()));
			}
		} catch (Exception ex) {
			PanelUtilities.errorBox.show("Try refreshing. Error occured: "
					+ ex.getMessage());
		}
	}

	private String getProviderID()
	{
		String dept = department.getValue(department.getSelectedIndex());
		String yr = year.getItemText(year.getSelectedIndex());
		String dipl = diploma.getValue(diploma.getSelectedIndex());

		String providerID = NameTokens.getProviderID(dipl, dept, yr);
		return providerID;
	}
	
	public void refresh() {
		pc.refresh(getProviderID() + (ignoreArchiveFlag ? "" : archive.isCurrent()));
	}

	boolean ignoreArchiveFlag = false;

	public void ignoreArchiveSettings() {
		if (archive == null)
			return;
		archive.setVisible(false);
		ignoreArchiveFlag = true;
		switchBuildingDisplay();
	}

	@Override
	public void onChange(ChangeEvent event) {
		switchBuildingDisplay();
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<TableMessage> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
