package com.fertiletech.nbte.adminclient.accounts;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.MessageListToGrid;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class ResidentAccountBlotter extends Composite {

	@UiField Button search;
	@UiField DateBox endBox;
	@UiField DateBox startBox;
	@UiField(provided=true) 
	ShowcaseTable display;
	@UiField TenantSelectionList tenantSelector;
	@UiField ListBox accTypes;
	@UiField MessageListToGrid summary;
	SimpleDialog infoBox;

	HashMap<String, List<TableMessage>> cachedResidentAccts = new HashMap<String, List<TableMessage>>();
	protected AsyncCallback<List<TableMessage>> searchCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Search failed: <br/>" + caught.getMessage());
			search.setEnabled(true);
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			display.showTable(result);
			search.setEnabled(true);
		}
	};
	protected AsyncCallback<List<TableMessage>> residentChangeCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error loading resident accounts" + caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			cachedResidentAccts.put(result.get(0).getMessageId(), result);
			updateAccountTypes(result);
		}
	};
	
	private static ResidentAccountBlotterUiBinder uiBinder = GWT
			.create(ResidentAccountBlotterUiBinder.class);

	interface ResidentAccountBlotterUiBinder extends
			UiBinder<Widget, ResidentAccountBlotter> {
	}

	public ResidentAccountBlotter() {
		display = new ShowcaseTable(null);
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		search.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				search.setEnabled(false);
	
				String acctID = accTypes.getValue(accTypes.getSelectedIndex());
				PanelUtilities.ACCT_SERVICE.getLedgerAccountEntries(acctID, startBox.getValue(), endBox.getValue(), searchCallback);
			}
		});
		
		tenantSelector.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				if(event.getValue() == null) return;
				display.clear();
				accTypes.clear();
				summary.clear();
				search.setEnabled(false);
				List<TableMessage> m = cachedResidentAccts.get(event.getValue().getMessageId());
				if(m == null)
					PanelUtilities.ACCT_SERVICE.getResidentAccountInfo(event.getValue().getMessageId(), residentChangeCallback );
				else
					updateAccountTypes(m);
			}
		});
	}
	
	boolean validateSearchFields()
	{
		StringBuilder errors = new StringBuilder();
		if(accTypes.getItemCount() == 0)
			errors.append("Select a student account").append("<br/>");
		if(startBox.getValue() == null)
			errors.append("Enter a valid start date").append("<br/>");
		if(endBox.getValue() == null)
			errors.append("Enter a valid end date").append("<br/>");
		if(errors.length() > 0)
		{
			infoBox.show(errors.toString());
			return false;
		}
		return true;
	}
	
	public void updateAccountTypes(List<TableMessage> result)
	{
		boolean firstLine = true;	
		int[] colCounts = null;
		HashMap<String, Double> amountTotals = new HashMap<String, Double>();
		HashMap<String, Double> txnTotals = new HashMap<String, Double>();
		for(TableMessage m : result)
		{	
			if(firstLine){ 
				firstLine = false; 
				continue; 
			}				
			String curr = m.getText(DTOConstants.RSD_ACCT_CURR_IDX);
			accTypes.addItem(GUIConstants.getAcctDisplayName(m.getText(DTOConstants.RSD_ACCT_TYPE_IDX),curr), m.getMessageId());
			
			Double aTotal = 0.0;
			Double xTotal = 0.0;
			if(amountTotals.containsKey(curr))
			{
				aTotal = amountTotals.get(curr);
				xTotal = txnTotals.get(curr);
			}
			aTotal += m.getNumber(DTOConstants.RSD_ACCT_AMT_IDX);
			xTotal += m.getNumber(DTOConstants.RSD_ACCT_TXN_IDX);
			amountTotals.put(curr, aTotal);
			txnTotals.put(curr, xTotal);
			
			if(colCounts != null) continue;
			colCounts = new int[3];
			colCounts[0] = m.getNumberOfTextFields();
			colCounts[1] = m.getNumberOfDoubleFields();
			colCounts[2] = m.getNumberOfDateFields();
		}
		
		int footerCount = 0;
		if(colCounts != null)
		{	
			for(String curr : amountTotals.keySet())
			{
				TableMessage m = new TableMessage(colCounts[0], colCounts[1], colCounts[2]);
				m.setNumber(DTOConstants.RSD_ACCT_AMT_IDX, amountTotals.get(curr));
				m.setNumber(DTOConstants.RSD_ACCT_TXN_IDX, txnTotals.get(curr));
				m.setText(DTOConstants.RSD_ACCT_CURR_IDX, curr);
				m.setText(DTOConstants.RSD_ACCT_TYPE_IDX, "Total");
				result.add(m);
				footerCount++;
			}			
			summary.populateTable(result);
			
			for(int i = footerCount; i > 0; i--)
				result.remove(result.size() - i);
		}
		
		if(accTypes.getItemCount() > 0)
			search.setEnabled(true);
		else
			search.setEnabled(false);		
	}

}
