/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient.accounts;

import java.util.ArrayList;
import java.util.List;

import com.fertiletech.nbte.adminclient.ContentWidget;
import com.fertiletech.nbte.adminclient.HelpPageGenerator;
import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.Showcase;
import com.fertiletech.nbte.adminclient.tables.MessageListToGrid;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.adminclient.tickets.EditTicket;
import com.fertiletech.nbte.adminclient.tickets.TicketSuggestBox;
import com.fertiletech.nbte.adminclient.utils.BillSearchPanel;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.LongBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Example file.
 */

public class ViewBillBlotter extends ContentWidget {

	@UiField
	BillSearchPanel billSearcher;

	/**
	 * The UiBinder interface used by this example.
	 */

	interface Binder extends UiBinder<Widget, ViewBillBlotter> {
	}

	/**
	 * The main CellTable.
	 */
	@UiField
	ShowcaseTable display;
	TicketSuggestBox ticketBox;
	LongBox id;
	PopupPanel popPanel;
	FlowPanel container;
	

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public ViewBillBlotter() {
		super("Bill Blotter", "View list of recently created student invoices");
	}

	@Override
	public boolean hasMargins() {
		return false;
	}

	/**
	 * Initialize this example.
	 */
	@Override
	public Widget onInitialize() {
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		Widget widget = uiBinder.createAndBindUi(this);
		id = new LongBox();
		ticketBox = new TicketSuggestBox();
		popPanel = new PopupPanel();
		container = new FlowPanel();
		billSearcher
				.addValueChangeHandler(new ValueChangeHandler<List<TableMessage>>() {

					@Override
					public void onValueChange(
							ValueChangeEvent<List<TableMessage>> event) {
						List<TableMessage> result = event.getValue();
						if (result == null)
							return;
						boolean initTicket = false;
						if (!display.isInitialized())
							initTicket = true;

						display.showTable(result);
						if (initTicket) {
							setupTicketPopup();
							initTicketColumn();
						}
					}
				});

		display.addValueChangeHandler(new ValueChangeHandler<TableMessage>() {

			@Override
			public void onValueChange(ValueChangeEvent<TableMessage> event) {
				TableMessage object = event.getValue();
				if (object == null)
					return;
				String token = Showcase
						.getContentWidgetToken(CWSearchInvoice.class)
						+ "/"
						+ GUIConstants.DEFAULT_INTEGER_FORMAT.format(object
								.getNumber(2));
				GWT.log("about to fire: " + token);
				History.newItem(token);
			}
		});
		display.setEmptyWidget(new HTML(
				"<font color='#070779'>No results displayed. Use search bar above to find recent invoices"));
		return widget;
	}

	@Override
	public boolean hasScrollableContent() {
		return false;
	}

	private void initTicketColumn() {
		ButtonCell viewBill = new ButtonCell();
		Column<TableMessage, String> invCol = new Column<TableMessage, String>(
				viewBill) {

			@Override
			public String getValue(TableMessage object) {
				Double ticketID = object.getNumber(3);
				String msg = "Add";
				if (ticketID != null)
					msg = "View "
							+ GUIConstants.DEFAULT_INTEGER_FORMAT
									.format(ticketID);
				return msg;
			}
		};
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {

			@Override
			public void update(int index, TableMessage object, String value) {
				Double ticketID = object.getNumber(3);
				String msg = null;
				if (ticketID != null)
					msg = GUIConstants.DEFAULT_INTEGER_FORMAT.format(ticketID);
				if (msg == null) {
					billUndergoingTicketEdit = object;
					id.setValue(Math.round(object.getNumber(2)));
					popPanel.clear();
					ticketBox.clear();
					popPanel.setAutoHideEnabled(true);
					popPanel.add(container);
					popPanel.center();
				} else
					History.newItem(Showcase
							.getContentWidgetToken(EditTicket.class)
							+ "/"
							+ GUIConstants.TICKET_ID_PREFIX + msg);
			}
		});
		display.addColumn(invCol, "Ticket");
	}

	TableMessage billUndergoingTicketEdit;

	public void setupTicketPopup() {
		// setup popup panel
		popPanel.setWidth("300px");
		popPanel.setAutoHideEnabled(true);
		popPanel.setAnimationEnabled(true);

		// setup ticket assignment popup content
		final Button assignToTicket = new Button("Assign Ticket");
		container.add(id);
		container.add(ticketBox);
		container.add(assignToTicket);
		final MessageListToGrid grid = new MessageListToGrid();

		// setup grid display popup content
		final HTML headerDisplay = new HTML();
		final FlowPanel gridContainer = new FlowPanel();
		gridContainer.add(headerDisplay);
		gridContainer.add(grid);

		final AsyncCallback<List<TableMessage>> ticketAddcallback = new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				popPanel.hide();
				popPanel.setAutoHideEnabled(true);
				billUndergoingTicketEdit = null;
				Window.alert("Ticket assignment failed:" + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				grid.populateTable(result);
				TableMessage sample = result.get(1);  //if success, then at least 1 data row present
				billUndergoingTicketEdit.setNumber(3, sample.getNumber(3));
				display.refresh(billUndergoingTicketEdit);
				billUndergoingTicketEdit = null;
				popPanel.setAutoHideEnabled(true);
				popPanel.clear();
				popPanel.add(new ScrollPanel(gridContainer));
				popPanel.center();
			}
		};

		assignToTicket.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String ticketID = ticketBox.getSelectedUser();
				popPanel.hide();
				if (ticketID == null) {
					Window.alert("Select a valid ticket from suggest box. Type in ticket title or use format {123} to specify ticket id");
					return;
				}
				popPanel.setAutoHideEnabled(false);
				popPanel.clear();
				grid.clear();
				TableMessageHeader m = new TableMessageHeader(1);
				m.setText(0, "Assigning ticket, please wait...",
						TableMessageContent.TEXT);
				List<TableMessage> temp = new ArrayList<TableMessage>(1);
				temp.add(m);
				grid.populateTable(temp);
				popPanel.add(gridContainer);
				popPanel.center();
				Long idVal = id.getValue();

				headerDisplay
						.setHTML("<div style='background-color:black; color: silver;margin-bottom:5px; width:100%'> Invoice attachments for ticket: "
								+ ticketBox.getSelectedUserDisplay()
								+ "</div><br/><hr/>");
				PanelUtilities.TICKET_SERVICE.addInvoiceID(ticketID, idVal,
						true, ticketAddcallback);

			}
		});
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(ViewBillBlotter.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
}
