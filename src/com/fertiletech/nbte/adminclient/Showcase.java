/*
 * Copyright 2008 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient;

import java.util.ArrayList;
import java.util.List;

import com.fertiletech.nbte.adminclient.MainMenuTreeViewModel.Category;
import com.fertiletech.nbte.adminclient.contentwrappers.WelcomePanelCW;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.client.MyAsyncCallback;
import com.fertiletech.nbte.client.MyRefreshCallback;
import com.fertiletech.nbte.client.OAuthLoginService;
import com.fertiletech.nbte.client.UtilsService;
import com.fertiletech.nbte.client.UtilsServiceAsync;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.prefetch.Prefetcher;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadElement;
import com.google.gwt.dom.client.LinkElement;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.TreeNode;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

public class Showcase {

	private final static String ADMIN_PAGE_URL_PREFIX = GWT
			.getHostPageBaseURL();
	private final static String ADMIN_PAGE_URL;
	static {
		if (!GWT.isProdMode()) // true if dev mode
			ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX
					+ GUIConstants.GWT_CODE_SERVER;
		else
			ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX;
	}

	// different views
	ContentWidget requestedPanel; // last panel requested as part of history
									// token
	String requestArgs;
	UtilsServiceAsync loginUtil = GWT.create(UtilsService.class);

	private void fetchOpsUser() {
		if(!ClientUtils.alreadyLoggedIn())
		{
			//Window.alert("Not logged in, updating the app");			
			updateAdminApp();
			return;
		}
		
		final MyAsyncCallback<SocialUser> ensureLoginCallback = new MyAsyncCallback<SocialUser>() {

			@Override
			public void onSuccess(SocialUser result) {
				//Window.alert("Fetch returned, making a call to update the screen");
				updateAdminApp();
			}

			@Override
			public void onFailure(Throwable caught) {
				//async callback handles this already
			}

			@Override
			protected void callService(AsyncCallback<SocialUser> cb) {
				OAuthLoginService.Util.getInstance().fetchMe(
						ClientUtils.getSessionIdFromCookie(), cb);	
			}
		};
		//Window.alert("Making call to verify staff credentials");
		ensureLoginCallback.go("Verifying staff credentials...");
	}
	
	private void updateAdminApp()
	{
		if(ClientUtils.alreadyLoggedIn())
		{
			//Window.alert("Fetching user cookie");
			BankUserCookie usercookie = ClientUtils.BankUserCookie.getCookie();
			LoginRoles role = usercookie.getRole();
			boolean nonStaff = role.ordinal() >= LoginRoles.ROLE_SCHOOL_STUDENT.ordinal();
			if(nonStaff)
			{
				WelcomePanelCW firstPanel = getFirstWidget(shell
						.getMainMenu());
				if(requestedPanel != firstPanel)
				{
				
					if (usercookie.getEmail().contains("@fcahptib.edu.ng")) {
						PanelUtilities.errorBox.show("You do not have necessary privileges to view this page."
								+ " Contact 9jedu-helpdesk@fcahptib.edu.ng to request access.");
		
					} else {
						PanelUtilities.errorBox.show("You do not have necessary privileges to view this page. Only accounts under FCAHPT IB domain"
								+ " may log in. If you have a college google account, please log out of all your other Google accounts and try accessing the portal again");
					}

					shell.showAdminScreen();
					return;
				}
			}
			else
			{
				if(requestedPanel.getHelpUrl().equals(HelpPageGenerator.HELP_ADMIN_URL) && 
						!LoginRoles.ROLE_SCHOOL_ADMIN_EDIT.equals(role))
				{
					PanelUtilities.errorBox.show("You do not have necessary privileges to view this page."
							+ " Contact 9jedu-helpdesk@fcahptib.edu.ng to request access.");					
					shell.showAdminScreen();
					return;
				}
			}
		}
		else
		{
			WelcomePanelCW firstPanel = getFirstWidget(shell
					.getMainMenu());
			requestedPanel = firstPanel;
			requestArgs = null;			
		}
		loadNewPanel(); // whew, finally show the screen requested.
	}

	private void loadNewPanel() {
		GWT.log("LOAD Arg: " + requestArgs);
		if(requestedPanel instanceof MyRefreshCallback)
		{
			((MyRefreshCallback) requestedPanel).updateScreen();
		}
		shell.setContent(requestedPanel);
		if (requestedPanel instanceof CWArguments && requestArgs != null) {
			((CWArguments) requestedPanel).setParameterValues(requestArgs);
		}
		Window.setTitle("FCAHPT Registry - Staff Zone: " + requestedPanel.getName());
	}

	private String setupLink(String url, String display) {
		return "<a href='" + url + "'>" + display + "</a>";
	}



	/**
	 * The type passed into the
	 * {@link com.google.gwt.sample.showcase.generator.ShowcaseGenerator}.
	 */
	private static final class GeneratorInfo {
	}

	/**
	 * The static images used throughout the Showcase.
	 */
	public static final ShowcaseResources images = GWT
			.create(ShowcaseResources.class);

	/**
	 * The name of the style theme used in showcase.
	 */
	public static final String THEME = "clean";

	/**
	 * Get the token for a given content widget.
	 * 
	 * @return the content widget token.
	 */
	public static String getContentWidgetToken(ContentWidget content) {
		return getContentWidgetToken(content.getClass());
	}

	/**
	 * Get the token for a given content widget.
	 * 
	 * @return the content widget token.
	 */
	public static <C extends ContentWidget> String getContentWidgetToken(
			Class<C> cwClass) {
		String className = cwClass.getName();
		className = className.substring(className.lastIndexOf('.') + 1);
		return "do" + className;
	}

	/**
	 * The main application shell.
	 */
	private ShowcaseShell shell;

	/**
	 * This is the entry point method. At least it used to be, now we call it
	 * from PortalEntryPoint
	 */
	public Showcase() {
		// Generate the source code and css for the examples
		GWT.create(GeneratorInfo.class);

		// Inject global styles.
		injectThemeStyleSheet();
		images.css().ensureInjected();

		// Create the application shell.
		final SingleSelectionModel<ContentWidget> selectionModel = new SingleSelectionModel<ContentWidget>();
		final MainMenuTreeViewModel treeModel = new MainMenuTreeViewModel(
				selectionModel);
		// Set<ContentWidget> contentWidgets = treeModel.getAllContentWidgets();
		shell = new ShowcaseShell(treeModel);
		RootLayoutPanel.get().add(shell);

		// Prefetch examples when opening the Category tree nodes. init to 
		final List<Category> prefetched = new ArrayList<Category>();
		final CellTree mainMenu = shell.getMainMenu();
		mainMenu.addOpenHandler(new OpenHandler<TreeNode>() {
			public void onOpen(OpenEvent<TreeNode> event) {
				Object value = event.getTarget().getValue();
				if (!(value instanceof Category)) {
					return;
				}

				Category category = (Category) value;
				if (!prefetched.contains(category)) {
					prefetched.add(category);
					Prefetcher.prefetch(category.getSplitPoints());
				}
			}
		});

		// Always prefetch.						selectionModel.setSelected(getFirstWidget(mainMenu), true);
		Prefetcher.start();
		selectionModel.setSelected(getFirstWidget(mainMenu), true);
		final WelcomePanelCW firstPanel = getFirstWidget(shell
				.getMainMenu());
		requestedPanel = firstPanel;
		OAuthLoginService.Util.handleRedirect(new MyRefreshCallback() {

			@Override
			public void updateScreen() {
				setupHandlers(selectionModel, treeModel, mainMenu);
				updateAdminApp();
				// Show the initial example.
				if (History.getToken().length() > 0) {
					GWT.log("About to fire an initial history of: "
							+ History.getToken());
					History.fireCurrentHistoryState();
				}else {
					String requestUrl = Window.Location.getParameter("history");
					if (requestUrl != null && requestUrl.length() > 0) {
						History.newItem(requestUrl);
					} else
						selectionModel.setSelected(getFirstWidget(mainMenu), true);
				}				
			}
		});
	}

	private void setupHandlers(
			final SingleSelectionModel<ContentWidget> selectionModel,
			final MainMenuTreeViewModel treeModel, final CellTree mainMenu) {
		// Change the history token when a main menu item is selected.
		selectionModel
				.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						ContentWidget selected = selectionModel
								.getSelectedObject();
						GWT.log("Menu Selection MODEL change detected: "
								+ selected);
						if (selected != null) {
							if (selected == requestedPanel)
								return;
							String token = getContentWidgetToken(selected);
							GWT.log("Selection model adding token: " + token);
							History.newItem(token, true);
						}
					}
				});

		// Setup a history handler to reselect the associate menu item.
		final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<String>() {
			public void onValueChange(ValueChangeEvent<String> event) {
				GWT.log("HISTORY: " + event.getValue());
				String[] urlParams = event.getValue().split("/");
				String token = urlParams[0];
				String args = null;
				if (urlParams.length == 2 && urlParams[1].trim().length() > 0)
					args = urlParams[1].trim();
				GWT.log("TOKEN: " + token + " Args: " + args);
				showRequestedHistory(treeModel, mainMenu, selectionModel,
						token, args);
			}
		};
		History.addValueChangeHandler(historyHandler);
	}

	public WelcomePanelCW getFirstWidget(CellTree mainMenu) {
		TreeNode root = mainMenu.getRootTreeNode();
		TreeNode category = root.setChildOpen(0, true);
		ContentWidget content = (ContentWidget) category.getChildValue(0);
		return (WelcomePanelCW) content; // first widget in mainmenutreemodel
											// class def is a welcomepanel

	}

	public void showRequestedHistory(MainMenuTreeViewModel treeModel,
			CellTree mainMenu,
			SingleSelectionModel<ContentWidget> selectionModel, String token,
			String args) {
		// Get the content widget associated with the history token.
		ContentWidget contentWidget = treeModel.getContentWidgetForToken(token);
		GWT.log("Content: " + contentWidget);
		if (contentWidget == null) {
			return;
		}

		// Expand the tree node associated with the content.
		Category category = treeModel
				.getCategoryForContentWidget(contentWidget);
		TreeNode node = mainMenu.getRootTreeNode();
		int childCount = node.getChildCount();
		for (int i = 0; i < childCount; i++) {
			if (node.getChildValue(i) == category) {
				node.setChildOpen(i, true, true);
				break;
			}
		}

		// Window.alert("History Called");

		// Select the node in the tree. is below really necessary?
		selectionModel.setSelected(contentWidget, true);

		// Display the content widget.
		displayContentWidget(contentWidget, args);

	}

	/**
	 * Set the content to the {@link ContentWidget}.
	 * 
	 * @param content
	 *            the {@link ContentWidget} to display
	 */
	private void displayContentWidget(ContentWidget content, String args) {
		if (content == null) {
			GWT.log("Received a null panel request");
			return;
		}
		GWT.log("Received a panel request for: " + content.getName());
		// Window.alert("Display got Called: " + firstTime + " - " + content);
		requestedPanel = content;
		requestArgs = args;
		shell.showLoading();
		String redirectURL = GWT.getHostPageBaseURL() + "#"
				+ getContentWidgetToken(requestedPanel);
		if (args != null && args.length() > 0)
			redirectURL += ("/" + args);
		GWT.log("URL REDIRECT:" + redirectURL);
		//loadNewPanel();
		fetchOpsUser();
	}

	/**
	 * Convenience method for getting the document's head element.
	 * 
	 * @return the document's head element
	 */
	private native HeadElement getHeadElement() /*-{
		return $doc.getElementsByTagName("head")[0];
	}-*/;

	/**
	 * Inject the GWT theme style sheet based on the RTL direction of the
	 * current locale.
	 */
	private void injectThemeStyleSheet() {
		// Choose the name style sheet based on the locale.
		String styleSheet = "gwt/" + THEME + "/" + THEME;
		styleSheet += LocaleInfo.getCurrentLocale().isRTL() ? "_rtl.css"
				: ".css";

		// Load the GWT theme style sheet
		String modulePath = GWT.getModuleBaseURL();
		LinkElement linkElem = Document.get().createLinkElement();
		linkElem.setRel("stylesheet");
		linkElem.setType("text/css");
		linkElem.setHref(modulePath + styleSheet);
		getHeadElement().appendChild(linkElem);
	}
}
