package com.fertiletech.nbte.adminclient.utils;

import java.util.List;
import java.util.Set;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.tables.ShowcaseTable;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;

public class BillTemplateSelector extends Composite{

	ShowcaseTable templateList;
	public BillTemplateSelector()
	{
		templateList = new ShowcaseTable(true, true, true);
		initWidget(templateList);
		PanelUtilities.ACCT_SERVICE.getBillTemplateNames(new AsyncCallback<List<TableMessage>>() {

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error fetching template names. " + caught.getMessage());
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				templateList.showTable(result);
			}
		});
	}
	
	public Set<TableMessage> getSelectedItems()
	{
		return templateList.getSelectedSet();
	}
	
	public void selectNone()
	{
		templateList.selectNone();
	}
}
