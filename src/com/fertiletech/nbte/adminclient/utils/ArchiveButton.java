package com.fertiletech.nbte.adminclient.utils;

import java.util.Date;

import com.fertiletech.nbte.adminclient.widgs.YesNoDialog;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;

public class ArchiveButton extends Composite implements HasValueChangeHandlers<String>{
	public static String ARCHIVE_WARNING = "Warning, you've switched to archive view for a departed student." +
			" Most likely you're violating a college policy by accessing this panel, obtain approval " +
			" from the office of the registrar before proceeding.";
	
	RadioButton archive;
	RadioButton existing;
	YesNoDialog warningBox;
	public ArchiveButton() {
		HorizontalPanel widget = new HorizontalPanel();
		Date d = new Date();
		String grpName = "archive" + d.getTime();
		archive = new RadioButton(grpName, "Archived");
		existing = new RadioButton(grpName, "Current");
		widget.setSpacing(5);
		warningBox = new YesNoDialog("Potential Policy Violation Alert");
		warningBox.setClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				archive.setValue(true, false); //we don't fire since it'd just send us back here
				fireEvent(); //logical fire
				warningBox.hide();
			}
		});
		widget.add(existing);
		widget.add(archive);
		initWidget(widget);
		existing.setValue(true);
		archive.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				existing.setValue(true, false); //first we cancel the user click
				warningBox.show(ARCHIVE_WARNING + "<p>Do you wish to continue?</p>");				
			}
		});
		existing.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				fireEvent();
			}
		});
		fireEvent();
	}
	
	private void fireEvent()
	{
		
		ValueChangeEvent.fire(this, String.valueOf(isCurrent()));
	}
	
	public boolean isCurrent()
	{
		return !archive.getValue();
	}
	
	public void enableBox(boolean enable)
	{
		archive.setEnabled(enable);
		existing.setEnabled(enable);
	}


	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}
