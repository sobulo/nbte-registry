/**
 * 
 */
package com.fertiletech.nbte.adminclient.utils;

import java.util.ArrayList;

import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MultipleMessageDialog extends SimpleDialog{

	VerticalPanel messagesPanel;
	
	/**
	 * @param title
	 */
	public MultipleMessageDialog(String title) {
		super(title);
	}
	
	public MultipleMessageDialog(String title, boolean isHTMLTitle) {
		super(title, isHTMLTitle);
	}	

	@Override
	protected Widget getButtonPanel()
	{
		VerticalPanel panel = new VerticalPanel();
		
		Button ok = new Button("OK");
		ok.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				alertBox.hide();
			}
		});
		messagesPanel = new VerticalPanel();
		Label spaceBuffer = new Label(" ");
		spaceBuffer.setHeight("10px");
		Label spaceBuffer2 = new Label(" ");
		spaceBuffer2.setHeight("10px");		
		panel.add(spaceBuffer);
		panel.add(messagesPanel);
		panel.add(spaceBuffer2);
		panel.add(ok);
		panel.setCellHorizontalAlignment(ok, HorizontalPanel.ALIGN_CENTER);
		return panel;
	}
	
	public void show(String prompt, ArrayList<String> messages)
	{
		messagesPanel.clear();
		for(String msg : messages)
		{
			messagesPanel.add(new Label(msg));
		}
		super.show(prompt);
	}
	
	public void show(String message)
	{
		messagesPanel.clear();
		super.show(message);
	}
}
