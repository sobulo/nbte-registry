/**
 * 
 */
package com.fertiletech.nbte.adminclient.utils;

import com.fertiletech.nbte.adminclient.PanelUtilities;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class StudentSuggestBox extends UserSuggestBox {
	
	final boolean currentlyEnrolled;
	
	public StudentSuggestBox()
	{
		this(true);
	}
	
	public StudentSuggestBox(boolean currentlyEnrolled)
	{
		super();
		this.currentlyEnrolled = currentlyEnrolled;
		setDisplayLabel("Select Strudent");
	}

	@Override
	public void populateSuggestBox()
	{
		PanelUtilities.READ_SERVICE.getAllStudents(currentlyEnrolled, getPopulateCallBack());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.custom.widgets.UserSuggestBox#getRole()
	 */
	@Override
	public String getRole() 
	{
		return "students";
	}

}
