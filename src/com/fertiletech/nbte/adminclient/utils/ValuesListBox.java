package com.fertiletech.nbte.adminclient.utils;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;

public class ValuesListBox extends Composite implements HasValue<String>{
	private ListBox values;
	private AsyncCallback<List<TableMessage>> callback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			values.addItem("ERROR");
			new SimpleDialog("ERROR LOADING VALUES").show(caught.getMessage());
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			result.remove(0); //remove header
			for(TableMessage m : result)
				values.addItem(m.getText(0));
			if(result.size() > 0)
				values.setEnabled(true);
		}
	};
	
	public ValuesListBox() {
		values = new ListBox();
		values.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				ValueChangeEvent.fire(ValuesListBox.this, getValue());
			}
		});
		initWidget(values);
	}
	
	public ValuesListBox loadBox(String id)
	{
		values.clear();
		values.setEnabled(false);
		PanelUtilities.READ_SERVICE.getApplicationParameter(id, callback);
		return this;
	}
	
	public ValuesListBox loadBox(HashMap<String, String> options)
	{
		values.clear();
		for(String k : options.keySet())
			values.addItem(options.get(k), k);
		if(values.getItemCount() == 0)
			values.setEnabled(false);
		else
			ValueChangeEvent.fire(this, getValue());
		return this;
	}	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}

	@Override
	public String getValue() {
		if(values.getItemCount() == 0 || !values.isEnabled())
			return null;
		return values.getValue(values.getSelectedIndex());
	}

	@Override
	public void setValue(String value) {
		if(values.getItemCount() > 0)
			values.setSelectedIndex(0);
		
		for(int i = 1; i < values.getItemCount(); i++)
			if(values.getValue(i).equals(value))
				values.setSelectedIndex(i);
	}

	@Override
	public void setValue(String value, boolean fireEvents) {
		setValue(value);
		ValueChangeEvent.fire(this, value);
	}
}
