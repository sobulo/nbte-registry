package com.fertiletech.nbte.adminclient.utils;

import java.util.Date;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class DepositSearchPanel extends Composite implements HasValueChangeHandlers<List<TableMessage>>
{
	@UiField DateBox startDateBox;
	@UiField DateBox endDateBox;
	@UiField Button searchDeposit;
	private SimpleDialog errorBox;
	private String buildingID;
	private Boolean balanceExists;
	
	private static DepositSearchPanelUiBinder uiBinder = GWT
			.create(DepositSearchPanelUiBinder.class);

	interface DepositSearchPanelUiBinder extends
			UiBinder<Widget, DepositSearchPanel> {}

	public DepositSearchPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new SimpleDialog("Error Loading Bills");
		searchDeposit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Date startDate = startDateBox.getValue();
				Date endDate = endDateBox.getValue();
				if(startDate == null)
				{
					errorBox.show("Please enter a valid start date");
					return;
				}
				if(endDate == null)
				{
					errorBox.show("Please enter a valid end date");
					return;
				}
				if(startDate.after(endDate))
				{
					errorBox.show("Start date can not be greater than end date");
					return;
				}
				GWT.log("Making a deposit search request" );
				GWT.log("Search with BID: " + buildingID + " and " + balanceExists);
				PanelUtilities.ACCT_SERVICE.getDeposits(startDate, endDate, billListCallback);
			}
		});		
	}
	
	public void setAdditionalSearchParameters(String buildingID, Boolean hasBalance)
	{
		GWT.log("Search with BID: " + buildingID + " and " + hasBalance);
		this.buildingID = buildingID;
		this.balanceExists = hasBalance;
	}

    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> billListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) 
        {
        	GWT.log("Search returned with: " + result.size());
        	fireDepositListKnown(result);
         	searchDeposit.setEnabled(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve invoices. Try refreshing your browser. " +
        			"Contact " + NameTokens.HELP_ADDRESS + " if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        	searchDeposit.setEnabled(true);
        }
    };	
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<List<TableMessage>> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	private void fireDepositListKnown(List<TableMessage> result)
	{
		ValueChangeEvent.fire(this, result);
	}
	
	
}
