package com.fertiletech.nbte.adminclient.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.Widget;

public abstract class UserSuggestBox extends Composite 
	implements HasValueChangeHandlers<String>, SelectionHandler<Suggestion>
{
	
	//abstract methods
	protected abstract void populateSuggestBox();
	public abstract String getRole();

	@UiField
	SuggestBox userBox;
	
	@UiField
	SimplePanel labelSlot;
	
	private static HashMap<String, HashMap<String, String>> cachedSuggestions = 
		new HashMap<String, HashMap<String, String>>();
	
	private HashMap<String, String> hackedSuggestions;
	private SimpleDialog alertBox;
	private String oldVal = "";		
    
	private final AsyncCallback<HashMap<String, String>> listCallback = new AsyncCallback<HashMap<String, String>>() {
		@Override
		public void onFailure(Throwable caught) {
			alertBox.show("Request failed: " + caught.getMessage());
		}

		@Override
		public void onSuccess(HashMap<String, String> result) {
			GWT.log("Retrieved: " + result.size() + " from server");
			setupSuggestBox(result);
			cachedSuggestions.put(getRole(), result);
		}
	};

	private static UserSuggestBoxUiBinder uiBinder = GWT
			.create(UserSuggestBoxUiBinder.class);

	interface UserSuggestBoxUiBinder extends UiBinder<Widget, UserSuggestBox> {
	}

	public UserSuggestBox() {
		this(null);
	}
	
	public UserSuggestBox(Widget labelWidget)
	{
		initWidget(uiBinder.createAndBindUi(this));
		hackedSuggestions = new HashMap<String, String>();
		userBox.addSelectionHandler(this);
		if(labelWidget == null)
			labelWidget = new Label("User");
		labelSlot.add(labelWidget);
		// setup alert box
		alertBox = new SimpleDialog("An error occured");
		initSuggestBox();
		
	}
	
	public AsyncCallback<HashMap<String, String>> getPopulateCallBack()
	{
		return listCallback;
	}
	
	protected void setDisplayLabel(String displayLabel)
	{
		Widget w = labelSlot.getWidget();
		if(w instanceof HasText)
			((HasText) w).setText(displayLabel);
	}
	
	protected Widget getDisplayWidget()
	{
		return labelSlot.getWidget();
	}
	
	
	private void initSuggestBox()
	{
		String cacheKey = getRole();
		if(cachedSuggestions.containsKey(cacheKey))
		{
			GWT.log("USER BOX HIT");
			setupSuggestBox(cachedSuggestions.get(cacheKey));
		}
		else
			populateSuggestBox();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.logical.shared.SelectionHandler#onSelection(com.
	 * google.gwt.event.logical.shared.SelectionEvent)
	 */
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		String oldValue = oldVal;
		GWT.log("OLD VAL: " + oldValue);
		
		// TODO Auto-generated method stub
		Suggestion suggest = event.getSelectedItem();
		GWT.log("Suggest Display: " + suggest.getDisplayString());
		GWT.log("Suggest Replace: " + suggest.getReplacementString());
		String newValue = hackedSuggestions.get(suggest.getReplacementString());	
		if(newValue.length() > 0)
		{
			ValueChangeEvent.fireIfNotEqual(this, oldValue, newValue);
			oldVal = newValue;
		}
		GWT.log("NEW VAL: " + newValue);
	}


	private void setupSuggestBox(HashMap<String, String> vals) {
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) userBox
				.getSuggestOracle();
		oracle.clear();
		hackedSuggestions.clear();
		Iterator<Map.Entry<String, String>> valItr = vals.entrySet().iterator();
		while (valItr.hasNext()) {
			Map.Entry<String, String> item = valItr.next();
			oracle.add(item.getValue());
			hackedSuggestions.put(item.getValue(), item.getKey());
		}
	}

	public String getSelectedUser() {
		String key = userBox.getValue();
		return hackedSuggestions.get(key);
	}
	
	public String getSelectedUserDisplay()
	{
		return userBox.getValue();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.HasValueChangeHandlers#addValueChangeHandler(com.google.gwt.event.logical.shared.ValueChangeHandler)
	 */
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	public void setEnabled(boolean enabled)
	{
		userBox.getValueBox().setEnabled(enabled);
	}
	
	public void clear()
	{
		oldVal = "";
		userBox.setValue("");
	}	
}
