package com.fertiletech.nbte.adminclient.utils;

import com.fertiletech.nbte.shared.DTOConstants;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;

public class MoneySignBox extends Composite{
	
	ListBox currency;
	public MoneySignBox()
	{
		currency = new ListBox();
		for(String key : DTOConstants.CURRENCY_MAP_NAME.keySet())
			currency.addItem(key);
		initWidget(currency);
		reset();
	}
	
	public void setVal(String currencyVal)
	{
		if(currencyVal == null)
		{
			reset();
			return;
		}
		
		for(int i = 0; i < currency.getItemCount(); i++)
		{
			if(currency.getValue(i).equals(currencyVal))
			{
				currency.setSelectedIndex(i);
				return;
			}
		}
	}
	
	public void reset()
	{
		setVal(DTOConstants.DEFAULT_CURRENCY);
	}
	
	public void enableBox(boolean enable)
	{
		currency.setEnabled(enable);
	}
	
	public String getValue()
	{
		if(currency.getItemCount() == 0)
			return null;
		else
			return currency.getValue(currency.getSelectedIndex());
	}
	
	public String getDescription()
	{
		String val = getValue();
		if( val == null) return null;
		return DTOConstants.CURRENCY_MAP_NAME.get(val);
	}
	
	public String getSymbol()
	{
		String val = getValue();
		if( val == null) return null;
		return DTOConstants.CURRENCY_MAP_SIGN.get(val);
	}
	
	public String getSign()
	{
		return getValue();
	}
	
	public HandlerRegistration addUserChangeHander(ChangeHandler handler)
	{
		return currency.addChangeHandler(handler);
	}
}
