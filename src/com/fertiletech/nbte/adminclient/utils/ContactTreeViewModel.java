/*
 * Copyright 2010 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.fertiletech.nbte.adminclient.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.accounts.ContactCell;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;
import com.google.gwt.view.client.TreeViewModel;

/**
 * The {@link TreeViewModel} used to organize contacts into a hierarchy.
 */
class ContactTreeViewModel implements TreeViewModel {

  /**
   * The images used for this example.
   */
  static interface Images extends ClientBundle {
    ImageResource contact();

    ImageResource contactsGroup();
  }

  //LocalDataProvider pc = new LocalDataProvider();
  /**
   * The cell used to render categories.
   */
  private static class CategoryCell extends AbstractCell<String> {

    /**
     * The html of the image used for contacts.
     */
    private final String imageHtml;

    public CategoryCell(ImageResource image) {
      this.imageHtml = AbstractImagePrototype.create(image).getHTML();
    }

    @Override
    public void render(Context context, String value, SafeHtmlBuilder sb) {
      if (value != null) {
        sb.appendHtmlConstant(imageHtml).appendEscaped(" ");
        sb.appendHtmlConstant("<b>").appendEscaped(value).appendHtmlConstant("</b>");
      }
    }
  }

  /**
   * The static images used in this model.
   */
  private static Images images;

  
  private final Cell<TableMessage> contactCell;
  private final DefaultSelectionEventManager<TableMessage> selectionManager =
      DefaultSelectionEventManager.createCheckboxManager();
  private final DefaultSelectionEventManager<String> selectionManager1 =
	      DefaultSelectionEventManager.createDefaultManager();  
  private final SelectionModel<TableMessage> selectionModel;
  private final SingleSelectionModel<String> selectionModel1;
  public ContactTreeViewModel(final SelectionModel<TableMessage> selectionModel, SingleSelectionModel<String> rootModel) {
	  selectionModel1 = rootModel;
	  selectionModel1.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
		
		@Override
		public void onSelectionChange(SelectionChangeEvent event) {
			List<String> choices = getBuildingNamesProvider().getList();
			for(String opt : choices)
			{
				boolean selected = selectionModel1.isSelected(opt);
				QueryArgStruct args = buildingNamesMap.get(opt);
				List<TableMessage> students = getBuildingTenantsDataProvider(args.diploma, 
						args.department, args.year).getList();
				for(TableMessage stud : students)
					selectionModel.setSelected(stud, selected);
			}
		}
	});
    this.selectionModel = selectionModel;
    if (images == null) {
      images = GWT.create(Images.class);
    }

    // Construct a composite cell for contacts that includes a checkbox.
    List<HasCell<TableMessage, ?>> hasCells = new ArrayList<HasCell<TableMessage, ?>>();
    hasCells.add(new HasCell<TableMessage, Boolean>() {

      private CheckboxCell cell = new CheckboxCell(true, false);

      public Cell<Boolean> getCell() {
        return cell;
      }

      public FieldUpdater<TableMessage, Boolean> getFieldUpdater() {
        return null;
      }

      public Boolean getValue(TableMessage object) {
        return selectionModel.isSelected(object);
      }
    });
    hasCells.add(new HasCell<TableMessage, TableMessage>() {

      private ContactCell cell = new ContactCell(images.contact());

      public Cell<TableMessage> getCell() {
        return cell;
      }

      public FieldUpdater<TableMessage, TableMessage> getFieldUpdater() {
        return null;
      }

      public TableMessage getValue(TableMessage object) {
        return object;
      }
    });
    contactCell = new CompositeCell<TableMessage>(hasCells) {
      @Override
      public void render(Context context, TableMessage value, SafeHtmlBuilder sb) {
        sb.appendHtmlConstant("<table><tbody><tr>");
        super.render(context, value, sb);
        sb.appendHtmlConstant("</tr></tbody></table>");
      }

      @Override
      protected Element getContainerElement(Element parent) {
        // Return the first TR element in the table.
        return parent.getFirstChildElement().getFirstChildElement().getFirstChildElement();
      }

      @Override
      protected <X> void render(Context context, TableMessage value,
          SafeHtmlBuilder sb, HasCell<TableMessage, X> hasCell) {
        Cell<X> cell = hasCell.getCell();
        sb.appendHtmlConstant("<td>");
        cell.render(context, hasCell.getValue(value), sb);
        sb.appendHtmlConstant("</td>");
      }
    };
  }

  public <T> NodeInfo<?> getNodeInfo(T value) {
    if (value == null) {
      // Return top level categories.
      return new DefaultNodeInfo<String>(getBuildingNamesProvider(),
          new CategoryCell(images.contactsGroup()), selectionModel1, selectionManager1, null);
    } else if (value instanceof String) {
      // Return the first letters of each first name.
      QueryArgStruct struct = buildingNamesMap.get((String) value);
      ListDataProvider<TableMessage> dataProvider = 
    		  getBuildingTenantsDataProvider(struct.diploma, struct.department, struct.year);
      return new DefaultNodeInfo<TableMessage>(
          dataProvider, contactCell, selectionModel, selectionManager, null);
    }   
    
    // Unhandled type.
    String type = value.getClass().getName();
    throw new IllegalArgumentException("Unsupported object type: " + type);
  }
  
	// Create an asynchronous callback to handle the result.
	final AsyncCallback<List<TableMessage>> tenantListCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onSuccess(List<TableMessage> result) {
			TableMessageHeader header = (TableMessageHeader) result.remove(0);
			String buildingID = header.getMessageId();
			List<TableMessage> tenantList = tenantProvider.get(buildingID).getList();
			GWT.log("Old: " + tenantList.size() + " New: " + result.size());
			tenantList.addAll(result);
		}

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Unable to retrieve building names. Try refreshing your browser. "
					+ caught.getMessage());
		}
	};    

	HashMap<String, ListDataProvider<TableMessage>> tenantProvider = new HashMap<String, ListDataProvider<TableMessage>>();
	ListDataProvider<String> buildingNames;
	HashMap<String, QueryArgStruct> buildingNamesMap;
	
	class QueryArgStruct
	{
		String department;
		String diploma;
		String year;
	}
	
	public ListDataProvider<String> getBuildingNamesProvider() {
		if(buildingNames == null)
		{
			buildingNames = new ListDataProvider<String>();
			buildingNamesMap = new HashMap<String, QueryArgStruct>();

			for(String dipl : NameTokens.DIPLM_DEPTS.keySet())
			{
				String shortDipl = "ERROR-UNKNOWN-" + dipl;
				if(dipl.equals(NameTokens.ALT_ND))
					shortDipl = NameTokens.ND;
				else if(dipl.equals(NameTokens.ALT_HND))
					shortDipl = NameTokens.HND;
				for(String dept: NameTokens.DIPLM_DEPTS.get(dipl))
				{
					for(String year: NameTokens.YEARS)
					{
						List<String> buildingList = buildingNames.getList();
						String providerID = NameTokens.getProviderID(shortDipl, dept, year);
						buildingList.add(providerID);
						QueryArgStruct struct = new QueryArgStruct();
						struct.department = dept;
						struct.diploma = shortDipl;
						struct.year = year;
						buildingNamesMap.put(providerID, struct);
					}
				}
			}
		}
		return buildingNames;
	}
	
	private ListDataProvider<TableMessage> getBuildingTenantsDataProvider(String dipl, String dept, String year) 
	{
		String providerID = NameTokens.getProviderID(dipl, dept, year);
		if(tenantProvider.get(providerID) == null)
		{
			tenantProvider.put(providerID, new ListDataProvider<TableMessage>());
			PanelUtilities.READ_SERVICE.getAllStudents(dipl, dept, year, true, tenantListCallback);
		}
		return tenantProvider.get(providerID);
	}  

  public boolean isLeaf(Object value) {
    return value instanceof TableMessage;
  }
}
