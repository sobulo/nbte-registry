package com.fertiletech.nbte.adminclient.utils;

import java.util.HashMap;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RatingsPanel extends CommentsPanel{
    public final static HashMap<Integer, String> RATINGS = new HashMap<Integer, String>();
    
    static
    {
    	RATINGS.put(1, "Poor");
    	RATINGS.put(2, "Satisfactory");
    	RATINGS.put(3, "Good");
    	RATINGS.put(4, "Excellent");
    }    
    
	MultiChoicePanelGenerator choice;	

	@Override
	protected void loadComments() {
		PanelUtilities.READ_SERVICE.loadRatings(commentID, loadCallback);
	}

	@Override
	protected void saveComments() {
		Integer rating = getRating();
		if(rating == null)
		{
			infoBox.show("You must select a rating");
			return;
		}
			PanelUtilities.READ_SERVICE.saveRatings(commentID, rating, 
				super.richText.getHTML(), false, super.saveCallback);
	}

	@Override
	protected void clear(boolean clearPrior) {
		super.clear(clearPrior);
		choice.setAnswer(null);
	}

	@Override
	
	protected VerticalPanel getRichTextContainer() {
		VerticalPanel p = super.getRichTextContainer();
		choice = new MultiChoicePanelGenerator();
		p.add(choice.getMultiChoicePanel("Specify a new rating", RATINGS));
		return p;
	}
	
	private Integer getRating()
	{
		String rating = choice.getAnswer();
		if(rating == null)
			return null;
		return Integer.valueOf(rating);
	}

}
