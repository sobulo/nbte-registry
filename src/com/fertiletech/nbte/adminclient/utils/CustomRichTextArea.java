package com.fertiletech.nbte.adminclient.utils;

import com.fertiletech.nbte.adminclient.text.RichTextToolbar;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class CustomRichTextArea extends Composite{
	RichTextArea richText;
	
	private Widget getRichTextArea(VerticalPanel p, String title)
	{
		richText = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(richText, title);
	    p.add(tb);
	    p.add(richText);    
	    richText.setHeight("10em");
	    richText.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}
	
	public CustomRichTextArea(VerticalPanel container, String title) {
		initWidget(getRichTextArea(container, title));
	}
	
	public CustomRichTextArea()
	{
		this(new VerticalPanel(), "Enter description below");
	}
	
	public void setValue(String text)
	{
		richText.setHTML(text);
	}
	
	public String getValue()
	{
		return richText.getHTML();
	}
	
	public void enable(boolean enabled)
	{
		richText.setEnabled(enabled);
	}
}
