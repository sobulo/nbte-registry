package com.fertiletech.nbte.adminclient.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;

public class DataProviderHelper {
	private final HashMap<String, ListDataProvider<TableMessage>> buildingTenantsDataProviderMap = new HashMap<String, ListDataProvider<TableMessage>>();

	public List<TableMessage> getTenantInfoList(String buildingID) {
		List<TableMessage> m = getInfoList(buildingID, buildingTenantsDataProviderMap);
		GWT.log("Retrieved from building tenant infolistcache " + m.size() + " for: " + buildingID );
		return m;
	}
	
	public void addViewToTenantListDataProvider(
			HasData<TableMessage> view, String buildingID) {
		addViewToListDataProvider(view, buildingID,
				buildingTenantsDataProviderMap);
	}	
	
	private static void addViewToListDataProvider(HasData<TableMessage> view,
			String buildingID,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		map.get(buildingID).addDataDisplay(view);
		view.setRowCount(map.get(buildingID).getList().size());
	}	
	
	private static List<TableMessage> getInfoList(String buildingID,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		ListDataProvider<TableMessage> dp = map.get(buildingID);
		if (dp == null) {
			dp = new ListDataProvider<TableMessage>();
			map.put(buildingID, dp);
		}
		return map.get(buildingID).getList();
	}
	
	private static void removeDisplayFromMap(HasData<TableMessage> view,
			HashMap<String, ListDataProvider<TableMessage>> map) {
		for (ListDataProvider<TableMessage> dp : map.values()) {
			Set<HasData<TableMessage>> displays = dp.getDataDisplays();
			if (displays.contains(view)) {
				dp.removeDataDisplay(view);
				break;
			}
		}
	}

	public void removeDisplayFromTenantMap(HasData<TableMessage> view) {
		removeDisplayFromMap(view, buildingTenantsDataProviderMap);
		view.setRowCount(0);
	}
	
	public void refresh(String buildingID) {
		buildingTenantsDataProviderMap.get(buildingID).refresh();
	}	
	
	public static void refreshSingleData(List<TableMessage> providerList, TableMessage m)
	{
		List<TableMessage> list = providerList;
		GWT.log("List has: " + providerList.size());
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i).getMessageId().equals(m.getMessageId()))
			{
				list.set(i, m);
				GWT.log("Found resident for refresh");
				return;
			}
		}
		GWT.log("Oops, no resident found");
	}	
}
