/**
 * 
 */
package com.fertiletech.nbte.adminclient.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class MultiChoicePanelGenerator {

	RadioButton[] rbList;

	public VerticalPanel getMultiChoicePanel(String question,
			Map<Integer, String> options) {
		// setup radio button group
		ArrayList<Integer> keys = new ArrayList<Integer>();
		keys.addAll(options.keySet());
		Collections.sort(keys);
		RadioButton rb;
		int count = 0;
		HorizontalPanel radioGroup = new HorizontalPanel();
		// radioGroup.setBorderWidth(1);
		radioGroup.setSpacing(5);
		rbList = new RadioButton[options.size()];
		Date d = new Date();
		String groupingID = String.valueOf(d.getTime());
		for (int i : keys) {

			rb = new RadioButton(groupingID, options.get(i));
			rb.setFormValue(String.valueOf(i));
			rbList[count++] = rb;
			radioGroup.add(rb);
		}

		// combine with question and return
		VerticalPanel choicePanel = new VerticalPanel();
		HTML questLabel = new HTML("<b>" + question + "</b>");
		choicePanel.add(questLabel);
		choicePanel.add(radioGroup);
		return choicePanel;
	}

	public String getAnswer() {
		for (int i = 0; i < rbList.length; i++)
			if (rbList[i].getValue())
				return rbList[i].getFormValue();
		return null;
	}

	public void setAnswer(String answer) {
		for (RadioButton rb : rbList)
			if (answer == null)
				rb.setValue(false);
			else if (rb.getFormValue().equals(answer)) {
				rb.setValue(true);
				break;
			}
	}
}
