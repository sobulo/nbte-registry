package com.fertiletech.nbte.adminclient.utils;

import java.util.Date;
import java.util.List;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

public class BillSearchPanel extends Composite implements HasValueChangeHandlers<List<TableMessage>>{
	
	@UiField DateBox startDateBox;
	@UiField DateBox endDateBox;
	@UiField Button searchBills;
	private SimpleDialog errorBox;
	private static BillSearchPanelUiBinder uiBinder = GWT
			.create(BillSearchPanelUiBinder.class);

	interface BillSearchPanelUiBinder extends UiBinder<Widget, BillSearchPanel> {
	}

    // Create an asynchronous callback to handle the result.
    private final AsyncCallback<List<TableMessage>> billListCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) 
        {
        	if(result.size() == 1)
        		PanelUtilities.infoBox.show("No resident bills found for date range specified");
        	fireBillListKnown(result);
         	searchBills.setEnabled(true);
        }

        @Override
        public void onFailure(Throwable caught) {
        	errorBox.show("Unable to retrieve invoices. Try refreshing your browser. " +
        			"Contact " + NameTokens.HELP_ADDRESS + " if problems persist. <p> Error msg: <b>" + caught.getMessage() + "</b></p>");
        	searchBills.setEnabled(true);
        }
    };	
	
	public BillSearchPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		errorBox = new SimpleDialog("Error Loading Bills");
		searchBills.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				Date startDate = startDateBox.getValue();
				Date endDate = endDateBox.getValue();
				if(startDate == null)
				{
					errorBox.show("Please enter a valid start date");
					return;
				}
				if(endDate == null)
				{
					errorBox.show("Please enter a valid end date");
					return;
				}
				if(startDate.after(endDate))
				{
					errorBox.show("Start date can not be greater than end date");
					return;
				}
				PanelUtilities.ACCT_SERVICE.getBills(startDate, endDate, billListCallback);
			}
		});
	}

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<List<TableMessage>> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
	
	private void fireBillListKnown(List<TableMessage> result)
	{
		ValueChangeEvent.fire(this, result);
	}

}
