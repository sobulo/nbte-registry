package com.fertiletech.nbte.adminclient.utils;

import com.fertiletech.nbte.adminclient.PanelUtilities;
import com.fertiletech.nbte.adminclient.text.RichTextToolbar;
import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class CommentsPanel extends Composite {
	@UiField SimplePanel saveSlot;
	Button save;
	@UiField SimplePanel commentsSlot;
	@UiField HTML priorComments;
	protected RichTextArea richText;
	SimpleDialog infoBox;
	private final int MIN_COMMENT_LENGTH = 30;
	protected String commentID;
	
	private static CommentsPanelUiBinder uiBinder = GWT
			.create(CommentsPanelUiBinder.class);

	interface CommentsPanelUiBinder extends UiBinder<Widget, CommentsPanel> {
	}

	protected final AsyncCallback<String> loadCallback = new AsyncCallback<String>() {

	      @Override
	      public void onSuccess(String result) {
	    	  priorComments.setHTML(result);
	      }

	      @Override
	      public void onFailure(Throwable caught) {
	      	infoBox.show("Error loading comments: " + caught.getMessage());
	      }
	  };

	protected final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {

		      @Override
		      public void onSuccess(Void result) {
		    	  loadComments();
		    	  infoBox.show("Saved Succesfully");
		    	  clear(false);
		    	  save.setEnabled(true);
		      }

		      @Override
		      public void onFailure(Throwable caught) {
		      	infoBox.show("Error loading: " + caught.getMessage());
		      	save.setEnabled(true);
		      }
		  };	  
		  
	public CommentsPanel(boolean includeSaveWidgets) {
		initWidget(uiBinder.createAndBindUi(this));
		infoBox = new SimpleDialog("INFO");
		if(includeSaveWidgets)
			addSaveWidgets();
	}
	
	public CommentsPanel()
	{
		this(true);
	}
	
	protected void addSaveWidgets()
	{
		save = new Button("Save Comments");
		saveSlot.add(save);
		commentsSlot.add(getRichTextArea(getRichTextContainer()));
		save.addClickHandler(getSaveHandler());		
	}

	private ClickHandler getSaveHandler()
	{
		return new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(richText.getText().length() < MIN_COMMENT_LENGTH)
				{
					infoBox.show("Comment length too short. Please enter a more detailed description");
					return;
				}
				else if(commentID == null)
				{
					infoBox.show("No object associated with this comment box, " +
							"please contact technology@fertiletech.com if problems persist");
					return;
				}
				save.setEnabled(false);
				saveComments();
			}
		};
	}

	private Widget getRichTextArea(VerticalPanel p)
	{
		richText = new RichTextArea();
	    RichTextToolbar tb = new RichTextToolbar(richText, "Enter comments below");
	    p.add(tb);
	    p.add(richText);    
	    richText.setHeight("10em");
	    richText.setWidth("99%");
	    tb.setWidth("100%");
	    p.setWidth("100%");		
	    return p;	
	}	
		
	public void setCommentID(String id)
	{
		commentID = id;
		loadComments();
		GWT.log("Comment ID after set is: " + commentID + " value passed in is: " + id);
	}
		
	public void clear()
	{
		clear(true);
	}
	
	protected void loadComments()
	{
  	  	priorComments.setHTML("<marquee>Loading comments ...</marquee>");
		PanelUtilities.READ_SERVICE.loadActivityComments(commentID, loadCallback);		
	}
	
	protected void saveComments()
	{
		PanelUtilities.READ_SERVICE.saveActivityComment(commentID, richText.getHTML(), false, saveCallback);		
	}
	 
	protected void clear(boolean clearPrior)
	{
		if(clearPrior)
		{
			priorComments.setHTML("");
			commentID = null;
		}
		richText.setText("");
	}
	
	protected VerticalPanel getRichTextContainer()
	{
		return new VerticalPanel();
	}
}