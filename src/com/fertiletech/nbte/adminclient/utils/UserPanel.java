/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.adminclient.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

import com.fertiletech.nbte.adminclient.widgs.SimpleDialog;
import com.fertiletech.nbte.client.GUIConstants;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 *
 * @author Administrator
 */
public class UserPanel extends Composite implements HasValueChangeHandlers<String>
{
    @UiField TextBox firstName;
    @UiField TextBox lastName;
    @UiField TextBox email;
    @UiField TextBox phone;
    @UiField TextBox otherNumber;
    @UiField TextBox otherMail;
    @UiField TextArea address;
    @UiField TextBox salutation;
    @UiField FlowPanel sexPanel;
    RadioButton male;
    RadioButton female;
    @UiField ListBox birthYear;
    @UiField DateBox dateOfBirth;
    String userID;
    String tempUserID;
    SimpleDialog infoBox;
    

    private static UserPanelUiBinder uiBinder = GWT.create(UserPanelUiBinder.class);
    private static final RegExp EMAIL_MATCHER = RegExp.compile(ApplicationFormConstants.REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(ApplicationFormConstants.REGEX_NUMS_ONLY);
    int DEFAULT_PHONE_PER_USER = 5;
    String DEFAULT_LB_WIDTH = "200px";
    
	AsyncCallback<TableMessage> callback = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			infoBox.show("Error loading contact information");
		}

		@Override
		public void onSuccess(TableMessage result) {
			GWT.log("received a table message with: " + result.getNumberOfTextFields());
			userID = result.getMessageId();
			setContactValues(result);
			infoBox.show("Succesfully loaded information for " + firstName.getValue());
			fireUserIDEvent();
		}
	};
    
    @SuppressWarnings("deprecation")
	public UserPanel()
    {
        initWidget(uiBinder.createAndBindUi(this));
        infoBox = new SimpleDialog("BIO MESSAGE");
        male=new RadioButton("sex", "Male");
        female=new RadioButton("sex", "Female");
        sexPanel.add(male);
        sexPanel.add(female);
        Date currentDate = new Date();
        int start = currentDate.getYear();
        for(int i = start; i > start - 100; i--)
        	birthYear.addItem(String.valueOf(i + 1900), String.valueOf(i));
        birthYear.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				int year = Integer.valueOf(birthYear.getValue(birthYear.getSelectedIndex()));
				Date d = dateOfBirth.getValue();
				d = d==null?new Date() : d;
				d.setYear(year);
				dateOfBirth.setValue(d);
			}
		});
        dateOfBirth.addValueChangeHandler(new ValueChangeHandler<Date>() {

			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				int year = event.getValue().getYear();
				for(int i = 0; i < birthYear.getItemCount(); i++)
					if(Integer.valueOf(birthYear.getValue(i)) == year)
						birthYear.setSelectedIndex(i);
			}
		});
        dateOfBirth.setFormat(GUIConstants.DEFAULT_DATEBOX_FORMAT);
    }
    
    private void fireUserIDEvent()
    {
    	ValueChangeEvent.fire(this, userID);
    }
    
	public void setContactValues(TableMessage m)
	{
		if(m == null) clear();
		this.setFirstName(m.getText(RegistryDTO.FIRST_NAME_IDX));
		this.setLastName(m.getText(RegistryDTO.LAST_NAME_IDX));
		this.setEmail(m.getText(RegistryDTO.EMAIL_IDX));
		//this.setPhone(m.getText(RegistryDTO.));
		/*dateOfBirth.setValue(m.getDate(DTOConstants.TNT_DATE_O));

		int startIdx = (int) m.getNumber(DTOConstants.TNT_OTHER_PHONE_START_IDX).doubleValue();
		if(startIdx < 0)
			this.setOtherNumbers(null);
		else
			this.setOtherNumbers(getMultiVals(startIdx, 
				(int) m.getNumber(DTOConstants.TNT_OTHER_PHONE_END_IDX).doubleValue(), m));
		

		startIdx = (int) m.getNumber(DTOConstants.TNT_OTHER_EMAIL_START_IDX).doubleValue();
		if(startIdx < 0)
			this.setOtherMails(null);
		else
			this.setOtherMails(getMultiVals(startIdx, 
					(int) m.getNumber(DTOConstants.TNT_OTHER_EMAIL_END_IDX).doubleValue(), m));

		this.setAddress(m.getText(DTOConstants.TNT_ADDRESS_IDX));
		this.setSalutation(m.getText(DTOConstants.TNT_TITLE_IDX));
		String boolString = m.getText(DTOConstants.TNT_GENDER_IDX);
		this.setMale(boolString==null?null:Boolean.valueOf(boolString));*/
		this.userID = m.getMessageId();
	}
	
	public String getUserID()
	{
		return userID;
	}
	
	public void setUserID(String id)
	{
		userID = id;
	}
	
	public void loadUserInfo(String userKey)
	{
		clear();
		GWT.log("make call to load prospective info");
		infoBox.show("Loading contact information...");
		tempUserID = userKey;
		Window.alert("Not yet supported");
		//LocalDataProvider.TENANT_SERVICE.getContact(userKey, callback);
	}
	
	
	public HashSet<String> getMultiVals(int start, int end, TableMessage m)
	{
		GWT.log("start: " + start + " end: " + end);
		if(start < 0 || end < 0)
			return null;		
		HashSet<String> multiVals = new HashSet<String>();
		for(int i=start; i<=end; i++)
			multiVals.add(m.getText(i));
		return multiVals;
	}	    
    
    public void disableNameEdits()
    {
    	firstName.setEnabled(false);
    	lastName.setEnabled(false);
    }

    public String getEmail() {
        return email.getValue().trim().toLowerCase();
    }
    
    public boolean isMale()
    {
    	if(male.getValue())
    		return true;
    	else
    		return false;
    }
    
    public String getPhone() {
        return phone.getValue().trim().toLowerCase();
    }

    public String getFirstName() {
        return firstName.getValue().trim();
    }

    public String getLastName() {
        return lastName.getValue().trim();
    }

    public Date getDateOfBirth()
    {
    	return dateOfBirth.getValue();
    }
    
    public String getOtherNumber() {
        return otherNumber.getValue();
    }

    public String getOtherMails() {
        return otherMail.getValue();
    }
    
    public String getAddress()
    {
    	return address.getValue().trim();
    }
    
    public void setEmail(String eml) {
        email.setValue(eml);
    }
    
    public void setPhone(String eml) {
        phone.setValue(eml);
    }
    
    public void setFirstName(String fnm) {
        firstName.setValue(fnm);
    }

    public void setLastName(String lnm) {
        lastName.setValue(lnm);
    }

    public void setOtherNumbers(String phnms) {
        otherNumber.setValue(phnms);
    }
    
    public void setAddress(String addy)
    {
    	address.setValue(addy);
    }
    
    public void setMale(Boolean isMale)
    {
    	if(isMale == null)
    	{
    		male.setValue(null);
    		female.setValue(null);
    	}
    	else if(isMale)
    		male.setValue(true);
    	else
    		female.setValue(true);
    }
    
    public void setOtherMail(String email) {
        otherMail.setValue(email);
    }
    
    public boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(ApplicationFormConstants.REGEX_PHONE_REPLACE, "")) != null;
    }
    
    
	private int selectAListBoxItem(ListBox l, String val)
	{
		for( int i = 0; i < l.getItemCount(); i++)
			if(l.getValue(i).equals(val))
			{
				l.setSelectedIndex(i);
				return i;
			}
		return -1;
	}

    public void clear()
    {
    	birthYear.setSelectedIndex(0);
    	dateOfBirth.setValue(null);
        otherNumber.setValue(null);
        otherMail.setValue(null);
        firstName.setValue(null);
        lastName.setValue(null);
        email.setValue(null);
        phone.setValue(null);
        salutation.setValue(null);
        male.setValue(null);
        female.setValue(null);
        address.setValue(null);        
        userID = null;
    }
    
    public void enablePanel(boolean enabled)
    {
    	birthYear.setEnabled(enabled);
    	dateOfBirth.setEnabled(enabled);

    	otherNumber.setEnabled(enabled);
    	otherMail.setEnabled(enabled);    		
    	firstName.setEnabled(enabled);
        lastName.setEnabled(enabled);
        email.setEnabled(enabled);
        phone.setEnabled(enabled);
        salutation.setEnabled(enabled);
        male.setEnabled(enabled);
        female.setEnabled(enabled);
        address.setEnabled(enabled);
    }    
    
    public String getSalutation() {
		return salutation.getValue();
	}

	public void setSalutation(String string) {
		salutation.setValue(string);
	}

	public ArrayList<String> validateFields()
    {
        ArrayList<String> result = new ArrayList<String>();

        //validate fields
        if(getFirstName().length() == 0)
        	result.add("First name field is blank");
        
        if(getLastName().length() == 0)
        	result.add("Last name field is blank");
                
        if(getEmail().length() != 0 && !isValidEmail(getEmail()))
        	result.add("Email address is not valid");

        if(getPhone().length() != 0 && !isValidNum(getPhone()))
        	result.add("Phone num is not valid");
        
        if(male.getValue() == false && female.getValue() == false)
        	result.add("No gender selected (male or female)");

        String altemail = email.getValue().trim();
        String altnum = phone.getValue().trim();
        if(altemail.length() != 0 && !isValidEmail(getEmail()))
        	result.add("Alternate Email address is not valid");

        if(altnum.length() != 0 && !isValidNum(getPhone()))
        	result.add("Alternate Phone num is not valid");        
        return result;
    }

    interface UserPanelUiBinder extends UiBinder<Widget, UserPanel> {
    }

	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return addHandler(handler, ValueChangeEvent.getType());
	}
}