package com.fertiletech.nbte.client.eventboxes;

import java.util.HashSet;

import com.fertiletech.nbte.shared.NameTokens;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.ListBox;

public class DiplDeptLB implements HasValueChangeHandlers<String>, HasEnabled{
	ListBox dl;
	ListBox dp;
	public DiplDeptLB(ListBox dipl, ListBox dept)
	{
		this.dl = dipl;
		this.dp = dept;
		for(String s : NameTokens.DIPLOMAS)
			dl.addItem(s);
		dl.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				populateDepts();
			}
		});
		dp.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				fireMe();	
			}
		});
		populateDepts();
	}
	
	void populateDepts()
	{
		dp.clear();
		String key = NameTokens.getNameTargetMap().get(getDiploma());
		HashSet<String> depts = NameTokens.DIPLM_DEPTS.get(key);
		for(String s : depts)
			dp.addItem(s);
		fireMe();
	}
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		dp.fireEvent(event);
	}
	
	void fireMe()
	{
		ValueChangeEvent.fire(this, getValue());
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return dp.addHandler(handler, ValueChangeEvent.getType());
	}
	
	public String getDiploma()
	{
		if(dl.getItemCount() == 0)
			return "";
		return dl.getValue(dl.getSelectedIndex());
	}

	@Override
	public boolean isEnabled() {
		return dp.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled) {
		dl.setEnabled(enabled);
		dp.setEnabled(enabled);
	}
	
	public String getValue()
	{
		if(dp == null || dp.getItemCount()==0) return "";
		return dp.getValue(dp.getSelectedIndex());
	}
	
	public void enableSelector(boolean enabled)
	{
		setEnabled(enabled);
	}
}
