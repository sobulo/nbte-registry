package com.fertiletech.nbte.client.eventboxes;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;

public class UniqueListBox implements HasValueChangeHandlers<String>{

	ListBox lb;
	public UniqueListBox(final ListBox lb, HashSet<String> vals)
	{
		this.lb = lb;
		for(String s : vals)
			lb.addItem(s);
		setupChangeHandler();
	}
	
	private void setupChangeHandler()
	{
		lb.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				ValueChangeEvent.fire(UniqueListBox.this, getValue());
			}
		});		
	}
	
	public UniqueListBox(final ListBox lb, HashMap<String, String> vals)
	{
		this.lb = lb;
		for(String s : vals.keySet())
			lb.addItem(s, vals.get(s));
		setupChangeHandler();
	}	
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		lb.fireEvent(event);
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<String> handler) {
		return lb.addHandler(handler, ValueChangeEvent.getType());
	}
	
	public String getValue()
	{
		if(lb.getItemCount() == 0)
			return "";
		return lb.getValue(lb.getSelectedIndex());
	}
}
