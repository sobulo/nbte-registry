package com.fertiletech.nbte.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class NBTERegistry implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
	}
}
