/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.fertiletech.nbte.client;
import com.fertiletech.nbte.shared.ClientUtils;
import com.fertiletech.nbte.shared.ClientUtils.BankUserCookie;
import com.fertiletech.nbte.shared.Credential;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.OurCallbackUrl;
import com.fertiletech.nbte.shared.OurException;
import com.fertiletech.nbte.shared.SocialUser;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../login")
public interface OAuthLoginService extends RemoteService
{
    public String     getAuthorizationUrl(Credential credential) throws OurException;
    public SocialUser verifySocialUser(Credential credential) throws OurException;
    public SocialUser fetchMe(String sessionId) throws OurException;
    public String     getAccessToken(String sessionId) throws OurException;
    public void       logout(String sessionId) throws OurException;
    /**
     * Utility class for simplifying access to the instance of async service.
     */
    public static class Util
    {
        private static OAuthLoginServiceAsync instance;

        public static OAuthLoginServiceAsync getInstance()
        {
            if (instance == null)
            {
                instance=GWT.create(OAuthLoginService.class);
            }
            return instance;
        }
        
        public static void getAuthorizationUrl(final int authProvider)
        {
            String authProviderName = ClientUtils.getAuthProviderName(authProvider);
            final String callbackUrl = (authProvider == ClientUtils.BANK)?OurCallbackUrl.ADMIN_CALLBACK_URL:ClientUtils.getCallbackUrl();
            
            final Credential credential = new Credential();
            credential.setRedirectUrl(callbackUrl);
            credential.setAuthProvider(authProvider);
            
            new MyAsyncCallback<String>()
            {
                @Override
                public void onSuccess(String result)
                {
                    String authorizationUrl = result;
                    
                    // clear all cookes first
                    ClientUtils.clearCookies(); 
                    
                    // save the auth provider to cookie
                    ClientUtils.saveAuthProvider(authProvider);
                    
                    // save the redirect url to a cookie as well
                    // we need to redirect there after logout
                    ClientUtils.saveRediretUrl(callbackUrl);
                    
                    //Window.alert("Redirecting to: " + authorizationUrl);
                    ClientUtils.redirect(authorizationUrl);
                }
                
                @Override
                protected void callService(AsyncCallback<String> cb)
                {
                    OAuthLoginService.Util.getInstance().getAuthorizationUrl(credential,cb);
                }

                @Override
                public void onFailure(Throwable caught)
                {
                    ClientUtils.handleException(caught, this.showWarning);
                }
            }.go("Getting Authorization URL from " + authProviderName + "...");
                    
        }        

        public static void logout()
        {
            final String sessionId = ClientUtils.getSessionIdFromCookie();
            
            new MyAsyncCallback<Void>()
            {

                @Override
                public void onFailure(Throwable caught)
                {
                    ClientUtils.reload(); // reload anyway otherwise we're toast! we will never be able to log out
                }

                @Override
                public void onSuccess(Void result)
                {
                    ClientUtils.reload();
                }

                @Override
                protected void callService(AsyncCallback<Void> cb)
                {
                    OAuthLoginService.Util.getInstance().logout(sessionId,cb);
                }
            }.go("Logging out..");
        }    
        
        
        private static void verifySocialUser(final MyRefreshCallback refreshCB)
        {
            final String authProviderName = ClientUtils.getAuthProviderNameFromCookie();
            final int authProvider = ClientUtils.getAuthProviderFromCookieAsInt();
                       
            new MyAsyncCallback<SocialUser>()
            {

                @Override
                public void onSuccess(SocialUser result)
                {
                    ClientUtils.saveSessionId(result.getSessionId());
                    //Window.alert("On verification, returned with id: " + result.getSessionId());
                    String name = "";
                    if (result.getName() != null)
                    {
                        name = result.getName();
                    }
                    else if (result.getNickname() != null) // yahoo
                    {
                        name = result.getNickname();
                    }
                    else if (result.getFirstName() != null) // linkedin
                    {
                        name = result.getFirstName();
                        String lastName = result.getLastName();
                        if (lastName != null)
                        {
                            name = name + " " + lastName;
                        }
                    }            		
            		//Window.alert("Loan ID set to: " + loanID + " length " + (loanID == null? "n/a":loanID.length()));

                    BankUserCookie userCookie = new BankUserCookie(name, result.getEmail(), result.ofyID, result.role.toString(),
                    		result.department, result.diploma, result.levelYear, NameTokens.getAcademicYearString(result.academicYear),
                    							NameTokens.getSemester(result.semester));
                    ClientUtils.BankUserCookie.setCookie(userCookie);
                    //ClientUtils.NPMBUserCookie.showCookie("Checking: ", userCookie);
                    refreshCB.updateScreen();
                }

                @Override
                protected void callService(AsyncCallback<SocialUser> cb)
                {
                    try
                    {
                        final Credential credential = ClientUtils.getCredential();
                        if (credential == null)
                        {
                            Window.alert("verifySocialUser: Could not get credential for " + authProvider + " user");
                            return;
                        }     
                        //Window.alert("Verifying creidentials with: " + credential.getAuthProvider() + "/" 
                        //		+ credential.getAuthProviderName() + " verifier: " + credential.getVerifier());
                        OAuthLoginService.Util.getInstance().verifySocialUser(credential,cb);
                    }
                    catch (Exception e)
                    {
                        Window.alert(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Throwable caught)
                {
                    Window.alert("Try refreshing your browser. Coult not verify" + authProvider + " user." + caught);
                }
            }.go("Verifying " + authProviderName + " user..");
        }
        
        public static void handleRedirect(MyRefreshCallback cb)
        {
            if (!ClientUtils.alreadyLoggedIn() && ClientUtils.redirected()) 
                    verifySocialUser(cb);
            else
            	cb.updateScreen();        
        }
        
        public static String requestAdditionalPrivileges()
        {
        	BankUserCookie cookie = ClientUtils.BankUserCookie.getCookie();
        	int authProvider = ClientUtils.getAuthProviderFromCookieAsInt();
        	boolean isOps = cookie.getRole().ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal();
    		if(isOps && authProvider != ClientUtils.BANK)
    		{
    			return "Looks like you initially logged in via public portal www.fcahptib.edu.ng " +
    					"[instead of www.fcahptib.edu.ng/staffzone/]. You will now be logged in again with your staff credentials"
    					+ "<b>Come back afterwards to this page and you'll then be able to access your google drive</b>";
    		}
    		return null;
        }
    }
    
    
}
