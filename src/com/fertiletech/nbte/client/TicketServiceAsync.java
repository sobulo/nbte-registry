package com.fertiletech.nbte.client;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface TicketServiceAsync {
	void getTickets(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);
	void createTicket(TableMessage ticketDetails,
			AsyncCallback<TableMessage> callback);
	void editTicket(TableMessage[] details, AsyncCallback<TableMessage> callback);
	void getTicket(Long ticketNumber, AsyncCallback<TableMessage> callback);
	void getTicket(String ticketID, AsyncCallback<TableMessage> callback);
	void getAllTickets(AsyncCallback<HashMap<String, String>> callback);
	void addInvoiceID(String ticketID, Long invoiceID, boolean isAdd,
			AsyncCallback<List<TableMessage>> callback);
	void addAllocationID(String ticketID, Long allocationID, boolean isAdd,
			String product, String location,
			AsyncCallback<List<TableMessage>> callback);
	void addAllocationID(String ticketID, String allocationID, boolean isAdd,
			AsyncCallback<List<TableMessage>> callback);	
	void getAllocations(String ticketID,
			AsyncCallback<List<TableMessage>> callback);
	void getInvoices(String ticketID, AsyncCallback<List<TableMessage>> callback);
}
