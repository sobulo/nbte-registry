package com.fertiletech.nbte.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface UtilsServiceAsync {
	void loadActivityComments(String parentKey, AsyncCallback<String> callback);

	void getRecentActivityComments(AsyncCallback<List<TableMessage>> callback);

	void saveActivityComment(String commentID, String html, boolean b,
			AsyncCallback<Void> saveCallback);

	void getAttachments(String loanID,
			AsyncCallback<List<TableMessage>> callback);

	void saveAttachments(String loanId, List<TableMessage> attachments,
			AsyncCallback<String> callback);

	void deleteAttachment(String attachID, AsyncCallback<String> callback);

	void getUploads(String loanID, AsyncCallback<List<TableMessage>> callback);

	void deleteUpload(String uploadID, AsyncCallback<String> callback);

	void getUploadUrl(AsyncCallback<String> callback);

	void getApplicationParameter(String ID,
			AsyncCallback<List<TableMessage>> callback);

	void saveApplicationParameter(String id, HashMap<String, String> val,
			AsyncCallback<Void> callback);

	void getImageUploadHistory(String imageID, AsyncCallback<String> callback);

	void getMapAttachments(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	void saveMapAttachment(String loanId, Double lat, Double lng,
			AsyncCallback<String> callback);

	void loadRatings(String pk, AsyncCallback<String> callback);

	void saveRatings(String parentKey, int rating, String text,
			boolean showPublic, AsyncCallback<Void> callback);

	void fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header, AsyncCallback<String> callback);

	void getAllStudents(boolean isCurrentlyEnrolled,
			AsyncCallback<HashMap<String, String>> callback);

	void getAllStudents(String diploma, String dept, String year,
			boolean isCurrentlyEnrolled,
			AsyncCallback<List<TableMessage>> callback);

	void getImageBlobID(String name, AsyncCallback<String> callback);
}
