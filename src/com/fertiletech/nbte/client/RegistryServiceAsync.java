package com.fertiletech.nbte.client;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface RegistryServiceAsync {
	void getDepartmentRoster(String dept, String diploma, String year,
			int academicYear, AsyncCallback<List<TableMessage>> callback);

	void getDepartmentCourseTemplates(String dept, String diploma, String year,
			int semester, int academicYear,
			AsyncCallback<List<TableMessage>> callback);

	void registerSelectedCourses(AsyncCallback<String> callback);

	void getCart(AsyncCallback<List<TableMessage>> callback);

	void updateCart(String courseID, boolean isAddition,
			AsyncCallback<String> callback);

	void getCourseTemplateInfo(String templateID,
			AsyncCallback<TableMessage> callback);

	void getSessionInfo(String sessionID,
			AsyncCallback<List<TableMessage>> callback);

	void getMySessions(AsyncCallback<List<TableMessage>> callback);

	void updateCart(AsyncCallback<String> callback);

	void getBio(AsyncCallback<HashMap<String, String>> callback);

	void saveBio(String id, HashMap<String, String> bio,
			AsyncCallback<HashMap<String, String>> callback);

	void getCourseRegistrationDownloadLink(String sessionID,
			AsyncCallback<String[]> callback);

	void getCourseRegistrationDownloadLink(String[] studs, AsyncCallback<String[]> callback);

	void getItemizedTuitionDownloadLink(String sessionID,
			AsyncCallback<String[]> callback);

	void getAccountStatementLink(String sessionID,
			AsyncCallback<String[]> callback);
}
