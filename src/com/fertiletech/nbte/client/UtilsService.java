package com.fertiletech.nbte.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../utils")
public interface UtilsService extends RemoteService {
	String loadActivityComments(String parentKey);
	List<TableMessage> getRecentActivityComments();
	void saveActivityComment(String commentID, String html, boolean b);
	List<TableMessage> getAttachments(String studentID);
	String saveAttachments(String loanId, List<TableMessage> attachments);
	String saveMapAttachment(String prospectId, Double lat, Double lng);
	String deleteAttachment(String attachID);	
	List<TableMessage> getUploads(String studentID) throws MissingEntitiesException;
	String getUploadUrl();
	String deleteUpload(String uploadID) throws MissingEntitiesException;
	List<TableMessage> getApplicationParameter(String ID);
	void saveApplicationParameter(String id, HashMap<String, String> val) throws MissingEntitiesException;
	String getImageUploadHistory(String imageID);
	List<TableMessage> getMapAttachments(Date startDate, Date endDate);
	String loadRatings(String pk);
	void saveRatings(String parentKey, int rating, String text,
			boolean showPublic);
	String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header);
	HashMap<String, String> getAllStudents(boolean isCurrentlyEnrolled);
	List<TableMessage> getAllStudents(String diploma, String dept, String year,
			boolean isCurrentlyEnrolled);
	String getImageBlobID(String name);
}
