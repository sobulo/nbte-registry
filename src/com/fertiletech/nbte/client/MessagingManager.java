package com.fertiletech.nbte.client;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.CustomMessageTypes;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("../messaging")
public interface MessagingManager extends RemoteService {
	
	public void sendMessage(CustomMessageTypes msgType, String[] addresses, String[] message, boolean isEmail)
		throws MissingEntitiesException;
	
	public HashMap<String, String> getMessagingControllerNames();
	
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException;
}
