
package com.fertiletech.nbte.client;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.LoginValidationException;
import com.fertiletech.nbte.shared.ManualVerificationException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 */
@RemoteServiceRelativePath("../accounts")
public interface AccountManager extends RemoteService {	
	public HashMap<Long, String> getAllBillIDs();
	public List<TableMessage> getAllAccounts();
	public String createDeposit(String companyAcctStr,
			Date depositDate, String referenceNumber, String comments,
			String acctStr, double initialAmount);
	
	TableMessage createAccount(String bank, String sortCode, String acctName,
			String acctNumber, String currency) throws DuplicateEntitiesException;
	TableMessage updateAccount(String accId, String bank, String sortCode, String acctName,
			String acctNumber, String currency) throws MissingEntitiesException;
	
	public List<TableMessage> getDeposits(Date startDate, Date endDate);
	public List<TableMessage> getDeposits(String tenantID);
	public List<TableMessage> getBills(Date startDate, Date endDate) throws MissingEntitiesException;	
	String createUserBill(String[] users, String[] templateIDs,
			boolean generatePayment) throws MissingEntitiesException;
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException;
	
	public HashMap<String, String> getStudentBillKeys() throws LoginValidationException;
		
	String savePayment(String billKey, String depositKey, double amount,
			Date payDate, String comments) throws MissingEntitiesException, ManualVerificationException;
		
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	public List<TableMessage> getBillTemplate(String templateID) throws MissingEntitiesException;
	public List<TableMessage> getBillPayments(String billKeyStr) throws MissingEntitiesException, LoginValidationException;
	
	String getInvoiceDownloadLink(String[] billIDs) throws MissingEntitiesException, LoginValidationException;
	String getDepositStatementDownloadLink(String contactID, String[] depositIDs, Date start,
			Date end) throws MissingEntitiesException;
	TableMessage createCompanyRep(String firstName, String lastName, Date dob,
			String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title) throws DuplicateEntitiesException;
	List<TableMessage> getAllReps();
	List<TableMessage> getCompanyTenants(String repID);
	List<TableMessage> saveCompanyTenants(String[] tenantIDs, String repID);
	List<TableMessage> removeCompanyTenants(String[] tenantIDs, String repID);
	TableMessage createResidentAccount(String tenantID, String acctType, boolean allowOverDraft, String currency, int depositLevel) throws DuplicateEntitiesException;
	TableMessage getResidentAccountInfo(String tenantID, String acctType,
			String ccy);
	List<TableMessage> getResidentAccountInfo(String tenantID);
	List<TableMessage> getLedgerAccountEntries(String acctID, Date startDate, Date endDate);
	List<TableMessage> getLedgerResidentAcctEntries(String tenantID, Date startDate, Date endDate);
	List<TableMessage> getResidentAccountsByBuilding(String buildingId) throws MissingEntitiesException;
	String getDepositRequestLink(String[] companyAcctId, Date invDate, String description, List<TableMessage> requests);
	TableMessage updateResidentAccount(String acctID, boolean allowOverDraft, int depositLevel) throws MissingEntitiesException;
	String createBillTemplate(LinkedHashSet<BillDescriptionItem> billDescs,
			String title, Date start, Date end, Date due, String ccy) throws DuplicateEntitiesException;
	List<TableMessage> getBillTemplateNames();
	String createDeposits(String tenantID, Date depositDate) throws MissingEntitiesException, 
																	ManualVerificationException;
}
