/**
 * 
 */
package com.fertiletech.nbte.client;


import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface AccountManagerAsync {


	void getStudentBills(String studentID,
			AsyncCallback<List<TableMessage>> callback);

	void savePayment(String billKey, String depositKey, double amount,
			Date payDate, String comments,
			AsyncCallback<String> callback);


	void getStudentBillKeys(String studentID, boolean isLogin,
			AsyncCallback<HashMap<String, String>> callback);

	void getStudentBillKeys(AsyncCallback<HashMap<String, String>> callback);	
	
	void getStudentBillAndPayment(String billKeyStr,
			AsyncCallback<List<TableMessage>> callback);


	void getBillPayments(String billKeyStr,
			AsyncCallback<List<TableMessage>> callback);

	void getInvoiceDownloadLink(String[] billIDs, AsyncCallback<String> callback);

	void getBills(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	void getDeposits(Date startDate, Date endDate, AsyncCallback<List<TableMessage>> callback);

	void getDeposits(String tenantID, AsyncCallback<List<TableMessage>> callback);

	void createDeposit(String companyAcctStr, Date depositDate,
			String referenceNumber, String comments, String acctStr,
			double initialAmount, AsyncCallback<String> callback);

	void createDeposits(String tenantID, Date depositDate, AsyncCallback<String> callback);
	
	void createResidentAccount(String tenantID, String acctType,
			boolean allowOverDraft, String currency, int depositLevel,
			AsyncCallback<TableMessage> callback);

	void updateResidentAccount(String acctID, boolean allowOverDraft,
			int depositLevel, AsyncCallback<TableMessage> callback);	
	
	void getAllAccounts(AsyncCallback<List<TableMessage>> callback);

	void getAllBillIDs(AsyncCallback<HashMap<Long, String>> callback);

	void getDepositStatementDownloadLink(String contactID, String[] depositIDs,
			Date start, Date end, AsyncCallback<String> callback);

	void createCompanyRep(String firstName, String lastName,
			Date dob, String primaryEmail, String primaryNumber,
			HashSet<String> secondaryNumbers, HashSet<String> secondaryEmails,
			String companyName, String address, Boolean isMale, String title,
			AsyncCallback<TableMessage> callback);

	void getAllReps(AsyncCallback<List<TableMessage>> callback);

	void getCompanyTenants(String repID,
			AsyncCallback<List<TableMessage>> callback);

	void saveCompanyTenants(String[] tenantIDs, String repID,
			AsyncCallback<List<TableMessage>> callback);

	void removeCompanyTenants(String[] tenantIDs, String repID,
			AsyncCallback<List<TableMessage>> callback);

	void getResidentAccountInfo(String tenantID,
			AsyncCallback<List<TableMessage>> callback);

	void createAccount(String bank, String sortCode, String acctName,
			String acctNumber, String currency,
			AsyncCallback<TableMessage> callback);

	void getResidentAccountInfo(String tenantID, String acctType, String ccy,
			AsyncCallback<TableMessage> callback);

	void getLedgerResidentAcctEntries(String tenantID, Date startDate,
			Date endDate, AsyncCallback<List<TableMessage>> callback);

	void getLedgerAccountEntries(String acctID, Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	void getResidentAccountsByBuilding(String buildingId,
			AsyncCallback<List<TableMessage>> callback);

	void updateAccount(String accId, String bank, String sortCode,
			String acctName, String acctNumber, String currency,
			AsyncCallback<TableMessage> callback);

	void getDepositRequestLink(String[] companyAcctId, Date invDate,
			String description, List<TableMessage> requests,
			AsyncCallback<String> callback);

	void createUserBill(String[] users, String[] templateIDs,
			boolean generatePayment, AsyncCallback<String> billTemplateCallback);

	void getBillTemplate(String templateID,
			AsyncCallback<List<TableMessage>> callback);

	void createBillTemplate(LinkedHashSet<BillDescriptionItem> billDescs,
			String title, Date value, Date value2, Date value3, String value4,
			AsyncCallback<String> billTemplateCallback);

	void getBillTemplateNames(AsyncCallback<List<TableMessage>> callback);
	
}