/**
 * 
 */
package com.fertiletech.nbte.client;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class GUIConstants {
	public final static String TICKET_ID_PREFIX = "ticket?";
	public final static String NAIRA_HTML = "&#8358;";
	public final static String NAIRA_UNICODE = new StringBuilder().appendCodePoint(0x20A6).toString();
	public final static String MAP_URL_SELECT = "http://www.zerofinance.com.ng/addosserops/#doApplicationBlotterCW/";
	public final static double YEAR_TO_DAYS = 365.242;
	public final static String STYLE_PS_TABLE_SPACER = "scrollTableSpacer"; 
    public final static String STYLE_STND_TABLE_HEADER = "standardTableHeader";
    public final static String STYLE_STND_TABLE = "standardTable";	
    public final static String STYLE_CAPITALIZE = "capitalizeText";    
	public final static String ROOT_ID_REDIRECT = "contentAreaRedirect";
	public final static String ROOT_ID_PUBLIC = "contentAreaCalculator";
	public final static String ROOT_ID_ADMIN = "contentAreaAdmin";
	public final static String ROOT_ID_APPLY_PORTAL = "contentAreaPortal";
	public final static String ROOT_ID_APPLY_NOW_PORTAL = "newContentAreaPortal";
	public final static String GUARANTOR_URL = "http://www.addosser.com/products-services/consumer-swift-lease/guarantor.pdf";
	public final static String CHECKLIST_URL = "http://www.addosser.com/products-services/consumer-swift-lease/checklist.pdf";
    public final static String DEFAULT_STACK_WIDTH = "100%";
    public final static String DEFAULT_STACK_HEIGHT = "100%";  
    public final static Double DEFAULT_STACK_HEADER = 30.0;
    
    public final static String DEFAULT_TAB_WIDTH = "100%";
    public final static String DEFAULT_TAB_HEIGHT = "600px";  
    public final static Double DEFAULT_TAB_HEADER = 40.0;
    public final static DefaultFormat DEFAULT_DATEBOX_FORMAT = new DateBox.DefaultFormat(DateTimeFormat.getFormat(PredefinedFormat.DATE_MEDIUM));
	public final static NumberFormat DEFAULT_NUMBER_FORMAT = NumberFormat.getFormat("#,###,###,##0.00");
	public final static NumberFormat DEFAULT_INTEGER_FORMAT = NumberFormat.getFormat("###");
	public final static DateTimeFormat DEFAULT_DATE_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM);  
	public final static DateTimeFormat DEFAULT_DATE_TIME_FORMAT = DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_TIME_SHORT);      
	public final static int MENU_SPACING = 20;    
    
    /**
     * urgh!!! different developers, different ports? equals commits containing code
     * server configuration. Hopefully everyone agrees to use same port configs
     */
    public final static String GWT_CODE_SERVER = "?gwt.codesvr=127.0.0.1:9997";
    
	static StackLayoutPanel getStackLayout(Widget[]widgets, String[] headers)
	{
		StackLayoutPanel result = new StackLayoutPanel(Unit.PX);
		//either this or add to a simple panel with size set
		result.setHeight(GUIConstants.DEFAULT_STACK_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_STACK_WIDTH);
		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), 
					headers[i], GUIConstants.DEFAULT_STACK_HEADER);
		return result;
	}
	
	public static TabLayoutPanel getTabLayout(Widget[] widgets, String[]headers)
	{
		TabLayoutPanel result = new TabLayoutPanel(GUIConstants.DEFAULT_TAB_HEADER, Unit.PX);
		for(int i = 0; i < widgets.length; i++)
			result.add(widgets[i], headers[i]);
		return result;
	}	
	
	public static String getAcctDisplayName(String type, String ccy)
	{
		return type + " [" + ccy + "]";
	}  	
}
