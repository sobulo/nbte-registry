/**
 * 
 */
package com.fertiletech.nbte.client;


import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.CustomMessageTypes;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface MessagingManagerAsync {

	void sendMessage(
			CustomMessageTypes msgType,
			String[] addresses, String[] message, boolean isEmail,
			AsyncCallback<Void> callback);

	void getMessagingControllerNames(AsyncCallback<HashMap<String, String>> callback);

	void getControllerDetails(String controllerName,
			AsyncCallback<List<TableMessage>> callback);

}
