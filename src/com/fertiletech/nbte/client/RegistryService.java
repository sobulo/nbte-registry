package com.fertiletech.nbte.client;

import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("../reg")
public interface RegistryService extends RemoteService {
	List<TableMessage> getDepartmentRoster(String dept, String diploma,
			String year, int academicYear);
	List<TableMessage> getDepartmentCourseTemplates(String dept, String diploma, String year, int semester, int academicYear);
	String registerSelectedCourses() throws MissingEntitiesException, DuplicateEntitiesException;
	List<TableMessage> getCart();
	String updateCart(String courseID, boolean isAddition);
	String updateCart();
	TableMessage getCourseTemplateInfo(String templateID);
	List<TableMessage> getSessionInfo(String sessionID) throws MissingEntitiesException;
	List<TableMessage> getMySessions();
	HashMap<String, String> getBio() throws MissingEntitiesException;
	HashMap<String, String> saveBio(String id, HashMap<String, String> bio) throws MissingEntitiesException;
	String[] getCourseRegistrationDownloadLink(String sessionID) throws MissingEntitiesException;
	String[] getItemizedTuitionDownloadLink(String sessionID) throws MissingEntitiesException;
	String[] getAccountStatementLink(String sessionID) throws MissingEntitiesException;
	String[] getCourseRegistrationDownloadLink(String[] studs) throws MissingEntitiesException;
}
