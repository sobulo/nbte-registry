package com.fertiletech.nbte.client;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("../ticket")
public interface TicketService extends RemoteService {
	TableMessage createTicket(TableMessage ticketDetails);
	TableMessage editTicket(TableMessage[] details);
	List<TableMessage> getTickets(Date startDate, Date endDate);
	TableMessage getTicket(Long ticketNumber);
	HashMap<String, String> getAllTickets();
	List<TableMessage> addInvoiceID(String ticketID, Long invoiceID, boolean isAdd) throws MissingEntitiesException;
	List<TableMessage> addAllocationID(String ticketID, Long allocationID, boolean isAdd, String product, String location) throws MissingEntitiesException;
	List<TableMessage> getAllocations(String ticketID);
	List<TableMessage> getInvoices(String ticketID);
	List<TableMessage> addAllocationID(String ticketID, String allocationID,
			boolean isAdd) throws MissingEntitiesException;
	TableMessage getTicket(String ticketID);
}
