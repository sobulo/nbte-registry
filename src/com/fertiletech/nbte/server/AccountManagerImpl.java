/**
 * 
 */
package com.fertiletech.nbte.server;


import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.nbte.client.AccountManager;
import com.fertiletech.nbte.server.accounting.BillTemplate;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.accounting.InetCompanyAccount;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.accounting.InetDeposit;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.accounting.InetWithdrawal;
import com.fertiletech.nbte.server.accounting.LedgerEntry;
import com.fertiletech.nbte.server.accounting.Representative;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.User;
import com.fertiletech.nbte.server.downloads.BillingInvoiceGenerator;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.LoginValidationException;
import com.fertiletech.nbte.shared.ManualVerificationException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class AccountManagerImpl extends RemoteServiceServlet implements AccountManager{

	private static final Logger log = Logger.getLogger(AccountManagerImpl.class.getName());
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	

	
	private List<TableMessage> getBillListSummary(List<InetBill> billList)
	{
		try
		{
			return getBillListSummary(billList, false, true, ObjectifyService.begin());
		}
		catch(MissingEntitiesException ex)
		{
			throw new RuntimeException("Unexpected error: " + ex.getMessage());
		}
	}
	
	public static TableMessageHeader getBillSummaryHeaderForBlotter(boolean includeBillBio)
	{
		int numOfFields = includeBillBio? 11 : 8; 
		TableMessageHeader result = new TableMessageHeader(numOfFields);
		result.setText(3, "Total Due", TableMessageContent.NUMBER);
		result.setText(0, "Dept", TableMessageContent.TEXT);
		result.setText(1, "Last Name", TableMessageContent.TEXT);
		result.setText(2, "First Name", TableMessageContent.TEXT);
		result.setText(4, "Accepted Charges", TableMessageContent.NUMBER);
		result.setText(5, "Invoice ID", TableMessageContent.NUMBER);
		result.setFormatOption(5, "###");
		result.setText(6, "Date", TableMessageContent.DATE);
		result.setText(7, "Due Date", TableMessageContent.DATE);
		if(includeBillBio)
		{
			result.setText(8, "Title", TableMessageContent.TEXT);
			result.setText(9, "Category", TableMessageContent.TEXT);
			result.setText(10, "Currency", TableMessageContent.TEXT);		
		}
		
		return result;
	}
	
	private static TableMessage getBillSummaryMessage(BillTemplate template, InetBill bill, boolean includeBillBio, boolean includeStudentBio, String[] studFields)
	{
		int numOfTextFields = 1; int textFieldCursor = 0;
		if(includeBillBio) numOfTextFields += 3;
		if(includeStudentBio) numOfTextFields += 3;
		TableMessage billInfo = new TableMessage(numOfTextFields, 4, 2);
		billInfo.setMessageId(bill.getKey().getString());
		
		if(includeStudentBio)
		{
			billInfo.setText(textFieldCursor++, studFields[0]);
			billInfo.setText(textFieldCursor++, studFields[1]);
			billInfo.setText(textFieldCursor++, studFields[2]);			
		}
		
		if(includeBillBio)
		{
			billInfo.setText(textFieldCursor++, template.getFormatedTitle());
			billInfo.setText(textFieldCursor++, template.getTitle());
			billInfo.setText(textFieldCursor++, template.getCurrency());
		}
		
		billInfo.setText(textFieldCursor++, "False");					
		billInfo.setNumber(0, bill.getTotalAmount());
		billInfo.setNumber(1, null);
		billInfo.setNumber(2, bill.getInvoiceID());
		billInfo.setNumber(3, bill.getTicketKey() == null? null : bill.getTicketKey().getId());
		billInfo.setDate(0, bill.getCreateDate());
		billInfo.setDate(1, template.getDueDate());
		return billInfo;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBills(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBills(String studentID) throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		Objectify ofy = ObjectifyService.begin();
		Key<Student> tenantKey = ofy.getFactory().stringToKey(studentID);
		List<InetBill> billInfoList = InetDAO4Accounts.getStudentBills(tenantKey, ofy);
		return getBillListSummary(billInfoList);
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#savePayment(java.lang.String, double, java.util.Date, java.lang.String, java.lang.String)
	 */
	@Override
	public String savePayment(String billKey, String acctKeyStr, double amount, Date payDate, String comments) 
			throws MissingEntitiesException, ManualVerificationException
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	String userAuditId = "";//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Key<InetResidentAccount> depKey = ObjectifyService.factory().stringToKey(acctKeyStr);
    	Key<InetBill> bk = ObjectifyService.factory().stringToKey(billKey);
    	String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetWithdrawal payment = InetDAO4Accounts.createPayment(bk, depKey, amount, comments, payDate, user);
		log.warning("CREATED: " + payment.getKey() + " payment --by user " + userAuditId);
		String msg = new StringBuilder("Payment ID: ").append(payment.getKey().getId()).append( " Amount: ").
			append(payment.getAmount()).append(" date: ").append(payment.getEffectiveDate()).append( " Reference ID: ").append(payment.getReferenceId()).
			append(" Deducted from Account: ").append(String.valueOf(payment.getKey().getParent().getName())).toString();
		String[] autoComments = {msg, comments};
		TaskQueueHelper.scheduleCreateComment(autoComments, InetResidentAccount.getResident(depKey).getString(), user);
		return msg;
	}
	
	public static List<TableMessage> getBillDescriptionInfo(Key<InetBill> bdKey) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		InetBill bill = ofy.find(bdKey);
		if(bill == null)
		{
			String msgFmt = "Unable to find bill for : ";
			log.severe(msgFmt + bdKey);
			throw new MissingEntitiesException(msgFmt + bdKey.getName());
		}
		BillTemplate template = ofy.find(bill.getTemplateKey());
		if(template == null)
		{
			String msgFmt = "Unable to find bill description for " + bill.getUserKey().getName() + ": ";
			log.severe(msgFmt + bill.getTemplateKey());
			throw new MissingEntitiesException(msgFmt + bill.getTemplateKey().getId());			
		}
		
		return getBillDescriptionInfo(template);
	}
	
	public static List<TableMessage> getBillDescriptionInfo(BillTemplate template)
	{
		HashSet<BillDescriptionItem> itemizedBill = template.getItemizedBill();
		List<TableMessage> result = new ArrayList<TableMessage>(2 + itemizedBill.size());
		//setup summary info
		TableMessage summary = new TableMessage(2, 1, 4);
		summary.setText(0, template.getTitle());
		summary.setText(1, template.getCurrency());
		summary.setNumber(0, template.getTotalAmount());
		summary.setDate(0, template.getDueDate());
		summary.setDate(1, template.getStartDate());
		summary.setDate(2, template.getEndDate());
		summary.setDate(3, template.getTimeStamp());
		result.add(summary);
		
		//setup header for itemized bill
		TableMessageHeader itemizedHeader = new TableMessageHeader(3);
		itemizedHeader.setText(0, "Type", TableMessageContent.TEXT);
		itemizedHeader.setText(1, "Description", TableMessageContent.TEXT);
		itemizedHeader.setText(2, "Amount", TableMessageContent.NUMBER);
		result.add(itemizedHeader);
		
		for(BillDescriptionItem item : itemizedBill)
		{
			TableMessage itemRow = new TableMessage(2, 1, 0);
			itemRow.setText(0, item.getName());
			itemRow.setText(1, item.getComments());
			itemRow.setNumber(0, item.getAmount());
			result.add(itemRow);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys(java.lang.String)
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys(String studentID, boolean isLogin) throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	Key<Student> studKey = null;
    	if(isLogin)
    		studKey = new Key<Student>(Student.class, studentID);
    	else
    		studKey = ObjectifyService.begin().getFactory().stringToKey(studentID);
    	return getStudentBillKeys(studKey);
	}
	
	private static HashMap<String, String> getStudentBillKeys(Key<Student> studKey)
	{
		return InetDAO4Accounts.getStudentBillKeys( studKey, ObjectifyService.begin());
	}
	
	private List<TableMessage> getBillPayments(List<InetWithdrawal> payments, String headerStr)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + payments.size());
		//setup header message
		TableMessageHeader header = new TableMessageHeader(5);
		header.setText(0, "Payment ID", TableMessageContent.TEXT);
		header.setText(1, "Payment Amount", TableMessageContent.NUMBER);
		header.setText(2, "Payment Date", TableMessageContent.DATE);
		header.setText(3, "Entry Date", TableMessageContent.DATE);
		header.setText(4, "Comments", TableMessageContent.TEXT);
		header.setMessageId(headerStr);
		result.add(header);
		
		for(InetWithdrawal payInfo : payments)
		{
			TableMessage m = new TableMessage(2, 1, 2);
			m.setText(0, Long.toString(payInfo.getKey().getId()));
			m.setText(1, payInfo.getDescription());
			m.setNumber(0, payInfo.getAmount());
			m.setDate(0, payInfo.getEffectiveDate());
			m.setDate(1, payInfo.getEntryDate());
			result.add(m);
		}
		return result;
	}
	
	private List<TableMessage> getBillPayments(Key<InetBill> billKey, Objectify ofy)
	{
		List<InetWithdrawal> payments = InetDAO4Accounts.getPayments(billKey, ofy);
		return getBillPayments(payments, billKey.getString());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillAndPayment(java.lang.String)
	 */
	@Override
	public List<TableMessage> getStudentBillAndPayment(String billKeyStr) throws MissingEntitiesException, LoginValidationException
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN, PanelServiceLoginRoles.ROLE_GUARDIAN, PanelServiceLoginRoles.ROLE_STUDENT};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());		
		Objectify ofy = ObjectifyService.begin();
		Key<InetBill> billKey = ofy.getFactory().stringToKey(billKeyStr);
		InetBill studBill = ofy.find(billKey);
		
		if(studBill == null)
		{
			String msgFmt = "Unable to find student bill: ";
			log.severe(msgFmt + billKey);
			throw new MissingEntitiesException(msgFmt + billKey.getName());
		}
		BillTemplate template = ofy.find(studBill.getTemplateKey());
		if(template == null)
		{
			String msgFmt = "Unable to find student bill description: ";
			log.severe(msgFmt + studBill.getTemplateKey());
			throw new MissingEntitiesException(msgFmt + studBill.getTemplateKey().getId());
		}		
		List<TableMessage> result = new ArrayList<TableMessage>();
		
		result.add(getBillSummaryMessage(template, studBill, false, false, null)); //add student bill
		result.addAll(getBillDescriptionInfo(studBill.getKey())); //add bill description
		result.addAll(getBillPayments(billKey, ofy));
		
		return result;
	}

    public static <T> Map<Key<T>, T> getEntities(Collection<Key<T>> entityKeys, Objectify ofy, boolean throwException) throws MissingEntitiesException
    {
    	if(entityKeys == null || entityKeys.size() == 0) return new HashMap<Key<T>, T>(1);
        Map<Key<T>, T> entityData = ofy.get(entityKeys);
        if(throwException && entityData.size() != entityKeys.size())
        {
        	printKeyDifferences(entityKeys, entityData.keySet());
    		throw new MissingEntitiesException("Batch get failed, see server log for details");
        }
        return entityData;
    }

    public static <T> void printKeyDifferences(Collection<Key<T>> requestedCollection, Collection<Key<T>> retrievedCollection)
    {
    	Key<T>[] requestedKeys = new Key[requestedCollection.size()];
    	requestedKeys = requestedCollection.toArray(requestedKeys);
    	Key<T>[] retrievedKeys = new Key[retrievedCollection.size()];
    	retrievedKeys = retrievedCollection.toArray(retrievedKeys);
    	ArrayList<Key<T>> retrievedOnly, requestedOnly, intersect;
    	retrievedOnly = new ArrayList<Key<T>>();
    	requestedOnly = new ArrayList<Key<T>>();
    	intersect = new ArrayList<Key<T>>();
    	GeneralFuncs.arrayDiff(requestedKeys, retrievedKeys, intersect, requestedOnly, retrievedOnly);
    	
    	log.severe("Unable to load some entites from database ....");
    	for(Key<T> k : requestedOnly)
    		log.severe("Unable to retrieve: " + k);
        log.severe("Keys initially requested are ...... ");
        for(Key<T> errKey : requestedCollection)
            log.severe("Requested Key: " + errKey);    	
    }    
    
	public static List<TableMessage> getBillListSummary(Collection<InetBill> bills, boolean includeStudentBio, 
			boolean includeBillBio, Objectify ofy) throws MissingEntitiesException
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(bills.size());
		if(bills.size() == 0) return result;
		Map<Key<Student>,Student> studentMap = null;
		String[] studFields = null;
		
		if(includeStudentBio)
		{
			HashSet<Key<Student>> studentKeys = new HashSet<Key<Student>>(bills.size());
			for(InetBill b : bills)
				studentKeys.add((Key<Student>) b.getUserKey());
			studentMap = getEntities(studentKeys, ofy, true);
			studFields = new String[3];
		}
		
		
		HashSet<Key<BillTemplate>> templateKeys = new HashSet<Key<BillTemplate>>();
		for(InetBill b : bills)
				templateKeys.add(b.getTemplateKey());
		Map<Key<BillTemplate>, BillTemplate> templateMap = getEntities(templateKeys, ofy, true);
		
		for(InetBill b : bills)
		{
			if(includeStudentBio)
			{
				Student billedStudent = studentMap.get(b.getUserKey());
				HashMap<String, String> values = billedStudent.getValues();
				studFields[0] = values.get(ApplicationFormConstants.DEPARTMENT);
				studFields[1] = values.get(ApplicationFormConstants.SURNAME);
				studFields[2] = values.get(ApplicationFormConstants.FIRST_NAME);
			}
			TableMessage m = getBillSummaryMessage(templateMap.get(b.getTemplateKey()),b, includeBillBio, includeStudentBio, studFields);
			result.add(m);
		}
		return result;		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getBillPayments(java.lang.String)
	 */
	@Override
	public List<TableMessage> getBillPayments(String billKeyStr)
			throws MissingEntitiesException, LoginValidationException 
	{
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());				
		Key<InetBill> billKey = ObjectifyService.factory().stringToKey(billKeyStr);
		return getBillPayments(billKey, ObjectifyService.begin());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getInvoiceDownloadLink(java.lang.String, java.lang.String[])
	 */
	@Override
	public String getInvoiceDownloadLink(String[] billKeyStrs) throws MissingEntitiesException, LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
    	//LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
		return BillingInvoiceGenerator.getBillingInvoicesLink(billKeyStrs, getThreadLocalRequest());
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.AccountManager#getStudentBillKeys()
	 */
	@Override
	public HashMap<String, String> getStudentBillKeys()
			throws LoginValidationException {
    	//PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_STUDENT};
    	//String userID = LoginPortal.verifyRole(allowedRoles, this.getThreadLocalRequest());
    	return getStudentBillKeys(Student.getKey(LoginHelper.getLoggedInUser(getThreadLocalRequest())));
	}

	@Override
	public List<TableMessage> getBills(Date startDate, Date endDate) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		List<InetBill> bills = InetDAO4Accounts.getAllBillsByDate(startDate, endDate, ofy);
		List<TableMessage> result = new ArrayList<TableMessage>(bills.size() + 1);
		result.add(getBillSummaryHeaderForBlotter(true));
		result.addAll(getBillListSummary(bills, true, true, ofy));
		return result;
	}

	@Override
	public List<TableMessage> getDeposits(Date startDate, Date endDate) 
	{
		Objectify ofy = ObjectifyService.begin();
		return getLedgerDTO(InetDAO4Accounts.getDeposits(startDate, endDate, ofy), null, true, InetDAO4Accounts.getCompanyAcctsMap(ofy));
	}
	
	public static List<TableMessage> getLedgerDTO(List<? extends LedgerEntry> deposits, String headerStr, boolean includeAcctBio, Map<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + deposits.size());
		//setup header message
		int numOfCols = includeAcctBio ? 10 : 9;
		TableMessageHeader header = new TableMessageHeader(numOfCols);
		header.setText(0, "No.", TableMessageContent.NUMBER);
		header.setFormatOption(0,"###");		
		header.setText(1, "Description", TableMessageContent.TEXT);
		header.setText(2, "Ref.", TableMessageContent.TEXT);		
		header.setText(3, "Type", TableMessageContent.TEXT);			
		header.setText(4, "Amount", TableMessageContent.NUMBER);
		header.setText(5, "Account Balance", TableMessageContent.NUMBER);		
		header.setText(6, "Value Date", TableMessageContent.DATE);
		header.setText(7, "Entry Date", TableMessageContent.DATE);
		header.setText(8, "Entry User", TableMessageContent.TEXT);		
		if(includeAcctBio)
		{
			header.setText(9, "Bank", TableMessageContent.TEXT);
		}
		
		header.setMessageId(headerStr);
		result.add(header);

		Collections.sort(deposits, InetResidentAccount.getComparator());
		for(LedgerEntry dep : deposits)
		{
			TableMessage m = new TableMessage(4 + (includeAcctBio?1:0), 3, 2);
			m.setMessageId(dep.getKey().getString());
			m.setText(0, dep.getDescription());
			m.setText(1, dep.getReferenceId());	
			m.setText(2, dep.isCredit()? "Credit" : "Debit");
			m.setNumber(0, dep.getEntryCount());
			m.setNumber(1, dep.getAmount());
			m.setNumber(2, dep.getAccountBalance());
			
			m.setDate(0, dep.getEffectiveDate());
			m.setDate(1, dep.getEntryDate());	
			m.setText(3, dep.getEntryUser());			
			if(includeAcctBio)
			{
				Key<InetResidentAccount> accountKey = dep.getKey().getParent();
				log.warning("ACCT KEY: " + accountKey);
				String companyAcctName = "";
				if(dep instanceof InetDeposit)
				{	
					InetDeposit deposit= (InetDeposit) dep;
					InetCompanyAccount ba = bankAccts.get(deposit.getCompanyAccount());
					int numLength = ba.getAccountNumber().length();
					companyAcctName = ba.getBank().substring(0, Math.min(ba.getBank().length(), 6)) + ".." + ba.getAccountNumber().substring(numLength-4, numLength);
				}
				m.setText(4, companyAcctName);
			}
			result.add(m);
		}
		return result;		
	}
	

	@Override
	public List<TableMessage> getDeposits(String tenantID) {
		Objectify ofy = ObjectifyService.begin();
		Key<Student> tk = ofy.getFactory().stringToKey(tenantID);
		return getLedgerDTO(InetDAO4Accounts.getDeposits(tk, ofy), tenantID, true, InetDAO4Accounts.getCompanyAcctsMap(ofy));	
	}

	@Override
	public String createDeposit(String companyAcctStr, Date depositDate,
			String referenceNumber, String comments, String accountStr, double initialAmount) {
		log.warning("Received key: " + companyAcctStr);
		String userId = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<InetCompanyAccount> acctKey = ObjectifyService.factory().stringToKey(companyAcctStr);
		Key<InetResidentAccount> accountKey = ObjectifyService.factory().stringToKey(accountStr);
		InetDeposit deposit = InetDAO4Accounts.createDeposit(acctKey, depositDate, referenceNumber, 
				comments, accountKey, initialAmount, userId);
		return "Succesfully created deposit with amount of " + NumberFormat.getNumberInstance().format(deposit.getAmount());
	}

	@Override
	public TableMessage createAccount(String bank, String sortCode, String acctName, String acctNumber, String currency) throws DuplicateEntitiesException {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetCompanyAccount acct = InetDAO4Accounts.createAccount(bank, sortCode, acctName, acctNumber, currency, null, user);
		return getCompanyAccountDTO(acct);
	}

	@Override
	public List<TableMessage> getAllAccounts() {
		List<InetCompanyAccount> accounts = InetDAO4Accounts.getAllAccounts(ObjectifyService.begin());
		List<TableMessage> result = new ArrayList<TableMessage>(accounts.size());
		
		TableMessageHeader h = new TableMessageHeader(5);
		
		h.setText(DTOConstants.CMP_ACCT_BNK_IDX, "Bank", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_NAME_IDX, "Account Name", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_NUM_IDX, "Account Number", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_SRT_IDX, "Sort Code", TableMessageContent.TEXT);
		h.setText(DTOConstants.CMP_ACCT_CURR_IDX, "Currency", TableMessageContent.TEXT);

		result.add(h);
		for(InetCompanyAccount a : accounts)
		{
			TableMessage m = getCompanyAccountDTO(a);
			result.add(m);
		}
		return result;
		
	}
	
	public static TableMessage getCompanyAccountDTO(InetCompanyAccount a)
	{
		TableMessage m = new TableMessage(6,0,0);
		m.setText(DTOConstants.CMP_ACCT_BNK_IDX, a.getBank());
		m.setText(DTOConstants.CMP_ACCT_NAME_IDX, a.getAccountName());
		m.setText(DTOConstants.CMP_ACCT_NUM_IDX, a.getAccountNumber());
		m.setText(DTOConstants.CMP_ACCT_SRT_IDX, a.getSortCode());
		m.setText(DTOConstants.CMP_ACCT_CURR_IDX, a.getCurrency());
		m.setText(DTOConstants.CMP_ACCT_TYPE_IDX, a.getSuggestedType());
		m.setMessageId(a.getKey().getString());
		return m;		
	}

	@Override
	public HashMap<Long, String> getAllBillIDs() {
		HashMap<Long, String> result = new HashMap<Long, String>();
		QueryResultIterable<InetBill> bills = ObjectifyService.begin().query(InetBill.class).fetch();
		for(InetBill b : bills)
			result.put(b.getInvoiceID(), b.getKey().getString());
		
		return result;
	}

	@Override
	public String getDepositStatementDownloadLink(String depOwner, String[] depositIDs, Date start, Date end) throws MissingEntitiesException {
		return BillingInvoiceGenerator.getDepositStatementLink(depOwner, depositIDs, 
				start, end, getThreadLocalRequest());
	}

	@Override
	public TableMessage createCompanyRep(String firstName,
			String lastName, Date dob, String primaryEmail,
			String primaryNumber, HashSet<String> secondaryNumbers,
			HashSet<String> secondaryEmails, String companyName,
			String address, Boolean isMale, String title)
			throws DuplicateEntitiesException {
		/*Representative rep = WriteDAO.createRepresentative(firstName, lastName, dob, primaryEmail, primaryNumber, secondaryNumbers, secondaryEmails, companyName, address, isMale, title, false);
		return TenantDAOManagerImpl.getContactDTO(rep, null);*/
		//TODO fix
		return null;
	}

	@Override
	public List<TableMessage> getAllReps() {
		return InetDAO4Accounts.getAllReps();
	}

	@Override
	public List<TableMessage> getCompanyTenants(String repID) {
		Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		return InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
	}

	@Override
	public List<TableMessage> saveCompanyTenants(String[] tenantIDs, String repID) {
		/*Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		HashSet<Key<Tenant>> tenantKeys = new HashSet<Key<Tenant>>();
		for(String tid : tenantIDs)
		{
			Key<Tenant> tk = ObjectifyService.factory().stringToKey(tid);
			tenantKeys.add(tk);
		}
		List<Tenant> updatedObjects = WriteDAO.updateRepresentative(tenantKeys, repKey);
		StringBuilder sb = new StringBuilder("<b>Company/Billing representative added for the following tenants</b><ul>");
		for(Tenant t : updatedObjects)
		{
			sb.append("<li>").append(t.getFirstName()).append(" ").append(t.getLastName()).append(" @ Apt: ").append(t.getApartmentUnit().getParent().getName()).append("/" ).append(t.getApartmentUnit().getName());
			sb.append(" Company: ").append(t.getCompanyName()).append("</li>");
		}
		sb.append("</ul>");
		List<TableMessage> result = InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
		TableMessageHeader h = (TableMessageHeader) result.get(0);
		h.setCaption(sb.toString());
		return result;*/
		return null;
	}

	@Override
	public List<TableMessage> removeCompanyTenants(String[] tenantIDs, String repID) 
	{
		/*Key<Representative> repKey = ObjectifyService.factory().stringToKey(repID);
		HashSet<Key<Tenant>> tenantKeys = new HashSet<Key<Tenant>>();
		for(String tid : tenantIDs)
		{
			Key<Tenant> tk = ObjectifyService.factory().stringToKey(tid);
			tenantKeys.add(tk);
		}
		List<Tenant> updatedObjects = WriteDAO.removeRepresentative(tenantKeys, repKey);		
		StringBuilder sb = new StringBuilder("<b>Company/Billing removed for the following tenants</b><ul>");
		for(Tenant t : updatedObjects)
		{
			sb.append("<li>").append(t.getFirstName()).append(" ").append(t.getLastName()).append(" @ Apt: ").append(t.getApartmentUnit().getParent().getName()).append("/" ).append(t.getApartmentUnit().getName());
			sb.append(" Company: ").append(t.getCompanyName()).append("</li>");
		}
		sb.append("</ul>");
		List<TableMessage> result = InetDAO4Accounts.getCompanyTenants(repKey, ObjectifyService.begin());
		TableMessageHeader h = (TableMessageHeader) result.get(0);
		h.setCaption(sb.toString());
		return result;*/
		return null;
	}

	@Override
	public TableMessage createResidentAccount(String tenantID, String acctType, boolean allowOverDraft, String currency, int depositWarning) throws DuplicateEntitiesException {
		Key<Student> tk = ObjectifyService.factory().stringToKey(tenantID);
		String[] acctTypes = {acctType};
		List<InetResidentAccount> ra =  InetDAO4Accounts.createResidentAccount(tk, acctTypes, LoginHelper.getLoggedInUser(getThreadLocalRequest()), allowOverDraft, currency, depositWarning);
		return InetDAO4Accounts.getResidentAcctDTO(ra.get(0));
	}

	@Override
	public TableMessage getResidentAccountInfo(String tenantID, String acctType, String ccy) {
		Key<Student> tk = ObjectifyService.factory().stringToKey(tenantID);
		Key<InetResidentAccount> raKey = InetResidentAccount.getKey(tk, acctType, ccy);
		Objectify ofy = ObjectifyService.begin();
		InetResidentAccount ra = ofy.find(raKey);
		if(ra == null) return null;
		return InetDAO4Accounts.getResidentAcctDTO(ra);
	}

	@Override
	public List<TableMessage> getResidentAccountInfo(String tenantID) {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends User> tenantKey = ObjectifyService.factory().stringToKey(tenantID);
		HashSet<Key<Student>> tkList = new HashSet<Key<Student>>();
		String headerId = "";
		/*if(tenantKey.getParent() == null)
		{
			List<Key<Student>> companyTenants = InetDAO4Accounts.getCompanyTenants((Key<Representative>) tenantKey, ofy);
			tkList.addAll(companyTenants);
		}
		else 
		{*/
			headerId = tenantKey.getString();
			tkList.add((Key<Student>) tenantKey);
		//}
		return getAccountInfoForResident(tkList, ofy, headerId);
		//return null;
	}
	
	public static List<TableMessage> getAccountInfoForResident(HashSet<Key<Student>> tkList, Objectify ofy, String headerID)
	{
		List<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = InetDAO4Accounts.getResidentAcctDTOHeader();
		h.setMessageId(headerID);
		result.add(h);		
		for(Key<Student> tenantKey : tkList)
		{
			List<InetResidentAccount> accts = ofy.query(InetResidentAccount.class).ancestor(tenantKey).list();
			for(InetResidentAccount a : accts)
				result.add(InetDAO4Accounts.getResidentAcctDTO(a));
		}
		return result;
	}

	@Override
	public List<TableMessage> getLedgerAccountEntries(String acctID,
			Date startDate, Date endDate) {
		Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(acctID);
		Objectify ofy = ObjectifyService.begin();
		List<LedgerEntry> entries = InetDAO4Accounts.getLedgerEntries(raKey, startDate, endDate, ofy);
		return getLedgerDTO(entries, acctID, false, InetDAO4Accounts.getCompanyAcctsMap(ofy));
	}

	@Override
	public List<TableMessage> getLedgerResidentAcctEntries(String tenantID,
			Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TableMessage> getResidentAccountsByBuilding(String buildingId) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		SchoolSession currentTerm = EntityDAO.getCurrentTerm(ofy);
		List<Key<Student>> studentKeys = EntityDAO.queryDepartmentRoster(buildingId, null, null,
				null, ofy).listKeys();
		Map<Key<Student>, Student> students = ofy.get(studentKeys);
		//fetch building accounts
		List<InetResidentAccount> tempList = ofy.query(InetResidentAccount.class).list();
		List<InetResidentAccount> residentAccounts = new ArrayList<InetResidentAccount>(students.size());
		for(InetResidentAccount ra : tempList)
			if(students.containsKey(ra.getParentAccountKey()))
				residentAccounts.add(ra);
						
		//build result
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(residentAccounts.size() + 1);
		TableMessageHeader h = new TableMessageHeader(10);
		h.setText(0, "Type", TableMessageContent.TEXT);
		h.setText(1, "Ccy.", TableMessageContent.TEXT);
		h.setText(2, "-ve?", TableMessageContent.TEXT);
		h.setText(3, "Dipl.", TableMessageContent.TEXT);
		h.setText(4, "Dept.", TableMessageContent.TEXT);
		h.setText(5, "Company", TableMessageContent.TEXT);
		h.setText(6, "Attn.", TableMessageContent.TEXT);
		h.setText(7, "Bal.", TableMessageContent.NUMBER);
		h.setText(8, "Warn", TableMessageContent.NUMBER);
		h.setText(9, "Req.", TableMessageContent.NUMBER);
		h.setFormatOption(9, "#,##0.00");
		h.setEditable(9, true);
		result.add(h);
		
		for(InetResidentAccount ra : residentAccounts)
		{	
			TableMessage m = new TableMessage(10, 3, 2);
			Student stud = students.get(ra.getParentAccountKey());
			HashMap<String, String> vals = stud.getValues();
			//id
			m.setMessageId(ra.getKey().getString());
			
			//numeric fields
			m.setNumber(DTOConstants.RSD_ACCT_AMT_IDX, ra.getCurrentAmount());
			m.setNumber(DTOConstants.RSD_ACCT_LEV_IDX, ra.getDepositWarningLevel());

			//text fields
			m.setText(DTOConstants.RSD_ACCT_BLD_IDX, vals.get(ApplicationFormConstants.DIPLOMA));
			m.setText(DTOConstants.RSD_ACCT_APT_IDX, vals.get(ApplicationFormConstants.DEPARTMENT));
			m.setText(DTOConstants.RSD_ACCT_TYPE_IDX, ra.getDepositType());			
			m.setText(DTOConstants.RSD_ACCT_CURR_IDX, ra.getCurrency());
			m.setText(DTOConstants.RSD_ACCT_OVER_IDX, String.valueOf(ra.isAllowOverDraft()));

			m.setText(DTOConstants.RSD_ACCT_ATTN_ADDR_IDX, vals.get(ApplicationFormConstants.EMAIL));
			boolean isCurrent = stud.getAcademicYear()==currentTerm.getAcademicYear();
			m.setText(DTOConstants.RSD_ACCT_ARCHIVE_IDX, String.valueOf(isCurrent));
			
			String id = vals.get(ApplicationFormConstants.MATRIC_NO);
			if(id == null || id.trim().length() == 0)
				id = vals.get(ApplicationFormConstants.JAMB_NO);
			m.setText(DTOConstants.RSD_ACCT_COMPANY_IDX, id);
			m.setText(DTOConstants.RSD_ACCT_ATTN_IDX, vals.get(ApplicationFormConstants.FIRST_NAME) + " " + 
												vals.get(ApplicationFormConstants.SURNAME));
			m.setText(DTOConstants.RSD_ACCT_ATTN_ID_IDX, stud.getKey().getString());
			
			//date fields
			m.setDate(DTOConstants.RSD_ACCT_CRT_DT_IDX, ra.getCreateDate());
			m.setDate(DTOConstants.RSD_ACCT_CRT_DT_IDX, ra.getLastModified());
			
			result.add(m);
		}
		log.warning("Hey, returning: ******" + result.size() + "   accounts:" + residentAccounts.size());
		return result;
	}

	@Override
	public TableMessage updateAccount(String accId, String bank,
			String sortCode, String acctName, String acctNumber, String currency) throws MissingEntitiesException {
		Key<InetCompanyAccount> ak = ObjectifyService.factory().stringToKey(accId);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		InetCompanyAccount acct = InetDAO4Accounts.updateAccount(ak, bank, sortCode, acctName, acctNumber, currency, user);
		return getCompanyAccountDTO(acct);
	}

	@Override
	public String getDepositRequestLink(String[] companyAcctId, Date invDate,
			String description, List<TableMessage> requests) 
	{
		
		Key<InetCompanyAccount>[] ck = new Key[companyAcctId.length];
		for(int i = 0; i < companyAcctId.length; i++)
			ck[i] = ObjectifyService.factory().stringToKey(companyAcctId[i]);
		Map<Key<InetCompanyAccount>, InetCompanyAccount> companyAccount = ObjectifyService.begin().get(ck);
		
		/*return BillingInvoiceGenerator.getDepositRequestLink(companyAccount.values(), invDate, description, requests, getThreadLocalRequest());*/
		//TODO fix
		return null;
	}

	@Override
	public TableMessage updateResidentAccount(String acctID,
			boolean allowOverDraft, int depositLevel)
			throws MissingEntitiesException {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(acctID);
		InetResidentAccount ra = InetDAO4Accounts.updateResidentAccount(raKey, user, allowOverDraft, depositLevel);
		return InetDAO4Accounts.getResidentAcctDTO(ra);
	}

	@Override
	public String createUserBill(String[] users, String billTemplateList[], boolean generatePayment)
			throws MissingEntitiesException {
		String userAuditId = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		StringBuilder failures = new StringBuilder();
		StringBuilder ok = new StringBuilder();
		int count = 0;
		for(String billTemplate : billTemplateList)
		{
			for(int i = 0; i < users.length; i++)
			{
				String student = users[i];
				String email = ObjectifyService.factory().stringToKey(student).getName();
				log.warning("scheduling bill creation for: " + student);
				try
				{
					
					TaskQueueHelper.scheduleCreateUserBill(billTemplate, student, generatePayment, userAuditId);
					ok.append(email).append("-").append(ObjectifyService.factory().stringToKey(billTemplate).getId()).append(",");
					count++;
				}
				catch(RuntimeException ex)
				{
					log.warning("Error occured: " + ex.getMessage());
					failures.append(student).append(",");
				}
			}
		}
		StringBuilder result = new StringBuilder("Bill creation scheduled succesfully" +
				" for ").append(count).append(" invoice(s).<br/>").append(ok.toString());
		if(failures.length() > 0)
			result.append(" <br/>Please retry for the following users as failures occured: ").append(failures.toString());
		log.warning("BILL CREATION SCHEDULED for: " + users.length + " students --by user " + userAuditId);
		return result.toString(); 
	}

	@Override
	public List<TableMessage> getBillTemplate(String templateID) throws MissingEntitiesException {
		Key<BillTemplate> templateKey = new Key<BillTemplate>(templateID);
		BillTemplate template = ObjectifyService.begin().find(templateKey);
		if(template == null)
			throw new MissingEntitiesException("Unable to find template for: "
												+ templateKey);
		return getBillDescriptionInfo(template);
	}

	@Override
	public String createBillTemplate(
			LinkedHashSet<BillDescriptionItem> billDescs, String title,
			Date start, Date end, Date due, String ccy) throws DuplicateEntitiesException {
		
		BillTemplate template = InetDAO4Accounts.createBillTemplate(billDescs, title, start, end, ccy, EntityDAO.getCurrentTerm().getKey(), due);
		String result = template.getFormatedTitle() + " - " + template.getKey().getId() +
				" for " + template.getKey().getParent().getName();
		String[] comments = {"An invoice template was created", result};
		TaskQueueHelper.scheduleCreateComment(comments, template.getKey().getString(), LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return result;
	}

	@Override
	public List<TableMessage> getBillTemplateNames() 
	{
		List<BillTemplate> templates = ObjectifyService.begin().query(BillTemplate.class).ancestor(EntityDAO.getCurrentTerm().getKey()).list();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(templates.size());
		result.add(getBillTemplateDTOHeader());
		for(BillTemplate bt : templates)
			result.add(getbillTemplateDTO(bt));
		
		return result;
	}

	public TableMessage getbillTemplateDTO(BillTemplate t)
	{
		TableMessage m = new TableMessage(5, 1, 3);
		m.setText(DTOConstants.BT_TITLE_IDX, t.getTitle());
		m.setText(DTOConstants.BT_CCY_IDX, t.getCurrency());
		m.setText(DTOConstants.BT_TYPE_IDX, InetDAO4Accounts.getBillTypes().get(t.getTitle()));
		if(t.getTitle().contains("ND1"))
		{
			m.setText(DTOConstants.BT_YR_IDX, NameTokens.ALT_YEAR1);
			m.setText(DTOConstants.BT_DIP_IDX, NameTokens.ND);
		}
		else if(t.getTitle().equals("College Service Charges")) //TODO Fix this badly hacked code
		{
			m.setText(DTOConstants.BT_DIP_IDX, NameTokens.NDX);
		}
		m.setNumber(DTOConstants.BT_AMOUNT_IDX, t.getTotalAmount());
		m.setDate(DTOConstants.BT_START_IDX, t.getStartDate());
		m.setDate(DTOConstants.BT_END_IDX, t.getEndDate());
		m.setDate(DTOConstants.BT_DUE_IDX, t.getDueDate());
		m.setMessageId(t.getKey().getString());
		return m;
	}
	
	
	public TableMessageHeader getBillTemplateDTOHeader()
	{
		TableMessageHeader header = new TableMessageHeader(6);
		header.setText(0, "Type", TableMessageContent.TEXT);
		header.setText(1, "Amount", TableMessageContent.NUMBER);
		header.setText(2, "Start", TableMessageContent.DATE);
		header.setText(3, "End", TableMessageContent.DATE);
		header.setText(4, "Due", TableMessageContent.DATE);
		header.setText(5, "Ccy", TableMessageContent.TEXT);
		return header;
	}

	@Override
	public String createDeposits(String tenantID, Date depositDate)
			throws MissingEntitiesException {
		String userId = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		String triggerType = "PAYMENT CLEARANCE for ";
		Key<Student> studKey = new Key<Student>(tenantID);
		return createDeposits(studKey, depositDate, triggerType, userId);
	}

	public static String createDeposits(Key<Student> studKey, Date depositDate, String depositCommentPrefix, String userId) throws MissingEntitiesException
	{
		HashSet<Key<InetResidentAccount>> accountKeys = new HashSet<Key<InetResidentAccount>>();
		HashMap<String, Key<InetCompanyAccount>> companyMap 
							= new HashMap<String, Key<InetCompanyAccount>>();
		List<InetCompanyAccount> compList = ObjectifyService.begin().query(InetCompanyAccount.class).list();
		for(InetCompanyAccount c : compList)
			companyMap.put(c.getSuggestedType(), c.getKey());
		
		Objectify ofy = ObjectifyService.beginTransaction();
		StringBuilder result = new StringBuilder();
		try
		{
			//find the withdrawals
	 		List<InetWithdrawal> withdrawals = ofy.query(InetWithdrawal.class).ancestor(studKey).list();
	 		for(InetWithdrawal w : withdrawals)
	 		{
	 			Key<InetResidentAccount> acctKey = w.getKey().getParent();
	 			accountKeys.add(acctKey);
	 		}
	 		Map<Key<InetResidentAccount>,InetResidentAccount> accountMap 
	 								= getEntities(accountKeys, ofy, true); 
	 		HashMap<Key<InetResidentAccount>, InetResidentAccount> accountsToSave = 
	 				new HashMap<Key<InetResidentAccount>, InetResidentAccount>();
	 		ArrayList<InetDeposit> deposits = new ArrayList<InetDeposit>();
	 		for(InetWithdrawal w : withdrawals)
	 		{
	 			Key<InetResidentAccount> raKey = w.getKey().getParent();
	 			InetResidentAccount studAccount = accountMap.get(raKey);
	 			if(studAccount.getNumberOfTxns() != w.getEntryCount())
	 			{
	 				result.append("<p><b style='color:red'>Ignoring settlement request for </b>").append(w.getDescription()).
	 				append(" as entry count doesn't match txn count for ").
	 				append(raKey.getName()).append("-").append(raKey.getParent().getName()).append("</p>");
	 				continue;
	 			}	
	 			Key<InetCompanyAccount> ca = companyMap.get(studAccount.getDepositType());
	 			InetDeposit dep = InetDAO4Accounts.createDeposit(ca, depositDate, 
	 					depositCommentPrefix + w.getReferenceId(), depositCommentPrefix + w.getDescription(), studAccount, Math.abs(w.getAmount()), userId);
	 			deposits.add(dep);
	 			accountsToSave.put(raKey, studAccount);
 				result.append("<p><b style='color:green'>").append(depositCommentPrefix).append("</b>").
 				append(w.getDescription()).append(" deposit processed for ").
 				append(raKey.getName()).append("-").append(raKey.getParent().getName()).append("</p>");

	 		}
	 		ofy.put(deposits);
	 		ofy.put(accountsToSave.values());
	 		String[] comments = {result.toString()};
	 		TaskQueueHelper.scheduleCreateComment(comments, studKey.getString(), userId);
	 		ofy.getTxn().commit();
	 		return comments[0];
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
	}
}
