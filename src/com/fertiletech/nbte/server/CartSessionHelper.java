package com.fertiletech.nbte.server;


import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

public class CartSessionHelper {
	private final static String CART_SESSION = "zeromecart";
	public final static int QTY_IDX = 0;
	public final static int MTH_IDX = 1;
	
	public static HashSet<Key<Course>> getCart(HttpSession sess)
	{
		HashSet<Key<Course>> cart = (HashSet<Key<Course>>) sess.getAttribute(CART_SESSION);
		if(cart == null)
			cart = new HashSet<Key<Course>>();
		return cart;
	}
	
	private static void saveCart(HashSet<Key<Course>> cart, HttpSession sess)
	{
		sess.setAttribute(CART_SESSION, cart);
	}
	
	public static void emptyCart(HttpSession sess)
	{
		sess.removeAttribute(CART_SESSION);
	}

	public static String addToCart(String productID, HttpSession sess, boolean isAddition)
	{
		Key<Course> pdk = ObjectifyService.factory().stringToKey(productID);
		return addToCart(pdk, sess, isAddition);
	}
	
	public static String addToCart(Key<Course> pdk, HttpSession sess, boolean isAddition)
	{
		String result = "";
		HashSet<Key<Course>> cart = getCart(sess);
		String display = pdk.getName().replaceAll("~", " - ");
		if(isAddition)
		{
			if(cart.contains(pdk))
				result =  display + " has already been added.";
			else
			{
				cart.add(pdk);
				result = display + " added succesfully.";
			}
		}
		else
		{
			cart.remove(pdk);
			result = display + " removed succesfully.";
		}
		saveCart(cart, sess);
		return result + "<b> Click Register Now button if you're done selecting courses</b>";
	}
	
	public static TableMessageHeader fetchCustomerCartHeader()
	{
		return EntityDAO.getCourseDTOHeader();
	}
	
	public static void fetchCustomerCart(HttpSession sess, List<TableMessage> result)
	{
		HashSet<Key<Course>> cart = getCart(sess);
		if(cart == null)
			return;

		Map<Key<Course>, Course> detailAndPricingMap = getCartObjects(cart);
		
		for(Key<Course> pdk : cart)
		{
			Course details = (Course) detailAndPricingMap.get(pdk);
			TableMessage m = EntityDAO.getCourseTemplateDTO(details);
			result.add(m);
		}
	}
	
	public static Map<Key<Course>, Course> getCartObjects(HashSet<Key<Course>> cart)
	{		
		Map<Key<Course>, Course> detailAndPricingMap = ObjectifyService.begin().get(cart);
		return detailAndPricingMap;
	}

}
