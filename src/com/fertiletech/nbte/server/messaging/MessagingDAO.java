/**
 * 
 */
package com.fertiletech.nbte.server.messaging;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class MessagingDAO {
	
	private static final Logger log = Logger.getLogger(MessagingDAO.class.getName());
	
	public final static String PUBLIC_EMAILER = "pubmail";
	public final static String SYSTEM_EMAILER = "sysmail";
	
	public static EmailController createEmailController(String key, String fromAddress, String bccAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		EmailController controller = new EmailController(key, fromAddress, bccAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		Key<EmailController> controllerKey = controller.getKey();
		try
		{
			EmailController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}
	
	public static SMSController createSMSController(String key, String fromAddress,
			int characterLimit, int dailyMessageLimit, int totalMessageLimit, 
			String server, int port, String username, String password, String dlr, String type) throws DuplicateEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		SMSController controller = new SMSController(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit, server, port, username, password, dlr, type);
		Key<SMSController> controllerKey = controller.getKey();
		try
		{
			SMSController tempController = ofy.find(controllerKey);
			if(tempController != null)
			{
				log.severe("Duplicate Controller: " + controllerKey);
				throw new DuplicateEntitiesException(controllerKey.toString());
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return controller;
	}	
	
	/*public static void scheduleMessageSending(Key<? extends MessagingController> controllerKey, 
			HashMap<String, String[]> messageMap) throws MissingEntitiesException, ManualVerificationException 
	{	
		String controllerId = validateController(controllerKey, messageMap.size());
		
		for(String address : messageMap.keySet())
			TaskQueueHelper.scheduleMessageSending(controllerId, address, messageMap.get(address));		
	}*/
	
	public static void scheduleSendEmail(String toAddress, String[] messages, String subject, boolean isPublic)
	{
		String name = isPublic ? PUBLIC_EMAILER : SYSTEM_EMAILER;
		Key<EmailController> controllerKey = new Key<EmailController>(EmailController.class, name); 
		String controllerId = validateController(controllerKey, 1);
		TaskQueueHelper.scheduleMessageSending(controllerId, toAddress, messages, subject);
	}
	
	public static String validateController(Key<? extends MessagingController> controllerKey, int numOfMessages)
	{
		if(numOfMessages == 0) return null;
		Objectify ofy = ObjectifyService.beginTransaction();
		MessagingController controller = ofy.find(controllerKey);
		
		if(controller == null)
		{
			String msgFmt = "Unable to find messaging controller: ";
			log.warning(msgFmt + controllerKey.toString());
			throw new RuntimeException(msgFmt + controllerKey.getName());
		}
		
		String controllerId = ofy.getFactory().keyToString(controllerKey);
		try
		{
			int currentTotal = controller.updateNumberOfMessagesSent(numOfMessages);
			
			if( currentTotal == 0)
				throw new IllegalStateException("Controller not initiated properly: " + controllerKey.toString());
			
			int limit = controller.getDailyMessageLimit();
			if(currentTotal > limit)
			{
				String msgFmt = "Sending messages would cause new counts (%d) to exceed daily limits (%d)";
				msgFmt = String.format(msgFmt, currentTotal, limit);
				log.warning(msgFmt);
				throw new RuntimeException(msgFmt);
			}
			
			limit = controller.getTotalMessageLimit();
			if(controller.getTotalSentMessages() > limit)
			{
				String msgFmt = "Sending messages would cause new counts (%d) to exceed maximum limits(%d)";
				msgFmt = String.format(msgFmt, currentTotal, limit);
				log.warning(msgFmt);
				throw new RuntimeException(msgFmt);				
			}
			ofy.put(controller);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}		
		return controllerId;
	}
	
	public static HashMap<String, String> getMessagingControllerNames()
	{
		Objectify ofy = ObjectifyService.begin();
		//TODO replace this with polymorphic queries, will need to do a data conversion first though
		//before updating to latest build of objectify
		Class[] controllerClasses = {EmailController.class, SMSController.class};
		HashMap<String, String> result = new HashMap<String, String>();
		for(Class cls : controllerClasses)
		{
			List controllerKeys = ofy.query(cls).listKeys();
			for(int i = 0; i < controllerKeys.size(); i++)
			{
				Key ck = (Key) controllerKeys.get(i);
				result.put(ofy.getFactory().keyToString(ck), ck.getName());
			}
		}
		return result;
	}
	
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerName);
		MessagingController controller = ofy.find(controllerKey);
		if(controller == null)
		{
			String msgFmt = "Unable to find messaging controller: ";
			log.severe(msgFmt + controllerKey);
			throw new MissingEntitiesException(msgFmt + controllerKey.getName());
		}
		
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessage info = new TableMessage(1, 3, 1);
		info.setText(0, controllerKey.getName());
		info.setDate(0, controller.getLastAccessed());
		info.setNumber(0, controller.getTotalMessageLimit());
		info.setNumber(1, controller.getDailyMessageLimit());
		info.setNumber(2, controller.getTotalSentMessages());
		result.add(info);
		//setup header
		TableMessageHeader header = new TableMessageHeader(2);
		header.setText(0, "Access Date", TableMessageContent.DATE);
		header.setText(1, "Messages Sent", TableMessageContent.TEXT);
		result.add(header);
		HashMap<Date, Integer> dailyCounts = controller.getDailyMessageCount();
		for(Date accessDate : dailyCounts.keySet())
		{
			info = new TableMessage(1, 0, 1);
			info.setDate(0, accessDate);
			info.setText(0, dailyCounts.get(accessDate).toString());
			result.add(info);
		}
		return result;
	}	
}
