/**
 * 
 */
package com.fertiletech.nbte.server.messaging;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SMSController extends MessagingController{
	public final static String SUCCESS_PREFIX = "1701";
	// Username that is to be used for submission
	String username; 
	
	// password that is to be used along with username 
	String password; 
	
	/** * What type of the message that is to be sent 
	 * *<ul> 
	 * * <li>0:means plain text</li> 
	 * * <li>1:means flash</li> 
	 * * <li>2:means Unicode (Message content should be in Hex)</li> 
	 * * <li>6:means Unicode Flash (Message content should be in Hex)</li> 
	 * * </ul> */
	String type;
	
	/** * Require DLR or not * 
	 * <ul> 
	 * <li>0:means DLR is not Required</li> 
	 * <li>1:means DLR is Required</li> 
	 * </ul> */
	String dlr;
	
	// To what server you need to connect to for submission
	String server; 
	
	// Port that is to be used like 8080 or 8000
	int port;
	
	public SMSController(){ super(); }
	
	public SMSController(String key, String fromAddress,int characterLimit, int dailyMessageLimit, 
			int totalMessageLimit,String server, int port, String username, String password,
			String dlr, String type)
	{
		super(key, fromAddress, characterLimit, dailyMessageLimit, totalMessageLimit);
		this.username = username;
		this.password = password; 
		this.dlr = dlr; 
		this.type = type; 
		this.server = server; 
		this.port = port;
	}
	
	private void submitMessage(String destination, String message) throws IOException{
		// Url that will be called to submit the message
		URL sendUrl = new URL("http://" + this.server + ":" + this.port + "/bulksms/bulksms");
		HttpURLConnection httpConnection = (HttpURLConnection) sendUrl .openConnection();
		
		// This method sets the method type to POST so that 
		// will be send as a POST request 
		httpConnection.setRequestMethod("POST"); 
		
		// This method is set as true wince we intend to send 
		// input to the server 
		httpConnection.setDoInput(true);
		
		// This method implies that we intend to receive data from server.
		httpConnection.setDoOutput(true); 
		
		// Implies do not use cached data
		httpConnection.setUseCaches(false);
		
		// Data that will be sent over the stream to the server.
		DataOutputStream dataStreamToServer = new DataOutputStream( httpConnection.getOutputStream());
		dataStreamToServer.writeBytes("username=" + 
				URLEncoder.encode(this.username, "UTF-8") + "&password=" + 
				URLEncoder.encode(this.password, "UTF-8") + "&type=" + 
				URLEncoder.encode(this.type, "UTF-8") + "&dlr=" + 
				URLEncoder.encode(this.dlr, "UTF-8") + "&destination=" + 
				URLEncoder.encode(destination, "UTF-8") + "&source=" + 
				URLEncoder.encode(getFromAddress(), "UTF-8") + "&message=" + 
				URLEncoder.encode(message, "UTF-8"));
		dataStreamToServer.flush(); dataStreamToServer.close();
		
		// Here take the output value of the server.
		BufferedReader dataStreamFromUrl = new BufferedReader( new InputStreamReader(httpConnection.getInputStream()));
		String dataFromUrl = "", dataBuffer = "";
		
		// Writing information from the stream to the buffer
		while ((dataBuffer = dataStreamFromUrl.readLine()) != null) 
		{ 
			dataFromUrl += dataBuffer;
		}
		
		/** * Now dataFromUrl variable contains the Response received from the 
		 * * server so we can parse the response and process it accordingly. */
		dataStreamFromUrl.close();
		if(!dataFromUrl.startsWith(SUCCESS_PREFIX))
			throw new RuntimeException(dataFromUrl);
	}
	
	
	/* (non-Javadoc)
	 * @see j9educationentities.GAEPrimaryKeyEntity#getKey()
	 */
	@Override
	public Key<SMSController> getKey(){
		return new Key<SMSController>(SMSController.class, key);
	}

	/* (non-Javadoc)
	 * @see j9educationentities.messaging.MessagingController#sendMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean sendMessage(String toAddress, String[] message, String subject) {
		try {
			submitMessage(toAddress, message[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
