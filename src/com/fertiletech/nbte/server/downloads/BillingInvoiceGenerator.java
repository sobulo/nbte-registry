/**
 * 
 */
package com.fertiletech.nbte.server.downloads;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.nbte.server.AccountManagerImpl;
import com.fertiletech.nbte.server.accounting.BillTemplate;
import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.accounting.InetCompanyAccount;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.accounting.InetWithdrawal;
import com.fertiletech.nbte.server.accounting.LedgerEntry;
import com.fertiletech.nbte.server.bulkupload.InetImageBlob;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.User;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskConstants;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageFooter;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.pdfjet.Align;
import com.pdfjet.Box;
import com.pdfjet.Cell;
import com.pdfjet.CoreFont;
import com.pdfjet.Font;
import com.pdfjet.Image;
import com.pdfjet.ImageType;
import com.pdfjet.Letter;
import com.pdfjet.PDF;
import com.pdfjet.Page;
import com.pdfjet.Point;
import com.pdfjet.RGB;
import com.pdfjet.Table;
import com.pdfjet.TextLine;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class BillingInvoiceGenerator extends HttpServlet
{
    private final static int RIGHT_MARGIN = 40;
    private final static int TOP_MARGIN = 40;
    private final static int ITEM_COL_WIDTH = 170;
    private final static int NOTES_COL_WIDTH = 225;
    private final static int AMOUNT_COL_WIDTH = 75;
    private final static int PADDING_SIZE = 5;
    private final static int SPACE_BTW_BOXES = 12;
    private final static int HEADER_FONT_SIZE = 12;
    private final static int REGULAR_FONT_SIZE = 10;
    private final static int SMALL_FONT_SIZE = 8;
    private final static float REQ_REP_WIDTH = 550;
 
    private final static int BOX_STUDENT_INFO_HEIGHT = 80;
    private final static int BOX_ADDRESS_INFO_HEIGHT = 50;
    private final static int BOX_COMMENT_INFO_HEIGHT = 80;
    private final static int BOX_OFFICIAL_INFO_HEIGHT = 30;
    private final static int BOX_DEFAULT_COMPANY_HEIGHT = 60;
     
    private final static String SESS_PREFIX = "fertiletech.";
    
    private static final Logger log = Logger.getLogger(BillingInvoiceGenerator.class.getName());
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
		/*PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_ADMIN};
        try 
        {
			LoginPortal.verifyRole(allowedRoles, req);
		} catch (LoginValidationException e) 
		{
			res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
					"</b></body></html>");
			return;
		}*/
		
    	Objectify ofy = ObjectifyService.begin();
    	
    	String id = req.getParameter(TaskConstants.BILL_DESC_KEY_PARAM);
    	
    	if(id == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the report cards panel. If problem persists contact IT</font></b>");
    		return;
    	}
    	HttpSession sess = req.getSession();
    	String type = req.getParameter("type");
		String fileName = EntityConstants.DATE_FORMAT.format(new Date(Long.valueOf(id)));
		fileName = fileName.replace(" ", "-");
		String[] companyInfo = (String[]) sess.getAttribute(getSchoolInfoSessionName(id));
    	if(type == null)
    	{
	    	HashMap<String, List<TableMessage>> billDesc = (HashMap<String, List<TableMessage>>) sess.getAttribute(getBillDescSessionName(id));
	    	List<TableMessage> reportData = (List<TableMessage>) sess.getAttribute(getBillListSessionName(id));
	
	    	if(reportData == null || billDesc == null)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<b><font color='red'>Unable to retrieve invoice sesison data. Please ensure this page is accessed via a link" +
	    				" recently generated from the invoice panel. If problem persists contact IT</font></b>");
	    		return;    		
	    	}    	
			fileName = fileName + "-" + reportData.size() + "-resident"; 
			fileName += "-invoices.pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        OutputStream out = res.getOutputStream();
	        writePDFToOutputStream(ofy, out, billDesc, reportData, companyInfo);
	        log.warning("all done: " + fileName);
    	}
    	else if(type.equals("depstat"))
    	{
    		TableMessage[] depInfo = (TableMessage[]) sess.getAttribute(getDepositDescSessionName(id));
    		List<TableMessage>[] payments = (List<TableMessage>[]) sess.getAttribute(getDepositListSessionName(id));
    		TableMessage addressee = (TableMessage) sess.getAttribute(getDepositOwnerName(id));
    		Date[] queryDates = (Date[]) sess.getAttribute(getDepositQueryDatesName(id));
	    	if(depInfo == null || payments == null || payments.length != depInfo.length)
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<b><font color='red'>Unable to retrieve deposit session data. Please ensure this page is accessed via a link" +
	    				" recently generated from the deposit statements panel. If problem persists contact IT</font></b>" + (depInfo==null?depInfo:depInfo.length) +" , "+(payments==null?payments:payments.length)); 
	    		return;    		
	    	}    	
			fileName = fileName + "-deposit-statement.pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        OutputStream out = res.getOutputStream();
	        writeDepositStatementToStream(ofy, out, queryDates[0], queryDates[1], addressee, depInfo, payments, companyInfo);	    	
    	}
    	else if(type.equals("depreqs"))
    	{
    		HashMap<String, List<TableMessage>> requestListMap = (HashMap<String, List<TableMessage>>) sess.getAttribute(getDepositRequestsListName(id));
    		HashMap<String, TableMessage> addresseeMap = (HashMap<String, TableMessage>) sess.getAttribute(getDepositRequestsToName(id));
    		HashMap<String, TableMessage> bankInfo = (HashMap<String, TableMessage>) sess.getAttribute(getDepositRequestsBankName(id));
    		Date invDate = (Date) sess.getAttribute(getDepositRequestsDateName(id));
    		String description = (String) sess.getAttribute(getDepositRequestsDescrName(id));
    		
	
	    	if(requestListMap == null || addresseeMap == null || bankInfo == null || invDate == null || description == null || requestListMap.size() == 0 || requestListMap.size() != addresseeMap.size())
	    	{
	    		res.setContentType("text/html");
	    		res.getOutputStream().println("<b><font color='red'>Unable to retrieve invoice sesison data. Please ensure this page is accessed via a link" +
	    				" recently generated from the invoice panel. If problem persists contact IT</font></b>");
	    		return;    		
	    	}    	
			fileName = fileName + "-deposit-request.pdf";
	        res.setContentType("application/octet-stream");        
	        res.setHeader("Content-disposition", "attachment; filename=" + fileName);	        
	        OutputStream out = res.getOutputStream();
	        String updateUser = LoginHelper.getLoggedInUser(req);
	        writeDepositRequestToOutputStream(updateUser, ofy, out, requestListMap, addresseeMap, bankInfo, companyInfo, invDate, description);
	        log.warning("all done: " + fileName);    		
    	}
    	else
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unrecognized op type. Are you sure you have the right url? If problem persists contact IT</font></b>"); 
    	}
        /*out.flush();
        log.warning("flushing done");
        out.close();
        log.warning("close done");*/
    }

    private static void writeDepositStatementToStream(Objectify ofy, OutputStream out, Date startDate, Date endDate, TableMessage addressee, TableMessage[] depositInfo, List<TableMessage>[] payments, String[] schoolInfo)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(SMALL_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(SMALL_FONT_SIZE-1);
	        
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printDepositStatements(startDate, endDate, addressee, payments, depositInfo, schoolInfo, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    
    private static void writePDFToOutputStream(Objectify ofy, OutputStream out, HashMap<String, List<TableMessage>> billDesc, List<TableMessage> reportData, String[] schoolInfo)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(REGULAR_FONT_SIZE);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(REGULAR_FONT_SIZE);
	        
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException( "Unable to find key: " + logoKey +
 						"Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printReportCards(billDesc, reportData, schoolInfo, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }
    
    private static void writeDepositRequestToOutputStream(String updateUser, Objectify ofy, OutputStream out, HashMap<String, List<TableMessage>> requestListMap,
    		HashMap<String, TableMessage> addresseeMap, HashMap<String, TableMessage> bankInfo, String[] schoolInfo, Date invDate, String invDesc)
    {    	    	
    	//init pdf file
    	try
    	{        	
	        PDF pdf = new PDF(out);
	        
	        Font f1 = new Font(pdf, CoreFont.HELVETICA_BOLD);
	        f1.setSize(REGULAR_FONT_SIZE-1);
	        Font f2 = new Font(pdf, CoreFont.HELVETICA);
	        f2.setSize(REGULAR_FONT_SIZE-1);
	        
			Key<InetImageBlob> logoKey = new Key(InetImageBlob.class, DTOConstants.PDF_REPORT_LOGO);
			InetImageBlob logo = ofy.find(logoKey);
			if(logo == null)
			{
				throw new IllegalArgumentException("Your company hasn't been properly setup for report" +
	    				" generation or setup data has been corrupted. Please contact IT to investigate");    					
			}	        
	        BufferedInputStream bis =
                new BufferedInputStream(new ByteArrayInputStream(logo.getImage()));
    	
	    	printDepositRequest(updateUser, requestListMap, addresseeMap, bankInfo, schoolInfo, invDate, invDesc, bis, f1, f2, pdf);
	    	pdf.flush();
    	}
    	catch(Exception ex)
    	{
    		StackTraceElement[] exLocations = ex.getStackTrace();
    		String msg = "";
    		for(StackTraceElement  exLoc : exLocations)
    		{
    			String exMsg = exLoc.toString();
    			if(exMsg.startsWith("com.fertile"))
    				msg += exMsg + "\n";
    		}
    		log.severe(msg);    		
    		throw new RuntimeException(ex.fillInStackTrace());
    	}    	
    }    
    
    private static void printReportCards(HashMap<String, List<TableMessage>> billDescMap,
    		List<TableMessage> studentBills, String[] schoolInfo, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.85);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        for(int i = 0; i < studentBills.size(); i++)
        {
        	TableMessage studentBill = studentBills.get(i);
        	
        	//convert bill desc itemized data to format experted by print report fn
        	List<TableMessage> billDescList = billDescMap.get(studentBill.getMessageId());
            TableMessage billDescSummary = billDescList.remove(0); 
        	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(billDescList, f1, f2, null);
        	
		    //new page for student report card	
		    Page page = new Page(pdf, Letter.PORTRAIT);
		    
		    //setup table
		    Table table = new Table(f1, f2);
		    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, tableY);
		    table.setCellPadding(PADDING_SIZE);
		    table.autoAdjustColumnWidths();
		    table.setColumnWidth(0, ITEM_COL_WIDTH);
		    table.setColumnWidth(1, NOTES_COL_WIDTH);
		    table.setColumnWidth(2, AMOUNT_COL_WIDTH);
		    table.rightAlignNumbers();
		    int numOfPages = table.getNumberOfPages(page);
		    if(numOfPages != 1)
		    	throw new IllegalArgumentException("Billing template data too Large. Contact IT to setup a custom pdf report for this invoice");
		    
		    Point tableEnd = table.drawOn(page);
		    log.warning("Table created succesfully");
		    //setup school info
		    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, table.getWidth(), BOX_COMPANY_INFO_HEIGHT);
		    TextLine text = new TextLine(f3, schoolInfo[DTOConstants.COMPANY_INFO_NAME_IDX]);
		    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
		    text.setPosition(textStart, textY);
		    
		    TextLine text1 = new TextLine(f4, schoolInfo[DTOConstants.COMPANY_INFO_ADDR_IDX] );
		    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    text1.setPosition(textStart, text1Y);
		    
		    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[DTOConstants.COMPANY_INFO_NUMS_IDX]);
		    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2.setPosition(textStart, text2Y);
		    
		    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[DTOConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[DTOConstants.COMPANY_INFO_EMAIL_IDX]);
		    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2b.setPosition(textStart, text2bY);		    
	    
		    logo.setPosition(RIGHT_MARGIN + table.getWidth() - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + PADDING_SIZE);
		    
		    schoolInfoBox.drawOn(page);
		    text.drawOn(page);
		    text1.drawOn(page);
		    text2.drawOn(page);
		    text2b.drawOn(page, true);
		    logo.drawOn(page);
		    log.warning("school info generated succesfully");
		    //setup student info
		    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , table.getWidth(), BOX_STUDENT_INFO_HEIGHT);
		    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Unit: " + studentBill.getText(0), 
		    		textStart, textY, f2, page);

		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Resident: " + studentBill.getText(2) + " " + studentBill.getText(1), 
		    		textStart, textY, f2, page);
		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Title: " + billDescSummary.getText(0), textStart, textY, f2, page);
		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    String createDate = (billDescSummary.getDate(3) == null?"":EntityConstants.DATE_FORMAT.format(billDescSummary.getDate(3)));
		    printLine("Creation Date: " + createDate, textStart, textY, f2, page);

		    String period = (billDescSummary.getDate(1) == null?"":EntityConstants.DATE_FORMAT.format(billDescSummary.getDate(1))) + " to " + 
		    		(billDescSummary.getDate(2) == null?"":EntityConstants.DATE_FORMAT.format(billDescSummary.getDate(2)));
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Service Period: " + period, textStart, textY, f2, page);
		    
		    studentBox.drawOn(page);
		    log.warning("tenant info generated ok");
		    
		    //setup comments box
		    double commentsBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
		    Box commentsBox = new Box(RIGHT_MARGIN, commentsBoxY , table.getWidth(), BOX_COMMENT_INFO_HEIGHT);
		    
		    textY = (int) Math.ceil(commentsBoxY + PADDING_SIZE + REGULAR_FONT_SIZE);
		    printLine("Invoice ID: " + Math.round(studentBill.getNumber(2)), 
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Due Date: " + (billDescSummary.getDate(0) == null?"":EntityConstants.DATE_FORMAT.format(billDescSummary.getDate(0))), 
		    		textStart, textY, f2, page);		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Total Due: " + EntityConstants.NUMBER_FORMAT.format(studentBill.getNumber(0)), 
		    		textStart, textY, f2, page);		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("Amount Paid: " + EntityConstants.NUMBER_FORMAT.format(studentBill.getNumber(1)), 
		    		textStart, textY, f2, page);
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    Font fob = studentBill.getText(3).equals("True") ? f2 : f1;
		    String currency = billDescSummary.getText(1);
		    printLine("Outstanding Balance: " + EntityConstants.NUMBER_FORMAT.format(studentBill.getNumber(0) - studentBill.getNumber(1)) + " " + currency, 
		    		textStart, textY, fob, page);  
		    
            commentsBox.drawOn(page);
            log.warning("comments box setup succesfully");
            //setup official box
            double officialBoxY = commentsBoxY + BOX_COMMENT_INFO_HEIGHT + SPACE_BTW_BOXES;
            Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , table.getWidth(), BOX_OFFICIAL_INFO_HEIGHT);
            TextLine text7 = new TextLine(f2, "Official Name:                                           ");
            double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
            text7.setPosition(textStart, text7Y);
            text7.setUnderline(true);
            TextLine text8 = new TextLine(f2, "Official Signature:                                      ");
            text8.setUnderline(true);
            text8.setPosition(textStart + table.getWidth()/2, text7Y);
            text7.drawOn(page);
            text8.drawOn(page);
            officialBox.drawOn(page);       
            log.warning("report card generated succesfully");
        }
    }
    
    private static void printDepositStatements(Date startDate, Date endDate, TableMessage addressee, List<TableMessage>[] payments,
    		TableMessage[] deposits, String[] schoolInfo, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 

	    //new page for student report card		    
	    Page page = new Page(pdf, Letter.PORTRAIT);
        
        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.85);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_STUDENT_INFO_HEIGHT + SPACE_BTW_BOXES;
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;

	    log.warning("Table created succesfully");
	    TextLine text = new TextLine(f3, schoolInfo[DTOConstants.COMPANY_INFO_NAME_IDX]);
	    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
	    text.setPosition(textStart, textY);
	    
	    TextLine text1 = new TextLine(f4, schoolInfo[DTOConstants.COMPANY_INFO_ADDR_IDX] );
	    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
	    text1.setPosition(textStart, text1Y);
	    
	    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[DTOConstants.COMPANY_INFO_NUMS_IDX]);
	    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2.setPosition(textStart, text2Y);
	    
	    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[DTOConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[DTOConstants.COMPANY_INFO_EMAIL_IDX]);
	    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
	    text2b.setPosition(textStart, text2bY);		    
    	    
	    text.drawOn(page);
	    text1.drawOn(page);
	    text2.drawOn(page);
	    text2b.drawOn(page, true);
	    log.warning("company info generated succesfully");        
	    //box borders
	    double maxWidth = 550;
	    //setup student info
	    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    printLine("NAME: " + addressee.getText(RegistryDTO.FIRST_NAME_IDX) + " " + 
	    addressee.getText(RegistryDTO.LAST_NAME_IDX), textStart, textY, f2, page);

	    printLine("MATRIC: " + addressee.getText(RegistryDTO.MATRIC_NO_IDX), textStart + (int) maxWidth/2, textY, f2, page);

	    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine("DEPT:  " + addressee.getText(RegistryDTO.STUD_DEPT_IDX),
		    		textStart, textY, f2, page);	    	
		    printLine("LEVEL: " + addressee.getText(RegistryDTO.STUD_DIPL_IDX) + " - " + addressee.getText(RegistryDTO.STUD_YEAR_IDX),
		    		textStart + (int) maxWidth/2, textY, f2, page);	    	

	    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    printLine("SESSION: " + addressee.getText(RegistryDTO.ACADEMIC_YEAR), textStart, textY, f2, page);
	    
	    
	    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
	    printLine("STATEMENT PERIOD: " + EntityConstants.DATE_FORMAT.format(startDate) + " to " + EntityConstants.DATE_FORMAT.format(endDate), textStart, textY, f2, page);	    
	    printLine("NO. OF ACCOUNTS INCLUDED: " + deposits.length, textStart + (int) maxWidth/2, textY, f2, page);	    

	    log.warning("tenant info generated ok");
	    
	    

	    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, maxWidth, BOX_COMPANY_INFO_HEIGHT);
	    logo.setPosition(RIGHT_MARGIN + maxWidth - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + (BOX_COMPANY_INFO_HEIGHT/2 - logo.getHeight()/2));
	    logo.drawOn(page);
	    schoolInfoBox.drawOn(page);
	    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , maxWidth, BOX_STUDENT_INFO_HEIGHT);
	    studentBox.drawOn(page);
	    
	    
	    boolean first = true;
	    Table table = null;
	    Point tableEnd = null;
	    textY = textY + SPACE_BTW_BOXES + PADDING_SIZE;
        for(int i = 0; i < deposits.length; i++)
        { 		     	
        	//convert bill desc itemized data to format experted by print report fn
        	List<TableMessage> payList = payments[i];
        	log.warning(payList.toString());

        	HashSet<Integer> ignore = new HashSet<Integer>();
        	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(payList, f1, f2, ignore);        	
        	
		    //print deposit header
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE + PADDING_SIZE;
		    StringBuilder depositHeader = new StringBuilder("[Account: ").append(deposits[i].getText(0)).append(" / ").append(deposits[i].getText(1));
		    depositHeader.append("]    [Balance: ").append(EntityConstants.NUMBER_FORMAT.format(deposits[i].getNumber(0))).
		    		append("]    [Last Modified: ").append(EntityConstants.DATE_FORMAT.format(deposits[i].getDate(0)) + "]");
		    double[] headerPositiveColr = {8/255.0, 159/255.0, 82/255.0};
		    double[] headerNegativeColr = {192/255.0, 50/255.0, 46/255.0};
		    double[] lineColor = deposits[i].getNumber(0) > 0? headerPositiveColr : headerNegativeColr ;
		    printLine(depositHeader.toString(), textStart, textY, f1, page, lineColor);
		    
		    //table aettings
		    table = new Table(f1, f2);
		    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, textY + (PADDING_SIZE));
		    table.setCellPadding(PADDING_SIZE);
		    table.setColumnWidth(0, maxWidth * 0.05);
		    table.setColumnWidth(1, maxWidth * 0.5);
		    table.setColumnWidth(2, maxWidth * 0.05);
		    table.setColumnWidth(3, maxWidth * 0.11);
		    table.setColumnWidth(4, maxWidth * 0.11);
		    table.setColumnWidth(5, maxWidth * 0.09);
		    table.setColumnWidth(6, maxWidth * 0.09);
		    table.rightAlignNumbers();
		    
		    //print payments table for this deposit
		    while(true)
		    {
			    tableEnd = table.drawOn(page);
			    if(!table.hasMoreData())
			    {
			    	textY = (int) tableEnd.getY();
			    	break;
			    }
			    else
			    {
			    	page = new Page(pdf, Letter.PORTRAIT);
			    	textY = TOP_MARGIN;
			    }
		    }    		    
        }
        //setup official box
        double officialBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
        Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , maxWidth, BOX_OFFICIAL_INFO_HEIGHT);
        TextLine text7 = new TextLine(f2, "Official Name:                                                                                    ");
        double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
        text7.setPosition(textStart, text7Y);
        text7.setUnderline(true);
        TextLine text8 = new TextLine(f2, "Official Signature:                                                                           Date: " 
        			+ EntityConstants.DATE_FORMAT.format(new Date()));
        text8.setUnderline(true);
        text8.setPosition(textStart + maxWidth/2, text7Y);
        text7.drawOn(page);
        text8.drawOn(page);
        officialBox.drawOn(page);       
        log.warning("deposit statements generated succesfully");
    }
    
    private static void printDepositRequest(String updateUser, HashMap<String, List<TableMessage>> requestListMap,
    		HashMap<String, TableMessage> addresseeMap, HashMap<String, TableMessage> bankInfo, String[] schoolInfo, Date invDate, String invDesc, BufferedInputStream imageStream,
    		Font f1, Font f2, PDF pdf) throws Exception
    {
    	//setup additional fonts
        Font f3 = new Font(pdf, CoreFont.HELVETICA_BOLD);
        f3.setSize(HEADER_FONT_SIZE);
        Font f4 = new Font(pdf, CoreFont.HELVETICA_OBLIQUE);
        f4.setSize(SMALL_FONT_SIZE); 
        Font totalFont = new Font(pdf, CoreFont.COURIER_BOLD);
        totalFont.setSize(SMALL_FONT_SIZE);
        HashMap<String, double[]> currencyColors = getCurrencyColors();

        //school logo
        Image logo = new Image(pdf, imageStream, ImageType.JPEG);
        logo.scaleBy(0.85);
        
        final int BOX_COMPANY_INFO_HEIGHT = (int) Math.max(logo.getHeight() + (2 * PADDING_SIZE), BOX_DEFAULT_COMPANY_HEIGHT);
        
        int textStart = RIGHT_MARGIN + PADDING_SIZE;
        
        int studentBoxY = TOP_MARGIN + BOX_COMPANY_INFO_HEIGHT + SPACE_BTW_BOXES;
        int tableY = studentBoxY + BOX_ADDRESS_INFO_HEIGHT + SPACE_BTW_BOXES;
        int footerCountx = 0;
        for(String attentionID : requestListMap.keySet())
        {
        	TableMessage recipient = addresseeMap.get(attentionID);
        	List<TableMessage> reqTableInfo = requestListMap.get(attentionID);
        	
        	//convert bill desc itemized data to format experted by print report fn
        	List<List<Cell>> itemizedBillDesc = PDFGenerationHelper.convertTableMessageToPDFTable(reqTableInfo, f1, f2, f1, null);
        	
		    //new page for student report card		    
		    Page page = new Page(pdf, Letter.PORTRAIT);
		    
		    //setup table
		    Table table = new Table(f1, f2);
		    table.setData(itemizedBillDesc, Table.DATA_HAS_1_HEADER_ROWS);
		    table.setLineWidth(0.2);
		    table.setPosition(RIGHT_MARGIN, tableY);
		    table.setCellPadding(PADDING_SIZE);
		    table.setColumnWidth(0, REQ_REP_WIDTH * .13);
		    table.setColumnWidth(1, REQ_REP_WIDTH * .13);
		    table.setColumnWidth(2, REQ_REP_WIDTH * .32);
		    table.setColumnWidth(3, REQ_REP_WIDTH * .08);
		    table.setColumnWidth(4, REQ_REP_WIDTH * .17);
		    table.setColumnWidth(5, REQ_REP_WIDTH * .17);
		    for(int i = reqTableInfo.size()-1; i >0; i--)
		    {
		    	TableMessage m = reqTableInfo.get(i);
		    	if(!(m instanceof TableMessageFooter)) break;
		    
	    		table.getCellAt(i, 0).setColspan(3);
	    		table.getCellAt(i, 0).setTextAlignment(Align.RIGHT);
		    }
		    //table.autoAdjustColumnWidths();
		    table.rightAlignNumbers();
		    int numOfPages = table.getNumberOfPages(page);
		    if(numOfPages != 1)
		    	throw new IllegalArgumentException("Request template data too Large. Contact IT to setup a custom pdf report for this invoice");
		    
		    Point tableEnd = table.drawOn(page);
		    log.warning("Table created succesfully");
		    
		    //setup fieldco company contact info
		    Box schoolInfoBox = new Box(RIGHT_MARGIN, TOP_MARGIN, table.getWidth(), BOX_COMPANY_INFO_HEIGHT);
		    TextLine text = new TextLine(f3, schoolInfo[DTOConstants.COMPANY_INFO_NAME_IDX]);
		    int textY = TOP_MARGIN + PADDING_SIZE + HEADER_FONT_SIZE;
		    text.setPosition(textStart, textY);
		    
		    TextLine text1 = new TextLine(f4, schoolInfo[DTOConstants.COMPANY_INFO_ADDR_IDX] );
		    int text1Y = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    text1.setPosition(textStart, text1Y);
		    
		    TextLine text2 = new TextLine(f4, "Tel: " + schoolInfo[DTOConstants.COMPANY_INFO_NUMS_IDX]);
		    int text2Y = text1Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2.setPosition(textStart, text2Y);
		    
		    TextLine text2b = new TextLine(f4, "Web: " + schoolInfo[DTOConstants.COMPANY_INFO_WEB_IDX] + "   Email: " + schoolInfo[DTOConstants.COMPANY_INFO_EMAIL_IDX]);
		    int text2bY = text2Y + PADDING_SIZE + SMALL_FONT_SIZE;
		    text2b.setPosition(textStart, text2bY);		    
	    
		    logo.setPosition(RIGHT_MARGIN + table.getWidth() - logo.getWidth() - PADDING_SIZE, TOP_MARGIN + PADDING_SIZE);
		    
		    schoolInfoBox.drawOn(page);
		    text.drawOn(page);
		    text1.drawOn(page);
		    text2.drawOn(page);
		    text2b.drawOn(page, true);
		    logo.drawOn(page);
		    log.warning("school info generated succesfully");
		    
		    //setup student info
		    Box studentBox = new Box(RIGHT_MARGIN, studentBoxY , table.getWidth(), BOX_ADDRESS_INFO_HEIGHT);
		    textY = studentBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine(recipient.getText(RQ_NAME_IDX), textStart, textY, f2, page);

		    if(recipient.getText(RQ_COMP_IDX) != null)
		    {
			    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
			    printLine(recipient.getText(RQ_COMP_IDX), 
			    		textStart, textY, f2, page);
		    }
		    
		    textY = textY + PADDING_SIZE + REGULAR_FONT_SIZE;
		    printLine(recipient.getText(RQ_ADDR_IDX), textStart, textY, f2, page);
		    		    
		    studentBox.drawOn(page);
		    log.warning("tenant info generated ok");
		    
		    //setup comments box
		    double commentsBoxY = tableEnd.getY() + SPACE_BTW_BOXES;
		    Box commentsBox = new Box(RIGHT_MARGIN, commentsBoxY , table.getWidth(), BOX_COMMENT_INFO_HEIGHT);
		    
		    textY = (int) Math.ceil(commentsBoxY + PADDING_SIZE + REGULAR_FONT_SIZE);		  
		    printLine(invDesc, textStart, textY, f1, page);
		    
		    //display footers
		    HashSet<String> footerCurrencies = new HashSet<String>();
		    for(int i = reqTableInfo.size() - 1; i >= 0; i--)
		    {
		    	TableMessage row = reqTableInfo.get(i);
		    	if(!(row instanceof TableMessageFooter)) break;
		    	textY = textY + PADDING_SIZE + SMALL_FONT_SIZE;
		    	
			    double amountRequested = reqTableInfo.get(i).getNumber(1);
			    String amountWords = NumericTextUtil.convert(Math.round(amountRequested));
			    String curr = row.getText(3);
			    String currName = DTOConstants.CURRENCY_MAP_NAME.get(curr);
			    String currSign = DTOConstants.CURRENCY_MAP_SIGN.get(curr);
			    String figureAmount = EntityConstants.NUMBER_FORMAT.format(amountRequested);
			    String line = "[" + curr + "]" + figureAmount + " (" + amountWords + " " + currName + ").";
			    printLine(line, textStart, textY, totalFont, page, currencyColors.get(curr));
			    footerCurrencies.add(curr);
		    }
		    
            commentsBox.drawOn(page);
            log.warning("comments box setup succesfully");
            
            //setup remit box
            double remitBoxY = commentsBoxY + BOX_COMMENT_INFO_HEIGHT + SPACE_BTW_BOXES;
            
            int remitBoxHeight = 70;
            boolean useHorizontal = bankInfo.size() > 2; 
            if(!useHorizontal)
            	remitBoxHeight = 90;
            
            Box remitBox = new Box(RIGHT_MARGIN, remitBoxY , table.getWidth(), remitBoxHeight);
            TextLine textR = new TextLine(f1, "Please remit by " + EntityConstants.DATE_FORMAT.format(invDate) + " to the following bank account(s): ");
            double textRY = remitBoxY + PADDING_SIZE + REGULAR_FONT_SIZE;
            textR.setPosition(textStart, textRY);
            textR.drawOn(page);
            double textRX = textStart - table.getWidth()/2;
            for(String curr : footerCurrencies)
            {
            	TableMessage bi = bankInfo.get(curr);
            	double[] colr = currencyColors.get(curr);
            	if(useHorizontal)
            	{
		            textR = new TextLine(totalFont, "[Acct.: " + bi.getText(DTOConstants.CMP_ACCT_NAME_IDX) + 
		            		"]   [Num.: " + bi.getText(DTOConstants.CMP_ACCT_NUM_IDX) +
		            		"]   [Bank: " + bi.getText(DTOConstants.CMP_ACCT_BNK_IDX) + 
		            		"]   [Sort Cd.: " + bi.getText(DTOConstants.CMP_ACCT_SRT_IDX) +
		            		"]   [Ccy: " + bi.getText(DTOConstants.CMP_ACCT_CURR_IDX)+ "]") ;
		            textRY = textRY + PADDING_SIZE + SMALL_FONT_SIZE;
		            textR.setPosition(textStart, textRY);
		            textR.setColor(colr);
		            textR.drawOn(page);            
            	}
            	else
            	{
		            String[] lines = {"Account Name: " + bi.getText(DTOConstants.CMP_ACCT_NAME_IDX), 
		            		"Account Number: " + bi.getText(DTOConstants.CMP_ACCT_NUM_IDX),
		            		"Bank Name: " + bi.getText(DTOConstants.CMP_ACCT_BNK_IDX), 
		            		"Sort Code: " + bi.getText(DTOConstants.CMP_ACCT_SRT_IDX),
		            		"Currency: [" + bi.getText(DTOConstants.CMP_ACCT_CURR_IDX) + "] / " + DTOConstants.CURRENCY_MAP_NAME.get(curr)};
		            textRX = textRX + table.getWidth()/2;
		            for(int i = 0; i < lines.length; i++)
		            {
		            	textR = new TextLine(totalFont, lines[i]);
		            	double ry = textRY + ((i+1) * (PADDING_SIZE + SMALL_FONT_SIZE));
			            textR.setPosition(textRX, ry);
			            textR.setColor(colr);
			            textR.drawOn(page);                        			
		            }
            	}
            }
            remitBox.drawOn(page);
            
            double officialBoxY = remitBoxY + remitBoxHeight + SPACE_BTW_BOXES;
            Box officialBox = new Box(RIGHT_MARGIN, officialBoxY , table.getWidth(), BOX_OFFICIAL_INFO_HEIGHT);
            double text7Y = officialBoxY + PADDING_SIZE * 2+ REGULAR_FONT_SIZE;
            TextLine text7 = new TextLine(f2, "Official Name:                                      ");            
            text7.setPosition(textStart, text7Y);
            text7.setUnderline(true);            
            TextLine text8 = new TextLine(f2, "Official Signature:                                      Date: " 
            		+ EntityConstants.DATE_FORMAT.format(new Date()));
            text8.setUnderline(true);
            text8.setPosition(textStart + table.getWidth()/2, text7Y);
            text7.drawOn(page);
            text8.drawOn(page);
            officialBox.drawOn(page);   
            
            String[] comments = {"Deposit request generated for " + footerCurrencies.size() + " accounts"};
            TaskQueueHelper.scheduleCreateComment(comments, attentionID, updateUser);
        }
    }
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page) throws Exception
    {
    	printLine(line, xPos, yPos, font, page, null);
    }
    
    private static void printLine(String line, int xPos, int yPos, Font font, Page page, double[] color) throws Exception
    {
    	if(line == null)
    		line = "{BLANK}";
	    TextLine text = new TextLine(font, line);
	    text.setPosition(xPos, yPos);
	    if(color != null)
	    	text.setColor(color);
	    text.drawOn(page);
    }
    
    public static HashMap<String, double[]> getCurrencyColors()
    {
    	if(DTOConstants.CURRENCY_MAP_NAME.size() != 4) throw new RuntimeException("Codebase disparity, currency map has changed");
    	HashMap<String, double[]> result = new HashMap<String, double[]>();
    	result.put("NGN", RGB.OLD_GLORY_RED);
    	result.put("USD", RGB.OLD_GLORY_BLUE);
    	result.put("GBP", RGB.GREEN);
    	result.put("EUR", RGB.MAGENTA);
    	return result;
    }
    
    public static List<TableMessage> getBillingTable(Collection<InetBill> billList, Objectify ofy) throws MissingEntitiesException
    {              
        List<TableMessage> billListMessages = AccountManagerImpl.getBillListSummary(billList, true, false, ofy);
        return billListMessages;
    }
    
    public static HashMap<String, List<TableMessage>> getItemizedBillDescriptions(Collection<InetBill> billList, Map<Key<BillTemplate>, BillTemplate> templateMap)
    {
        HashMap<String, List<TableMessage>> descriptions = new HashMap<String, List<TableMessage>>();
        for(InetBill b  : billList)
        	descriptions.put(b.getKey().getString(), AccountManagerImpl.getBillDescriptionInfo(templateMap.get(b.getTemplateKey())));
         return descriptions;
    }
    
	public static String getBillingInvoicesLink(String[] billingKeyStrs, 
			HttpServletRequest req)
		throws MissingEntitiesException
	{	
        Objectify ofy = ObjectifyService.begin();
        List<Key<InetBill>> billKeys = new ArrayList<Key<InetBill>>(billingKeyStrs.length);
        HashSet<Key<BillTemplate>> templateKeys = new HashSet<Key<BillTemplate>>();
        for(String bkStr : billingKeyStrs)
        {
        	Key<InetBill> bk = ofy.getFactory().stringToKey(bkStr);
        	billKeys.add(bk);
        }
        Collection<InetBill> billList = AccountManagerImpl.getEntities(billKeys, ofy, true).values();        
        for(InetBill b : billList)
        	templateKeys.add(b.getTemplateKey());
        Map<Key<BillTemplate>, BillTemplate> templateMap = AccountManagerImpl.getEntities(templateKeys, ofy, true);
        HashMap<String, List<TableMessage>> descriptionMap = getItemizedBillDescriptions(billList, templateMap);
        List<TableMessage> billingInfo = getBillingTable(billList, ofy);        
        HttpSession sess = req.getSession();
        String id = String.valueOf(new Date().getTime());
        sess.setAttribute(getBillListSessionName(id), billingInfo);
        sess.setAttribute(getBillDescSessionName(id), descriptionMap);
        sess.setAttribute(getSchoolInfoSessionName(id), DTOConstants.getCompanyInfo());
        
        return "<b>DOWNLOAD: </b><a href='" + "http://" + req.getHeader("Host") + "/pdf/billing?" + 
        		TaskConstants.BILL_DESC_KEY_PARAM + "=" + id + "'>Download pdf report with " + 
        		billingKeyStrs.length + " student invoice(s)</a>";         
	}
	
	public static String getDepositStatementLink(String depOwner, String[] depKeyStrs, Date startDate, Date endDate,
			HttpServletRequest req)
		throws MissingEntitiesException
	{	
        Objectify ofy = ObjectifyService.begin();
        Key<Student> addresseeKey = ofy.getFactory().stringToKey(depOwner);
        Student adresseeObj = ofy.get(addresseeKey);
        List<Key<InetResidentAccount>> depKeys = new ArrayList<Key<InetResidentAccount>>(depKeyStrs.length);
        for(String bkStr : depKeyStrs)
        {
        	Key<InetResidentAccount> bk = ofy.getFactory().stringToKey(bkStr);
            log.warning("Print Deposit Requested by " + LoginHelper.getLoggedInUser(req) + " for " + startDate + " to " + endDate + " and " + bk);
        	depKeys.add(bk);
        }
        Collection<InetResidentAccount> depList = AccountManagerImpl.getEntities(depKeys, ofy, true).values();
        HashSet<Key<InetBill>> invoiceList = new HashSet<Key<InetBill>>();
        HashMap<Key<InetResidentAccount>, List<LedgerEntry>> paymentMap = new HashMap<Key<InetResidentAccount>, List<LedgerEntry>>();
        //first pass, fetch payments and invoices
        for(InetResidentAccount dep : depList)
        {
        	List<LedgerEntry> payments = InetDAO4Accounts.getLedgerEntries(dep.getKey(), startDate, endDate, ofy);
        	Collections.sort(payments, dep);
        	paymentMap.put(dep.getKey(), payments);
        	for(LedgerEntry pay : payments)
        	{
        		if(pay instanceof InetWithdrawal)
            		invoiceList.add(((InetWithdrawal) pay).getAllocatedBill());        			
        	}
        }
        Map<Key<InetBill>, InetBill> invoiceMap = AccountManagerImpl.getEntities(invoiceList, ofy, true);
        List<TableMessage>[] tablesToPrint = new List[depList.size()];
        TableMessage[] depositInfo = new TableMessage[depList.size()];
        
        //TODO fix below
        /*Apartment a = null;
        if(adresseeObj instanceof Tenant)
        	a = ofy.get(((Tenant) adresseeObj).getApartmentUnit());*/
        TableMessage addressee = EntityDAO.getStudentDTO(adresseeObj);
        
        int i = 0;
        for(InetResidentAccount dep : depList)
        {
        	TableMessage m = new TableMessage(2,2,2);
        	m.setText(0, dep.getKey().getName());
        	Key<? extends User> tk = dep.getParentAccountKey();
        	m.setText(1, tk.getName());
        	m.setNumber(0, dep.getCurrentAmount());
        	m.setNumber(1, dep.getNumberOfTxns());
        	m.setDate(0, dep.getLastModifiedDate());
        	m.setDate(1, dep.getCreateDate());
        	m.setMessageId(dep.getKey().getString());
        	depositInfo[i++] = m;        	
        }
        
        Arrays.sort(depositInfo, new Comparator<TableMessage>() {

        	int[] compareFields = {1, 0};
			@Override
			public int compare(TableMessage o1, TableMessage o2) {
				for(int i = 0; i < compareFields.length; i++)
				{
					int compareIdx = i;
					int compare = o1.getText(compareIdx).compareToIgnoreCase(o1.getText(compareIdx));
					if(compare == 0) continue;
					return compare;
				}
				return 0;
			}
		});                
        
        for(i = 0; i < depositInfo.length; i++)
        {
        	Key<InetResidentAccount> raKey = ObjectifyService.factory().stringToKey(depositInfo[i].getMessageId());
        	//build deductions table
        	List<LedgerEntry> payments = paymentMap.get(raKey);
        	tablesToPrint[i] = DTOUtils.getLedgerDTO(payments, depositInfo[i].getMessageId(), InetDAO4Accounts.getCompanyAcctsMap(ofy));        	
        }

        HttpSession sess = req.getSession();
        String id = String.valueOf(new Date().getTime());
        sess.setAttribute(getDepositListSessionName(id), tablesToPrint);
        sess.setAttribute(getDepositDescSessionName(id), depositInfo);
        sess.setAttribute(getSchoolInfoSessionName(id), DTOConstants.getCompanyInfo());
        //TODO fix below
        sess.setAttribute(getDepositOwnerName(id), addressee);
        Date[] qd = {startDate, endDate};
        sess.setAttribute(getDepositQueryDatesName(id), qd);
        return "<b>DOWNLOAD: </b><a href='" + "http://" + req.getHeader("Host") + "/pdf/billing?" + 
        		TaskConstants.BILL_DESC_KEY_PARAM + "=" + id + "&type=depstat'>Download statements for " + 
        		depList.size() + " resident account(s)</a>";         
	}
	
	private final static int RQ_NAME_IDX = 0;
	private final static int RQ_COMP_IDX = 1;
	private final static int RQ_ADDR_IDX = 2;
	private static final String TOTAL_FLAG = "TOTAL:";
	private final static int TOTAL_IDX = 0;
	
	public static String getDepositRequestLink(Collection<InetCompanyAccount> compAccounts, Date invDate, String description, List<TableMessage> requests, HttpServletRequest httpreq)
	{
		//build hashmap of attention-id to table message index
		HashMap<String, List<TableMessage>> attentionListMap = new HashMap<String, List<TableMessage>>();
		HashMap<String, TableMessage> attentionInfoMap = new HashMap<String, TableMessage>();
		HashMap<String, HashMap<String, Double>> balanceTotals = new HashMap<String, HashMap<String, Double>>();
		HashMap<String, HashMap<String, Double>> requestTotals = new HashMap<String, HashMap<String, Double>>();
		
		for(TableMessage m : requests)
		{
			TableMessage info = new TableMessage(3,0,0);
			String attentionID = m.getText(DTOConstants.RSD_ACCT_ATTN_ID_IDX);
			info.setText(RQ_NAME_IDX, m.getText(DTOConstants.RSD_ACCT_ATTN_IDX));
			info.setText(RQ_COMP_IDX, m.getText(DTOConstants.RSD_ACCT_COMPANY_IDX));
			info.setText(RQ_ADDR_IDX, m.getText(DTOConstants.RSD_ACCT_ATTN_ADDR_IDX));
			attentionInfoMap.put(attentionID, info);
			
			List<TableMessage> reqList = attentionListMap.get(attentionID);
			if(reqList == null)
			{
				//add request list header
				reqList = new ArrayList<TableMessage>();
				TableMessageHeader h = new TableMessageHeader(6);
				h.setText(0, "Location", TableMessageContent.TEXT);
				h.setText(1, "Unit", TableMessageContent.TEXT);
				h.setText(2, "Acct. Type", TableMessageContent.TEXT);
				h.setText(3, "Ccy.", TableMessageContent.TEXT);				
				h.setText(4, "Acct. Balance", TableMessageContent.NUMBER);
				h.setText(5, "Amount Due", TableMessageContent.NUMBER);
				reqList.add(h);
				attentionListMap.put(attentionID, reqList);
				balanceTotals.put(attentionID, new HashMap<String, Double>());
				requestTotals.put(attentionID, new HashMap<String, Double>());
			}
			
			//capture request
			TableMessage req = new TableMessage(4,2,0);
			String ccy = m.getText(DTOConstants.RSD_ACCT_CURR_IDX);
			req.setText(0, m.getText(DTOConstants.RSD_ACCT_BLD_IDX));
			req.setText(1, m.getText(DTOConstants.RSD_ACCT_APT_IDX));
			req.setText(2, m.getText(DTOConstants.RSD_ACCT_TYPE_IDX));
			req.setText(3, ccy);
			req.setNumber(0, m.getNumber(DTOConstants.RSD_ACCT_AMT_IDX));
			req.setNumber(1, m.getNumber(DTOConstants.RSD_ACCT_TXN_IDX));
			reqList.add(req);
			
			//update balances
			Double balanceTotal = balanceTotals.get(attentionID).get(ccy);
			if(balanceTotal == null) balanceTotal = 0.0;
			Double requestTotal = requestTotals.get(attentionID).get(ccy);
			if(requestTotal == null) requestTotal = 0.0;
			balanceTotal += m.getNumber(DTOConstants.RSD_ACCT_AMT_IDX);
			requestTotal += m.getNumber(DTOConstants.RSD_ACCT_TXN_IDX);
			balanceTotals.get(attentionID).put(ccy, balanceTotal);
			requestTotals.get(attentionID).put(ccy, requestTotal);
		}
		
		//2nd pass, add balance to requests
		for(Entry<String, List<TableMessage>>  reqs : attentionListMap.entrySet())
		{
			String attentionID = reqs.getKey();
			HashMap<String, Double> balanceCCyTotal = balanceTotals.get(attentionID);
			HashMap<String, Double> requestCcyTotal = requestTotals.get(attentionID);			
			List<TableMessage> reqList = reqs.getValue();
			for(String ccy : balanceCCyTotal.keySet())
			{
				TableMessage footer = new TableMessageFooter(4,2,0);
				footer.setText(TOTAL_IDX, TOTAL_FLAG);
				footer.setText(3, ccy);
				footer.setNumber(0, balanceCCyTotal.get(ccy));
				footer.setNumber(1, requestCcyTotal.get(ccy));
				reqList.add(footer);
			}
		}
		
		//build company info
		HashMap<String, TableMessage> bankInfo = new HashMap<String, TableMessage>();
		for(InetCompanyAccount compAcct : compAccounts)
				bankInfo.put(compAcct.getCurrency(), AccountManagerImpl.getCompanyAccountDTO(compAcct));
		
        HttpSession sess = httpreq.getSession();
        String id = String.valueOf(new Date().getTime());
        sess.setAttribute(getDepositRequestsListName(id), attentionListMap);
        sess.setAttribute(getDepositRequestsToName(id), attentionInfoMap);
        sess.setAttribute(getSchoolInfoSessionName(id), DTOConstants.getCompanyInfo());
        sess.setAttribute(getDepositRequestsBankName(id), bankInfo);
        sess.setAttribute(getDepositRequestsDateName(id), invDate);
        sess.setAttribute(getDepositRequestsDescrName(id), description);
        return "<b>DOWNLOAD: </b><a href='" + "http://" + httpreq.getHeader("Host") + "/pdf/billing?" + 
        		TaskConstants.BILL_DESC_KEY_PARAM + "=" + id + "&type=depreqs'>Download deposit requests for " + requests.size() + " resident accounts and addressed to " +
        		attentionInfoMap.size() + " recipents(s)</a>";         

	}
	
	public static String getBillListSessionName(String id)
	{
		return SESS_PREFIX + id + "bioinv";
	}
	
	public static String getBillDescSessionName(String id)
	{
		return SESS_PREFIX + id + "tableinv";
	}

	public static String getDepositListSessionName(String id)
	{
		return SESS_PREFIX + id + "deplist";
	}
	
	public static String getDepositDescSessionName(String id)
	{
		return SESS_PREFIX + id + "depdesc";
	}
	
	public static String getDepositOwnerName(String id)
	{
		return SESS_PREFIX + id + "depownr";
	}
	
	public static String getDepositQueryDatesName(String id)
	{
		return SESS_PREFIX + id + "depdates";
	}	
	
	public static String getDepositRequestsToName(String id)
	{
		return SESS_PREFIX + id + "deptoinf";
	}
	
	public static String getDepositRequestsBankName(String id)
	{
		return SESS_PREFIX + id + "drbank";
	}
	
	public static String getDepositRequestsDateName(String id)
	{
		return SESS_PREFIX + id + "drdate";
	}	

	public static String getDepositRequestsDescrName(String id)
	{
		return SESS_PREFIX + id + "drdsc";
	}	
	
	public static String getDepositRequestsListName(String id)
	{
		return SESS_PREFIX + id + "depreqlst";
	}
	
	public static String getSchoolInfoSessionName(String id)
	{
		return SESS_PREFIX + id + "compinv";
	}	
}
