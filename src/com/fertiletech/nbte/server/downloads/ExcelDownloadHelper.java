/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.nbte.server.downloads;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.write.DateFormats;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class ExcelDownloadHelper {

    public static void doDownload(HttpServletResponse res, String fileName, List<TableMessage> data) throws IOException {
        if (data.size() < 1) {
            throw new IllegalArgumentException("data should at least have a header");
        }

        TableMessageHeader header = (TableMessageHeader) data.remove(0);

        res.setContentType("application/vnd.ms-excel");
        //res.setContentType("application/octet-stream");
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        try {
            //initialize workbook
            WritableWorkbook w = Workbook.createWorkbook(res.getOutputStream());
            String caption = header.getCaption();
            int captionLength = (caption == null?0:caption.length());
            int maxExcelSheetNameLength = 30;
            int endSheetNameIdx = Math.min(captionLength, maxExcelSheetNameLength);
            WritableSheet s = w.createSheet((caption == null?
                "Download-" + EntityConstants.DATE_FORMAT.format(new Date()) : caption.substring(0, endSheetNameIdx)), 0);

            //setup headers
            WritableCellFormat headerFormat =
                    new WritableCellFormat(new WritableFont(WritableFont.ARIAL,
                    WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD));

            int textIndex, dateIndex, numIndex, numOfHeaders;
            textIndex = dateIndex = numIndex = 0;
            numOfHeaders = header.getNumberOfHeaders();
            int[] messageIndex = new int[numOfHeaders];
            int headerRowIndex = 0;
            if(numOfHeaders > 0 && header.getGroupName(0) != null)
            	headerRowIndex = 1;
            for (int i = 0; i < numOfHeaders; i++)
            {
            	if(headerRowIndex == 1)
            		s.addCell(new Label(i, 0, header.getGroupName(i), headerFormat));
                s.addCell(new Label(i, headerRowIndex, header.getText(i), headerFormat));
                TableMessageHeader.TableMessageContent type = header.getHeaderType(i);
                if(type == TableMessageHeader.TableMessageContent.TEXT)
                    messageIndex[i] = textIndex++;
                else if(type == TableMessageHeader.TableMessageContent.NUMBER)
                    messageIndex[i] = numIndex++;
                else if(type == TableMessageHeader.TableMessageContent.DATE)
                    messageIndex[i] = dateIndex++;
            }

            //add sample content
            int count = 1 + headerRowIndex;
            for (TableMessage m : data)
            {
                for(int i = 0; i < numOfHeaders; i++)
                {
                TableMessageHeader.TableMessageContent type = header.getHeaderType(i);
                if(type == TableMessageHeader.TableMessageContent.TEXT)
                    s.addCell(getCell(m.getText(messageIndex[i]), i, count));
                else if(type == TableMessageHeader.TableMessageContent.NUMBER)
                    s.addCell(getCell(m.getNumber(messageIndex[i]), i, count));
                else if(type == TableMessageHeader.TableMessageContent.DATE)
                    s.addCell(getCell(m.getDate(messageIndex[i]), i, count));
                }
                count++;
            }
            w.write();
            w.close();
        } catch (WriteException ex) {
            throw new RuntimeException(ex.fillInStackTrace());
        }
    }

    private static Label getCell(String s, int col, int row)
    {
        if(s == null)
            return new Label(col, row, "");
        return new Label(col, row, s);
    }

    private static WritableCell getCell(Date d, int col, int row)
    {
        if(d == null)
            return new Label(col, row, "");
        return new DateTime(col, row, d, new WritableCellFormat(DateFormats.FORMAT2));
    }

    private static WritableCell getCell(Double num, int col, int row)
    {
        if(num == null)
            return new Label(col, row, "");
        return new jxl.write.Number(col, row, num, new WritableCellFormat(NumberFormats.FORMAT3));
    }
}
