package com.fertiletech.nbte.server.downloads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fertiletech.nbte.server.accounting.InetCompanyAccount;
import com.fertiletech.nbte.server.accounting.InetDeposit;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.accounting.LedgerEntry;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;

public class DTOUtils {
	public static List<TableMessage> getLedgerDTO(List<? extends LedgerEntry> deposits, String headerStr, Map<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts)
	{
		List<TableMessage> result = new ArrayList<TableMessage>(1 + deposits.size());
		//setup header message
		int numOfCols = 7;
		TableMessageHeader header = new TableMessageHeader(numOfCols);
		header.setText(0, "No.", TableMessageContent.NUMBER);
		header.setFormatOption(0,"###");		
		header.setText(1, "Description", TableMessageContent.TEXT);
		header.setText(2, "Type", TableMessageContent.TEXT);			
		header.setText(3, "Amount", TableMessageContent.NUMBER);
		header.setText(4, "Account Bal.", TableMessageContent.NUMBER);		
		header.setText(5, "Value Date", TableMessageContent.DATE);
		header.setText(6, "Entry Date", TableMessageContent.DATE);
		
		header.setMessageId(headerStr);
		result.add(header);

		Collections.sort(deposits, InetResidentAccount.getComparator());
		for(LedgerEntry dep : deposits)
		{
			TableMessage m = new TableMessage(2, 3, 2);
			m.setMessageId(dep.getKey().getString());
			m.setNumber(0, dep.getEntryCount());
			m.setNumber(1, dep.getAmount());
			m.setNumber(2, dep.getAccountBalance());
			
			m.setDate(0, dep.getEffectiveDate());
			m.setDate(1, dep.getEntryDate());	
			String description = "";
			if(dep instanceof InetDeposit)
			{	
				InetDeposit deposit = (InetDeposit) dep;
				InetCompanyAccount ba = bankAccts.get(deposit.getCompanyAccount());
				String helper = "Payment into " + ba.getAccountName() + " acct. with " + ba.getBank();			
				description = deposit.getReferenceId() == null? helper : (helper + ".  [Ref: " + deposit.getReferenceId()+"]"); 
			}
			else
				description = dep.getReferenceId();
			m.setText(0, description);	
			m.setText(1, dep.isCredit()? "Credit" : "Debit");
			result.add(m);
		}
		return result;		
	}

	
	public static void getConsolidatedLedgerDTO(List<TableMessage> result, List<? extends LedgerEntry> deposits, double[] outstandingBalance, Map<Key<InetCompanyAccount>, InetCompanyAccount> bankAccts)
	{
		Collections.sort(deposits, InetResidentAccount.getComparator());
		for(LedgerEntry dep : deposits)
		{
			TableMessage m = new TableMessage(1, 3, 1);
			m.setMessageId(dep.getKey().getString());
			double amount = dep.getAmount();
			if(dep.isCredit())
			{
				m.setNumber(1, amount);
				outstandingBalance[1] += amount;
			}
			else
			{
				amount *= -1;
				m.setNumber(0, amount);
				outstandingBalance[0] += amount;
			}
			outstandingBalance[2] += amount;
			m.setNumber(2, outstandingBalance[2]);
			m.setDate(0, dep.getEffectiveDate());
			
			String description = "";
			if(dep instanceof InetDeposit)
			{	
				InetDeposit deposit = (InetDeposit) dep;
				InetCompanyAccount ba = bankAccts.get(deposit.getCompanyAccount());
				description = "Payment into " + blurNum(ba.getAccountNumber()) + " acct. with " + ba.getBank();			
			//	description = deposit.getReferenceId() == null? helper : (helper + ".  [Ref: " + deposit.getReferenceId()+"]"); 
			}
			else
				description = dep.getReferenceId();
			m.setText(0, description);
			m.setDate(0, new Date());
			result.add(m);
		}		
	}
	
	static String blurNum(String acctNumber)
	{
		char[] acctArray = acctNumber.toCharArray();
		int midIdx = acctArray.length / 2;
		acctArray[midIdx-1] = '.';
		acctArray[midIdx] = '.';
		acctArray[midIdx+1] = '.';
		return new String(acctArray);
	}
	
}
