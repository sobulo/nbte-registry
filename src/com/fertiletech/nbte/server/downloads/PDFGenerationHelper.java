/**
 * 
 */
package com.fertiletech.nbte.server.downloads;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageFooter;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.pdfjet.Cell;
import com.pdfjet.Font;
import com.pdfjet.RGB;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PDFGenerationHelper {
	
	public static List<List<Cell>> convertTableMessageToPDFTable(List<TableMessage> tableData, Font headerFont, Font rowFont, HashSet<Integer> ignoreCols)
	{
		return convertTableMessageToPDFTable(tableData, headerFont, rowFont, headerFont, ignoreCols);
	}
	
	public static List<List<Cell>> convertTableMessageToPDFTable(List<TableMessage> tableData, Font headerFont, Font rowFont, Font footerFont, HashSet<Integer> ignoreCols)
	{
		if(ignoreCols == null) ignoreCols = new HashSet<Integer>();
		List<List<Cell>> result = new ArrayList<List<Cell>>(tableData.size());
		int textCursor, dateCursor, numberCursor;
		
		//setup header
		TableMessageHeader headerRow = (TableMessageHeader) tableData.get(0);
		int numOfCols = headerRow.getNumberOfHeaders() - ignoreCols.size();
		List<Cell> headerCell = new ArrayList<Cell>(numOfCols);		
		for(int i = 0; i < numOfCols; i++)
		{
			if(ignoreCols.contains(i)) continue;
			Cell h = new Cell(headerFont, headerRow.getText(i));
			h.setBgColor(RGB.DARK_GRAY);
			h.setFgColor(RGB.WHITE);
			headerCell.add(h);
		}
		result.add(headerCell);
		
		//setup body
		for(int i = 1; i < tableData.size(); i++)
		{
			List<Cell> rowCell = new ArrayList<Cell>(numOfCols);
			TableMessage rowMessage = tableData.get(i);
			textCursor = dateCursor = numberCursor = 0;
			String dataVal = null;
			Double numVal = null;
			Date dateVal = null;
			Font cellFont = rowFont;
			double[] bgColor = null;
			if(rowMessage instanceof TableMessageFooter)
			{
				bgColor = RGB.LIGHT_GRAY;
				cellFont = footerFont;
			}
			for(int j = 0; j < numOfCols; j++)
			{
				TableMessageContent columnType = headerRow.getHeaderType(j);
				dataVal = "Invalid Column Type";
				if(columnType.equals(TableMessageContent.TEXT))
				{
					dataVal = rowMessage.getText(textCursor++);
					dataVal = dataVal==null?"":dataVal;
				}
				else if (columnType.equals(TableMessageContent.NUMBER))
				{
					numVal = rowMessage.getNumber(numberCursor++);
					dataVal = numVal == null?"":EntityConstants.NUMBER_FORMAT.format(numVal);
				}
				else if(columnType.equals(TableMessageContent.DATE))
				{
					dateVal = rowMessage.getDate(dateCursor++);
					dataVal = dateVal == null?"":EntityConstants.DATE_FORMAT.format(dateVal);
				}
				if(ignoreCols.contains(j)) continue;
				Cell colCell = new Cell(cellFont, dataVal);
				if(bgColor != null)
					colCell.setBgColor(bgColor);
				rowCell.add(colCell);
				
			}
			result.add(rowCell);
		}
		return result;
	}
}
