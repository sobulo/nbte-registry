package com.fertiletech.nbte.server.downloads;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.college.StudentSession;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.FileInfo;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
 
public class ListGCSFiles extends HttpServlet{
	BlobstoreService blobstore = BlobstoreServiceFactory.getBlobstoreService();
	
	final int BUFFER_SIZE = 1024 * 512;
	public final static String GCS_BUCKET_NAME = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
	
	  private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
      .initialRetryDelayMillis(10)
      .retryMaxAttempts(10)
      .totalRetryPeriodMillis(15000)
      .build());	
	
	private static final Logger log =
	        Logger.getLogger(ListGCSFiles.class.getName());	
	
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
    	   Map<String, List<FileInfo>> uploads = blobstore.getFileInfos(req);
           List<FileInfo> fileInfos = uploads.get("chooseFile");
           if (fileInfos == null || fileInfos.size() == 0) {
        	   log.warning("No file has been uploaded");
        	   res.getOutputStream().println("No file has been uploaded<br/>");
               res.getOutputStream().close();
               return;
           }
           
           String loanID = req.getParameter(DTOConstants.GCS_LOAN_ID_PARAM);
           if (loanID == null || loanID.trim().length() == 0) {
        	   log.warning("No file has been uploaded");
        	   res.getOutputStream().println("No student bio file specified<br/>");
               res.getOutputStream().close();
               return;
           }
           //validate key
           Key<StudentSession> ssk = new Key<StudentSession>(loanID);
           ObjectifyService.begin().get(ssk); //fails if session object doesn't exist
           loanID = ssk.getParent().getString(); //ignore session, dump in parent object
           
           res.getOutputStream().println("Found: " + fileInfos.size() + " files<br/>");

           // store info about uploaded files in the Datastore for later use
           // in HomeServlet to list all uploaded content so far.
           for (FileInfo fileInfo : fileInfos) 
           {
        	   GcsFilename src = new GcsFilename(GCS_BUCKET_NAME, getGcsObjectName(fileInfo.getGsObjectName()));
        	   GcsFilename dest = new GcsFilename(GCS_BUCKET_NAME, loanID + "/" + fileInfo.getFilename());
               gcsService.copy(src, dest);
               gcsService.delete(src);
               String url = EntityConstants.GCS_HOST + GCS_BUCKET_NAME + "/" + dest.getObjectName();
               String comments[] = {"<div style='background-color:black;color:white;text-align:center;'>FILE UPLOADED</div>",
            		   "<a href='" + url + "'> View " + fileInfo.getFilename() 
            		   + "</a>"};
               String user = LoginHelper.getLoggedInUser(req);
               
               TaskQueueHelper.scheduleCreateComment(comments,loanID, user==null?"anoynmous":user);

               res.getOutputStream().println("<p>" + fileInfo.getFilename() + " uploaded on " + fileInfo.getCreation());
               res.getOutputStream().println(". Uploaded Link: <a href='" + url + "'>" + url + "</a>. ");
               res.getOutputStream().println("Click refresh if displayed file listing doesn't reflect your upload</p>");
           }
           res.getOutputStream().close();
    }
    
    private String getGcsObjectName(String gsObjectName)
    {
    	return gsObjectName.split("/", 4)[3];
    }
}
