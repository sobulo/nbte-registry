/**
 * 
 */
package com.fertiletech.nbte.server.comments;

import java.util.HashMap;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetMultiAnswers
{
	@Id
	Long key;
	
	@Serialized
	HashMap<Integer, String> answerMapping;
	
	public InetMultiAnswers(){}
	
	public InetMultiAnswers(HashMap<Integer, String> answerMapping)
	{
		this.answerMapping = answerMapping;
	}
	
	public Key<InetMultiAnswers> getKey()
	{
		return new Key<InetMultiAnswers>(InetMultiAnswers.class, key);
	}
	
	public HashMap<Integer, String> getAnswerMapping()
	{
		return answerMapping;
	}
}
