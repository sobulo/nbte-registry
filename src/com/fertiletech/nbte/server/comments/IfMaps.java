package com.fertiletech.nbte.server.comments;

import com.googlecode.objectify.condition.ValueIf;

public class IfMaps extends ValueIf<String>{

	@Override
	public boolean matches(String value) {
		if(value == null) return false;
		return "MAPS".equals(value);
	}

}
