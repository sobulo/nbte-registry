package com.fertiletech.nbte.server.comments;

import java.util.Date;

import javax.persistence.Id;

import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.shared.DrivePickerUtils;
import com.fertiletech.nbte.shared.TableMessage;
import com.google.appengine.api.datastore.GeoPt;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class MortgageAttachment {
	@Id
	Long myId;
	String title;
	@Indexed(IfMaps.class) String type;
	String userName;
	String viewUrl;
	String editUrl;
	String parentId;
	String thumbUrl;
	GeoPt location;
	Date driveDate;
	@Indexed Date attachDate;
	@Parent Key<Student> leadKey;
	
	
	public MortgageAttachment() {}
	
	
	private MortgageAttachment(Key<Student> leadKey, String title, String type, String userName,
			String viewUrl, String editUrl, String parentId, String thumbUrl,
			Double lat, Double lng, Date driveDate) 
	{
		super();
		this.title = title;
		this.type = type;
		this.userName = userName;
		this.viewUrl = viewUrl;
		this.editUrl = editUrl;
		this.parentId = parentId;
		this.thumbUrl = thumbUrl;
		this.location = getPt(lat, lng);
		this.driveDate = driveDate;
		this.attachDate = new Date();
		this.leadKey = leadKey;
	}
	
	public MortgageAttachment(Key<Student> leadKey, TableMessage m)
	{
		this.title = m.getText(DrivePickerUtils.ATTACH_TITLE_IDX);
		this.type = m.getText(DrivePickerUtils.ATTACH_TYPE_IDX);
		this.userName = m.getText(DrivePickerUtils.ATTACH_USR_IDX);
		this.viewUrl = m.getText(DrivePickerUtils.ATTACH_VIEW_IDX);
		this.editUrl = m.getText(DrivePickerUtils.ATTACH_EDIT_IDX);
		this.parentId = m.getText(DrivePickerUtils.ATTACH_PARENT_IDX);
		this.thumbUrl = m.getText(DrivePickerUtils.ATTACH_THUMB_IDX);
		Double lat = m.getNumber(DrivePickerUtils.ATTACH_LAT_IDX);
		Double lng = m.getNumber(DrivePickerUtils.ATTACH_LONG_IDX);
		this.location = getPt(lat, lng);
		this.attachDate = new Date();
		this.driveDate = m.getDate(DrivePickerUtils.ATTACH_DATE_IDX);
		this.leadKey = leadKey;
	}
	
	private GeoPt getPt(Double lat, Double lng)
	{
		if(lat == null || lng == null)
			return null;
		return new GeoPt(lat.floatValue(), lng.floatValue());
	}
	
	public Key<MortgageAttachment> getKey()
	{
		return new Key<MortgageAttachment>(leadKey, MortgageAttachment.class, myId);
	}

	public Long getMyId() {
		return myId;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getUserName() {
		return userName;
	}

	public String getViewUrl() {
		return viewUrl;
	}

	public String getEditUrl() {
		return editUrl;
	}

	public String getParentId() {
		return parentId;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public Date getDriveDate() {
		return driveDate;
	}

	public Date getAttachDate() {
		return attachDate;
	}

	public Key<Student> getLeadKey() {
		return leadKey;
	}
	
	public Float getLatitude()
	{
		if(location == null) return null;
		return location.getLatitude();
	}
	
	public Float getLongitude()
	{
		if(location == null) return null;
		return location.getLongitude();
	}
	
    public TableMessage getDTO()
	{
		TableMessage info = new TableMessage(7, 2, 2);
		info.setText(DrivePickerUtils.ATTACH_TITLE_IDX, getTitle());
		info.setText(DrivePickerUtils.ATTACH_PARENT_IDX, getParentId());
		info.setText(DrivePickerUtils.ATTACH_VIEW_IDX, getViewUrl());
		info.setText(DrivePickerUtils.ATTACH_EDIT_IDX, getEditUrl());
		info.setText(DrivePickerUtils.ATTACH_THUMB_IDX, getThumbUrl());
		info.setText(DrivePickerUtils.ATTACH_USR_IDX, getUserName());
		info.setText(DrivePickerUtils.ATTACH_TYPE_IDX, getType());
		info.setDate(DrivePickerUtils.ATTACH_DATE_IDX, getDriveDate());
		info.setDate(DrivePickerUtils.ATTACH_LINKDATE_IDX, getAttachDate());
		info.setNumber(DrivePickerUtils.ATTACH_LONG_IDX, getLongitude());
		info.setNumber(DrivePickerUtils.ATTACH_LAT_IDX, getLatitude());
		if(myId != null)
			info.setMessageId(getKey().getString());
		return info;
	}
}
