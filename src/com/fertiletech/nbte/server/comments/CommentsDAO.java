package com.fertiletech.nbte.server.comments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class CommentsDAO {
	//comment array indicates auto-generation
	public static AcitivityComment createComment(String[] commentList, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		StringBuilder joiner = new StringBuilder();
		for(String comment : commentList)
			joiner.append("<p>").append(comment).append("</p>");
		return createComment(new Text(joiner.toString()), showPublic, leadKey, user);
	}
	
	
	
	public static AcitivityComment createComment(Text text,
			boolean showPublic, Key<? extends Object> leadKey, String user) {
		return createComment(null, text, showPublic, leadKey, user);
	}



	public static AcitivityComment createComment(Objectify ofy, Text comment, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		if(ofy == null)
			ofy = ObjectifyService.beginTransaction();
		AcitivityComment loanComment = new AcitivityComment(comment, leadKey, showPublic, user);			
		try
		{
			ofy.put(loanComment);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}

	public static RatingComment createRating(int rating, Text comment, boolean showPublic, Key<? extends Object> leadKey, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		RatingComment loanComment = new RatingComment(comment, leadKey, showPublic, user, rating);			
		try
		{
			ofy.put(loanComment);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return loanComment;
	}	
	
	public static List<AcitivityComment> getActivityComments(Objectify ofy, Key<? extends Object> leadKey)
	{
		if(ofy == null)
			ofy = ObjectifyService.begin();
		Key<AcitivityComment> parentKey = AcitivityComment.getParentKey(leadKey);
		List<AcitivityComment> list = ofy.query(AcitivityComment.class).ancestor(parentKey).list();
		Collections.sort(list, Collections.reverseOrder()); //sort list in descending order
		return list;
	}
	
	public static List<RatingComment> getRatings(Objectify ofy, Key<? extends Object> leadKey)
	{
		if(ofy == null)
			ofy = ObjectifyService.begin();
		Key<AcitivityComment> parentKey = AcitivityComment.getParentKey(leadKey);
		List<RatingComment> list = ofy.query(RatingComment.class).ancestor(parentKey).list();
		Collections.sort(list, Collections.reverseOrder()); //sort list in descending order
		return list;
	}
	
	public static ApplicationParameters createParams(String name, HashMap<String, String> map)
	{
		Key<ApplicationParameters> key = ApplicationParameters.getKey(name);
		Objectify ofy = ObjectifyService.beginTransaction();
		ApplicationParameters p = null;
		try
		{
			p = ofy.find(key);
			if(p == null) //ignore if object exists already
			{
				p =new ApplicationParameters(name, map);
				ofy.put(p);
				ofy.getTxn().commit();
			}
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return p;
	}
	
	public static void updateApplicationParameters(String userID,
			Key<ApplicationParameters> key, HashMap<String, String> parameters)
			throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.beginTransaction();
		try {
			ApplicationParameters paramsObject = ofy.find(key);
			if (paramsObject == null)
				throw new MissingEntitiesException("Parameter does not exist, unable to update" + key.getName());

			HashMap<String, String> oldParameters = paramsObject.getParams();
			paramsObject.setParams(parameters);
			ofy.put(paramsObject);
			ArrayList<String> diffs = new ArrayList<String>();
			for(String o : oldParameters.keySet())
			{
				if(parameters.containsKey(o))
				{
					if(!parameters.get(o).equalsIgnoreCase(oldParameters.get(o)))
						diffs.add("Value for " + o + " changed from: [" + oldParameters.get(o) + "] to: [" + parameters.get(o) + "]");
				}
				else
					diffs.add("Entry for " + o +" REMOVED. Its description at time of removal was: [" + oldParameters.get(o) + "]");
					
			}
			for(String o : parameters.keySet())
			{
				if(!oldParameters.containsKey(o))
					diffs.add("Entry for " + o + " ADDED. Its description is: [" + parameters.get(o) + "]");
			}
			String[] comments = diffs.toArray(new String[diffs.size()]);
			TaskQueueHelper.scheduleCreateComment(comments, key.getString(), userID);
			ofy.getTxn().commit();

		} finally {
			if (ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
}
