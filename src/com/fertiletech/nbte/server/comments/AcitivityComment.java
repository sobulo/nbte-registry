/**
 * 
 */
package com.fertiletech.nbte.server.comments;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class AcitivityComment implements Comparable<AcitivityComment>{
	@Id
	Long key;
	
	Text comment;
	@Indexed Date dateUpdated;
	@Parent Key<AcitivityComment> ownerKey;
	boolean showPublic;
	String user;
	
	/**
	 * 
	 */
	public AcitivityComment() {}
	
	/**
	 * @param key
	 * @param comment
	 * @param loanKey
	 * @param showPublic
	 * @param user
	 */
	AcitivityComment(Text comment,
			Key<? extends Object> associatedObject, boolean showPublic, String user) 
	{
		this.comment = comment;
		this.ownerKey = getParentKey(associatedObject);
		this.showPublic = showPublic;
		this.user = user;
	}
	
	//hacky ... we want comments associated with same objects grouped together but we don't want them in the same entity groups
	//as those objects. Do not even think about changing this code without close scrutiny as any change could prevent existing
	//comments from loading
	public static Key<AcitivityComment> getParentKey(Key<? extends Object> associatedObject)
	{
		//flatten the key
		Key<? extends Object> childKey = associatedObject.getParent();
		StringBuilder bd = new StringBuilder(getKeyAsString(associatedObject));
		while(childKey != null)
		{
			bd.append("-").append(getKeyAsString(childKey));
			childKey = childKey.getParent();
		}
		return new Key<AcitivityComment>(AcitivityComment.class, bd.toString());
	}
	
	private static String getKeyAsString(Key<? extends Object> objKey)
	{
		return objKey.getName() == null ? String.valueOf(objKey.getId()) : objKey.getName();  
	}
	
	public Text getComment() {
		return comment;
	}
	
	void setComment(Text comment) {
		this.comment = comment;
	}
	
	public boolean isShowPublic() {
		return showPublic;
	}
	
	void setShowPublic(boolean showPublic) {
		this.showPublic = showPublic;
	}
	
	public String getUser() {
		return user;
	}
	
	void setUser(String user) {
		this.user = user;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	public Key<AcitivityComment> getKey()
	{
		return new Key<AcitivityComment>(ownerKey, AcitivityComment.class, key);
	}
	
	public Key<AcitivityComment> getParentKey()
	{
		return ownerKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AcitivityComment o) {
		int parentKeyComparison = ownerKey.compareTo(o.ownerKey);
		if(parentKeyComparison == 0)
			return this.dateUpdated.compareTo(o.dateUpdated);
		else
			return parentKeyComparison;
	}
}
