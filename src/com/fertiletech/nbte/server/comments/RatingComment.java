/**
 * 
 */
package com.fertiletech.nbte.server.comments;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class RatingComment extends AcitivityComment
{	
	int rating;
	final static String ALL_AVG_KEY = "AVERAGE RATING";
	
	RatingComment(){};
	
	//right now we only have one rating system so we don't store a pointer to multi-answers	
	RatingComment(Text comment, Key<? extends Object> associatedObject, boolean showPublic, String user, int rating) 
	{
		super(comment, associatedObject, showPublic, user);
		this.rating = rating;
	}
	
	public int getRating()
	{
		return rating;
	}
	
	public static LinkedHashMap<String, Double> getCalculatedAverage(List<RatingComment> ratings)
	{
		if(ratings.size() == 0) return null;
		LinkedHashMap<String, Double> result = new LinkedHashMap<String, Double>();
		HashMap<String, Integer> resultCounts = new HashMap<String, Integer>();
		//calculate totals
		for(RatingComment r : ratings)
		{
			String user = r.getUser();
			if(!result.containsKey(user))
			{
				resultCounts.put(user, 0);
				result.put(user, 0.0);
			}
			int count = resultCounts.get(user) + 1;
			resultCounts.put(user, count);
			result.put(user, result.get(user) + r.getRating());
		}
		//calculate averages
		double total = 0.0;
		for(String user : result.keySet())
		{
			total += result.get(user);
			result.put(user, result.get(user) / resultCounts.get(user));
		}
		result.put(ALL_AVG_KEY, total / ratings.size());
		return result;
	}
}
