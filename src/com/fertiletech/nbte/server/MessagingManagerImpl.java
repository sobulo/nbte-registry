package com.fertiletech.nbte.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.nbte.client.MessagingManager;
import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.messaging.EmailController;
import com.fertiletech.nbte.server.messaging.MessagingController;
import com.fertiletech.nbte.server.messaging.MessagingDAO;
import com.fertiletech.nbte.server.messaging.SMSController;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.CustomMessageTypes;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class MessagingManagerImpl extends RemoteServiceServlet implements MessagingManager {
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	

	private static final Logger log = Logger.getLogger(MessagingManagerImpl.class.getName());
	

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#sendMessage(java.util.HashSet, java.lang.String, boolean)
	 */
	@Override
	public void sendMessage(CustomMessageTypes msgType, String[] addresses, String message[],
			boolean isEmail) throws MissingEntitiesException {
		Key<? extends MessagingController> controllerKey = null;
		if(msgType.equals(CustomMessageTypes.GENERIC_MESSAGE))
		{
			if(isEmail)
				controllerKey = new Key<EmailController>(EmailController.class, EntityConstants.EMAIL_CONTROLLER_ID);
			else
				controllerKey = new Key<SMSController>(SMSController.class, EntityConstants.SMS_CONTROLLER_ID);
			String controllerId = MessagingDAO.validateController(controllerKey, addresses.length);
			scheduleMessageSending(controllerId, addresses, message);
		}
		else
			throw new RuntimeException("unsupported message type");
	}
	
	private static void scheduleMessageSending(String controllerId,
			String[] addresses, String[] message) throws MissingEntitiesException
	{
		for(String address : addresses)
			TaskQueueHelper.scheduleMessageSending(controllerId, address, message, null);
	}	
	
	private void schedulePDFEmail(String controllerId, String[] addresses, String[] message)
	{
		for(int i = 0; i < addresses.length; i++)
		{
			String[] msgParts = new String[2];
			msgParts[0] = message[0];
			msgParts[1] = message[i+1];
			//TaskQueueHelper.scheduleMessageSending(controllerId, addresses[i], msgParts);
		}		
	}
	
	private void scheduleSMSTexts(String controllerId, String[] addresses, String[] allTextMessages) 
	{
		if(allTextMessages == null) throw new RuntimeException("Unable to generate message body");
		String[] messageWrapper = new String[1];
		for(int i = 0; i < allTextMessages.length; i++)
		{
			messageWrapper[0] = allTextMessages[i];
			//TaskQueueHelper.scheduleMessageSending(controllerId, addresses[i], messageWrapper);
		}		
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#getMessagingControllerNames()
	 */
	@Override
	public HashMap<String, String> getMessagingControllerNames() {
		return MessagingDAO.getMessagingControllerNames();
	}

	/* (non-Javadoc)
	 * @see j9educationgwtgui.client.MessagingManager#getControllerDetails(java.lang.String)
	 */
	@Override
	public List<TableMessage> getControllerDetails(String controllerName) throws MissingEntitiesException {
		Objectify ofy = ObjectifyService.begin();
		Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerName);
		MessagingController controller = ofy.find(controllerKey);
		if(controller == null)
		{
			String msgFmt = "Unable to find messaging controller: ";
			log.severe(msgFmt + controllerKey);
			throw new MissingEntitiesException(msgFmt + controllerKey.getName());
		}
		
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessage info = new TableMessage(1, 3, 1);
		info.setText(0, controllerKey.getName());
		info.setDate(0, controller.getLastAccessed());
		info.setNumber(0, controller.getTotalMessageLimit());
		info.setNumber(1, controller.getDailyMessageLimit());
		info.setNumber(2, controller.getTotalSentMessages());
		result.add(info);
		//setup header
		TableMessageHeader header = new TableMessageHeader(2);
		header.setText(0, "Access Date", TableMessageContent.DATE);
		header.setText(1, "Messages Sent", TableMessageContent.TEXT);
		result.add(header);
		HashMap<Date, Integer> dailyCounts = controller.getDailyMessageCount();
		for(Date accessDate : dailyCounts.keySet())
		{
			info = new TableMessage(1, 0, 1);
			info.setDate(0, accessDate);
			info.setText(0, dailyCounts.get(accessDate).toString());
			result.add(info);
		}
		return result;
	}
}
