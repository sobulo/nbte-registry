package com.fertiletech.nbte.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.nbte.client.TicketService;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.tickets.RequestTicket;
import com.fertiletech.nbte.server.tickets.TicketsDAO;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class TicketServiceImpl extends RemoteServiceServlet implements TicketService
{
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}

	@Override
	public TableMessage createTicket(TableMessage m) {
		/*String priority = m.getText(DTOConstants.MRS_PRIORITY_IDX);
		String issue = m.getText(DTOConstants.MRS_ISSUE_IDX);
		Key<? extends SemesterCourse> site = ObjectifyService.factory().stringToKey(m.getText(DTOConstants.MRS_SITE_IDX));
		RequestTicket t = TicketsDAO.createTicket(site, m.getText(DTOConstants.MRS_STAFF_IDX), priority, 
				m.getDate(DTOConstants.MRS_REQUEST_IDX), m.getText(DTOConstants.MRS_STATUS_IDX), 
				m.getText(DTOConstants.MRS_TITLE_IDX), m.getText(DTOConstants.MRS_DETAILS_IDX), issue, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return TicketsDAO.getTicketTableMessage(t);*/
		return null;
	}
	
	@Override
	public TableMessage editTicket(TableMessage[] edits) {
		/*
		RequestTicket t = TicketsDAO.editTicket(edits, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return TicketsDAO.getTicketTableMessage(t);*/
		return null;
	}

	@Override
	public List<TableMessage> getTickets(Date startDate, Date endDate) {
		List<RequestTicket> tickets = TicketsDAO.getTickets(ObjectifyService.begin(), true, startDate, endDate, null);
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(getTicketHeader());
		for(RequestTicket t : tickets)
			result.add(TicketsDAO.getTicketTableMessage(t));
		return result;
	}

	@Override
	public TableMessage getTicket(Long ticketNumber) {
		Key<RequestTicket> ticketKey = RequestTicket.getKey(ticketNumber);
		return getTicket(ticketKey);
	}
	
	public TableMessage getTicket(Key<RequestTicket> ticketKey)
	{
		RequestTicket ticket = ObjectifyService.begin().find(ticketKey);
		if(ticket == null)
			return null;
		return TicketsDAO.getTicketTableMessage(ticket);		
	}
	
	public TableMessageHeader getTicketHeader()
	{		
		TableMessageHeader h = new TableMessageHeader(9);
		h.setText(0, "No.", TableMessageContent.NUMBER);
		h.setFormatOption(0, "###");
		h.setText(1, "Location", TableMessageContent.TEXT);
		h.setText(2, "Type", TableMessageContent.TEXT);
		h.setText(3, "Title", TableMessageContent.TEXT);
		h.setText(4, "Priority", TableMessageContent.TEXT);
		h.setText(5, "Status", TableMessageContent.TEXT);
		h.setText(6, "Assigned To", TableMessageContent.TEXT);				
		h.setText(7, "Effective Date", TableMessageContent.DATE);		
		h.setText(8, "Entry Date", TableMessageContent.DATE);
		return h;
	}

	@Override
	public HashMap<String, String> getAllTickets() {
		HashMap<String, String> result = new HashMap<String, String>();
		List<RequestTicket> tickets = ObjectifyService.begin().query(RequestTicket.class).list();
		for(RequestTicket t : tickets)
			result.put(
					ObjectifyService.factory().keyToString(t.getKey()),
					t.getTitle() + " {" + t.getKey().getId() + "}");			
		return result;
	}

	@Override
	public List<TableMessage> addInvoiceID(String ticketID, Long invoiceID,
			boolean isAdd) throws MissingEntitiesException {
		if(ticketID == null || ticketID.trim().length() == 0)
			throw new RuntimeException("No ticket ID specified");
		
		Key<RequestTicket> ticket = ObjectifyService.factory().stringToKey(ticketID);
		if(invoiceID != null)
		{
			List<InetBill> bill = ObjectifyService.begin().query(InetBill.class).filter("invoiceId", invoiceID).list(); 
			if(bill.size() == 0)
			{
				throw new MissingEntitiesException("No invoices found with id: " + invoiceID);
			}
			else if(bill.size() > 1)
			{
				StringBuilder bd = new StringBuilder("Error multiple bills for InvoiceID: ").append(invoiceID);
				for(InetBill b : bill)
				{
					bd.append("\nBill: ").append(b.getKey().toString());
				}
				throw new RuntimeException(bd.toString());
			}			
			Objectify ofy = ObjectifyService.beginTransaction();
			try
			{			
				if(isAdd)
					bill.get(0).setTicketKey(ticket);
				else
				{
					if(bill.get(0).getTicketKey().equals(ticket))
						bill.get(0).setTicketKey(null);
					else
						throw new RuntimeException("Ticket key mismatch for ticket invoice removal: " + bill.get(0).getKey() + " vs. " + ticket + " for bill id: " + invoiceID);				
				}
				ofy.put(bill);
				ofy.getTxn().commit();
			}
			finally
			{
				if(ofy.getTxn().isActive())
					ofy.getTxn().rollback();
			}
		}
		return getBills(ticket);
	}
	
	private List<TableMessage> getBills(Key<RequestTicket> ticketKey)
	{
		Objectify ofy = ObjectifyService.begin();
		List<InetBill> bills = ofy.query(InetBill.class).filter("ticketKey", ticketKey).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		try {
			result.add(AccountManagerImpl.getBillSummaryHeaderForBlotter(true));
			result.addAll(AccountManagerImpl.getBillListSummary(bills, true, true, ofy));
			return result;
		} catch (MissingEntitiesException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<TableMessage> addAllocationID(String ticketID,
			String allocationID, boolean isAdd) throws MissingEntitiesException {
		/*Key<RequestTicket> tk = ObjectifyService.factory().stringToKey(ticketID);
		Key<InventoryAllocation> ak = ObjectifyService.factory().stringToKey(allocationID);
		return addAllocationID(tk, ak, isAdd);*/
		return null;
	}	
	
	public List<TableMessage> addAllocationID(Key<RequestTicket> ticket, /*Key<InventoryAllocation>*/ Key ak, boolean isAdd) throws MissingEntitiesException
	{
		/*Objectify ofy = ObjectifyService.beginTransaction();
		if(ak != null)
		{
			try
			{			
				InventoryAllocation alloc = ofy.find(ak);
				if(alloc == null)
					throw new MissingEntitiesException("Unable to find a transfer/allocation record for specified product, location and/or id: " + ak);
				if(isAdd)
					alloc.setTicketKey(ticket);
				else
				{
					if(alloc.getTicketKey().equals(ticket))
						alloc.setTicketKey(null);
					else
						throw new RuntimeException("Ticket key mismatch for ticket invoice removal. For " + alloc.getKey() + " Found ticket "  + alloc.getTicketKey() + " vs. requested " + ticket);				
					
				}
				ofy.put(alloc);
				ofy.getTxn().commit();
			}
			finally
			{
				if(ofy.getTxn().isActive())
					ofy.getTxn().rollback();
			}
		}
		return getAllocations(ticket);*/
		return null;
	}
	
	@Override
	public List<TableMessage> addAllocationID(String ticketID,
			Long allocationID, boolean isAdd, String productID, String locationID) throws MissingEntitiesException {
		/*if(ticketID == null || ticketID.trim().length() == 0)
			throw new MissingEntitiesException("No ticket ID specified");

		Key<RequestTicket> ticket = ObjectifyService.factory().stringToKey(ticketID);

		Key<InventoryAllocation> ak = null;
		if(allocationID != null)
		{
			if(locationID == null || locationID.trim().length() == 0)
				throw new MissingEntitiesException("No location specified");
			
			if(productID == null || productID.trim().length() == 0)
				throw new MissingEntitiesException("No location specified");
	
			Key<Location> loc = ObjectifyService.factory().stringToKey(locationID);
			Key<ProductDetails> prod = ObjectifyService.factory().stringToKey(productID);
			Key<InventoryItem> parentKey = InventoryItem.getKey(prod, loc);
			ak = new Key(parentKey,InventoryAllocation.class, allocationID);
		}
		return addAllocationID(ticket, ak, isAdd);*/
		return null;
	}
	
	private List<TableMessage> getAllocations(Key<RequestTicket> ticketKey)
	{
		/*Objectify ofy = ObjectifyService.begin();
		List<InventoryAllocation> allocations = ofy.query(InventoryAllocation.class).filter("ticketKey", ticketKey).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		result.add(InventoryServiceImpl.getAllocationsHeader());
		for(InventoryAllocation alc : allocations)
			result.add(InventoryServiceImpl.getAllocationDTO(alc));
		return result;*/
		return null;
	}

	@Override
	public List<TableMessage> getAllocations(String ticketID) {
		Key<RequestTicket> ticketKey = null;
		if(ticketID != null)
			ticketKey = ObjectifyService.factory().stringToKey(ticketID);
		return getAllocations(ticketKey);
	}

	@Override
	public List<TableMessage> getInvoices(String ticketID) {
		Key<RequestTicket> ticketKey = null;
		if(ticketID != null)
			ticketKey = ObjectifyService.factory().stringToKey(ticketID);
		return getBills(ticketKey);
	}

	@Override
	public TableMessage getTicket(String ticketID) {
		Key<RequestTicket> ticketKey = ObjectifyService.factory().stringToKey(ticketID);
		return getTicket(ticketKey);
	}
}