package com.fertiletech.nbte.server.accounting;

import javax.persistence.Id;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class UniqueIDGenerationHelper {
	@Id
	Long id;

	String dummy;
	
	public static long getID()
	{
		return ObjectifyService.factory().allocateId(UniqueIDGenerationHelper.class);
	}
}
