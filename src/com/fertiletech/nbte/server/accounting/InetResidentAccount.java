package com.fertiletech.nbte.server.accounting;

import java.util.Comparator;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.User;
import com.fertiletech.nbte.shared.DTOConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class InetResidentAccount implements Comparator<LedgerEntry> {
	@Id
	private String keyName;

	@Parent
	Key<? extends User> parentAccountKey;
	@Indexed
	double currentAmount;
	private int entryCount;
	private Date createDate;
	private Date lastModified;
	boolean allowOverDraft;

	int depositWarningLevel;
	private final static char KEY_SEPERATOR = '-';

	public InetResidentAccount() {
		createDate = new Date();
	}

	public InetResidentAccount(Key<Student> tenantKey, String depositType,
			boolean allowOverDraft, String currency, int depositWarningLevel) {
		this();
		if (depositType == null || currency == null
				|| depositType.length() == 0
				|| currency.length() != DTOConstants.CURRENCY_LENGTH)
			throw new RuntimeException("Invalid type or currency. Type: "
					+ depositType + " currency: " + currency);

		this.entryCount = 0;
		this.currentAmount = 0;
		this.parentAccountKey = tenantKey;
		this.allowOverDraft = allowOverDraft;
		this.keyName = getKeyName(depositType, currency);
		this.depositWarningLevel = depositWarningLevel;
	}

	static String getKeyName(String depositType, String currency)
	{
		return currency + KEY_SEPERATOR + depositType;
	}
	
	public Key<? extends User> getParentAccountKey() {
		return parentAccountKey;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public boolean isAllowOverDraft() {
		return allowOverDraft;
	}

	public int getDepositWarningLevel() {
		return depositWarningLevel;
	}

	public Key<InetResidentAccount> getKey() {
		return getKeyHelper(parentAccountKey, keyName);
	}

	public double getCurrentAmount() {
		return currentAmount;
	}

	void processLedger(LedgerEntry entry) {
		currentAmount += (entry.getAmount() * (entry.isCredit() ? 1 : -1));
		entry.setAccountBalance(currentAmount);
		entry.setEntryCount(++entryCount);
	}

	public int getNumberOfTxns() {
		return entryCount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getLastModifiedDate() {
		return lastModified;
	}

	@PrePersist
	void changeModificationDate() {
		lastModified = new Date();
	}

	public static Key<InetResidentAccount> getKey(Key<? extends User> tenantKey,
			String type, String ccy) {
		return getKeyHelper(tenantKey, getKeyName(type, ccy));
	}

	private static Key<InetResidentAccount> getKeyHelper(
			Key<? extends User> pk, String typeAndCCy) {
		return new Key<InetResidentAccount>(pk, InetResidentAccount.class, typeAndCCy);
	}

	public static Key<Student> getResident(
			Key<InetResidentAccount> parentAccountKey) {
		
		Key<Student> studKey = parentAccountKey.getParent();
		return studKey;
	}



	public String getFormattedTitle() 
	{
		StringBuilder result = new StringBuilder();
		Key<InetResidentAccount> cursor = getKey();
		while(cursor != null)
		{
			result.append(cursor.getName());
			cursor = cursor.getParent();
		}
		return result.toString();
	}

	@Override
	public int compare(LedgerEntry o1, LedgerEntry o2) {
		return compareFunction(o1, o2);
	}

	private static int compareFunction(LedgerEntry o1, LedgerEntry o2) {
		if(o1 == o2 || o1.getKey().equals(o2.getKey())) return 0;
		if (o1.getKey().getParent().compareTo(o2.getKey().getParent()) == 0) {
			if (o1.getEntryCount() == o2.getEntryCount())
				return 0;
			if (o1.getEntryCount() > o2.getEntryCount())
				return 1;
			else
				return -1;
		} else
			return o1.getKey().getParent().compareTo(o2.getKey().getParent());

	}

	public static Comparator<LedgerEntry> getComparator() {
		return new Comparator<LedgerEntry>() {

			@Override
			public int compare(LedgerEntry o1, LedgerEntry o2) {
				return compareFunction(o1, o2);
			}
		};
	}

	public String getDepositType() {
		return keyName.substring(DTOConstants.CURRENCY_LENGTH + 1);
	}

	public String getCurrency() {
		return keyName.substring(0, DTOConstants.CURRENCY_LENGTH);
	}
}
