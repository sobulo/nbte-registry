package com.fertiletech.nbte.server.accounting;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.shared.BillDescriptionItem;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
@Unindexed
public class BillTemplate {
	@Id
	Long id;
	
	public BillTemplate() {}
	
	@Parent Key<SchoolSession> billTerm;    
	@Serialized
    private LinkedHashSet<BillDescriptionItem> itemizedBill;	
	@Unindexed private double settledAmount;
	//title - possibly use it for categories /i.e. no free-form stuff
	@Indexed private String title;
	@Unindexed private Date servicePeriodStartDate;
	@Unindexed private Date servicePeriodEndDate;
	final static DateFormat DF = DateFormat.getDateInstance(DateFormat.MEDIUM);
	@Unindexed private String currency;
	@Unindexed private Date dueDate;
	@Indexed private Date lastModified;

    
    public BillTemplate(LinkedHashSet<BillDescriptionItem> billDescription, 
    		 String category, Date start, Date end, String ccy, Key<SchoolSession> term, Date due) {
		this.itemizedBill = billDescription;
		this.title = category;
		this.servicePeriodStartDate = start;
		this.servicePeriodEndDate = end;
		this.currency = ccy;
		this.billTerm = term;
		this.dueDate = due;

	}

	public HashSet<BillDescriptionItem> getItemizedBill() {
		return itemizedBill;
	}
	
	public void setItemizedBill(LinkedHashSet<BillDescriptionItem> itemizedBill)
	{
		this.itemizedBill = itemizedBill;
	}
	
	public double getTotalAmount()
	{
		double total = 0;
		for(BillDescriptionItem bdi : itemizedBill)
			total += bdi.getAmount();
		return total;
	}
	
	public Key<BillTemplate> getKey()
	{
		return new Key<BillTemplate>(billTerm, BillTemplate.class, id);
	}
	
    public String getFormatedTitle()
    {
    	return getFormatedTitle(this.title, this.servicePeriodStartDate, this.servicePeriodEndDate);
    }
    
    private static String getFormatedTitle(String category, Date start, Date end)
    {
    	return category + " " + DF.format(start) + " - " + DF.format(end);    	
    }
    
	public String getCurrency() {
		return currency;
	}; 
	
	public Date getDueDate() {
		return dueDate;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public Date getStartDate()
	{
		return servicePeriodStartDate;
	}
	
	public Date getEndDate()
	{
		return servicePeriodEndDate;
	}	

	public Date getTimeStamp()
	{
		return lastModified;
	}		
	
	@PrePersist
	void timeStamp()
	{
		lastModified = new Date();
	}
}
