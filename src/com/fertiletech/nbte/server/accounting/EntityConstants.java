package com.fertiletech.nbte.server.accounting;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

/**
 * @author sobulo@fertiletech.com
 */
public class EntityConstants {
	
	public static final String GCS_HOST = "https://storage.googleapis.com/";
	
	public final static String[][] EMPLOYEE_NAMES = {
		// this is a comment, below are non technician employees
	{ "Segun", "Sobulo", "segun.sobulo@fcahptib.edu.ng", "123" }, // ok
			{ "Help", "Desk", "9jedu-helpdesk@fcahptib.edu.ng", "123" }, // ok
	};
	
	
	/*static String[] ACCTS_COLLEGE_CHARGES = 
		{"Registration Fee", 
		"Other Ish Fees ..."
	};
	static String[] ACCTS_EED_CHARGES = 
	{
		"EED Miscellaneous",
		"EED General",
		
	};
	
	static String[] ACCTS_DEPT_CHARGES = {
		"Lab", 
		"Excursion",			
		"Other",			
	};
	
	
	static String[] ACCT_TYPES = {
		"General College Fees",
		"EED Fees",
		"Department Fees"
	};
	
	static String[] ACCT_DESCRIPTIONS = 
	{
		"Fees paid into the colleges wema bank",
		"Enterpreneurship Fees",
		"Fees paid at the department level"		
	};
		
	public static HashMap<String, String> BILL_MAPPINGS = new HashMap<String, String>();
	public static HashMap<String, String> ACCOUNT_MAPPINGS = new HashMap<String, String>();
	public static HashMap<String, String> PROD_CAT_MAPPINGS = new HashMap<String, String>();
	static
	{
		
		for(int i = 0; i < ACCT_TYPES.length;i++)
		{
			BILL_MAPPINGS.put(ACCTS_COLLEGE_CHARGES[i], ACCT_TYPES[i]);
			ACCOUNT_MAPPINGS.put(ACCT_TYPES[i], ACCT_DESCRIPTIONS[i]);
		}
		
	}*/
	
	
	//public final static String APP_PARAM_VENDOR_CATEGORY_LIST_KEY = "vendor-category-names
	
    public final static String EMAIL_CONTROLLER_ID = "EMAIL Controller";    
    public final static String SMS_CONTROLLER_ID = "SMS Controller";
    public final static String[] FACILITY_TYPES = {"Maintenance Bin", "Trash Bin", "Repairs Bin"};
    
	//number and date formats
	public final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();
	public final static DateFormat DATE_FORMAT = new SimpleDateFormat("d MMM yyyy");
	static
	{
		NUMBER_FORMAT.setMaximumFractionDigits(2);
		NUMBER_FORMAT.setMinimumIntegerDigits(1);
	}
}
