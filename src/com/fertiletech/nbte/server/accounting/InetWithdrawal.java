/**
 * 
 */
package com.fertiletech.nbte.server.accounting;


import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
public class InetWithdrawal implements LedgerEntry{
	@Id Long key;
	private Date withdrawalDate;
	@Indexed private Key<InetBill> allocatedBill;
	private String referenceId;
	private String comments;
	private double amount;
	private double accountBalance;
	@Indexed private Date createDate;
	@Parent private Key<InetResidentAccount> accountKey;
	long countStamp;
	String user;
	
	
	InetWithdrawal(){
		createDate = new Date();
	}
	
	InetWithdrawal(Key<InetBill> billKey, Key<InetResidentAccount> accKey, double amount, String referenceId, 
			String comments, Date withdrawDate, String entryUser)
	{
		this();
		this.amount = amount;
		this.comments = comments;
		this.referenceId = referenceId;
		this.withdrawalDate = withdrawDate;
		this.allocatedBill = billKey;
		this.accountKey = accKey;
		this.user = entryUser;
	}

	public Key<InetWithdrawal> getKey() {
		return new Key<InetWithdrawal>(accountKey, InetWithdrawal.class, key);
	}

	public Key<InetBill> getAllocatedBill() {
		return allocatedBill;
	}

	public Date getEffectiveDate() {
		return withdrawalDate;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public String getDescription() {
		return comments;
	}

	public double getAmount() {
		return amount;
	}
	
	public long getEntryCount()
	{
		return countStamp;
	}
	
	public void setEntryCount(long count)
	{
		countStamp = count;
	}
	
	public Date getEntryDate()
	{
		return createDate;
	}

	@Override
	public boolean isCredit() {
		return false;
	}
	
	public void setAccountBalance(double amount)
	{
		accountBalance = amount;
	}
	
	public double getAccountBalance()
	{
		return accountBalance;
	}

	@Override
	public String getEntryUser() {
		return user;
	}
}
