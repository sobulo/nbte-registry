package com.fertiletech.nbte.server.accounting;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;
import com.googlecode.objectify.condition.IfNotEmpty;

@Unindexed
@Cached
public class InetDeposit implements Serializable, LedgerEntry 
{
	@Id private Long key;
	@Indexed private Date createDate;
	private Date depositDate;
	
	/**
	 *indexing this field allows us to take a deposit amount and then
	 *allocate it among residents using ref number as a grouping field
	 *group individual deposits this way, group all related deposits by checking
	 *company rep field on tenant class
	 */
	@Indexed(IfNotEmpty.class) private String referenceNumber;
	
	private String comments;
	@Parent private Key<InetResidentAccount> accountKey;
	@Indexed private Key<InetCompanyAccount> companyAccount;

	private double amount;
	private  double accountBalance;
	private long entryCount;
	private String entryUser;
	
	InetDeposit(){createDate = new Date();}
	
	InetDeposit(Key<InetCompanyAccount> companyAccount,
			Date depositDate, String referenceNumber, String comments,
			Key<InetResidentAccount> accountKey, double amount, String entryUser) {
		this();
		this.companyAccount = companyAccount;
		this.depositDate = depositDate;
		this.referenceNumber = referenceNumber;
		this.comments = comments;
		this.accountKey = accountKey;
		this.amount = amount;
		this.entryUser = entryUser;
	}
	
	
	public Key<InetCompanyAccount> getCompanyAccount() {
		return companyAccount;
	}

	public Key<InetDeposit> getKey()
	{
		return new Key<InetDeposit>(accountKey, InetDeposit.class, key);
	}

	public Date getEffectiveDate() {
		return depositDate;
	}

	public String getReferenceId() {
		return referenceNumber;
	}

	public String getDescription() {
		return comments;
	}

	public double getAmount() {
		return amount;
	}

	@Override
	public boolean isCredit() {
		return true;
	}

	@Override
	public double getAccountBalance() {
		return accountBalance;
	}

	@Override
	public void setAccountBalance(double balance) {
		this.accountBalance = balance;
	}

	@Override
	public Date getEntryDate() {
		return createDate;
	}

	@Override
	public long getEntryCount() {
		return entryCount;
	}

	@Override
	public void setEntryCount(long count) {
		this.entryCount = count;
	}
	
	@Override
	public String getEntryUser()
	{
		return entryUser;
	}
}
