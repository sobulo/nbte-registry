package com.fertiletech.nbte.server.accounting;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
@Unindexed
public class InetCompanyAccount {
	@Id private Long id;
	private String accountNumber;
	private String accountName;
	private String bank;
	private String sortCode;
	private final static int MIN_ACCT_NUM_LENGTH = 10;
	private String currency;
	private String suggestedType;
	
	public InetCompanyAccount() {}
	
	InetCompanyAccount(String accountNumber, String accountName, String bank, String sortCode, String currency) {
		if(accountNumber.length() < MIN_ACCT_NUM_LENGTH)
			throw new IllegalStateException("Account number must have at least " + MIN_ACCT_NUM_LENGTH + " digits/characters");
		this.accountNumber = accountNumber;
		this.accountName = accountName;
		this.bank = bank;
		this.sortCode = sortCode;
		this.setCurrency(currency);
	}

	public String getSortCode() {
		return sortCode;
	}
	
	public String getSuggestedType() {
		return suggestedType;
	}

	public void setSuggestedType(String suggestedType) {
		this.suggestedType = suggestedType;
	}

	public void setSortCode(String sortCode) {
		this.sortCode = sortCode;
	}
	
	public Key<InetCompanyAccount> getKey()
	{
		return new Key<InetCompanyAccount>(InetCompanyAccount.class, id);
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
