/**
 * 
 */
package com.fertiletech.nbte.server.accounting;


import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.nbte.server.college.User;
import com.fertiletech.nbte.server.tickets.RequestTicket;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetBill{
	@Id private String key;
	@Unindexed private double settledAmount;
	@Unindexed private double totalAmount;
	@Parent private Key<? extends User> userKey;
	@Indexed private boolean settled;  //indexed field for finding which bills are fully paid already
	
	//billing period
	@Indexed private Date createDate;
	@Indexed private Long invoiceId;
	
	@Indexed Key<RequestTicket> ticketKey;
	@Indexed Key<BillTemplate> templateKey;
	
	
	
	final static double THRESHOLD = 0.01;
	
	InetBill(){ createDate = new Date();}
	
	InetBill(Key<? extends User> billedUserKey, Key<BillTemplate> templateKey, Key<RequestTicket> ticketKey,  double totalAmount)
	{
		this();
		this.settled = false;
		this.userKey = billedUserKey;
		this.templateKey = templateKey;
		this.key = getKeyString();
		this.ticketKey = ticketKey;
		this.totalAmount = totalAmount;
	}
	
    private String getKeyString()
    {
    	return getKeyString(templateKey); 
    }
    
    public static String getKeyString(Key<BillTemplate> templateKey)
    {
        String format = "%s~%s";
        return String.format(format, templateKey.getParent().getName(), String.valueOf(templateKey.getId()));    	
    }
    
	public Key<InetBill> getKey() {
		return new Key<InetBill>(userKey, InetBill.class, key);
	}	

	public Date getCreateDate() {
		return createDate;
	}
	
	public double getSettledAmount()
	{
		return settledAmount;
	}
	
	public void setSettledAmount(double amount)
	{
		settledAmount = amount;
	}
	
	public Key<? extends User> getUserKey()
	{
		return userKey;
	}
		
	public long getInvoiceID()
	{
		updateInvoiceId();
		return invoiceId;
	}
	
	public Key<RequestTicket> getTicketKey() {
		return ticketKey;
	}

	public void setTicketKey(Key<RequestTicket> ticketKey) {
		this.ticketKey = ticketKey;
	}
	
	public double getTotalAmount(){ 
		return totalAmount; 
	};
	
	public Key<BillTemplate> getTemplateKey()
	{
		return templateKey;
	}


	@PrePersist void updateInvoiceId()
	{
		if(invoiceId == null)
		{
			invoiceId = UniqueIDGenerationHelper.getID();
		}
	}
	@PrePersist void updateSettled() { settled = Math.abs(totalAmount - settledAmount) < THRESHOLD; }

}
