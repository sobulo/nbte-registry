/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import com.fertiletech.nbte.server.accounting.BillTemplate;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.accounting.InetCompanyAccount;
import com.fertiletech.nbte.server.accounting.InetDeposit;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.accounting.InetWithdrawal;
import com.fertiletech.nbte.server.accounting.Representative;
import com.fertiletech.nbte.server.bulkupload.InetImageBlob;
import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.server.college.RosterEntry;
import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.StudentSession;
import com.fertiletech.nbte.server.comments.AcitivityComment;
import com.fertiletech.nbte.server.comments.ApplicationParameters;
import com.fertiletech.nbte.server.comments.InetMultiAnswers;
import com.fertiletech.nbte.server.comments.MortgageAttachment;
import com.fertiletech.nbte.server.comments.RatingComment;
import com.fertiletech.nbte.server.tickets.RequestTicket;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;


//import java.util.GregorianCalendar;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class GeneralFuncs {
	
    public static <T> boolean addToHashSet( Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());
        //seems we can't rely on boolean result of add as datanucleus backed
        //hashet subclass successfully adds @ least on local server even when
        //element is already present. contains works though so check that first
        if(hash.contains(o))
            return false;
        return hash.add(o);
    }

    public static <T> boolean removeFromHashSet(Set<T> hash, T o)
    {
        if(o == null)
            throw(new NullPointerException());

        if(!hash.contains(o))
            return false;

        return hash.remove(o);
    }

    public static <K, V> boolean addToHashMap(HashMap<K, V> hashMap, K key, V value)
    {
        if(hashMap.containsKey(key))
            return false;
        hashMap.put(key, value);
        return true;
    }

    public static <K, V> boolean updateHashMap(HashMap<K, V> hashMap, K key, V val)
    {
        if(!hashMap.containsKey(key))
            return false;
        hashMap.put(key, val);
        return true;
    }

    public static <K, V> boolean removeFromHashMap(HashMap<K, V> hashMap, K key)
    {
        return( hashMap.remove(key) == null? false : true);
    }

   /* public static Date getDate(int year, int month, int day)
    {
        //offset month by 1 as gregorian calendar represents months from 0-11
        return (new GregorianCalendar(year, month - 1, day)).getTime();
    }*/

    /**
     *
     * @param <T>
     * @param a - first array
     * @param b - second array to compare with first
     * @param intersect
     * @param aOnly - elements found in aOnly
     * @param bOnly - elements found in bOnly
     */
    public static <T extends Comparable< T > > void arrayDiff(T[] a, T[] b,
            ArrayList<T> intersect, ArrayList<T> aOnly, ArrayList<T> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }
    }
    
    public static boolean isOfyRegistered = false;
    public static void registerClassesWithOfy()
    {
    	if(isOfyRegistered)
    		return;
	    ObjectifyService.register(Student.class);
	    ObjectifyService.register(Course.class);
	    ObjectifyService.register(RosterEntry.class);
	    ObjectifyService.register(StudentSession.class);
	    ObjectifyService.register(SchoolSession.class);
	    ObjectifyService.register(ApplicationParameters.class);
	    ObjectifyService.register(AcitivityComment.class);
	    ObjectifyService.register(InetMultiAnswers.class);
	    ObjectifyService.register(MortgageAttachment.class);
	    ObjectifyService.register(RatingComment.class);
	    ObjectifyService.register(InetResidentAccount.class);
	    ObjectifyService.register(BillTemplate.class);
	    ObjectifyService.register(InetBill.class);
	    ObjectifyService.register(InetDeposit.class);
	    ObjectifyService.register(Representative.class);
	    ObjectifyService.register(RequestTicket.class);
	    ObjectifyService.register(InetCompanyAccount.class);
	    ObjectifyService.register(InetWithdrawal.class);
	    ObjectifyService.register(InetImageBlob.class);
    	isOfyRegistered = true;
    	
    }
    
    public static <T> void arrayDiff(Key<T>[] a, Key<T>[] b,
            ArrayList<Key<T>> intersect, ArrayList<Key<T>> aOnly, ArrayList<Key<T>> bOnly )
    {
        Arrays.sort(a);
        Arrays.sort(b);
        int i = 0;
        int j = 0;
        while(i < a.length || j < b.length)
        {
            if(i == a.length)
            {
                bOnly.add(b[j]);
                j++;
                continue;
            }

            if(j == b.length)
            {
                aOnly.add(a[i]);
                i++;
                continue;
            }

            int comp = a[i].compareTo(b[j]);
            if(comp == 0)
            {
                intersect.add(a[i]);
                i++;
                j++;
            }
            else if(comp < 0 )
            {
                aOnly.add(a[i]);
                i++;
            }
            else
            {
                bOnly.add(b[j]);
                j++;
            }
        }

    }    
}
