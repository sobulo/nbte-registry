package com.fertiletech.nbte.server;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.geronimo.mail.util.SessionUtil;

import com.fertiletech.nbte.client.RegistryService;
import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.Registrar;
import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.StudentSession;
import com.fertiletech.nbte.server.college.print.RegFormGenerator;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class RegistryServiceImpl extends RemoteServiceServlet implements
		RegistryService {

	@Override
	public List<TableMessage> getDepartmentRoster(String dept, String diploma,
			String year, int academicYear) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		List<Student> students = EntityDAO.getDepartmentRoster(dept, diploma, year, academicYear, ObjectifyService.begin());
		result.add(EntityDAO.getStudentDTOHeader());
		for(Student s : students)
			result.add(EntityDAO.getStudentDTO(s));
		return result;
	}

	@Override
	public List<TableMessage> getDepartmentCourseTemplates(String dept,
			String diploma, String year, int semester, int academicYear) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		List<Course> templates = EntityDAO.getDepartmentCourseTemplates(dept, diploma, year, semester, academicYear,ObjectifyService.begin());
		result.add(EntityDAO.getCourseDTOHeader());
		for(Course t : templates)
			result.add(EntityDAO.getCourseTemplateDTO(t));
		return result;
	}

	@Override
	public String registerSelectedCourses() throws MissingEntitiesException, DuplicateEntitiesException{
		Collection<Course> courses = CartSessionHelper.getCartObjects(CartSessionHelper.getCart(getThreadLocalRequest().getSession())).values();
		SchoolSession term = EntityDAO.getCurrentTerm();
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<Student> studKey = Student.getKey(email);
		Key<StudentSession> studSessKey = StudentSession.getKey(studKey, term.getAcademicYear(), term.getSemester());
		try 
		{
			Registrar.registerCourses(courses, studSessKey, term);
		} catch (DuplicateEntitiesException e) 
		{
			throw e;
		}
		finally
		{
			CartSessionHelper.emptyCart(getThreadLocalRequest().getSession());
		}
		return studSessKey.getString();
	}

	@Override
	public List<TableMessage> getCart() {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		result.add(CartSessionHelper.fetchCustomerCartHeader());
		CartSessionHelper.fetchCustomerCart(getThreadLocalRequest().getSession(), result);
		return result;
	}

	@Override
	public String updateCart(String courseID, boolean isAddition) {
		return CartSessionHelper.addToCart(courseID, getThreadLocalRequest().getSession(), isAddition);
	}

	@Override
	public TableMessage getCourseTemplateInfo(String templateID) {
		Key<Course> ck = new Key<Course>(templateID);
		Objectify ofy = ObjectifyService.begin();
		Course c = ofy.get(ck);
		return EntityDAO.getCourseTemplateDTO(c);
	}

	@Override
	public List<TableMessage> getSessionInfo(String sessionID)
			throws MissingEntitiesException {
		Key<StudentSession> ssk = new Key<StudentSession>(sessionID);
		Objectify ofy = ObjectifyService.begin();
		Map<Key<Object>, Object> objs = ofy.get(ssk, ssk.getParent());
		StudentSession sess = (StudentSession) objs.get(ssk);
		Student stud = (Student) objs.get(ssk.getParent());
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		HashSet<Key<Course>> courseKeys = Registrar.getRegisteredCourses(ssk, ofy);
		Collection<Course> courses = ofy.get(courseKeys).values();
		
		TableMessage m = sess.getDTO(stud, true);
		HashMap<Key<StudentSession>, StringBuilder> errorMap = 
				new HashMap<Key<StudentSession>, StringBuilder>();
		HashSet<Key<StudentSession>> selectedStud = new HashSet<Key<StudentSession>>();
		selectedStud.add(ssk);
		Registrar.getOutstandingPaymentMessage(selectedStud, errorMap, ofy, false);
		Registrar.updateTableMessageStatus(errorMap, m, ssk);
		result.add(m);
		TableMessageHeader h = EntityDAO.getCourseDTOHeader();
		h.setMessageId(sessionID);
		result.add(h);
		for(Course c : courses)
			result.add(EntityDAO.getCourseTemplateDTO(c));
		return result;
	}

	@Override
	public List<TableMessage> getMySessions() {
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(email == null || email.length() == 0)
			throw new IllegalStateException("Attempted to access a login protected resource");
		Key<Student> studKey = Student.getKey(email);
		return Registrar.getStudentSessionsList(studKey, ObjectifyService.begin());
	}

	@Override
	public String updateCart() {
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(email == null || email.length() == 0)
			return "Add request failed. Try logging in and out";
		Objectify ofy = ObjectifyService.begin();
		Key<Student> sk = Student.getKey(email);
		Student stud = ofy.find(sk);
		SchoolSession sess = EntityDAO.getCurrentTerm(ofy);
		if(stud == null)
			return "Add request failed, as you're not enrolled in the system for " + 
					NameTokens.getAcademicSessionSemesterString(sess.getAcademicYear(), sess.getSemester());
		HashMap<String, String> vals = stud.getValues();
		String year = vals.get(ApplicationFormConstants.YEAR);
		String dept = vals.get(ApplicationFormConstants.DEPARTMENT);
		String diploma = vals.get(ApplicationFormConstants.DIPLOMA);
		List<Key<Course>> courses = EntityDAO.queryDepartmentCourses(dept, diploma, year, sess.getSemester(), sess.getAcademicYear(), ofy).listKeys();
		StringBuilder result = new StringBuilder("<ol>");
		for(Key<Course> ck : courses)
			result.append("<li>").append(CartSessionHelper.addToCart(ck, getThreadLocalRequest().getSession(), true)).append("</li>");
		result.append("</ol>");
		return result.toString();
	}

	@Override
	public HashMap<String, String> getBio() throws MissingEntitiesException {
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<Student> studKey = Student.getKey(email);
		Student stud = ObjectifyService.begin().find(studKey);
		if(stud == null)
			throw new MissingEntitiesException("unable to find user profile for: " + email);
		return stud.getValues();
	}

	@Override
	public HashMap<String, String> saveBio(String id, HashMap<String, String> bio)
			throws MissingEntitiesException {
		String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		Key<Student> studKey = null;
		if(id == null)
			studKey = Student.getKey(email);
		else
			studKey = new Key<Student>(id);
		
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Student stud = ofy.find(studKey);
			if(stud == null)
				throw new MissingEntitiesException("Unable to get student values for: " + studKey.getName());
			stud.setValues(bio);
			ofy.put(stud);
			ofy.getTxn().commit();
			return stud.getValues();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}

	@Override
	public String[] getCourseRegistrationDownloadLink(String sessionID)
			throws MissingEntitiesException {
		Key<StudentSession> ssk = null; 
		if(sessionID == null)
		{
			SchoolSession s = EntityDAO.getCurrentTerm();
			String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
			Key<Student> sk = Student.getKey(email);
			ssk = StudentSession.getKey(sk, s.getAcademicYear(), s.getSemester());
		}
		else
			ssk = new Key<StudentSession>(sessionID);
		HashSet<Key<StudentSession>> arg = new HashSet<Key<StudentSession>>();
		arg.add(ssk);
		return getCourseREgistrationDownloadLink(arg, getThreadLocalRequest(), false);
	}

	@Override
	public String[] getItemizedTuitionDownloadLink(String sessionID)
			throws MissingEntitiesException {
		List<Key<InetBill>> billKeys = ObjectifyService.begin().query(InetBill.class).ancestor(new Key<StudentSession>(sessionID).getParent()).listKeys();
		String[] result = {"Click here to download your invoice/fee breakdowns",
				RegFormGenerator.getBillingInvoicesLink(billKeys, getThreadLocalRequest()), ""};
		return result;
	}

	@Override
	public String[] getAccountStatementLink(String sessionID)
			throws MissingEntitiesException {
		Key<StudentSession> ssk = new Key<StudentSession>(sessionID);
		Key<Student> sk = ssk.getParent();
		Collection<String> depTypes = InetDAO4Accounts.getBillTypes().values();
		List<Key<InetResidentAccount>> depKeys = new ArrayList<Key<InetResidentAccount>>(depTypes.size());
		HashSet<String> uniqueKeys = new HashSet<String>();
		uniqueKeys.addAll(depTypes);
		for(String depType : uniqueKeys)
		{
			Key<InetResidentAccount> accKey = InetResidentAccount.getKey(sk, depType, "NGN");
			depKeys.add(accKey);
		}
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(GregorianCalendar.MONTH, 11);
		cal.set(GregorianCalendar.YEAR, 2014);
		cal.set(GregorianCalendar.DAY_OF_MONTH, 15);
		Date startDate = cal.getTime();
		Date endDate = new Date();
		String[] result = { "Click here to download your account statement " + 
				EntityConstants.DATE_FORMAT.format(startDate) + " - " + EntityConstants.DATE_FORMAT.format(endDate),
				RegFormGenerator.getDepositStatementLink(sk.getString(), depKeys, startDate, endDate, getThreadLocalRequest()), ""};
		return result;
	}

	@Override
	public String[] getCourseRegistrationDownloadLink(String[] studs)
			throws MissingEntitiesException {		
		HashSet<Key<StudentSession>> studList = new HashSet<Key<StudentSession>>();
		Objectify ofy = ObjectifyService.begin();
		SchoolSession term =  EntityDAO.getCurrentTerm(ofy);
		for(String s : studs)
		{
			Key<Student> sk = new Key<Student>(s);
			Key<StudentSession> ssk = StudentSession.getKey(sk, 
					term.getAcademicYear(), term.getSemester());
			studList.add(ssk);
		} 
		return getCourseREgistrationDownloadLink(studList, getThreadLocalRequest(), true);
	}
	
	private static String[] getCourseREgistrationDownloadLink(HashSet<Key<StudentSession>> studList, HttpServletRequest req, boolean includeSignature) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.begin();
		HashMap<Key<StudentSession>, StringBuilder> errorMap = new HashMap<Key<StudentSession>, StringBuilder>();
		HashMap<Key<StudentSession>, Double> clearanceInfo = Registrar.getOutstandingPaymentMessage(studList, errorMap, ofy, includeSignature);
		StringBuilder errorAggregate = new StringBuilder();
		for(Key<StudentSession> ssk : errorMap.keySet())
		{
			StringBuilder err = errorMap.get(ssk);
			if(err.length() > 0)
			{
				studList.remove(ssk);
				errorAggregate.append(err);
			}
		}
		String[] result = new String[3];
		result[2] = errorAggregate.toString(); //should contain total outstanding balance
		if(studList.size() > 0)
		{
			result[0] = "Click here to download your course registration form";
			result[1] = RegFormGenerator.getRegFormLink(studList, req);
		}
		return result;
		
	}

	
	
}
