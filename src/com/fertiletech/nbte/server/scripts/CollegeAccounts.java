package com.fertiletech.nbte.server.scripts;

import java.util.HashMap;

import com.fertiletech.nbte.shared.NameTokens;

public class CollegeAccounts {
	final static String[][] ACCOUNT_INFO = 
	{
		{"FEDERAL COLLEGE OF ANIMAL HEALTH","Registration Charges","0120811470","035190574","Wema Bank"},
		{"FCAH&PT SERVICE CHARGE ACCT","College Service Charges","0122191125","035190574","Wema Bank"},
		{"FED COL OF ANIMAL HLTH &PROD TISHIP","Medical Charges","0120821068","035190574","Wema Bank"},
		{"FEDERAL COLLEGE OF ANIMAL HEALTH&PRODUCTION TECHNOLOGY (BORA) IBADAN","Entrepreneurship Charges","2014986365","011194594","First Bank"},
		{"DEPT OF GNS/LIBRARY FCAH&PT","General Studies Charges","0122216086","035190574","Wema Bank"},
		{"KNOWLEDGEPOOL CONSULTING LTD","Life Insurance Charges","1016637420","33193395","UBA"},
		{"FERTILE TECH BUSINESS SYST.LTD","Online Access Charges","0128981396","058194049","GT Bank"},
		/*{"FCAH&PT DEPT OF COMPUTER","Comp. Sci. Dept. Charges","0122214147","035190574","Wema Bank"},
		{"DEPT OF STATISTICS FCAH&PT","Statistics Dept. Charges","0122216103","035190574","Wema Bank"},
		{"ASS.OF ANIMAL HLTH & PROD STUD","Animal Hlth. Dept. Charges","0030539544","058194049","GT Bank"},
		{"SCIENCE LAB TECH DEP FCAH&PT","SLT Dept. Charges","0030719742","058194049","GT Bank"},
		{"FISHERIES DEPARTMENT (F.C.A.H&P.T)","Fisheries Dept. Charges","0122122477","035190574","Wema Bank"},*/	
	};
	
	public final static int ACCT_NAME_IDX = 0;
	public final static int ACCT_TYPE_IDX = 1;
	public final static int ACCT_NUM_IDX = 2;
	public final static int ACCT_SORT_IDX = 3;
	public final static int ACCT_BANK_IDX = 4;
	public final static HashMap<String, String> ACCT_TYPES = new HashMap<String, String>();
	public final static HashMap<String, String> BILL_TYPES = new HashMap<String, String>();
	
	static String TESTING_STUDENTS[] = { 
		    "abasiama.udoh@students.fcahptib.edu.ng",
			/*"abiodun.lasisi@students.fcahptib.edu.ng",
			"abass.adetunji@students.fcahptib.edu.ng",
			"adebayo.sangolade@students.fcahptib.edu.ng",
			"aderemi.adesanmi@students.fcahptib.edu.ng",
			"abioye.olufunmilayo@students.fcahptib.edu.ng"*/ };

	static String[] NEW_STUDENTS = {
		/*"ebenezer.adeniyi@students.fcahptib.edu.ng",
			"oluyomi.oyedele@students.fcahptib.edu.ng",
			"jelilu.oyeyemi@students.fcahptib.edu.ng",
			"samuel.adisa@students.fcahptib.edu.ng",
			"oluwapelumi.abiola@students.fcahptib.edu.ng",
			"samuel.adebayo@students.fcahptib.edu.ng",
			"miracle.olanrewaju@students.fcahptib.edu.ng",
			"abosede.akinlade@students.fcahptib.edu.ng",
			"abisoye.ogunleye@students.fcahptib.edu.ng",
			"temitope.lawal@students.fcahptib.edu.ng",
			"oluwaferanmi.adeniyi@students.fcahptib.edu.ng",
			"micheal.adeniyi@students.fcahptib.edu.ng",
			"aremu.mudashir@students.fcahptib.edu.ng",
			"oluwabusola.oke@students.fcahptib.edu.ng",
			"racheal.akingbade@students.fcahptib.edu.ng" */};
	
	static
	{
		for(String[] acctDesc : ACCOUNT_INFO)
		{
			ACCT_TYPES.put(acctDesc[ACCT_TYPE_IDX], NameTokens.getAccountID(acctDesc[ACCT_SORT_IDX], acctDesc[ACCT_NUM_IDX]));
			BILL_TYPES.put(acctDesc[ACCT_TYPE_IDX], acctDesc[ACCT_TYPE_IDX]);
		}
	}
	
	/*public static void main(String [] args) {
		for(int i = 0; i < ACCOUNT_INFO.length; i++)
		{
			String[] row = ACCOUNT_INFO[i];
			for(int j = 0; j < row.length; j++)
				System.out.print(row[j] + " -- ");
			System.out.println();
		}
	}*/
}
