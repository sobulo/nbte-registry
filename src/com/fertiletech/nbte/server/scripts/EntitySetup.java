/**
 * 
 */
package com.fertiletech.nbte.server.scripts;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.accounting.InetCompanyAccount;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.comments.ApplicationParameters;
import com.fertiletech.nbte.server.comments.CommentsDAO;
import com.fertiletech.nbte.server.messaging.EmailController;
import com.fertiletech.nbte.server.messaging.MessagingDAO;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 * 
 */
public class EntitySetup extends HttpServlet {
	private static final Logger log = Logger.getLogger(EntitySetup.class
			.getName());

	static {
		GeneralFuncs.registerClassesWithOfy();
	}

	/**
	 * web.xml contains entries to ensure only registered developers for the app
	 * can execute this script. Does not go through login logic
	 */

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try {
			String type = req.getParameter("type");
			if (type == null) {
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b><br/>");
			if (type.equals("params")) {
				String result = createParameter();
				out.println(result);
				// EntityDAO.createSchoolSession(2015, 1, true);
				// EntityDAO.createSchoolSession(2015, 2, false);
				// out.println("sessions created");
				// log.warning("Created 2015 sessions" +
				// LoginHelper.getLoggedInUser(req));
			} else if (type.equals("email")) {
				String fromAddress = "customerservices@addosser.com";
				String bccAddress = "customerservices@addosser.com";
				EmailController controller = MessagingDAO
						.createEmailController(MessagingDAO.PUBLIC_EMAILER,
								fromAddress, bccAddress, 5000, 95, 10000);
				log.warning("created: " + controller.getKey());
				controller = MessagingDAO.createEmailController(
						MessagingDAO.SYSTEM_EMAILER, fromAddress, null, 5000,
						5, 1000);
				log.warning("created: " + controller.getKey());
			} else if (type.equals("staff")) {
				HashMap<String, String> params = new HashMap<String, String>(
						EntityConstants.EMPLOYEE_NAMES.length);
				for (String[] info : EntityConstants.EMPLOYEE_NAMES) {
					String email = info[2].toLowerCase();
					String name = info[0] + " " + info[1];
					params.put(email, name);
				}
				ApplicationParameters a = CommentsDAO.createParams(
						DTOConstants.APP_PARAM_EMPLOYEE_LIST, params);
				ApplicationParameters c = CommentsDAO.createParams(
						DTOConstants.APP_PARAM_ACCTS, params);
				out.println("Created " + a.getKey() + " and " + c.getKey());
			} else if (type.equals("resacctypes")) {
				ApplicationParameters a = CommentsDAO.createParams(
						DTOConstants.APP_PARAM_RES_ACCT_LIST_KEY,
						CollegeAccounts.ACCT_TYPES);
				ApplicationParameters b = CommentsDAO.createParams(
						DTOConstants.APP_PARAM_RES_BILL_LIST_KEY,
						CollegeAccounts.BILL_TYPES);

				out.println("Created: " + a.getKey() + " and " + b.getKey());
			} else if (type.equals("resaccts")) {
				Objectify ofy = ObjectifyService.begin();
				
				/*List<Key<Student>> studKeys = EntityDAO.queryDepartmentRoster(NameTokens.ALT_CS, NameTokens.ND,
						NameTokens.ALT_YEAR2, EntityDAO.getCurrentTerm(ofy).getAcademicYear(), ofy).listKeys();
				*/
				/*
				QueryResultIterable<Key<Student>> tenantKeys = ofy.query(
						Student.class).fetchKeys();
				*/
				
				ArrayList<Key<Student>> studKeys = new ArrayList<Key<Student>>();
				ArrayList<Key<InetResidentAccount>> resKeys = new ArrayList<Key<InetResidentAccount>>();
				StringBuilder output = new StringBuilder();
				for(String email : CollegeAccounts.TESTING_STUDENTS)
				{
					Key<Student> s = Student.getKey(email.trim().toLowerCase());
					if(ofy.find(s) == null)
						output.append("<br/>unable to find: " + s);
					else
						output.append("<br/>adding " + s + " for student account creation");
					for(String r : CollegeAccounts.ACCT_TYPES.keySet())
					{
						Key<InetResidentAccount> ra = InetResidentAccount.getKey(s, r, "NGN");
						if(ofy.find(ra) == null)
							output.append("<br/>unable to find: " + ra);
						else
							output.append("<br/>adding " + ra + " for student account deletion");
						resKeys.add(ra);
					}
					studKeys.add(s); //for account creation
				}
				
				//now let's delete the keys
				ofy.delete(resKeys);
				output.append("deleted " + resKeys.size() + " account");
				/*for(String email : CollegeAccounts.NEW_STUDENTS)
				{
					Key<Student> s = Student.getKey(email.trim().toLowerCase());
					if(ofy.find(s) == null)
						output.append("<br/>unable to find: " + s);
					else
						output.append("<br/>adding " + s + " for student account creation");
					studKeys.add(s);
				}*/
				String[] template = new String[CollegeAccounts.ACCT_TYPES.keySet().size()];
				int count = 0;
				for (Key<Student> tk : studKeys) {
					count++;
					TaskQueueHelper.scheduleCreateResidentAccount(tk
							.getString(), CollegeAccounts.ACCT_TYPES
							.keySet().toArray(template), "admin-account");
				}
				out.println("scheduled creation of "
						+ CollegeAccounts.ACCT_TYPES.size()
						+ " accounts for " + count + " students");
				//out.println("<br/><br/>Other output: <br/>" + output.toString());
			} else if (type.equals("gtbacct")) {
				String usr = "admin-account";
				for (int i = 0; i < CollegeAccounts.ACCOUNT_INFO.length; i++) 
				{
					String[] info = CollegeAccounts.ACCOUNT_INFO[i];
					InetCompanyAccount a = InetDAO4Accounts.createAccount(
							info[CollegeAccounts.ACCT_BANK_IDX], info[CollegeAccounts.ACCT_SORT_IDX], 
							info[CollegeAccounts.ACCT_NAME_IDX], info[CollegeAccounts.ACCT_NUM_IDX], "NGN",
							info[CollegeAccounts.ACCT_TYPE_IDX], usr);
					out.println("Created: " + a.getKey());
				}
			}
			out.println("<b>setup done</b><br/>");
		} catch (DuplicateEntitiesException ex) {
			out.println("An error occured when creating objects: "
					+ ex.getMessage());
		}
	}

	private static String createParameter() throws DuplicateEntitiesException {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("sobulo@fertiletech.com",
				"added for support purposes during deployment of admin profiles");
		params.put("segun.sobulo@fcahptib.edu.ng",
				"added for support purposes during deployment of admin profiles");
		params.put("sodeeq-9jedu-support@fcahptib.edu.ng",
				"added for support purposes during deployment of admin profiles");
		String[] paramNames = { DTOConstants.APP_PARAM_ADMINS,
				DTOConstants.APP_PARAM_EDIT_COURSES,
				DTOConstants.APP_PARAM_EDIT_STUDENTS,
				DTOConstants.APP_PARAM_EDIT_REGISTRY,
				DTOConstants.APP_PARAM_EDIT_PAY_STATUS };
		// params.put("Current Residence", "");
		// params.put("Work Location", "");
		// params.put("Guarantor 1 Location", "");
		// params.put("Guarantor 2 Location", "");
		// String[] paramNames = {DTOConstants.APP_PARAM_MAP_TITLES};
		StringBuilder result = new StringBuilder();
		for (String name : paramNames) {
			ApplicationParameters paramObj = CommentsDAO.createParams(name,
					params);
			result.append("<p>Created: " + paramObj.getKey() + " Size: "
					+ paramObj.getParams().size() + "</p>");
		}
		/*
		 * params = new HashMap<String, String>(); String[] otherNames =
		 * {DTOConstants.APP_PARAM_MAILING_LIST}; for(String name : otherNames)
		 * { ApplicationParameters paramObj =
		 * EntityDAO.createApplicationParameters(name, params);
		 * result.append("<p>Created: " + paramObj.getKey()+ "</p>"); }
		 */
		return result.toString();
	}
}
