/**
 * 
 */
package com.fertiletech.nbte.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class ServiceImplUtilities {
	public static void logAndThrowDuplicateException(Logger log, String msgPrefix, Key<?> objectifyKey) throws DuplicateEntitiesException
	{
		String msg = msgPrefix + ". Attempt to create a duplicate object with Key: ";
		log.warning(msg + objectifyKey);
		throw new DuplicateEntitiesException(msg + objectifyKey.getName());
	}

	public static void logAndThrowMissingEntityException(Logger log, String msgPrefix, Key<?> objectifyKey) throws MissingEntitiesException
	{
		String msg = msgPrefix + ".  object not found - Key is: ";
		log.warning(msg + objectifyKey);
		throw new MissingEntitiesException(msg + objectifyKey.getName());
	}	
	
	public static void logUserUpdate(Logger log, Key<?> objectifyKey, String user)
	{
		log.warning(objectifyKey + " updated by: " + user);
	}
	
	public static void logSystemUpdate(Logger log, Key<?> objectifyKey, String msg)
	{
		log.warning(objectifyKey + " additional info: " + msg);
	}
	
	private static void addDifference(StringBuilder diffs, String oldVal, String fieldName, String newVal)
	{
		diffs.append("<li>").append(fieldName).append(" changed from <font color='red'>[").append(oldVal).append("]</font> to <font color='green'>[").append(newVal).append("]</font></li>");
	}

	private static boolean addDifference(StringBuilder diffs, HashMap<String, String> oldVals, HashMap<String, String> newVals, String keyPrefix)
	{
		boolean foundDiff = false;
		for(String key : newVals.keySet())
		{
			String ov = wrapNullValues(oldVals.get(key));
			String nv = wrapNullValues(newVals.get(key));
			if(!ov.equals(nv))
			{
				addDifference(diffs, ov, keyPrefix + key, nv);
				foundDiff = true;
			}
		}
		return foundDiff;
	}
	
	private static String wrapNullValues(String val)
	{
		if(val == null) return "";
		return val;
	}
	
	private static void addNewEntries(StringBuilder diffs, HashMap<String, String> map, String entryDescription)
	{
		for(String fieldName : map.keySet())
			diffs.append("<li>").append(fieldName).append(" <font color='green'>[").append(map.get(fieldName)).append("]</font> ").append(entryDescription).append("</li>");
	}
	
	public static void addNewEntries(StringBuilder diffs, HashMap<String, String> fields, ArrayList<HashMap<String, String>>[] tables)
	{
		addNewEntries(diffs, fields, " added");
		if(tables == null) return;
		for(int i = 0; i < tables.length; i++)
		{
			//TODO fix table name
			String tableName = "----";//NPMBFormConstants.TABLE_DESCRIPTIONS[i];
			diffs.append("<li>[Table: ").append(tableName).append("]</li>");
			if(tables[i].size() > 0)
			{
				for(int t = 0; t < tables[i].size(); t++)
					addNewEntries(diffs, tables[i].get(t), " added to [" + tableName + " Table - Row " + (i + 1)+ "]");
			}
		}
	}
	
	private static StringBuilder addDifference(String tableName, ArrayList<HashMap<String, String>> oldVals, ArrayList<HashMap<String, String>> newVals)
	{
		int max = Math.min(oldVals.size(), newVals.size());
		
		boolean foundDiff = false;
		StringBuilder result = new StringBuilder("<ul>");
		for(int i = 0; i < max; i++)
		{
			foundDiff = foundDiff || addDifference(result, oldVals.get(i), newVals.get(i), "Row " + (i + 1) + " - ");
		}
		if(newVals.size() > max)
		{
			String prefix = " added to [";
			foundDiff = true;
			for(int i = max; i < newVals.size(); i++)
			{
				String description = prefix + tableName + " - Row " + (i + 1) + "]";
				addNewEntries(result, newVals.get(i), description);
			}
		}
		if(oldVals.size() > max)
		{
			String prefix = " removed from [";
			foundDiff = true;
			for(int i = max; i < oldVals.size(); i++)
			{
				String description = prefix + tableName + "Row " + (i + 1) + "]";
				addNewEntries(result, oldVals.get(i), description);
			}
		}
		result.append("</ul>");
		if(foundDiff)
			return result;
		return new StringBuilder();
	}
	
	public static void addDifference(StringBuilder diffs, HashMap<String, String> oldVals, HashMap<String, String> newVals, ArrayList<HashMap<String, String>>[] oldTables, ArrayList<HashMap<String, String>>[] newTables)
	{
		addDifference(diffs, oldVals, newVals, "");
		ArrayList<HashMap<String, String>> emptyList = new ArrayList<HashMap<String,String>>();
		for(int i = 0; i < newTables.length; i++)
		{
			//TODO Fix Table Name 
			String tableName = "----"; //NPMBFormConstants.TABLE_DESCRIPTIONS[i];
			StringBuilder tableDiffs = addDifference(tableName, oldTables == null?emptyList:oldTables[i], newTables[i]);
			if(tableDiffs.length() > 0)
				diffs.append("<li>[").append(tableName).append("]").append(tableDiffs).append("</li>");
		}
	}
	
	  public static String stackTraceToString(Throwable throwable) {
		    String text = "";
		    if (throwable != null) {
		      while (throwable != null) {
		        String text1 = "";
		        /* Use throwable.toString() and not throwable.getClass().getName() and
		         * throwable.getMessage(), so that instances of UnwrappedClientThrowable, when stack trace
		         * deobfuscation is enabled) display properly
		         */
		        text1 += throwable.toString() + "\n";
		        StackTraceElement[] stackTraceElements = throwable.getStackTrace();
		        for (StackTraceElement element : stackTraceElements) {
		          text1 += "    at " + element + "\n";
		        }
		        text += text1;
		        throwable = throwable.getCause();
		        if (throwable != null) {
		          text += "Caused by: ";
		        }
		      }
		    }
		    return text;
		  }	
}