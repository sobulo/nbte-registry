/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server.college;


import java.util.HashMap;
import java.util.HashSet;

import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;


/**
 *
 * @author Segun Razaq Sobulo
 */
@Unindexed
@Cached
public class Student extends User{
    String matricNo;
    String jambNo;
    @Indexed 
    String year;
    @Indexed
    String department;
    @Indexed
    String diploma;
    @Indexed int academicYear; //use this to filter out alumni / drop outs
    @Serialized
    HashMap<String, String> bio;
    
    Student()
    {
        super();
        bio = new HashMap<String, String>();
    }

    Student(String fname, String lname, String email, String year, String department, String jambNo, String matricNo, String diploma, int academicYear)
    {
        super(fname, lname, email);
        this.department = department;
        this.jambNo = jambNo;
        this.matricNo = matricNo; 
        this.year = year;
        this.diploma = diploma;
        this.academicYear = academicYear;
        bio = new HashMap<String, String>();
    }
    
    public int getAcademicYear()
    {
    	return academicYear;
    }
    
    Student(HashMap<String, String> vals)
    {
    	super(vals);
    	setValues(vals);
    }


    @Override
    public Key<Student> getKey()
    {
        return getKey(key);
    }
    
    public static Key<Student> getKey(String email)
    {
    	return new Key<Student>(Student.class, email);
    }
    @Override
    public void setValues(HashMap<String, String> vals)
    {
    	super.setValues(vals);
    	if(vals.containsKey(ApplicationFormConstants.DEPARTMENT))
    		this.department = vals.get(ApplicationFormConstants.DEPARTMENT);
    	if(vals.containsKey(ApplicationFormConstants.DIPLOMA))
    		this.diploma = vals.get(ApplicationFormConstants.DIPLOMA);
    	if(vals.containsKey(ApplicationFormConstants.YEAR))
    		this.year = vals.get(ApplicationFormConstants.YEAR);
    	if(vals.containsKey(ApplicationFormConstants.MATRIC_NO))
    		this.matricNo = vals.get(ApplicationFormConstants.MATRIC_NO);
     	if(vals.containsKey(ApplicationFormConstants.JAMB_NO))
    		this.jambNo = vals.get(ApplicationFormConstants.JAMB_NO);
     	for(String key : getPrimaryKeys())
     		vals.remove(key);
     	bio = vals;
    }
    
    @Override
	public HashMap<String, String> getValues()
    {
    	HashMap<String, String> vals = super.getValues();
    	vals.put(ApplicationFormConstants.DEPARTMENT, department);
    	vals.put(ApplicationFormConstants.DIPLOMA, diploma);
    	vals.put(ApplicationFormConstants.YEAR, year);
    	vals.put(ApplicationFormConstants.MATRIC_NO, matricNo);
    	vals.put(ApplicationFormConstants.JAMB_NO, jambNo);
    	vals.putAll(bio);
    	return vals;
    }
    
    HashSet<String> getPrimaryKeys()
    {
    	HashSet<String> vals = new HashSet<String>();
    	vals.add(ApplicationFormConstants.FIRST_NAME);
    	vals.add(ApplicationFormConstants.SURNAME);
    	vals.add(ApplicationFormConstants.ALTERNATE_EMAIL);
    	vals.add(ApplicationFormConstants.PRIMARY_NO);
    	vals.add(ApplicationFormConstants.MOBILE_NO);
    	vals.add(ApplicationFormConstants.GENDER);
    	vals.add(ApplicationFormConstants.ADDRESS);
    	vals.add(ApplicationFormConstants.EMAIL);
    	vals.add(ApplicationFormConstants.DEPARTMENT);
    	vals.add(ApplicationFormConstants.DIPLOMA);
    	vals.add(ApplicationFormConstants.YEAR);
    	vals.add(ApplicationFormConstants.MATRIC_NO);
    	vals.add(ApplicationFormConstants.JAMB_NO);
    	vals.add(ApplicationFormConstants.STUD_ID);
    	return vals;
    }
}




