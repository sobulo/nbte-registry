package com.fertiletech.nbte.server.college;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
public class RosterEntry {

	@Id
	Long id;
	@Parent Key<StudentSession> studentSession;
	@Indexed Key<Course> course;	

	public Key<Student> getStudent() {
		return studentSession.getParent();
	}
	
	public Key<StudentSession> getStudentSession() {
		return studentSession;
	}

	public Key<Course> getCourse() {
		return course;
	}
	
	public Key<RosterEntry> getKey()
	{
		return new Key<RosterEntry>(studentSession, RosterEntry.class, id);
	}

	RosterEntry(){}
	
	RosterEntry(Key<StudentSession> studKey, Key<Course> courseKey)
	{
		this.studentSession = studKey;
		this.course = courseKey;
	}
}
