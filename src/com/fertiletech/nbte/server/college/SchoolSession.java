package com.fertiletech.nbte.server.college;

import javax.persistence.Id;

import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class SchoolSession implements HasAcademicSession{
	@Id
	String id;
	int academicYear;
	int semester;
	@Indexed boolean current;
	SchoolSession(){}
	SchoolSession(int academicYear, int semester, boolean current)
	{
		this.academicYear = academicYear;
		this.semester = semester;
		this.current = current;
		this.id = getID(academicYear, semester);
	}

	public int getAcademicYear()
	{
		return academicYear;
	}
	
	public int getSemester()
	{
		return semester;
	}
	public Key<SchoolSession> getKey()
	{
		return getKey(academicYear, semester);
	}	
	
	public static Key<SchoolSession> getKey(int academicYear, int semester)
	{
		return new Key<SchoolSession>(SchoolSession.class, 
				getID(academicYear, semester));		
	}
	
	public static String getID(int academicYear, int semester)
	{
		return NameTokens.getAcademicYearString(academicYear) + " - " + NameTokens.getSemester(semester);		
	}
	
	public TableMessage getDTO()
	{
		TableMessage m = new TableMessage(3,0,0);
		m.setText(RegistryDTO.SESS_SCHOOL_CURRENT_IDX,String.valueOf(current));
		m.setText(RegistryDTO.SESS_SCHOOL_ACADEMIC_YEAR_IDX, NameTokens.getAcademicYearString(academicYear));
		m.setText(RegistryDTO.SESS_SCHOOL_SEMESTER_IDX, NameTokens.getSemester(semester));
		return m;
	}
	
	public static TableMessageHeader getDTOHeader()
	{
		TableMessageHeader h = new TableMessageHeader(3);
		h.setText(RegistryDTO.SESS_SCHOOL_ACADEMIC_YEAR_IDX, "Academic Year", TableMessageContent.TEXT);
		h.setText(RegistryDTO.SESS_SCHOOL_SEMESTER_IDX, "Semester", TableMessageContent.TEXT);
		h.setText(RegistryDTO.SESS_SCHOOL_CURRENT_IDX, "Current Semester", TableMessageContent.TEXT);
		return h;
	}

	@Override
	public String getInstanceLabel() {
		return "FCAHPT IB";
	}
}
