/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server.college;


import java.util.HashMap;

import javax.persistence.Id;

import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Unindexed;

/**
 *
 * @author Segun Razaq Sobulo
 */
@Unindexed
public class User{
    @Id
    protected String key;

    String fname;
    
    String lname;

    String alternateEmail;

    String primaryNumber;
    String alternateNumber;
    String gender;
    String address;

    public User(){}

    User(String fname, String lname, String email)
    {
        this.fname = fname;
        this.lname = lname;
        key = email.toLowerCase();
        this.primaryNumber = primaryNumber;
    }
    
    User(HashMap<String, String> vals)
    {
    	this();
    	if(vals.containsKey(ApplicationFormConstants.EMAIL) )
    		this.key = vals.get(ApplicationFormConstants.EMAIL).toLowerCase();
    	setValues(vals);
    }

    @Override
    public String toString()
    {
        return fname + " " + lname;
    }

    public Key<? extends User> getKey() {
        return getKey(this.getClass(), key);
    }
    
    public static Key<? extends User> getKey(Class<? extends User> cls, String key) {
        return new Key(cls, key.toLowerCase().trim());
    }    

    public String getLoginEmail()
    {
        return key;
    }
    
    void setValues(HashMap<String, String> vals)
    {
    	if(vals.containsKey(ApplicationFormConstants.FIRST_NAME))
    		this.fname = vals.get(ApplicationFormConstants.FIRST_NAME);
    	if(vals.containsKey(ApplicationFormConstants.SURNAME))
    		this.lname = vals.get(ApplicationFormConstants.SURNAME);
    	if(vals.containsKey(ApplicationFormConstants.ALTERNATE_EMAIL))
    		this.alternateEmail = vals.get(ApplicationFormConstants.ALTERNATE_EMAIL);
    	if(vals.containsKey(ApplicationFormConstants.PRIMARY_NO))
    		this.primaryNumber = vals.get(ApplicationFormConstants.PRIMARY_NO);
    	if(vals.containsKey(ApplicationFormConstants.MOBILE_NO))
    		this.alternateNumber = vals.get(ApplicationFormConstants.MOBILE_NO);
     	if(vals.containsKey(ApplicationFormConstants.GENDER))
    		this.gender = vals.get(ApplicationFormConstants.GENDER);
    	if(vals.containsKey(ApplicationFormConstants.ADDRESS))
    		this.address = vals.get(ApplicationFormConstants.ADDRESS);
    }
    
    public HashMap<String, String> getValues()
    {
    	HashMap<String, String> vals = new HashMap<String, String>();
    	vals.put(ApplicationFormConstants.FIRST_NAME, fname);
    	vals.put(ApplicationFormConstants.SURNAME, lname);
    	vals.put(ApplicationFormConstants.ALTERNATE_EMAIL, alternateEmail);
    	vals.put(ApplicationFormConstants.PRIMARY_NO, primaryNumber);
    	vals.put(ApplicationFormConstants.MOBILE_NO, alternateNumber);
    	vals.put(ApplicationFormConstants.GENDER, gender);
    	vals.put(ApplicationFormConstants.ADDRESS, address);
    	vals.put(ApplicationFormConstants.EMAIL, key);
    	vals.put(ApplicationFormConstants.STUD_ID, getKey().getString());
    	return vals;
    }
}
