package com.fertiletech.nbte.server.college;

import java.util.HashMap;

import javax.persistence.Id;

import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class Course implements HasAcademicSession{
	@Id
	String id;
	@Indexed String courseCode;
	@Indexed String department;
	@Indexed int semester;
	@Indexed String year;
	int courseUnit;
	String courseName;
	@Indexed String diploma;
	String lectureNoteUrl;
	String descriptionUrl;
	Text features;
	String thumbUrl;
	String bannerUrl;
	Text scheduleDesc;
	@Indexed int academicYear;
	
	public Course() {
	}

	public Course(HashMap<String, String> vals) {
		courseCode = vals.get(ApplicationFormConstants.COURSE_CODE);
		setValues(vals);
	}	
	
	Course(String courseCode, String department, int semester,
			String year, int courseUnit, String courseName, String diploma,
			String lectureNoteUrl, String descriptionUrl, String features,
			String thumbUrl, String bannerUrl, int academicYear) {
		super();
		this.courseCode = courseCode;
		this.department = department;
		this.semester = semester;
		this.year = year;
		this.courseUnit = courseUnit;
		this.courseName = courseName;
		this.diploma = diploma;
		this.lectureNoteUrl = lectureNoteUrl;
		this.academicYear = academicYear;
		this.descriptionUrl = descriptionUrl;
		setFeature(features);
		this.thumbUrl = thumbUrl;
		this.bannerUrl = bannerUrl;
		this.id = getID(department, courseCode, academicYear);
	}
	
	public void setScheduleDesc(Text desc)
	{
		scheduleDesc = desc;
	}
	
	public String getScheduleDesc()
	{
		if(scheduleDesc == null)
			return null;
		return scheduleDesc.getValue();
	}
	
	public Key<Course> getKey()
	{
		return getKey(department, courseCode, academicYear);
	}
	
	public static Key<Course> getKey(String dept, String courseCode, int academicYear)
	{
		return new Key<Course>(Course.class, getID(dept, courseCode, academicYear));
	}
	
	public static String getID(String dept, String code, int academicYear)
	{
		return dept + "~" + code + "~" + academicYear; 
	}

    void setValues(HashMap<String, String> vals)
    {
    	if(vals.containsKey(ApplicationFormConstants.COURSE_NAME))
    		this.courseName = vals.get(ApplicationFormConstants.COURSE_NAME);
    	if(vals.containsKey(ApplicationFormConstants.DEPARTMENT))
    		this.department = vals.get(ApplicationFormConstants.DEPARTMENT);
    	if(vals.containsKey(ApplicationFormConstants.DIPLOMA))
    		this.diploma = vals.get(ApplicationFormConstants.DIPLOMA);
    	if(vals.containsKey(ApplicationFormConstants.YEAR))
    		this.year = vals.get(ApplicationFormConstants.YEAR);
    	if(vals.containsKey(ApplicationFormConstants.COURSE_UNITS))
    	{
    		String temp = vals.get(ApplicationFormConstants.COURSE_UNITS);
    		try
    		{
    			courseUnit = Integer.valueOf(courseUnit);
;    		}
    		catch(NumberFormatException ex){}
    	}
     	if(vals.containsKey(ApplicationFormConstants.LECTURE_NOTE))
    		this.lectureNoteUrl = vals.get(ApplicationFormConstants.LECTURE_NOTE);
    	if(vals.containsKey(ApplicationFormConstants.DESCR))
    		this.courseName = vals.get(ApplicationFormConstants.DESCR);
    	if(vals.containsKey(ApplicationFormConstants.FEATURES))
    		setFeature(vals.get(ApplicationFormConstants.FEATURES));
    	if(vals.containsKey(ApplicationFormConstants.SEMESTER))
    		this.semester = Integer.valueOf(vals.get(ApplicationFormConstants.SEMESTER));
    	if(vals.containsKey(ApplicationFormConstants.THUMB_URL))
    		this.thumbUrl = vals.get(ApplicationFormConstants.THUMB_URL);
    	if(vals.containsKey(ApplicationFormConstants.BANNER_URL))
    		this.bannerUrl = vals.get(ApplicationFormConstants.BANNER_URL);
    }
	
    void setFeature(String feat)
    {
    	if(feat == null) return;
    	features = new Text(feat);
    }
    
    HashMap<String, String> getValues()
    {
    	HashMap<String, String> vals = new HashMap<String, String>();
    	vals.put(ApplicationFormConstants.COURSE_NAME, courseName);
    	vals.put(ApplicationFormConstants.DEPARTMENT, department);
    	vals.put(ApplicationFormConstants.DIPLOMA, diploma);
    	vals.put(ApplicationFormConstants.YEAR, year);
    	vals.put(ApplicationFormConstants.COURSE_UNITS, String.valueOf(courseUnit));
    	vals.put(ApplicationFormConstants.LECTURE_NOTE, lectureNoteUrl);
    	vals.put(ApplicationFormConstants.DESCR, descriptionUrl);
    	vals.put(ApplicationFormConstants.FEATURES, getFeatures());
    	vals.put(ApplicationFormConstants.SEMESTER, String.valueOf(semester));
    	vals.put(ApplicationFormConstants.THUMB_URL, thumbUrl);
    	vals.put(ApplicationFormConstants.BANNER_URL, bannerUrl);
    	vals.put(ApplicationFormConstants.COURSE_CODE, courseCode);
    	return vals;
    }
    
    String getFeatures()
    {
    	if(features == null) return null;
    	return features.getValue();
    }
	public String getDepartment() {
		return department;
	}
	public int getSemester() {
		return semester;
	}
	public String getYear() {
		return year;
	}
	public int getCourseUnit() {
		return courseUnit;
	}
	public String getCourseName() {
		return courseName;
	}
	public String getDiploma() {
		return diploma;
	}
	public String getLectureNoteUrl() {
		return lectureNoteUrl;
	}
	public String getDescriptionUrl() {
		return descriptionUrl;
	}
	public String getThumbUrl() {
		return thumbUrl;
	}
	public String getBannerUrl() {
		return bannerUrl;
	}
	public String getCourseCode()
	{
		return courseCode;
	}

	@Override
	public int getAcademicYear() {
		return academicYear;
	}

	@Override
	public String getInstanceLabel() {
		return year + " " + courseCode;
	}
}
