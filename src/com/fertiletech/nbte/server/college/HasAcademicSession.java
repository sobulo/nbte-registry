package com.fertiletech.nbte.server.college;

public interface HasAcademicSession {
	int getAcademicYear();
	int getSemester();
	String getInstanceLabel();
}
