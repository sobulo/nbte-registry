package com.fertiletech.nbte.server.college;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.accounting.InetResidentAccount;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

public class Registrar {
	
	public static List<RosterEntry> registerCourses(Collection<Course> courses, Key<StudentSession> studKey, SchoolSession sch) throws MissingEntitiesException, DuplicateEntitiesException
	{
		ArrayList<RosterEntry> newRegistrations = new ArrayList<RosterEntry>();
		Objectify ofy = ObjectifyService.beginTransaction();		
		StringBuilder duplicateCourses = new StringBuilder();
		try
		{
			StudentSession studSession = ofy.get(studKey);
			String mismatch = getSessionMismatch(studSession, sch);
			if(mismatch.length() > 0)
				throw new MissingEntitiesException(mismatch);
			HashSet<Key<Course>> priorRegistrations = getRegisteredCourses(studKey, ofy);
			for(Course c : courses)
			{
				mismatch = getSessionMismatch(c, sch);
				if(mismatch.length() > 0)
					throw new MissingEntitiesException(mismatch);
				if(priorRegistrations.contains(c.getKey()))
					duplicateCourses.append(c.getKey().getName()).append("<br/>");
				else
				{
					RosterEntry re = new RosterEntry(studKey, c.getKey());
					newRegistrations.add(re);
				}
			}
			ofy.put(newRegistrations);
			ofy.getTxn().commit();
			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		if(duplicateCourses.length() > 0)
			throw new DuplicateEntitiesException(duplicateCourses.toString());
		
		return newRegistrations;
	}
	
	private static String getSessionMismatch(HasAcademicSession a, HasAcademicSession b)
	{
		if((a.getAcademicYear() != b.getAcademicYear()) || (a.getSemester() != b.getSemester()))
		{
			String aYear = NameTokens.getAcademicSessionSemesterString(a.getAcademicYear(), a.getSemester());
			String bYear = NameTokens.getAcademicSessionSemesterString(b.getAcademicYear(), b.getSemester());
			return  "MISMATCH: " + a.getInstanceLabel() + " has semester session " + aYear + " but " + 
					 b.getInstanceLabel() + " has semester session " + bYear;
		}
		return "";
	}
	
	public static HashSet<Key<Course>> getRegisteredCourses(Key<StudentSession> sess, Objectify ofy)
	{
		List<RosterEntry> entries = ofy.query(RosterEntry.class).ancestor(sess).list();
		HashSet<Key<Course>> keySet = new HashSet<Key<Course>>();
		for(RosterEntry e : entries)
			keySet.add(e.getCourse());
		return keySet;
	}
	
	private static Query<StudentSession> getStudentSessions(Key<Student> studKey, Objectify ofy)
	{
		return ofy.query(StudentSession.class).ancestor(studKey);
	}
	
	public static List<TableMessage> getStudentSessionsList(Key<Student> studKey, Objectify ofy)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		List<StudentSession> mySessList = getStudentSessions(studKey, ofy).list();
		result.add(StudentSession.getDTOHeader(false));
		HashSet<Key<StudentSession>> ssKeys = new HashSet<Key<StudentSession>>();
		for(StudentSession s : mySessList)
			ssKeys.add(s.getKey());
		HashMap<Key<StudentSession>, StringBuilder> errorMap = new HashMap<Key<StudentSession>, StringBuilder>();
		getOutstandingPaymentMessage(ssKeys, errorMap, ofy, false);
		for(StudentSession sess : mySessList)
		{
			TableMessage m = sess.getDTO(null, false);
			updateTableMessageStatus(errorMap, m, sess.getKey());
			result.add(m);
		}
		return result;
	}
	
	public static void updateTableMessageStatus(HashMap<Key<StudentSession>, StringBuilder> errorMap, TableMessage m, Key<StudentSession> ssk)
	{
		StringBuilder error = errorMap.get(ssk);
		if(error.length() > 0)
		{
			m.setText(RegistryDTO.STUD_SESS_STATUS_IDX, "Pending Payment Verification");
			m.setText(RegistryDTO.STUD_SESS_MESSAGE_IDX, error.toString());
		}
		else
			m.setText(RegistryDTO.STUD_SESS_STATUS_IDX, "Department Clearance Obtained");
		
	}
	
	
	public static List<TableMessage> getStudentSessionsList(Student stud, Objectify ofy)
	{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		List<StudentSession> mySessList = getStudentSessions(stud.getKey(), ofy).list();
		result.add(StudentSession.getDTOHeader(true));
		for(StudentSession sess : mySessList)
			result.add(sess.getDTO(stud, true));
		return result;
	}
	
	public static HashMap<Key<StudentSession>, Double> getOutstandingPaymentMessage(
			HashSet<Key<StudentSession>> selectedStudents, HashMap<Key<StudentSession>, 
			StringBuilder> errorMap, Objectify ofy, boolean includeSignature)
	{
		//TODO needs a date check! otherwise will not work for returning students
		//suggest using a combination of txn count and last modified
		Collection<String> billDepositTypes = InetDAO4Accounts.getBillTypes().values();
		HashSet<String> depositTypes = new HashSet<String>();
		for(String s : billDepositTypes)
			depositTypes.add(s);
		
		HashSet<Key<InetResidentAccount>> studAccounts = new HashSet<Key<InetResidentAccount>>();
		for(Key<StudentSession> ssk : selectedStudents)
		{
			Key<Student> sk = ssk.getParent();
			for(String type : depositTypes)
				studAccounts.add(InetResidentAccount.getKey(sk, type, "NGN"));
		}
		
		Map<Key<InetResidentAccount>, InetResidentAccount> studentAccountObjects = ofy.get(studAccounts);
		HashMap<Key<StudentSession>, Double> studentPayments = new HashMap<Key<StudentSession>, Double>();
		for(Key<StudentSession> ssk : selectedStudents)
		{
			Key<Student> sk = ssk.getParent();
			double accountBalance = 0;
			StringBuilder errors = new StringBuilder();
			errorMap.put(ssk, errors);
			for(String type : depositTypes)
			{
				Key<InetResidentAccount> accKey = InetResidentAccount.getKey(sk, type, "NGN");
				InetResidentAccount studentAccount = studentAccountObjects.get(accKey);
				if(studentAccount == null)
				{
					//studentPayments.put(sk, null); //umm why set to null when it is null already 
					/*TODO is this better than reporting an 
					 * inaccurate figure. Confirm this is the case vs there's a
					 *  usecase where it's useful to report total of ok accounts
					 */
					errors.append("<li>").append("Unable to verify payments for ")
						.append(sk.getName()).append(" as no fee records found for ")
						.append(type).append(". send debug info: ").append(accKey.toString())
						.append(" to ").append(NameTokens.HELP_ADDRESS).append("</li>");
					break;
				}
				int txnCount = studentAccount.getNumberOfTxns();
				if(txnCount == 0)
				{
					//studentPayments.put(sk, null); 
					errors.append("<li>").append("Unable to verify payments ");
						errors.append(sk.getName()).append(" as no fees have been charged for ")
						.append(type).append(" fees</li>");
					break;					
				}
				double total = studentAccount.getCurrentAmount();
				if(total < -0.01)
				{
					errors.append("<li>").append("There is an outanding balance of ")
					.append(EntityConstants.NUMBER_FORMAT.format(total)).append(" Naira for ").append(type);
					if(includeSignature)
						errors.append(" that is yet to be paid by ").append(sk.getName());
					errors.append("</li>");					
				}
					
				accountBalance = accountBalance + total;
			}
			if(errors.length() == 0)
				studentPayments.put(ssk, accountBalance);
		}
		return studentPayments;
	}
}
