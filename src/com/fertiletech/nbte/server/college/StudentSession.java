package com.fertiletech.nbte.server.college;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.StudentSessionStatus;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

@Cached
@Unindexed
public class StudentSession implements HasAcademicSession{
	@Id
	String id;
	int academicYear;
	int semester;
	String diploma;
	String department;
	@Parent Key<Student> studentKey;
	StudentSessionStatus status;
	Text statusMessage;
	@Indexed Date lastModified;
	StudentSession(){}
	StudentSession(Key<Student> studentKey, int academicYear, int semester, String diploma, String department)
	{
		this.academicYear = academicYear;
		this.semester = semester;
		this.diploma = diploma;
		this.department = department;
		this.studentKey = studentKey;
		id = getID(academicYear, semester);
		this.status = StudentSessionStatus.PENDING_PAYMENT_VERIFICATION;
	}
	
	public String getStatusMessage()
	{
		if(statusMessage == null) return "";
		return statusMessage.getValue();
	}

	public Key<StudentSession> getKey()
	{
		return getKey(studentKey, academicYear, semester);
	}	
	
	public static Key<StudentSession> getKey(Key<Student> studentKey, int academicYear, int semester)
	{
		return new Key<StudentSession>(studentKey, StudentSession.class, 
				getID(academicYear, semester));		
	}
	
	public static String getID(int academicYear, int semester)
	{
		return NameTokens.getAcademicYearString(academicYear) + " - " + NameTokens.getSemester(semester);		
	}
	
	public TableMessage getDTO(Student stud, boolean includeBio)
	{
		TableMessage m = null;
		if(includeBio)
		{
			if(!stud.getKey().equals(studentKey))
				throw new IllegalArgumentException("Mismatch on " + department + " " + diploma + ": " +
							studentKey + " vs. " + stud.getKey());
			m = new TableMessage(10,0,1);
			m.setText(RegistryDTO.STUD_SESS_SURNAME_IDX, stud.lname);
			m.setText(RegistryDTO.STUD_SESS_OTHER_NAMES_IDX, stud.fname);
			m.setText(RegistryDTO.STUD_SESS_MATRIC_IDX, getMatricRepNo(stud.matricNo, stud.jambNo));
			m.setText(RegistryDTO.STUD_SESS_YEAR_IDX, stud.year);
			m.setDate(RegistryDTO.STUD_SESS_DATE_IDX, lastModified);			
		}
		else
			m = new TableMessage(6, 0, 0);
		
		m.setText(RegistryDTO.STUD_SESS_DIPL_IDX,diploma);
		m.setText(RegistryDTO.STUD_SESS_DEPT_IDX,department);
		m.setText(RegistryDTO.STUD_SESS_ACADEMIC_YEAR_IDX, NameTokens.getAcademicYearString(academicYear));
		m.setText(RegistryDTO.STUD_SESS_SEMESTER_IDX, NameTokens.getSemester(semester));
		m.setText(RegistryDTO.STUD_SESS_STATUS_IDX, status.getDisplayString());
		m.setText(RegistryDTO.STUD_SESS_MESSAGE_IDX, getStatusMessage());
		m.setMessageId(getKey().getString());
		
		return m;
	}
	
	String getMatricRepNo(String matricNo, String jambNo)
	{
		if(matricNo == null || matricNo.trim().length() == 0)
		{
			if(jambNo == null || jambNo.trim().length() == 0)
				return "";
			else
				return "JAMB:" + jambNo;
		}
		else
			return matricNo;
	}
	
	public static TableMessageHeader getDTOHeader(boolean includeBio)
	{
		TableMessageHeader h = new TableMessageHeader(includeBio?11:5);
		h.setText(RegistryDTO.STUD_SESS_ACADEMIC_YEAR_IDX, "Academic Year", TableMessageContent.TEXT);
		h.setText(RegistryDTO.STUD_SESS_SEMESTER_IDX, "Semester", TableMessageContent.TEXT);
		h.setText(RegistryDTO.STUD_SESS_DIPL_IDX, "Diploma", TableMessageContent.TEXT);
		h.setText(RegistryDTO.STUD_SESS_DEPT_IDX, "Department", TableMessageContent.TEXT);
		h.setText(RegistryDTO.STUD_SESS_STATUS_IDX, "Status", TableMessageContent.TEXT);
		
		if(includeBio)
		{
			h.setText(RegistryDTO.STUD_SESS_MESSAGE_IDX, "Message", TableMessageContent.TEXT);
			h.setText(RegistryDTO.STUD_SESS_OTHER_NAMES_IDX + 1, "Other Names", TableMessageContent.TEXT);
			h.setText(RegistryDTO.STUD_SESS_SURNAME_IDX + 1, "Surname", TableMessageContent.TEXT);
			h.setText(RegistryDTO.STUD_SESS_YEAR_IDX + 1, "Year", TableMessageContent.TEXT);
			h.setText(RegistryDTO.STUD_SESS_MATRIC_IDX + 1, "Matric No.", TableMessageContent.TEXT);
			h.setText(RegistryDTO.STUD_SESS_MATRIC_IDX + 2, "Timestamp", TableMessageContent.DATE);
		}
		
		return h;
	}
	
	@PrePersist
	void modifyTimeStamp()
	{
		lastModified = new Date();
	}
	@Override
	public int getAcademicYear() {
		return academicYear;
	}
	@Override
	public int getSemester() {
		return semester;
	}
	@Override
	public String getInstanceLabel() {
		return studentKey.getName();
	}
}
