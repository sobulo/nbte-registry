package com.fertiletech.nbte.server.college;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.comments.MortgageAttachment;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.DrivePickerUtils;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.RegistryDTO;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

public class EntityDAO {
	private static final Logger log =
	        Logger.getLogger(EntityDAO.class.getName());	
	
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}
	
	public static SchoolSession createSchoolSession(int academicYear, int semester, boolean current) throws DuplicateEntitiesException
	{
		SchoolSession newTerm = new SchoolSession(academicYear, semester, current);
		Key<SchoolSession> termKey = newTerm.getKey();
		if(current)
		{
			SchoolSession currentTerm = getCurrentTerm();
			if(currentTerm != null)
				throw new DuplicateEntitiesException("There's already a session/semester " +
						"combo marked as current: " + NameTokens.getAcademicSessionSemesterString(currentTerm.academicYear, currentTerm.semester));
		}
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			SchoolSession term = ofy.find(termKey);
			if(term != null)
				throw new DuplicateEntitiesException("Session/term info exists already for " + 
						NameTokens.getAcademicSessionSemesterString(academicYear, semester));
			ofy.put(newTerm);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return newTerm;
	}
	
	public static SchoolSession getCurrentTerm()
	{
		return getCurrentTerm(ObjectifyService.begin());
	}
	
	public static SchoolSession getCurrentTerm(Objectify ofy)
	{
		List<SchoolSession> termList = ofy.query(SchoolSession.class).filter("current", true).list();
		if(termList.size() == 0)
			return null;
		if(termList.size() > 1)
		{
			StringBuilder msg = new StringBuilder();
			for(SchoolSession s : termList)
				msg.append(NameTokens.getAcademicSessionSemesterString(s.academicYear, s.semester)).append(". ");
			throw new IllegalStateException("Found more than one term/semester marked as current. " + 
				msg.toString());
		}
		return termList.get(0);
	}
	
	public static Student createStudent(String fname, String lname, String email, 
			String year, String department, String jambNo, String matricNo, String diploma, 
			SchoolSession currentTerm) throws DuplicateEntitiesException
	{
		Student stud = new Student(fname, lname, email, year, department, jambNo, matricNo, diploma, currentTerm.academicYear);
		Key<Student> studKey = (Key<Student>) Student.getKey(Student.class, email);
		
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Student existingStud = ofy.find(studKey);
			if(existingStud != null)
				throw new DuplicateEntitiesException(existingStud.lname + ", " + existingStud.fname +
						"(" + existingStud.matricNo+ ") already exists for email address " + email);
			StudentSession studSess = new StudentSession(studKey, currentTerm.academicYear, currentTerm.semester, diploma, department);
			ofy.put(stud, studSess);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return stud;
	}	
	
	public static Course createCourseTemplate(String courseCode, String department, 
			String year, int courseUnit, String courseName, String diploma,
			String lectureNoteUrl, String descriptionUrl, String features,
			String thumbUrl, String bannerUrl, int semester, int academicYear) throws DuplicateEntitiesException
	{
		Course template = new Course(courseCode, department, semester, year, courseUnit,
				courseName, diploma, lectureNoteUrl, descriptionUrl, features, thumbUrl, bannerUrl, academicYear);
		Key<Course> tk = Course.getKey(department, courseCode, academicYear);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Course existingTemplate = ofy.find(tk);
			if(existingTemplate != null)
				throw new DuplicateEntitiesException(existingTemplate.courseName + " already exist for course code " + courseCode);				
			ofy.put(template);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return template;
	}

	public static Query<Student> queryDepartmentRoster(String dept, String diploma, String year, Integer academicYear, Objectify ofy)
	{
		
		Query<Student> query = ofy.query(Student.class);
		if(dept != null)
			query.filter("department", dept);
		if(diploma != null)
			query.filter("diploma", diploma);
		if(year != null)
			query.filter("year", year);
		if(academicYear != null)
			query.filter("academicYear", academicYear);
		return query;
	}
	
	public static List<Student> getDepartmentRoster(String dept, String diploma, String year, int academicYear, Objectify ofy)
	{
		List<Student> students = queryDepartmentRoster(dept, diploma, year, academicYear, ofy).list();
		return students;
	}
	
	public static Query<Course> queryDepartmentCourses(String dept, String diploma, String year, int semester, int academicYear, Objectify ofy)
	{
		 return ofy.query(Course.class).filter("department", dept).filter("year", year).
				filter("semester", semester).filter("diploma", diploma).filter("academicYear", academicYear);
	}

	public static List<Course> getDepartmentCourseTemplates(String dept, String diploma, String year, int semester, int academicYear, Objectify ofy)
	{		
		List<Course> templates = queryDepartmentCourses(dept, diploma, year, semester, academicYear, ofy).list();
		return templates;
	}
	
	public static String getAcademicYearDisplay(int year)
	{
		return (year-1) + "/" + year;
	}
	
	public static TableMessage getStudentDTO(Student stud)
	{
		TableMessage m = new TableMessage(9, 0, 0);
		m.setText(RegistryDTO.LAST_NAME_IDX, stud.lname);
		m.setText(RegistryDTO.FIRST_NAME_IDX, stud.fname);
		m.setText(RegistryDTO.STUD_DEPT_IDX, stud.department);
		m.setText(RegistryDTO.STUD_YEAR_IDX, stud.year);
		m.setText(RegistryDTO.STUD_DIPL_IDX, stud.diploma);
		m.setText(RegistryDTO.MATRIC_NO_IDX, stud.matricNo);
		m.setText(RegistryDTO.ACADEMIC_YEAR, getAcademicYearDisplay(stud.academicYear));
		m.setText(RegistryDTO.JAMB_NO_IDX, stud.jambNo);
		m.setText(RegistryDTO.EMAIL_IDX, stud.key);
		m.setMessageId(stud.getKey().getString());
		return m;
	}
	
	public static TableMessage getCourseTemplateDTO(Course template)
	{
		TableMessage m = new TableMessage(13, 1, 0);
		m.setText(RegistryDTO.COURSE_CODE_IDX, template.courseCode);
		m.setText(RegistryDTO.COURSE_NAME_IDX, template.courseName);
		m.setText(RegistryDTO.COURSE_DEPT_IDX, template.department);
		m.setText(RegistryDTO.COURSE_YEAR_IDX, template.year);
		m.setText(RegistryDTO.COURSE_DIPLOMA_IDX, template.diploma);
		m.setText(RegistryDTO.COURSE_SEMESTER_IDX, NameTokens.getSemester(template.semester));
		m.setText(RegistryDTO.COURSE_ACADEMIC_YEAR_IDX, NameTokens.getAcademicYearString(template.academicYear));
		m.setText(RegistryDTO.COURSE_FEATURE_IDX, template.getFeatures());
		m.setText(RegistryDTO.COURSE_DESC_IDX, template.descriptionUrl);
		m.setText(RegistryDTO.COURSE_NOTES_IDX, template.lectureNoteUrl);
		m.setText(RegistryDTO.COURSE_THUMB_IDX, template.thumbUrl);
		m.setText(RegistryDTO.COURSE_BANN_IDX, template.bannerUrl);
		m.setText(RegistryDTO.COURSE_SCHEDULE_IDX, template.getScheduleDesc());
		m.setNumber(RegistryDTO.COURSE_UNIT_IDX, template.courseUnit);
		m.setMessageId(template.getKey().getString());
		return m;
		
	}

	
	public static TableMessageHeader getStudentDTOHeader()
	{
		TableMessageHeader h = new TableMessageHeader(9);
		h.setText(0, "Surname", TableMessageContent.TEXT);
		h.setText(1, "Other Names", TableMessageContent.TEXT);
		h.setText(2, "Department", TableMessageContent.TEXT);
		h.setText(3, "Year", TableMessageContent.TEXT);
		h.setText(4, "Diploma", TableMessageContent.TEXT);
		h.setText(5, "Matric No", TableMessageContent.TEXT);
		h.setText(6, "Jamb No", TableMessageContent.TEXT);
		h.setText(7, "Session", TableMessageContent.TEXT);
		h.setText(8, "Email", TableMessageContent.TEXT);
		return h;
	}
		
	public static TableMessageHeader getCourseDTOHeader()
	{
		TableMessageHeader h = new TableMessageHeader(8);
		h.setText(0, "Code", TableMessageContent.TEXT);
		h.setText(1, "Title", TableMessageContent.TEXT);
		h.setText(2, "Units", TableMessageContent.NUMBER);
		h.setFormatOption(2, "###");
		h.setText(3, "Department", TableMessageContent.TEXT);
		h.setText(4, "Diploma", TableMessageContent.TEXT);
		h.setText(5, "Year", TableMessageContent.TEXT);
		h.setText(6, "Semester", TableMessageContent.TEXT);
		h.setText(7, "Session", TableMessageContent.TEXT);
		
		return h;
	}
		
	public static List<MortgageAttachment> createAttachment(Key<Student> leadKey, List<TableMessage> attachList, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		List<MortgageAttachment> attachments = new ArrayList<MortgageAttachment>();
		try
		{
			boolean first = true;
			for(TableMessage m : attachList)
			{
				if(first) { first = false; continue; } //skip header
				MortgageAttachment attach = new MortgageAttachment(leadKey, m);
				attachments.add(attach);
			}
			ofy.put(attachments);
			String[] comment = {DrivePickerUtils.getAttachmentDescription(attachList)};
			TaskQueueHelper.scheduleCreateComment(comment, leadKey.getString(), user);
			ofy.getTxn().commit();			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return attachments;
	}
	
	public static void removeAttachment(Key<MortgageAttachment> mtgAttachment, String user)
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			MortgageAttachment attachment = ofy.get(mtgAttachment);
			String[] comments = {DrivePickerUtils.getAttachmentDescription(attachment.getDTO(), "REMOVED ATTACHMENT")};
			ofy.delete(mtgAttachment);
			TaskQueueHelper.scheduleCreateComment(comments, mtgAttachment.getParent().getString(), user);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}
	
	private static TableMessageHeader getAttachmentHeader(Key<Student> leadKey)
	{
		TableMessageHeader h = new TableMessageHeader(5);
		h.setText(0, "Title", TableMessageContent.TEXT);
		h.setText(1, "Type", TableMessageContent.TEXT);
		h.setText(2, "Drive Date", TableMessageContent.DATE);
		h.setText(3, "Attach Date", TableMessageContent.DATE);
		h.setText(4, "User", TableMessageContent.TEXT);
		h.setMessageId(leadKey.getString());
		h.setCaption("Attachments for M.A.P. ID: " + leadKey.getId());
		return h;
	}
	
	public static List<TableMessage> getAttachmentsDTO(Key<Student> leadKey)
	{
		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>();
		attachments.add(getAttachmentHeader(leadKey));
		for(MortgageAttachment a : getMortgageAttachments(leadKey))
			attachments.add(a.getDTO());
		return attachments;
	}
	
	private static List<MortgageAttachment> getMortgageAttachments(Key<Student> leadKey)
	{
		return ObjectifyService.begin().query(MortgageAttachment.class).ancestor(leadKey).order("-attachDate").list();
	}
	
	public static List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		List<MortgageAttachment> mapList = ObjectifyService.begin().query(MortgageAttachment.class).
				filter("type", "MAPS").filter("attachDate >=", startDate).
				filter("attachDate <=", endDate).order("-attachDate").limit(400).list();
		ArrayList<TableMessage> attachments = new ArrayList<TableMessage>(mapList.size());
		HashSet<Key<Student>> applicationKeys = new HashSet<Key<Student>>();
		for(MortgageAttachment a : mapList)
			applicationKeys.add(a.getLeadKey());
		Map<Key<Student>, Student> applicaions = ObjectifyService.begin().get(applicationKeys);		
		for(MortgageAttachment a : mapList)
		{
			Student lead = applicaions.get(a.getLeadKey());
			String attachDate = a.getAttachDate()==null? "" : String.format("%tc", a.getAttachDate());
			StringBuilder desc = new StringBuilder();
			String loanAmount = "";
			String salary = "";
			desc.append("<div style='width:180px;height:100px;color:#7b04a2;font-size:smaller'>").append("<a href='#doStatusCW/").
			append(a.getKey().getParent().getString()).append("'>View ID# ").
			append(String.valueOf(a.getKey().getParent().getId())).append("</a>").
			append("<br/>Annual Income: [").append(salary).append("]<br/>Loan Amount: [").append(loanAmount).
			append("]<br/>State: [").append("").
			append("]<br/>Company: [").append("").
			append("]<br/>Date: [").append(attachDate).
			append("]</div>");
			
			TableMessage m = new TableMessage(2,2,0);
			m.setText(DTOConstants.CATEGORY_IDX, a.getTitle());
			m.setText(DTOConstants.DETAIL_IDX, desc.toString());
			m.setNumber(DTOConstants.LAT_IDX, a.getLatitude());
			m.setNumber(DTOConstants.LNG_IDX, a.getLongitude());
			m.setMessageId(a.getKey().getParent().getString());
			attachments.add(m);
		}
		return attachments;
	}
	
	public static Collection<Student> getAllStudents(boolean currentlyEnrolled, String diploma, String dept, String year, Objectify ofy)
	{
		SchoolSession sess = getCurrentTerm(ofy);
		Query<Student> query = queryDepartmentRoster(dept, diploma, year, sess.academicYear, ofy);
		
		if(currentlyEnrolled)
			return query.list();
		
		List<Key<Student>> enrolledStudents = query.listKeys();
		List<Key<Student>> allStudKeys = queryDepartmentRoster(dept, diploma, year, null, ofy).listKeys();
		HashSet<Key<Student>> enrolledSet = new HashSet<Key<Student>>();
		for(Key<Student> s : enrolledStudents)
			enrolledSet.add(s);
		List<Key<Student>> nonEnrolledKeys = new ArrayList<Key<Student>>();
		for(Key<Student> s : allStudKeys)
			if(!enrolledSet.contains(s))
				nonEnrolledKeys.add(s);
		return ofy.get(nonEnrolledKeys).values();	
	}
	
	public static HashMap<String, String> getUserKeyNameMap(
			Collection<? extends User> users) {
		HashMap<String, String> result = new HashMap<String, String>();
		for (User c : users)
		{
			String location = "";
			HashMap<String, String> vals = c.getValues();
			if(c instanceof Student)
			{
				String department = vals.get(ApplicationFormConstants.DEPARTMENT);
				String deptCode = NameTokens.getNameTargetMap().get(department).toUpperCase();
				String matric = vals.get(ApplicationFormConstants.MATRIC_NO);
				if(matric == null)
					matric = vals.get(ApplicationFormConstants.JAMB_NO);
				if(matric == null)
					matric = "";
				else
					matric = " - " + matric;
				location = " [" + deptCode + matric + "]";
			}
			String lastName = vals.get(ApplicationFormConstants.SURNAME);
			String firstName = vals.get(ApplicationFormConstants.FIRST_NAME);
			result.put(
					c.getKey().getString(),
					lastName + " " + firstName + location + " {"
							+ c.getKey().getName() + "}");
		}
		return result;
	}	
}
