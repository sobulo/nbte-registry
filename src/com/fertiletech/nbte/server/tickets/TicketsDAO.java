package com.fertiletech.nbte.server.tickets;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Query;

public class TicketsDAO {
	private static final Logger log = Logger
			.getLogger(TicketsDAO.class.getName());
		
	public static boolean editTicketAndLogChanges(TableMessageHeader diffMetaHead, TableMessage diffMetaPos, TableMessage diffVals, RequestTicket t, StringBuilder commentBuffer)
	{
		int textCounter = 0; int dateCounter = 0;
		TableMessage oldVals = getTicketTableMessage(t);
		boolean siteChanged = false;
		for(int i = 0; i < diffMetaHead.getNumberOfTextFields(); i++)
		{
			int fieldToChange = (int) Math.round(diffMetaPos.getNumber(i));
			String fieldName = diffMetaHead.getText(i); //e.g. DETAILS
			TableMessageContent type = diffMetaHead.getHeaderType(i);
			String oldVal, newVal;
			oldVal = newVal = null;
			if(type.equals(TableMessageContent.DATE))
			{
				Date reqDate = diffVals.getDate(dateCounter++);
				newVal = newVal == null ? null : reqDate.toString();
				oldVal = oldVal == null ? null : oldVals.getDate(fieldToChange).toString();
				addDifference(commentBuffer, oldVal, fieldName, newVal);
				t.setRequestDate(reqDate);
				continue; //ensure this date indices don't clash with text indices
			}
			else if(type.equals(TableMessageContent.TEXT))
			{
				newVal = diffVals.getText(textCounter++);
				oldVal = oldVals.getText(fieldToChange);
				addDifference(commentBuffer, oldVal, fieldName, newVal);
			}
			else
			{
				//sanity check. can replace this with code similar to above if in the future ther's a need to diff numbers
				throw new RuntimeException("Number diffs not currently supported for ticket diffs");
			}
			
			switch (fieldToChange) {
			case DTOConstants.MRS_DETAILS_IDX:
				t.setDetails(new Text(newVal));
				break;
			case DTOConstants.MRS_ISSUE_IDX:
				t.setIssue(newVal);
				break;
			case DTOConstants.MRS_PRIORITY_IDX:
				t.setPriority(newVal);
				break;
			case DTOConstants.MRS_SITE_IDX:
				Key<? extends Course> newSite = ObjectifyService.factory().stringToKey(newVal);
				t.setSite(newSite);	
				siteChanged = true;
				break;
			case DTOConstants.MRS_STAFF_IDX:
				t.setAssignedUser(newVal);
				break;
			case DTOConstants.MRS_STATUS_IDX:
				t.setStatus(newVal);
				break;
			case DTOConstants.MRS_TITLE_IDX:
				t.setTitle(newVal);
				break;
			case DTOConstants.MRS_SITE_NAME_IDX: //display purposes only
				break;
			default:
				throw new RuntimeException("Unrecognized edit index: " + fieldToChange + " with old val: " + oldVal + " and new val: " + newVal);
			}
		}
		return siteChanged;
	}
	
	public static RequestTicket createTicket(Key<? extends Course> site, String assignedUser,
			String priority, Date requestDate, String status,
			String title, String detailStr, String issue, String loggedInUser)
	{
		log.warning("Received site: " + site);
		Text details = detailStr == null ? null : new Text(detailStr);
		RequestTicket ticket = new RequestTicket(site, assignedUser, priority, requestDate, status, title,
				details, issue);			
		Objectify ofy = ObjectifyService.beginTransaction();
		
		try
		{
			ofy.put(ticket);
			String[] comment = {"Created " + ticket.getPriority() + " priority ticket: " + ticket.getTicketNumber() + " regarding <b>" + ticket.getTitle() + "</b>. "};
			TaskQueueHelper.scheduleCreateComment(comment, ticket.getKey().getString(), loggedInUser);
			ofy.getTxn().commit();
			//creates comment and commits transaction
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return ticket;
	}
	
	private static void addDifference(StringBuilder diffs, String oldVal, String fieldName, String newVal)
	{
		diffs.append("<li>").append(fieldName).append(" changed from [").append(oldVal).append("] to [").append(newVal).append("]</li>");
	}
	
	public static RequestTicket editTicket(TableMessage[] edits, String loggedInUser)
	{		
		Objectify ofy = ObjectifyService.beginTransaction();
		Key<RequestTicket> tKey = ofy.getFactory().stringToKey(edits[0].getMessageId());
		RequestTicket t = null;
		try
		{
			t = ofy.get(tKey);
			StringBuilder diffs = new StringBuilder("Updated maintenance request ticket: " + t.getTicketNumber() + ". Modified fields: <br/><ul>");
			editTicketAndLogChanges((TableMessageHeader) edits[DTOConstants.TM_DIFF_HEADER], edits[DTOConstants.TM_DIFF_POS],
					edits[DTOConstants.TM_DIFF_VALS], t, diffs);
			diffs.append("</ul>");
			ofy.put(t);
			//create comment and commit transaction
			String[] comment = {diffs.toString()};
			TaskQueueHelper.scheduleCreateComment(comment, t.getKey().getString(), loggedInUser);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return t;
	}
	
	public static List<RequestTicket> getTickets(Objectify ofy, boolean byCreation, Date startDate, Date endDate, Key<Course> siteKey)
	{
		log.warning("Start " + startDate + " End " + endDate + " siteKey " + siteKey);
		Query<RequestTicket> q = ofy.query(RequestTicket.class);
		String dateFilter = "createDate";
		if(byCreation)
			dateFilter = "createDate";
		if(startDate != null)
			q = q.filter(dateFilter +" >=", startDate);
		if(endDate != null)
			q = q.filter(dateFilter +" <=", endDate);
		if(siteKey != null)
			q = q.filter("site =", siteKey);
		
		return q.list();
	}
	
	public static TableMessage getTicketTableMessage(RequestTicket t)
	{
		TableMessage result = new TableMessage(8, 1, 2);
		//number fields
		result.setNumber(DTOConstants.MRS_TICKET_IDX, t.getTicketNumber());
		//text fields
		result.setText(DTOConstants.MRS_DETAILS_IDX, t.getDetails().getValue());
		result.setText(DTOConstants.MRS_ISSUE_IDX, t.getIssue().toString());
		result.setText(DTOConstants.MRS_PRIORITY_IDX, t.getPriority().toString());
		result.setText(DTOConstants.MRS_SITE_IDX, t.getSite().getString());
		result.setText(DTOConstants.MRS_STAFF_IDX, t.getAssignedUser());
		result.setText(DTOConstants.MRS_TITLE_IDX, t.getTitle());
		result.setText(DTOConstants.MRS_STATUS_IDX, t.getStatus());
		result.setText(DTOConstants.MRS_SITE_NAME_IDX, t.getSite().toString());
		//date fields
		result.setDate(DTOConstants.MRS_CREATE_IDX, t.getCreateDate());
		result.setDate(DTOConstants.MRS_REQUEST_IDX, t.getRequestDate());
		result.setMessageId(t.getKey().getString());
		return result;
	}
}
