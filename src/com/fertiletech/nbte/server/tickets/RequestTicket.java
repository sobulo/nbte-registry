package com.fertiletech.nbte.server.tickets;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;

import com.fertiletech.nbte.server.college.Course;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

@Unindexed
@Cached
public class RequestTicket implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	Long ticketNumber;
	@Indexed Key<? extends Course> site;
	String assignedUser;
	String priority;
	@Indexed Date createDate;
	@Indexed Date requestDate;
	String status;
	String title;
	Text details;
	String issue;
	
	RequestTicket() {}

	RequestTicket(Key<? extends Course> site, String assignedUser,
			String priority, Date requestDate, String status,
			String title, Text details, String issue) {
		this.site = site;
		this.assignedUser = assignedUser;
		this.priority = priority;
		this.requestDate = requestDate;
		this.status = status;
		this.title = title;
		this.details = details;
		this.issue = issue;
		createDate = new Date();
	}

	public Key<RequestTicket> getKey()
	{
		return getKey(ticketNumber);
	}
	
	public static Key<RequestTicket> getKey(long ticketNumber)
	{
		return new Key<RequestTicket>(RequestTicket.class, ticketNumber);
	}
	
	public Long getTicketNumber() {
		return ticketNumber;
	}

	public Key<? extends Course> getSite() {
		return site;
	}

	public String getAssignedUser() {
		return assignedUser;
	}

	public String getPriority() {
		return priority;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public String getStatus() {
		return status;
	}

	public String getTitle() {
		return title;
	}

	public Text getDetails() {
		return details;
	}

	public String getIssue() {
		return issue;
	}

	public void setSite(Key<? extends Course> site) {
		this.site = site;
	}

	public void setAssignedUser(String assignedUser) {
		this.assignedUser = assignedUser;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDetails(Text details) {
		this.details = details;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}
}
