package com.fertiletech.nbte.server;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.fertiletech.nbte.client.UtilsService;
import com.fertiletech.nbte.server.accounting.EntityConstants;
import com.fertiletech.nbte.server.bulkupload.InetImageBlob;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.comments.AcitivityComment;
import com.fertiletech.nbte.server.comments.ApplicationParameters;
import com.fertiletech.nbte.server.comments.CommentsDAO;
import com.fertiletech.nbte.server.comments.MortgageAttachment;
import com.fertiletech.nbte.server.comments.RatingComment;
import com.fertiletech.nbte.server.downloads.GenericExcelDownload;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.DrivePickerUtils;
import com.fertiletech.nbte.shared.FormHTMLDisplayBuilder;
import com.fertiletech.nbte.shared.LoginRoles;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.nbte.shared.TableMessage;
import com.fertiletech.nbte.shared.TableMessageHeader;
import com.fertiletech.nbte.shared.TableMessageHeader.TableMessageContent;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.blobstore.UploadOptions;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class UtilsServiceImpl extends RemoteServiceServlet implements
		UtilsService {

	private static final Logger log =
	        Logger.getLogger(UtilsServiceImpl.class.getName());	
	
	  /**
	   * This is where backoff parameters are configured. Here it is aggressively retrying with
	   * backoff, up to 10 times but taking no more that 15 seconds total to do so.
	   */
	  private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	      .initialRetryDelayMillis(10)
	      .retryMaxAttempts(10)
	      .totalRetryPeriodMillis(15000)
	      .build());

	  /**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	  private static final int BUFFER_SIZE = 2 * 1024 * 1024;
	
	BlobstoreService blobstore = BlobstoreServiceFactory.getBlobstoreService();
	public final static String GCS_BUCKET_NAME = AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName();
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}
	private final static String LAST_OPS_LOAN_ID_REQUEST = "com.fertiletech.addosser.opslastloanidviewed";
	
	public static void setLastOpsLoan(String id, HttpServletRequest req)
	{
		LoginRoles role = LoginHelper.getRole(req);
		HttpSession sess = req.getSession();
		boolean cacheID = role.ordinal() < LoginRoles.ROLE_SCHOOL_STUDENT.ordinal();
		if(cacheID)
		{
			//log.warning("Last OPS ID Set to: " + LAST_OPS_LOAN_ID_REQUEST);
			sess.setAttribute(LAST_OPS_LOAN_ID_REQUEST, id);
		}
	}

	@Override
	public List<TableMessage> getAttachments(String loanID) {
		Key<Student> leadKey = ObjectifyService.factory().stringToKey(loanID);
		return EntityDAO.getAttachmentsDTO(leadKey);
	}
	
	@Override
	public List<TableMessage> getMapAttachments(Date startDate, Date endDate) {
		return EntityDAO.getMapAttachments(startDate, endDate);
	}	

	@Override
	public String saveAttachments(String loanId, List<TableMessage> attachments) {
		Key<Student> leadKey = ObjectifyService.factory().stringToKey(loanId);
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		user = (user == null)? "anonymous user entry" : user; 
		List<MortgageAttachment> attch = EntityDAO.createAttachment(leadKey, attachments, user);
		return "Attached " + attch.size() + " new files for student: " + leadKey.getName();
	}
	
	@Override
	public String saveMapAttachment(String loanId, Double lat, Double lng) {
		String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		if(user == null) user = "anoymous user entry";
		TableMessage clientAttachment = DrivePickerUtils.getUserMapAttachment("User Edits", lat, lng, user);
		HttpServletRequest request = getThreadLocalRequest();
		Double altLat, altLng;
		altLng = altLat = null;
		String title="Untitled";
		try
		{
			String[] latlng = request.getHeader("X-AppEngine-CityLatLong").split(",");
			title = request.getHeader("X-AppEngine-Country") + "/" + request.getHeader("X-AppEngine-City"); 
			altLat = Double.valueOf(latlng[0]);
			altLng = Double.valueOf(latlng[1]);
		}
		catch(Exception e)
		{
			log.severe("Ignoring error while detecting lat/lng server side. Error: " + e.getMessage());
		}
		TableMessage serverAttachment = DrivePickerUtils.getUserMapAttachment("User Edits - " + title, altLat, altLng, user);
		ArrayList<TableMessage> result = new ArrayList<TableMessage>(3);
		TableMessageHeader h = new TableMessageHeader();
		h.setCaption("User Edits - Location Detection");
		result.add(h);
		if(clientAttachment != null)
			result.add(clientAttachment);
		if(serverAttachment != null)
			result.add(serverAttachment);
		if(result.size() == 1)
			return null;
		return saveAttachments(loanId, result);
	}	

	@Override
	public String deleteAttachment(String attachID) {
		Key<MortgageAttachment> attachmentKey = ObjectifyService.factory().stringToKey(attachID);
		EntityDAO.removeAttachment(attachmentKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
		return "Removed 1 attachment for M.A.P. ID: " + attachmentKey.getParent().getId();
	}

	public static String getStudentID(String email)
	{
		if(email == null)
			throw new IllegalStateException("Attempted to fetch uploads, yet not signed in");
		Key<Student> studKey = Student.getKey(email);
		return studKey.getString();	
	}
	
	@Override
	public List<TableMessage> getUploads(String loanID) throws MissingEntitiesException{
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "File Name", TableMessageContent.TEXT);
		h.setText(1, "Date", TableMessageContent.DATE);
		if(loanID == null)
		{
			String email = LoginHelper.getLoggedInUser(getThreadLocalRequest());
			loanID = getStudentID(email);
		}
		h.setMessageId(loanID);
		result.add(h);
		try 
		{
			ListOptions listOpt = new ListOptions.Builder().setPrefix(loanID).build();
			ListResult items = gcsService.list(GCS_BUCKET_NAME, listOpt);
			while(items.hasNext())
			{
				ListItem record = items.next();
				if(record.isDirectory()) continue;
				TableMessage m = new TableMessage(2, 0, 1);
				m.setText(0, record.getName().replace(loanID, ""));
				m.setText(1, record.getName());
				m.setDate(0, record.getLastModified());
				m.setMessageId(EntityConstants.GCS_HOST + GCS_BUCKET_NAME + "/" + record.getName());
				//log.warning("Message id is: " + m.getMessageId());
				result.add(m);
			}
			//log.warning("Found Records: " + (result.size() - 1));
		} catch (IOException e) {
			throw new MissingEntitiesException("Unable to connect to cloud storage");
		}
		return result;
	}

	@Override
	public String getUploadUrl() {
		UploadOptions opts = UploadOptions.Builder.withGoogleStorageBucketName(GCS_BUCKET_NAME).maxUploadSizeBytes(1024*550);
		String url = blobstore.createUploadUrl("/mydownload/success", opts);
		return url;
	}

	@Override
	public String deleteUpload(String uploadID) throws MissingEntitiesException {
		GcsFilename fileName = new GcsFilename(GCS_BUCKET_NAME, uploadID);
		boolean deleted = false;
		try {
			log.warning("Deleting: " + fileName);
			deleted = gcsService.delete(fileName);
			String user = LoginHelper.getLoggedInUser(getThreadLocalRequest());
			String[] loanParts = uploadID.split("/");
			if(loanParts.length > 1)
			{
				log.warning("deleted file");
				String comments[] = {"<div style='background-color:#c40069; color: white; text-align:center;'>Upload Deleted</div>", "Filename: " + loanParts[1]};
				TaskQueueHelper.scheduleCreateComment(comments, loanParts[0], user ==null?"anoymous":user);
			}
			else
				log.warning("Nothing deleted");
		} catch (IOException e) {
			throw new MissingEntitiesException("Failed to delete " + fileName.toString() 
					+ " from cloud storage. Error was: " + e.getMessage());
		}
		
		return deleted?  ("Successfully deleted " + fileName.getObjectName()) : ("Failed to delete: " + fileName.getObjectName()); 
	}
	
	@Override
	public List<TableMessage> getApplicationParameter(String ID) {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		HashMap<String, String> params = appObj.getParams();
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = new TableMessageHeader(2);
		h.setText(0, "Unique Identifier", TableMessageContent.TEXT);
		h.setText(1, "Description", TableMessageContent.TEXT);
		h.setMessageId(appObj.getKey().getString());
		result.add(h);
		for(String s : params.keySet())
		{
			TableMessage m = new TableMessage(2, 0, 0);
			m.setText(0, s);
			m.setText(1, params.get(s));
			m.setMessageId(s);
			result.add(m);
		}
		Collections.sort(result);
		return result;
	}


	@Override
	public void saveApplicationParameter(String ID, HashMap<String, String> val) throws MissingEntitiesException {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(ID);
		String updateUser = LoginHelper.getLoggedInUser(getThreadLocalRequest());
		CommentsDAO.updateApplicationParameters(updateUser, pk, val);
	}


	@Override
	public String getImageUploadHistory(String imageID) {
		return ObjectifyService.begin().get(InetImageBlob.getKey(imageID)).getAuditTrail();
	}
	
	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#loadSaleLeadComments(java.lang.String)
	 */
	@Override
	public String loadActivityComments(String parentKey) {

		return loadActivityComments(parentKey, false);
	}
	
	private String loadActivityComments(String leadID, boolean respectShowPublic)
	{
		return loadActivityComments(new FormHTMLDisplayBuilder(), leadID, respectShowPublic);
	}
	
	private String loadActivityComments(FormHTMLDisplayBuilder result, String leadID, boolean respectShowPublic)
	{
		Key<? extends Object> key = ObjectifyService.factory().stringToKey(leadID);
		List<AcitivityComment> commentList = CommentsDAO.getActivityComments(ObjectifyService.begin(), key);
		result.formBegins();
		result.headerBegins().appendTextBody("<h3>Prior Comments</h3>").headerEnds();
		boolean commentFound = false;
		
		for(AcitivityComment c : commentList)
		{
			if(respectShowPublic && !c.isShowPublic())
				continue;
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();			
			result.blankLine();
			result.sectionEnds();
		}
		if(!commentFound)
			result.lineBegins().appendTextBody("No comments found").lineEnds();
		result.formEnds();
		return result.toString();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#saveLoanComment(java.lang.String, java.lang.String)
	 */
	@Override
	public void saveActivityComment(String loanKeyStr, String text, boolean showPublic) {
		Key<? extends Object> leadKey = ObjectifyService.factory().stringToKey(loanKeyStr);
		CommentsDAO.createComment(new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));
	}


	@Override
	public String loadRatings(String pk)
	{
		return loadRatings(pk, false);
	}
	
	public String loadRatings(String parentKey, boolean respectShowPublic) {
		Key<? extends Object> key = ObjectifyService.factory().stringToKey(parentKey);
		List<RatingComment> commentList = CommentsDAO.getRatings(ObjectifyService.begin(), key);
		Map<String, Double> calculatedAverages = RatingComment.getCalculatedAverage(commentList);
		FormHTMLDisplayBuilder result = new FormHTMLDisplayBuilder().formBegins();
		result.headerBegins().appendTextBody("<h3>Prior Ratings</h3>").headerEnds();
		boolean commentFound = false;
		for(RatingComment c : commentList)
		{
			if(respectShowPublic && !c.isShowPublic())
				continue;
			commentFound = true;
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
			DateFormat tf = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			result.sectionBegins();
			result.lineBegins().appendTextBody("Rating", String.valueOf(c.getRating())).lineEnds();
			result.lineBegins().appendTextBody(c.getComment().getValue()).lineEnds();
			result.lineBeginsRight().appendTextBody("User", c.getUser()).appendTextBody("Date", df.format(c.getDateUpdated()) + " " + tf.format(c.getDateUpdated())).lineEndsRight();
			result.blankLine();
			result.sectionEnds();
		}
		
		if(calculatedAverages != null)
		{
			result.sectionBegins();
			result.headerBegins().appendTextBody("<h4>Average Rating</h4>").headerEnds();
			for(String k : calculatedAverages.keySet())
				result.lineBegins().appendTextBody(k, String.valueOf(calculatedAverages.get(k))).lineEnds();
			result.sectionEnds();
		}
		
		if(!commentFound)
			result.lineBegins().appendTextBody("No ratings found").lineEnds();
		
		result.formEnds();
		result.blankLine();
		
		return loadActivityComments(result, parentKey, respectShowPublic);
	}

	@Override
	public void saveRatings(String parentKey, int rating, String text,
			boolean showPublic) {
		Key<? extends Object> leadKey = ObjectifyService.factory().stringToKey(parentKey);
		CommentsDAO.createRating(rating, new Text(text), showPublic, leadKey, LoginHelper.getLoggedInUser(getThreadLocalRequest()));		
	}


	@Override
	public String fetchGenericExcelLink(List<TableMessage> data,
			TableMessageHeader header) {
		//log.severe("RECEIVED EXCEL REQUEST");
		return GenericExcelDownload.getGenericExcelDownloadLink(data, header, getThreadLocalRequest());
	}

	@Override
	public List<TableMessage> getRecentActivityComments() {
		Objectify ofy = ObjectifyService.begin();
		List<AcitivityComment> comments = ofy.query(AcitivityComment.class).order("-dateUpdated").limit(100).list();
		List<TableMessage> result = new ArrayList<TableMessage>();
		for(AcitivityComment c : comments)
		{
			TableMessage m = new TableMessage(2, 0, 1);
			m.setMessageId(c.getKey().getString());
			Text descriptiveComment = c.getComment();
			m.setText(0, c.getUser());
			m.setText(1, descriptiveComment == null?"Oops, no comment found, contact IT":descriptiveComment.getValue());
			//log.warning(descriptiveComment.toString());
			m.setDate(0, c.getDateUpdated());
			result.add(m);
		}
		return result;
	}

	@Override
	public HashMap<String, String> getAllStudents(boolean isCurrentlyEnrolled) {
		return EntityDAO.getUserKeyNameMap(EntityDAO.getAllStudents(isCurrentlyEnrolled, null, null, null, ObjectifyService.begin()));
	}

	@Override
	public List<TableMessage> getAllStudents(String diploma, String dept, String year,
			boolean isCurrentlyEnrolled) {
		Collection<Student> students = EntityDAO.getAllStudents(isCurrentlyEnrolled, diploma, dept, year, ObjectifyService.begin());
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		TableMessageHeader h = EntityDAO.getStudentDTOHeader();
		h.setMessageId(NameTokens.getProviderID(diploma, dept, year));
		result.add(h);
		for(Student s : students)
			result.add(EntityDAO.getStudentDTO(s));
		return result;
	}

	@Override
	public String getImageBlobID(String name) {
			return InetImageBlob.getKey(name).getString();
	}	
}
