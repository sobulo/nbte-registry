package com.fertiletech.nbte.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;

public class BulkCourseUpload extends HttpServlet {
	static {
		GeneralFuncs.registerClassesWithOfy();
	}
	private static final Logger log = Logger.getLogger(BulkCourseUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "vendorUploadData";
	private final static String VENDOR_FILE_NAME = "vendorUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			if (upldSessObj != null) {
				// retrive data from session object
				ArrayList<VendorStruct> data = (ArrayList<VendorStruct>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				String fileName = (String) req.getSession().getAttribute(
						VENDOR_FILE_NAME);
				try {

					out.println("Creating courses");
					int count = 0;
					for (VendorStruct v : data)
					{
						okVendors.append("<br/>Attempting " + ++count + " of " + data.size()).append("<br/>");
						try
						{
						Course c = EntityDAO.createCourseTemplate(v.courseCode,
								v.deptName, v.year, v.courseUnit, v.courseName,
								v.diploma, v.lectureUrl, v.descriptionUrl,
								v.features, v.urlThumb, v.urlBanner, v.semester, v.academicYear);
						okVendors.append(c.getKey().getName()).append("<br/>");
						}
						catch(Exception ex)
						{
							out.println("Ignoring " + v.courseCode + " in " + v.deptName + " for " + 
										v.academicYear + "-" + NameTokens.getSemester(v.semester));
							out.println("ERROR WAS: " + ex.getMessage());
						}
					}
					out.println("Succesfully created " + count + " courses " + " out of " + data.size());
					out.println(okVendors.toString());
					
				} catch (Exception ex) {
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
			

				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
			} else {
				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				String extensionName = ".jpg";
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
						// extensionName = "." + Streams.asString(stream);
					} else {
						log.warning("Bulk Course upload attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.COURSE_UPLOAD_ACCESSORS;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<VendorStruct> sessionData = new ArrayList<VendorStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);

						log.warning("Making getValue Calls");
						SchoolSession sess = EntityDAO.getCurrentTerm();
						HashSet<String> collisionDetector = new HashSet<String>();
						for (int i = 1; i < numRows; i++) {
							log.warning("Row: " + (i + 1) + " of " + numRows);
							try {
								StringBuilder row = new StringBuilder("<tr>");
								VendorStruct tempVendorData = new VendorStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData,
											extensionName, sess);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}

								if (tempVendorData.isValid) {
									String key = tempVendorData.deptName + "-" + tempVendorData.courseCode + 
											"-"+tempVendorData.academicYear;
									if(collisionDetector.contains(key))
									{
										row.append("<td>").append("Duplicate course detected: ").append(key).
										append("</td>");
										tempVendorData.isValid = false;
									}
									else
									{
									row.append("<td>")
											.append(tempVendorData.courseCode)
											.append("</td>")
											.append("<td>")
											.append(tempVendorData.descriptionUrl)
											.append("</td>").append("<td>")
											.append(tempVendorData.features)
											.append("</td></tr>");
									goodDataHTML += row.toString();
									sessionData.add(tempVendorData);
									collisionDetector.add(key);
									}
								}
								
								if(!tempVendorData.isValid)
								{
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							String goodHeaderSuffix = "<TH>Model OK?</TH><TH>Description OK?</TH><TH>Features OK?</TH>";
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ goodHeaderSuffix
									+ "</TR>"
									+ goodDataHTML
									+ htmlTableEnd;
						}

						log.warning("Printing output ...");
						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME,
								sessionData);
						req.getSession().setAttribute(VENDOR_FILE_NAME,
								item.getName());
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, VendorStruct courseData, String ext, SchoolSession sch)
			throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = "\\/\\/|\\|\\|";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			String folderPrefix = NameTokens.BASE_ASSET_URL;
			if (headerName.equals(BulkConstants.COURSE_CODE_HEADER)) {
				String imageName = val.replaceAll(" ", "").toUpperCase();
				courseData.urlBanner = folderPrefix + imageName + "Banner"
						+ ext;
				courseData.urlThumb = folderPrefix + imageName + "Thumb" + ext;
				courseData.courseCode = val.toUpperCase();

				val = courseData.courseCode
						+ "<br/><a href='"
						+ courseData.urlBanner
						+ "'><img src='"
						+ courseData.urlBanner
						+ "'"
						+ " alt='"
						+ val
						+ "' width='20%' height='20%' style='float:left'/><img src='"
						+ courseData.urlThumb + "'" + " alt='" + val
						+ "' width='30%' height='30%'/></a>";

			} else if (headerName.equals(BulkConstants.DEPARTMENT_HEADER)) {
				String category = val;
				if (courseData.diploma == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is null");
				String key = NameTokens.getNameTargetMap().get(
						courseData.diploma);
				if (key == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is not supported: "
									+ courseData.diploma);
				HashSet<String> departments = NameTokens.DIPLM_DEPTS.get(key);
				if (departments == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma full name field is not supported: "
									+ key);
				if (departments.contains(category))
					courseData.deptName = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(departments));

			} else if (headerName.equals(BulkConstants.FEATURES_HEADER)) {
				String[] vals = val.split(splitSentinent);
				StringBuilder desc = new StringBuilder("<ul>");
				for (String v : vals) {
					if (v.length() > 0)
						desc.append("<li>").append(v).append("</li>");
				}
				desc.append("</ul>");
				courseData.features = desc.length() > 9 ? desc.toString() : "";
			} else if (headerName.equals(BulkConstants.COURSE_NAME_HEADER)) {
				courseData.courseName = val;
			} else if (headerName.equals(BulkConstants.DESCRIPTION_HEADER)) {
				String lowVal = val;
				if (lowVal.startsWith("http") || lowVal.length() == 0)
					courseData.descriptionUrl = lowVal;
				else
					throw new IllegalArgumentException("Url " + val
							+ " should begin with http");
			} else if (headerName.equals(BulkConstants.LECTURE_NOTES_HEADER)) {
				String lowVal = val;
				if (lowVal.startsWith("http") || lowVal.length() == 0)
					courseData.lectureUrl = lowVal;
				else
					throw new IllegalArgumentException("Url " + val
							+ " should begin with http");
			} else if (headerName.equals(BulkConstants.DIPLOMA_HEADER)) {
				String manufacturer = val.toUpperCase();
				if (NameTokens.DIPLOMAS.contains(manufacturer))
					courseData.diploma = manufacturer;
				else
					throw new IllegalArgumentException(manufacturer
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedDiplomas());
			} else if (headerName.equals(BulkConstants.COURSE_UNIT_HEADER))
				courseData.courseUnit = getNumber(temp);
			else if (headerName.equals(BulkConstants.ACADEMIC_YEAR_HEADER)) {
				courseData.academicYear = getNumber(temp);
				if(courseData.academicYear != sch.getAcademicYear())
					throw new IllegalArgumentException("Current sessions is " + sch.getAcademicYear() +
							" yet you specified " + courseData.academicYear);
			} else if (headerName.equals(BulkConstants.YEAR_HEADER)) {
				String category = val;
				if (NameTokens.YEARS.contains(category))
					courseData.year = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(NameTokens.YEARS));
			} else if (headerName.equals(BulkConstants.SEMESTER_HEADER)) {
				String category = val;
				if (NameTokens.SEMESTERS.contains(category))
					courseData.semester = NameTokens.getSemesterInt(category);
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(NameTokens.SEMESTERS));
			} else
				throw new IllegalArgumentException(
						"Header setup not done yet. Contact IT" + headerName);
		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			courseData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			courseData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			courseData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public Integer getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return (int) Math.round((Double) (val));
	}

	private class VendorStruct implements Serializable {
		String deptName;
		String courseCode;
		String courseName;
		String features;
		String descriptionUrl;
		String diploma;
		String year;
		String lectureUrl;
		Integer courseUnit;
		String urlThumb;
		String urlBanner;
		int semester;
		int academicYear;
		boolean isValid = true;
	}
}
