package com.fertiletech.nbte.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkDepositUpload extends HttpServlet {
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	
	private static final Logger log = Logger.getLogger(BulkDepositUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "depUploadData";
	private final static String VENDOR_FILE_NAME = "depUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			log.warning("About to make a cast call");
			if (upldSessObj != null) {
				// retrive data from session object
				HashSet<Key<Student>> vendorDataList = (HashSet<Key<Student>>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				//String fileName = (String) req.getSession().getAttribute(
				//		VENDOR_FILE_NAME);
				try {
	
					String user = LoginHelper.getLoggedInUser(req);
					for(Key<Student> elem : vendorDataList)
					{
						TaskQueueHelper.scheduleCreateDeposit(elem.getString(), user);
					}
					out.println("Succesfully scheduled creation of " +
							"student deposits for: " + vendorDataList.size());
				} catch (Exception ex) {
					out.println("Exception Occured");
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
				req.setAttribute(VENDOR_FILE_NAME, null);
			} else {
    				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				String dealListID = null;
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
						dealListID = Streams.asString(stream);
					} else {
						log.warning("Bulk Pricing update attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.DEPOSIT_UPLOAD_ACCESSORS ;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<DealStruct> sessionData = new ArrayList<DealStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);

						log.warning("Making getValue Calls");
						Objectify ofy = ObjectifyService.begin();
						int goodSheetCount = 0;
						int badSheetCount = 0;
						for (int i = 1; i < numRows; i++) {
							log.warning("Row: " + (i+1) + " of " + numRows);
							try {
								StringBuilder row = new StringBuilder("<tr>");
								DealStruct tempVendorData = new DealStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}

								if (tempVendorData.isValid) {
									goodDataHTML += row.toString();
									goodSheetCount++;
									if(tempVendorData.isCleared)
										sessionData.add(tempVendorData); 
								}
								else
								{
									badSheetCount++;
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							goodDataHTML = "<b>Below shows " + goodSheetCount + 
									" rows that passed spreadsheet checks. Scroll down for database checks:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ "</TR>"
									+ goodDataHTML
									+ htmlTableEnd;
						}

						log.warning("Printing output ...");
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows " + badSheetCount +
									" records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}
						out.println(goodDataHTML);

						//2nd pass, check for existence of keys
						HashSet<Key<Student>> studKeys = new LinkedHashSet<Key<Student>>();
						for(DealStruct d : sessionData)
							studKeys.add(d.id);
						Map<Key<Student>, Student> studentMap = ofy.get(studKeys);
						
						HashSet<Key<Student>> clearedStudents = new HashSet<Key<Student>>();
						StringBuilder clearanceOutput = new StringBuilder();
						StringBuilder notClearedOutput = new StringBuilder();
						for(Key<Student> sk : studKeys)
						{
							Student stud = studentMap.get(sk);
							if(stud == null)
							{
								notClearedOutput.append("<li>").append(sk.getName()).
												 append("<li>");
								continue;
							}
							HashMap<String, String> vals = stud.getValues();
							String fname = vals.get(ApplicationFormConstants.FIRST_NAME);
							String lname = vals.get(ApplicationFormConstants.SURNAME);
							clearanceOutput.append("<li>").append(fname).append(" ").
											append(lname).append(" <b>cleared!</b></li>");
							clearedStudents.add(sk);
						}
						
						out.println("<p><h4>FAILED CLEARANCE</h4></p>");
						out.println(notClearedOutput.toString());
						out.println("<p><h1>Push save to clear " + clearedStudents.size() +
								" students below out of " + numRows + 
								" total students </h1></p>");
						out.println(clearanceOutput.toString());
						
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME,
								clearedStudents);
						req.getSession().setAttribute(VENDOR_FILE_NAME,
								dealListID);
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}


	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, DealStruct studentData)
			throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = "\\/\\/|\\|\\|";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			if (headerName.equals(BulkConstants.DEPARTMENT_HEADER)) {
				String category = val;
				if (studentData.diploma == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is null");
				String key = NameTokens.getNameTargetMap().get(
						studentData.diploma);
				if (key == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is not supported: "
									+ studentData.diploma);
				HashSet<String> departments = NameTokens.DIPLM_DEPTS.get(key);
				if (departments == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma full name field is not supported: "
									+ key);
				if (departments.contains(category))
					studentData.dept = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(departments));
			}else if (headerName.equals(BulkConstants.EMAIL_HEADER)) {
				studentData.id = Student.getKey(val.toLowerCase());
			}else if (headerName.equals(BulkConstants.DIPLOMA_HEADER)) {
					String diploma = val.toUpperCase();
					if (NameTokens.DIPLOMAS.contains(diploma))
						studentData.diploma = diploma;
					else
						throw new IllegalArgumentException(diploma
								+ " not supported currently." + " Use one of: "
								+ NameTokens.getAllowedDiplomas());
			} else if (headerName.equals(BulkConstants.YEAR_HEADER)) {
				String category = val;
				if (NameTokens.YEARS.contains(category))
					studentData.year = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(NameTokens.YEARS));
			}else if (headerName.equals(BulkConstants.DEPARTMENT_CLEARANCE_HEADER))
			{
				String yesNo = val;
				if(yesNo.equalsIgnoreCase("yes"))
					studentData.isCleared = true;
				else if(yesNo.equalsIgnoreCase("no"))
					studentData.isCleared = false;
				else 
					throw new IllegalArgumentException("Only yes or no values allowed yet" +
							" yet value specified is: " + val);
			}

		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			studentData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			studentData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			studentData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public long getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return Math.round((Double) (val));
	}
}

class DealStruct 
{
	Key<Student> id;
	transient String year;
	transient String diploma;
	transient String dept;
	transient boolean isValid = true;
	transient boolean isCleared = false;
}