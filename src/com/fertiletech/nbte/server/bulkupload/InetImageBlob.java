/**
 * 
 */
package com.fertiletech.nbte.server.bulkupload;
import java.util.Date;

import javax.persistence.Id;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Unindexed;


/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class InetImageBlob{
    @Id
    private String key;

    private Blob image;
    
    private Text auditTrail;
    
    public InetImageBlob(){}
    
    public InetImageBlob(String key)
    {
    	this.key = key;
    }
    
    public void setImage(byte[] bytes)
    {
    	image = new Blob(bytes);
    }
    
    public byte[] getImage()
    {
    	return image.getBytes();
    }

	public Key<InetImageBlob> getKey() {
		return getKey(key);
	}
	
	public static Key<InetImageBlob> getKey(String keyName) {
		return new Key<InetImageBlob>(InetImageBlob.class, keyName);
	}	
	
	public void addToAuditTrail(String comment, String user)
	{ 
		StringBuilder commentWrap;
		commentWrap = new StringBuilder();
		commentWrap.append("<div style='border:1px solid blue; margin:5px;padding:5px'>").
				append(comment).append("<p style='text-align:right; font-size: smaller;'>By ").append(user).
				append(" on ").append(new Date()).append("</p></div>");
		if(auditTrail != null)
			commentWrap.append(auditTrail.getValue());				
		auditTrail = new Text(commentWrap.toString());
	}
	
	public String getAuditTrail()
	{
		if(auditTrail == null) return "No history available";
		return auditTrail.getValue(); 
	}
}


