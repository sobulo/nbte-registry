package com.fertiletech.nbte.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.college.EntityDAO;
import com.fertiletech.nbte.server.college.SchoolSession;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.shared.ApplicationFormConstants;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkStudentUpload extends HttpServlet {
	static {
		GeneralFuncs.registerClassesWithOfy();
	}
	private static final Logger log = Logger.getLogger(BulkStudentUpload.class
			.getName());
	private final static String STUDENT_UPLD_SESSION_NAME = "studentUploadData";
	private final static String STUDENT_FILE_NAME = "studentUploadName";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					STUDENT_UPLD_SESSION_NAME);
			if (upldSessObj != null) {
				// retrive data from session object
				ArrayList<StudentStruct> studentDataList = (ArrayList<StudentStruct>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int okCount = 0;
				int failCount = 0;				
				int createCount = 0;
				String fileName = (String) req.getSession().getAttribute(
						STUDENT_FILE_NAME);
				try {
					// create product listings
					SchoolSession sess = EntityDAO.getCurrentTerm();
					int count = 0;
					for (StudentStruct stud : studentDataList) {
						try 
						{
							okVendors.append("<br/>").append("Attempting " + ++count + " of " + studentDataList.size()).append("<br/>");

							Student s = EntityDAO.createStudent(
									stud.otherNames, stud.lastName, stud.email,
									stud.year, stud.dept, stud.jambNo,
									stud.matricNo, stud.diploma, sess);
							HashMap<String, String> vals = s.getValues();
							String dept = vals
									.get(ApplicationFormConstants.DEPARTMENT);
							okVendors.append(count).append(": Created student ")
									.append(s.getLoginEmail()).append(" in ")
									.append(dept).append("<br/>");
							okCount++;
						} catch (Exception ex) {
							badMsg.append(count + ": Ignoring " + stud.email
									+ stud.lastName + " - " + stud.matricNo + " - "
									+ stud.jambNo + " in " + stud.dept + " - "
									+ stud.year).append("<br/>");
							out.println("ERROR WAS: " + ex.getMessage());
							failCount++;
						}
					}
				} catch (Exception ex) {
					out.println("<b>ERROR, halting Execution<br/>" + ex.getMessage());
				}
				out.println("<b>Success: " + okCount + " students " + " out of " 
						+ studentDataList.size()+"<br/>Failures:" + failCount + "</b><br/><hr/>");
				out.println("<p><b>SUCCESS</b></p>");
				out.println(okVendors.toString());
				out.println("<p><b>FAILURES</b></p>");
				out.println(badMsg.toString());
				out.flush();
				
				// set session data to null as we've saved the data
				req.setAttribute(STUDENT_UPLD_SESSION_NAME, null);
			} else {
				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
					} else {
						log.warning("Bulk Pricing update attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.STUDENT_UPLOAD_ACCESSORS;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						ArrayList<StudentStruct> sessionData = new ArrayList<StudentStruct>();

						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);

						log.warning("Making getValue Calls");
						Objectify ofy = ObjectifyService.begin();
						HashMap<String, String> emailTable = new HashMap<String, String>();
						SchoolSession sess = EntityDAO.getCurrentTerm();
						for (int i = 1; i < numRows; i++) {
							log.warning("Row: " + (i + 1) + " of " + numRows);
							try {
								StringBuilder row = new StringBuilder("<tr>");
								StudentStruct tempStudData = new StudentStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempStudData, sess);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}

								if (tempStudData.isValid) {
									// generate login name and password
									String[] lnameComponents = tempStudData.lastName
											.split(" ");
									String[] fnameComponents = tempStudData.otherNames
											.split(" ");

									// TODO: Fix this, make it more generic ...
									// URGH ... quick way to deploy for moor
									// plantation
									String generatedEmail = fnameComponents[0]
											+ "." + lnameComponents[0]
											+ "@students.fcahptib.edu.ng";

									if (tempStudData.email.trim().length() == 0)
										tempStudData.email = generatedEmail;
									String fullName = tempStudData.lastName
											+ ", " + tempStudData.otherNames;

									if (emailTable
											.containsKey(tempStudData.email)) {
										String errMsg = String
												.format("Cannot add %s with generated email %s. An earlier record (student: %s) in the file maps to that"
														+ " same email. Remove one of these rows from the file and later try adding via new student form if"
														+ " this is not a duplicate record",
														fullName,
														tempStudData.email,
														emailTable
																.get(tempStudData.email));
										row = new StringBuilder(
												"<tr><td colspan=")
												.append(htmlHeaders.length)
												.append(">").append(errMsg)
												.append("</td>");
										tempStudData.isValid = false;
									} else {
										emailTable.put(tempStudData.email,
												fullName);
										row.append("<td>")
												.append(tempStudData.email)
												.append(" OK</td></tr>");
										sessionData.add(tempStudData);
										goodDataHTML += row.toString();
									}
								}

								if (!tempStudData.isValid) {
									badDataHTML += (row + "</tr>");
								}
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}

						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						htmlHeader += "<TH>Comments</TH>";
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							goodDataHTML = "<b>Below shows data that passed prelim checks. Hit save to store the data:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ "</TR>" + goodDataHTML + htmlTableEnd;
						}

						log.warning("Printing output ...");
						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}

						out.println(goodDataHTML);

						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(
								STUDENT_UPLD_SESSION_NAME, sessionData);
						req.getSession().setAttribute(STUDENT_FILE_NAME,
								item.getName());
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, StudentStruct studentData,
			SchoolSession sess) throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = " ";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			if (headerName.equals(BulkConstants.DEPARTMENT_HEADER)) {
				String category = val;
				if (studentData.diploma == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is null");
				String key = NameTokens.getNameTargetMap().get(
						studentData.diploma);
				if (key == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma field is not supported: "
									+ studentData.diploma);
				HashSet<String> departments = NameTokens.DIPLM_DEPTS.get(key);
				if (departments == null)
					throw new IllegalArgumentException(
							"Unable to validate departments as diploma full name field is not supported: "
									+ key);
				if (departments.contains(category))
					studentData.dept = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(departments));
			} else if (headerName.equals(BulkConstants.OTHER_NAMES_HEADER)) {
				studentData.otherNames = val;
			} else if (headerName.equals(BulkConstants.SURNAME_HEADER)) {
				studentData.lastName = val;
			} else if (headerName.equals(BulkConstants.JAMB_HEADER)) {
				studentData.jambNo = val;
			} else if (headerName.equals(BulkConstants.EMAIL_HEADER)) {
				studentData.email = val.toLowerCase();
			} else if (headerName.equals(BulkConstants.DIPLOMA_HEADER)) {
				String manufacturer = val.toUpperCase();
				if (NameTokens.DIPLOMAS.contains(manufacturer))
					studentData.diploma = manufacturer;
				else
					throw new IllegalArgumentException(manufacturer
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedDiplomas());
			} else if (headerName.equals(BulkConstants.MATRIC_NO_HEADER))
				studentData.matricNo = val.toUpperCase();
			else if (headerName.equals(BulkConstants.YEAR_HEADER)) {
				String category = val;
				if (NameTokens.YEARS.contains(category))
					studentData.year = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(NameTokens.YEARS));
			} else if (headerName.equals(BulkConstants.ACADEMIC_YEAR_HEADER)) {
				studentData.academicYear = (int) Math.round(getNumber(temp));
				if (studentData.academicYear != sess.getAcademicYear())
					throw new IllegalArgumentException("Current sessions is "
							+ sess.getAcademicYear() + " yet you specified "
							+ studentData.academicYear);
			} else
				throw new IllegalArgumentException(
						"Header setup not done yet. Contact IT" + headerName);

		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			studentData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			studentData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			studentData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public Double getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return (Double) (val);
	}
}

class StudentStruct implements Serializable {
	String lastName;
	String otherNames;
	String email;
	String jambNo;
	String matricNo;
	String year;
	String diploma;
	String dept;
	int academicYear;
	boolean isValid = true;
}