/**
 * 
 */
package com.fertiletech.nbte.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import com.fertiletech.nbte.server.login.LoginHelper;
import com.fertiletech.nbte.server.tasks.TaskQueueHelper;
import com.fertiletech.nbte.shared.DTOConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ImageUpload extends HttpServlet {
	private static final Logger log = Logger.getLogger(ImageUpload.class
			.getName());
	public final static int MAX_SIZE = 32768 / 4;
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		try {

			log.warning("received request");
			/*PanelServiceLoginRoles[] allowedRoles = {PanelServiceLoginRoles.ROLE_SUPER};
	        try 
	        {
				LoginPortal.verifyRole(allowedRoles, req);
			} catch (LoginValidationException e) 
			{
				res.getOutputStream().println("<html><body><b>Illegal Access: " + e.getMessage() +
				"</b></body></html>");
				return;
			}*/			
			ServletFileUpload upload = new ServletFileUpload();
			res.setContentType("text/plain");

			String blobName = null;
			FileItemIterator iterator = upload.getItemIterator(req);
			while (iterator.hasNext()) {
				FileItemStream item = iterator.next();
				InputStream stream = item.openStream();

				if (item.isFormField()) {
					log.warning("Got a form field: " + item.getFieldName());
					blobName = Streams.asString(stream);
					log.warning("Value is: " + blobName);
				} else {
					log.warning("Got an uploaded file: " + item.getFieldName()
							+ ", name = " + item.getName());

					// You now have the filename (item.getName() and the
					// contents (which you can read from stream). Here we just
					// print them back out to the servlet output stream, but you
					// will probably want to do something more interesting (for
					// example, wrap them in a Blob and commit them to the
					// datastore).
					int len = 0;
					byte[] buffer = new byte[MAX_SIZE];
					int totalBytesRead = 0;
					while ((len = stream.read(buffer, len, buffer.length)) != -1) {
						totalBytesRead += len;
						if (totalBytesRead >= buffer.length)
							break;
						// res.getOutputStream().write(buffer, 0, len);
						log.warning("wrote " + len + " bytes for school report card image");
					}

					String[] comments = {blobName, "File Name: " + item.getName(), "File Size: " + totalBytesRead + " bytes"};
					
					if (totalBytesRead < buffer.length) {
						byte[] imgData = Arrays.copyOf(buffer, totalBytesRead);
						InetImageBlob image = new InetImageBlob(blobName);
						image.setImage(imgData);
						Objectify ofy = ObjectifyService.beginTransaction();
						try
						{
							Key<InetImageBlob> dsKey = ofy.put(image);
							TaskQueueHelper.scheduleCreateComment(comments, image.getKey().getString(), LoginHelper.getLoggedInUser(req));
							ofy.getTxn().commit();
							log.warning("Succesfully saved: " + dsKey);
						}
						finally
						{
							if(ofy.getTxn().isActive())
								ofy.getTxn().rollback();
						}
						res.getOutputStream().println("Saved file successfully");
					}
					else
						res.getOutputStream().println("File is too large. Can not exceed: " + MAX_SIZE + " bytes");
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
	}

}
