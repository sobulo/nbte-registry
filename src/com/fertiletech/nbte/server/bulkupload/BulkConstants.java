package com.fertiletech.nbte.server.bulkupload;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.NumberColumnAccessor;

public class BulkConstants {
	public final static ColumnAccessor[] COURSE_UPLOAD_ACCESSORS = new ColumnAccessor[11];

	public final static ColumnAccessor[] STUDENT_UPLOAD_ACCESSORS = new ColumnAccessor[9];

	public final static ColumnAccessor[] DEPOSIT_UPLOAD_ACCESSORS = new ColumnAccessor[5];

	public final static ColumnAccessor[] TIME_UPLOAD_ACCESSORS = new ColumnAccessor[9];

	public final static String DIPLOMA_HEADER = "DIPLOMA";
	public final static String YEAR_HEADER = "YEAR";
	public final static String DEPARTMENT_HEADER = "DEPARTMENT";
	public final static String COURSE_NAME_HEADER = "COURSE NAME";
	public final static String COURSE_CODE_HEADER = "COURSE CODE";
	public final static String FEATURES_HEADER = "FEATURES";
	public final static String DESCRIPTION_HEADER = "DESCRIPTION";
	public final static String COURSE_UNIT_HEADER = "COURSE UNIT";
	public final static String LECTURE_NOTES_HEADER = "LECTURE NOTES";
	public final static String SEMESTER_HEADER = "SEMESTER";
	public final static String ACADEMIC_YEAR_HEADER = "SESSION";

	public final static String EMAIL_HEADER = "EMAIL";
	public final static String SURNAME_HEADER = "SURNAME";
	public final static String OTHER_NAMES_HEADER = "OTHER NAMES";
	public final static String JAMB_HEADER = "JAMB REG. NO";
	public final static String MATRIC_NO_HEADER = "MATRIC";
	public final static String DEPARTMENT_CLEARANCE_HEADER = "DEPARTMENT CLEARANCE";

	public final static String WEEKDAY_HEADER = "DAY OF THE WEEK";
	public final static String THEORY_PRACT_HEADER = "THEORY OR PRACTICAL";
	public final static String START_TIME_HEADER = "START TIME";
	public final static String END_TIME_HEADER = "END TIME";
	public final static String LECTURERS_HEADER = "LECTURERS";
	public final static String VENUE_HEADER = "VENUE";
	final static String SPLIT_REGEX = "\\/\\/|\\|\\|";

	static {
		STUDENT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(
				SURNAME_HEADER, true);
		STUDENT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(
				YEAR_HEADER, true);
		STUDENT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(
				DIPLOMA_HEADER, true);
		STUDENT_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(
				DEPARTMENT_HEADER, true);
		STUDENT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(
				OTHER_NAMES_HEADER, true);
		STUDENT_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(
				EMAIL_HEADER, false);
		STUDENT_UPLOAD_ACCESSORS[6] = new ExcelManager.DefaultAccessor(
				MATRIC_NO_HEADER, false);
		STUDENT_UPLOAD_ACCESSORS[7] = new ExcelManager.DefaultAccessor(
				JAMB_HEADER, false);
		STUDENT_UPLOAD_ACCESSORS[8] = new NumberColumnAccessor(
				ACADEMIC_YEAR_HEADER, true);

		COURSE_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(
				DIPLOMA_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(
				DEPARTMENT_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(
				COURSE_CODE_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(
				COURSE_NAME_HEADER, false);
		COURSE_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(
				SEMESTER_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(
				YEAR_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[6] = new ExcelManager.DefaultAccessor(
				FEATURES_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[7] = new ExcelManager.DefaultAccessor(
				DESCRIPTION_HEADER, false);
		COURSE_UPLOAD_ACCESSORS[8] = new NumberColumnAccessor(
				COURSE_UNIT_HEADER, true);
		COURSE_UPLOAD_ACCESSORS[9] = new ExcelManager.DefaultAccessor(
				LECTURE_NOTES_HEADER, false);
		COURSE_UPLOAD_ACCESSORS[10] = new NumberColumnAccessor(
				ACADEMIC_YEAR_HEADER, true);

		DEPOSIT_UPLOAD_ACCESSORS[0] = new ExcelManager.DefaultAccessor(
				YEAR_HEADER, true);
		DEPOSIT_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(
				DIPLOMA_HEADER, true);
		DEPOSIT_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(
				DEPARTMENT_HEADER, true);
		DEPOSIT_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(
				EMAIL_HEADER, true);
		DEPOSIT_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(
				DEPARTMENT_CLEARANCE_HEADER, true);

		TIME_UPLOAD_ACCESSORS[0] = new NumberColumnAccessor(ACADEMIC_YEAR_HEADER, true);
		TIME_UPLOAD_ACCESSORS[1] = new ExcelManager.DefaultAccessor(DEPARTMENT_HEADER, true);
		TIME_UPLOAD_ACCESSORS[2] = new ExcelManager.DefaultAccessor(COURSE_CODE_HEADER, true);
		TIME_UPLOAD_ACCESSORS[3] = new ExcelManager.DefaultAccessor(WEEKDAY_HEADER, true);
		TIME_UPLOAD_ACCESSORS[4] = new ExcelManager.DefaultAccessor(THEORY_PRACT_HEADER, true);
		TIME_UPLOAD_ACCESSORS[5] = new ExcelManager.DefaultAccessor(START_TIME_HEADER,true);
		TIME_UPLOAD_ACCESSORS[6] = new ExcelManager.DefaultAccessor(END_TIME_HEADER, true);
		TIME_UPLOAD_ACCESSORS[7] = new ExcelManager.DefaultAccessor(LECTURERS_HEADER,false);
		TIME_UPLOAD_ACCESSORS[8] = new ExcelManager.DefaultAccessor(VENUE_HEADER, false);
		DAY_OF_THE_WEEK = new LinkedHashSet<String>();
		THEORY_OR_PRACTICAL = new LinkedHashSet<String>();
		initSets();
	}

	public final static HashSet<String> DAY_OF_THE_WEEK;
	public final static HashSet<String> THEORY_OR_PRACTICAL;

	public static void initSets() {
		
		String[] daySet = { "Monday", "Tuesday", "Wednesday", "Thursday",
				"Friday" };
		for (String s : daySet)
			DAY_OF_THE_WEEK.add(s);
		THEORY_OR_PRACTICAL.add("Theory");
		THEORY_OR_PRACTICAL.add("Practical");
		// THEORY_OR_PRACTICAL.add(isValidLecturer(isValidLecturer("")));

	}

	public static void isValidDayOfTheWeek(String val) {
		if (DAY_OF_THE_WEEK.contains(val))
			return;
		throw new IllegalArgumentException(val
				+ " not a recognized value, permissible values are: "
				+ getAllVals(DAY_OF_THE_WEEK));

	}

	public static void isValidTheoryOrPractical(String val) {
		if (THEORY_OR_PRACTICAL.contains(val))
			return;
		throw new IllegalArgumentException(val
				+ " not a recognised value, values accepted are: "
				+ getAllVals(THEORY_OR_PRACTICAL));
	}

	public static void isValidTimeOfWork(String val) {
		boolean passedCheck = (val.endsWith("AM") || val.endsWith("PM"));
		if (!passedCheck)
			throw new IllegalArgumentException(val + " not a recognised value,"
					+ " only permitted values are AM or PM ");
		passedCheck = val.contains(":");
		if (!passedCheck)
			throw new IllegalArgumentException(
					val + " must contain <b>:</b> (e.g 10<b style='color:red'>:</b>23 PM)");
		passedCheck = (val.length() == 8) || (val.length() == 7);
		if (!passedCheck)
			throw new IllegalArgumentException(
					"time string " + "[" + val + "]" + "must be exactly 7 or 8 characters, e.g. 1:00 PM or 10:00 AM. " +
							"Length calculated of input specified is "+ val.length());

	}

	public static StringBuilder getValidLecturer(String val) {
		String[] vals = val.split(SPLIT_REGEX);
		StringBuilder desc = new StringBuilder("<b>Lecturer:</b><br/>");
		for (String v : vals) {
			if (v.length() > 0)
				desc.append(v).append("<br/>");
		}
		return desc;
	}

	public static String getAllVals(HashSet<String> set) {
		String result = "";
		Iterator<String> cursor = set.iterator();

		while (cursor.hasNext())
			result += cursor.next() + ", ";

		return result;
	}
	
	public static String[] ROW_TYPE = {"a", "b"};
	static StringBuilder formatScheduleInfo(String weekDay, String theoryOrPractical,
			String startTime, String endTime, String lecturer, String venue, int typeIdx) 
	{
		String type = ROW_TYPE[typeIdx];
		StringBuilder output = new StringBuilder("<table width='100%' class='ccitem' border='0'><tr class='rtype").append(type)
				.append("'><td class='cclect' width='50%'>");	
		if(lecturer.trim().length() > 0)
			output.append(getValidLecturer(lecturer));
		else
			output.append("instructor info not available");
		output.append("</td><td class='cctime' width='50%'>").append(weekDay).append("<br/>");
		output.append(startTime).append(" - ").append(endTime).append("<br/>");
		output.append(theoryOrPractical).append("<br/>");
		if(venue.length() > 0)
			output.append(venue);
		output.append("</td></tr></table");
		
		return output;
	}

}
