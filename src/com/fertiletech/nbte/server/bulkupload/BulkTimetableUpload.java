package com.fertiletech.nbte.server.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.college.Course;
import com.fertiletech.nbte.shared.NameTokens;
import com.fertiletech.utils.ColumnAccessor;
import com.fertiletech.utils.EmptyColumnValueException;
import com.fertiletech.utils.ExcelManager;
import com.fertiletech.utils.InvalidColumnValueException;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class BulkTimetableUpload extends HttpServlet {
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}	
	private static final Logger log = Logger.getLogger(BulkTimetableUpload.class
			.getName());
	private final static String VENDOR_UPLD_SESSION_NAME = "timetableploadData";
	private final static String LOAD_PARAM_NAME = "loadData";
	private final static String GOOD_DATA_PREFIX = "<i></i>";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		// TODO add validation

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		if (req.getParameter(LOAD_PARAM_NAME) != null) {
			Object upldSessObj = req.getSession().getAttribute(
					VENDOR_UPLD_SESSION_NAME);
			if (upldSessObj != null) {
				// retrive data from session object
				HashMap<Key<Course>, StringBuilder> vendorDataList = (HashMap<Key<Course>, StringBuilder>) upldSessObj;
				StringBuilder badMsg = new StringBuilder();
				StringBuilder okVendors = new StringBuilder();
				int createCount = 0;
				try {
					// create product listings
					Objectify ofy = ObjectifyService.begin();
					Map<Key<Course>, Course> courseMap = ofy.get(vendorDataList.keySet());
					for (Key<Course> elem : vendorDataList.keySet())
						courseMap.get(elem).setScheduleDesc(new Text(vendorDataList.get(elem).toString()));
					
					ofy.put(courseMap.values());
					out.println("updated: " + vendorDataList.size() + " courses. # found in databsae is also: " + courseMap.size());
				} catch (Exception ex) {
					out.println(badMsg.toString());
					out.println("<b>" + ex.getMessage() + "</b>");
				}
				out.flush();
				// set session data to null as we've saved the data
				req.setAttribute(VENDOR_UPLD_SESSION_NAME, null);
			} else {
    				out.println("<b>unable to retrieve session info. Try "
						+ "enabling cookies in your browser.</b>");
			}
		} else {
			/**
			 * First pass: this is where we read excel file and save contents in
			 * a session object In the 2nd pass above, we create tenant objects
			 * in the database based on this
			 */
			ServletFileUpload upload = new ServletFileUpload();
			// upload.setHeaderEncoding("ISO-8858-2");
			try {
				FileItemIterator iterator = upload.getItemIterator(req);
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream stream = item.openStream();
					if (item.isFormField()) {
						log.warning("Got a form field: " + item.getFieldName()
								+ " with yet another name: " + item.getName());
					} else {
						log.warning("Bulk Pricing update attempt: "
								+ item.getFieldName() + ", name = "
								+ item.getName());
						ColumnAccessor[] headerAccessors = BulkConstants.TIME_UPLOAD_ACCESSORS ;
						String[] expectedHeaders = ColumnAccessor
								.getAccessorNames(headerAccessors);
						// TODO, return error page if count greater than 200
						ExcelManager sheetManager = new ExcelManager(stream);
						// confirm headers in ssheet match what we expect
						String[] sheetHeaders;
						try {
							sheetHeaders = sheetManager.getHeaders();

							// expectedheaders are uppercase so convert sheet
							// prior to compare
							for (int s = 0; s < sheetHeaders.length; s++) {
								sheetHeaders[s] = sheetHeaders[s].toUpperCase();
							}
						} catch (InvalidColumnValueException ex) {
							log.severe(ex.getMessage());
							out.println("<b>Unable to read excel sheet headers</b><br/> Error was: "
									+ ex.getMessage());
							return;
						}
						ArrayList<String> sheetOnly = new ArrayList<String>();
						ArrayList<String> intersect = new ArrayList<String>();
						ArrayList<String> expectedOnly = new ArrayList<String>();
						GeneralFuncs.arrayDiff(expectedHeaders, sheetHeaders,
								intersect, expectedOnly, sheetOnly);
						// handle errors in column headers
						if (expectedOnly.size() > 0 || sheetOnly.size() > 0) {
							out.println("<b>Uploaded sheet should contain only these headers:</b>");
							printHtmlList(expectedHeaders, out);
							if (expectedOnly.size() > 0) {
								out.println("<b>These headers are missing from ssheet:</b>");
								printHtmlList(expectedOnly, out);
							}
							if (sheetOnly.size() > 0) {
								out.println("<b>These headers are not expected but were found in ssheet</b>");
								printHtmlList(sheetOnly, out);
							}
							return;
						}

						sheetManager.initializeAccessorList(headerAccessors);
						// todo, replace these with string builders
						String goodDataHTML = "";
						String badDataHTML = "";
						String tempVal = "";
						String[] htmlHeaders = new String[headerAccessors.length];
						for (int i = 0; i < headerAccessors.length; i++) {
							htmlHeaders[i] = headerAccessors[i].getHeaderName();
						}

						int numRows = sheetManager.totalRows();
						HashMap<String, String> companyNameTable = new HashMap<String, String>(
								numRows);


						log.warning("Making getValue Calls");
						
						HashMap<Key<Course>, StringBuilder> courseTable = 
								new HashMap<Key<Course>, StringBuilder>();
						int entryCount = 0;
						int badCount = 0;
						int goodCount = 0;
						for (int i = 1; i < numRows; i++) {
							try {
								StringBuilder row = new StringBuilder("<tr>");
								PricingStruct tempVendorData = new PricingStruct();
								for (int j = 0; j < headerAccessors.length; j++) {
									tempVal = getValue(i, headerAccessors[j],
											sheetManager, tempVendorData);
									row.append("<td>").append(tempVal)
											.append("</td>");
								}
								row.append("</tr>");

								if (tempVendorData.isValid) 
								{
									goodCount++;
									tempVendorData.ck = Course.getKey(tempVendorData.dept, 
											tempVendorData.courseCode,(int) tempVendorData.year);
									if(!courseTable.containsKey(tempVendorData.ck))
										courseTable.put(tempVendorData.ck, new StringBuilder("<div class='classcal'>"));
									courseTable.get(tempVendorData.ck).append(BulkConstants.formatScheduleInfo
									(tempVendorData.dayOfWeek, 
									 tempVendorData.theoryOrPract, 
									 tempVendorData.startTime, 
									 tempVendorData.endTime, 
									 tempVendorData.lecturers,
									 tempVendorData.venue, goodCount % 2));

									goodDataHTML += row.toString();
								}
								if(!tempVendorData.isValid){
									badCount++;
									badDataHTML += row.toString();
								}
								entryCount++;
							} catch (EmptyColumnValueException ex) {
							} // logic to help skip blank lines
						}
						
						//2nd pass checks
						Map<Key<Course>, Course> courseObjects = ObjectifyService.begin().get(courseTable.keySet());
						HashMap<Key<Course>, StringBuilder> sessionData = new HashMap<Key<Course>, StringBuilder>();
						StringBuilder foundObj = new StringBuilder();
						StringBuilder missingObj = new StringBuilder();
						for(Key<Course> ck : courseTable.keySet())
						{
							Course c = courseObjects.get(ck);
							if(c == null)
								missingObj.append("<li>").append("Unable to find: ").append(ck.getName()).append("<li>");
							else
							{
								sessionData.put(ck, courseTable.get(ck).append("</div>"));
								foundObj.append("<p>").append(c.getCourseCode()).append(" ").append(sessionData.get(ck)).append("</p>");
							}
						}						
						String htmlTableStart = "<TABLE border='1' class='themePaddedBorder'>";
						String htmlHeader = "";
						for (String s : htmlHeaders) {
							htmlHeader += "<TH>" + s + "</TH>";
						}
						String htmlTableEnd = "</TABLE>";

						if (goodDataHTML.length() > 0) {
							out.println(GOOD_DATA_PREFIX);
							goodDataHTML = "<b>Below shows data that passed spreadsheet checks. Scroll down for list that passed database checks:</b><br/><hr/>"
									+ htmlTableStart
									+ "<TR>"
									+ htmlHeader
									+ "</TR>"
									+ goodDataHTML
									+ htmlTableEnd;
						}

						log.warning("Printing output ...");
						// TODO, possibly fixed but check
						if (badDataHTML.length() > 0) {
							out.println("<b>Below shows records with sheet errors</b>");
							out.println(htmlTableStart);
							out.println("<TR>");
							out.println(htmlHeader);
							out.println("</TR>");
							out.print(badDataHTML);
							out.println(htmlTableEnd);
						}
						out.println(goodDataHTML);
						out.println("<p>Total number of spreadsheet entries: " + entryCount + "</p>");
						out.println("<p>Total number of good sheet entries: " + goodCount + "</p>");
						out.println("<p>Total number of bad sheet entries: " + badCount + "</p>");
						out.println("<p>Total number of aggregated/unique course entries: " + courseTable.size() + "</p>");
						out.println("<p>Total number of unique course entries actually found in database: " + courseObjects.size() + "</p>");

						if(missingObj.length() == 0)
							out.println("no data found that failed database checks: " + sessionData.size());
						else
						{
							out.println("<b>Database Failures. Investigate and fix, e.g. most likely due to typo in course code</b><ul>");
							out.println(missingObj.toString());
							out.println("</ul>");
						}						

						if(foundObj.length() == 0)
							out.println("<b>none of your ssheet rows passed database checks</b>");
						else
						{
							out.println("<b style='color:green'>Below shows schedule that will be loaded into the database</b>");
							out.println(foundObj.toString());
						}
						// TODO, test what happens with sheet containing only
						// headers
						req.getSession().setAttribute(VENDOR_UPLD_SESSION_NAME,
								sessionData);
						log.warning("All done");
					}
				}
			} catch (FileUploadException ex) {
				out.println("<b>File upload failed:</b>" + ex.getMessage());
			} catch (InvalidColumnValueException ex) {
				log.severe(ex.getMessage());
				out.println("<b>Unable to read excel sheet</b><br/> Error was: "
						+ ex.getMessage());
			}
		}
	}

	private String getValue(int row, ColumnAccessor accessor,
			ExcelManager sheetManager, PricingStruct courseData)
			throws InvalidColumnValueException {
		Object temp;
		String val = null;
		String headerName = accessor.getHeaderName();
		String splitSentinent = "\\/\\/|\\|\\|";
		try {
			temp = sheetManager.getData(row, accessor);

			if (temp == null)
				val = "";
			else if (temp instanceof String) {
				val = ((String) temp).trim();
				val = val.replaceAll("[^\\p{Graph}\\p{Space}]", "");
				// val = val.replaceAll("[\u0000-\u001f]", "");
			} else
				val = String.valueOf(temp);

			if (headerName.equals(BulkConstants.ACADEMIC_YEAR_HEADER)) {
				courseData.year = Math.round(getNumber(temp));
			}
			else if (headerName.equals(BulkConstants.DEPARTMENT_HEADER)) {
				String category = val;
				HashSet<String> departments = NameTokens.getAllDepartments();
				if (departments == null)
					throw new IllegalArgumentException(
							"Unable to validate departments");
				if (departments.contains(category))
					courseData.dept = category;
				else
					throw new IllegalArgumentException(category
							+ " not supported currently." + " Use one of: "
							+ NameTokens.getAllowedValues(departments));			
			} else if (headerName.equals(BulkConstants.COURSE_CODE_HEADER))
				courseData.courseCode = val;
			else if (headerName.equals(BulkConstants.WEEKDAY_HEADER))
			{
				BulkConstants.isValidDayOfTheWeek(val);
				courseData.dayOfWeek = val;
			}
			else if (headerName.equals(BulkConstants.THEORY_PRACT_HEADER))
			{
				BulkConstants.isValidTheoryOrPractical(val);
				courseData.theoryOrPract = val;
			}
			else if (headerName.equals(BulkConstants.START_TIME_HEADER))
			{
				BulkConstants.isValidTimeOfWork(val);
				courseData.startTime = val;
			}
			else if (headerName.equals(BulkConstants.END_TIME_HEADER))
			{
				BulkConstants.isValidTimeOfWork(val);
				courseData.endTime = val;
			}
			else if (headerName.equals(BulkConstants.LECTURERS_HEADER))
			{
				courseData.lecturers = val;
			}
			else if (headerName.equals(BulkConstants.VENUE_HEADER))
				courseData.venue = val;
			else
				throw new IllegalStateException("Sanity check failed, header issue: " + headerName);
		} catch (EmptyColumnValueException ex) {
			if (sheetManager.isBlankRow(row))
				throw ex;
			val = ex.getMessage();
			courseData.isValid = false;
		} catch (InvalidColumnValueException ex) {
			val = ex.getMessage();
			courseData.isValid = false;
		} catch (RuntimeException ex) {
			val = ex.getMessage();
			courseData.isValid = false;
		}
		return val;
	}

	private void printHtmlList(String[] list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	private void printHtmlList(List<String> list, PrintWriter out)
			throws IOException {
		out.println("<ul>");
		for (String s : list) {
			out.println("<li>" + s + "</li>");
		}
		out.println("</ul>");
	}

	public Double getNumber(Object val) {
		if (val == null)
			throw new NumberFormatException("[Null Value]");
		return (Double) (val);
	}
}

class PricingStruct {
	Key<Course> ck;
	String schedulingInfo;
	String dept;
	long year;
	String courseCode;
	String dayOfWeek;
	String theoryOrPract;
	String startTime;
	String endTime;
	String lecturers;
	String venue;
	boolean isValid = true;
}