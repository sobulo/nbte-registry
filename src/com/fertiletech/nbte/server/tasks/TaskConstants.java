/**
 * 
 */
package com.fertiletech.nbte.server.tasks;

import java.util.Random;

import com.google.appengine.api.utils.SystemProperty;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class TaskConstants {
    public final static String SERVICE_TYPE = "service";    
    public final static String SERVICE_SEND_MESSAGE = "sndmsg";  
    public final static String SERVICE_CREATE_COMMENT = "crtcmt";
    public final static String SERVICE_CREATE_USER_BILL = "newuserbill";
    public final static String SERVICE_CREATE_DEPOSIT = "newstudeposit";
    public final static String SERVICE_CREATE_RESIDENT_ACCTS = "resdacct";    
    
    //parameters
    public final static String STUDENT_GENERATE_CHARGE = "stpygenato";
    public final static String STUDENT_KEY_PARAM = "studks";
    public final static String BILL_DESC_KEY_PARAM = "btks";
    public final static String EXCEL_DOWN_KEY_PARAM = "excl";   
    public final static String TO_ADDR_PARAM = "toAddy";
    public final static String MSG_BODY_PARAM = "msgCntent";  
    public final static String MSG_SUBJECT_PARAM = "msgSubject";
    public final static String MSG_CONTROLLER_KEY_PARAM = "mcks";
    public final static String COMMENT_PARENT_OBJ = "cpobj";
    //public final static String CMT_PUBLIC = "cmtpub";
    //public final static String LOAN_ID_PARAM = "loanid";
    public final static String UPDATE_USR_PARAM = "upsr";
    
    //task delay constant
    private final static int MINUTE_MILLISECONDS = 1000 * 60; //1000milliseconds * 60seconds
    private final static int MIN_TASK_DELAY = 5 * MINUTE_MILLISECONDS; //5 minute minimum
    private final static int MAX_TASK_DELAY = 60 * MINUTE_MILLISECONDS; //1 hour maximum
    private final static int DEV_TASK_DELAY = 1000 * 30; //30 seconds
    public final static long TASK_DELAY; // =
    static
    {
    	if(SystemProperty.environment.value() == SystemProperty.Environment.Value.Production)
    		TASK_DELAY =  new Random().nextInt(MAX_TASK_DELAY - MIN_TASK_DELAY) + MIN_TASK_DELAY;
    	else
    		TASK_DELAY = DEV_TASK_DELAY;
    }
}
