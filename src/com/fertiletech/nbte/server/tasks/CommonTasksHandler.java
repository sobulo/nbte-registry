/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server.tasks;


import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.nbte.server.AccountManagerImpl;
import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.accounting.BillTemplate;
import com.fertiletech.nbte.server.accounting.InetBill;
import com.fertiletech.nbte.server.accounting.InetDAO4Accounts;
import com.fertiletech.nbte.server.college.Student;
import com.fertiletech.nbte.server.college.User;
import com.fertiletech.nbte.server.comments.AcitivityComment;
import com.fertiletech.nbte.server.comments.CommentsDAO;
import com.fertiletech.nbte.server.messaging.MessagingController;
import com.fertiletech.nbte.shared.DuplicateEntitiesException;
import com.fertiletech.nbte.shared.ManualVerificationException;
import com.fertiletech.nbte.shared.MissingEntitiesException;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class CommonTasksHandler extends HttpServlet{
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}
	
    private static final Logger log =
            Logger.getLogger(CommonTasksHandler.class.getName());
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException
    {
    	log.warning("*******TASK EXECUTION BEGINNING!!!********");
        StringBuilder requestDetails = new StringBuilder(64);
        Enumeration<String> paramNames = req.getParameterNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Param Name: " + name.toString() + " Values: ").append(Arrays.toString(req.getParameterValues(name))).append("\n");
        }
        paramNames = req.getHeaderNames();
        while(paramNames.hasMoreElements())
        {
        	String name = paramNames.nextElement();
        	requestDetails.append("Header Name: " + name.toString() + " Values: ").append(req.getHeader(name)).append("\n");
        }

        //log.warning(requestDetails.toString());
        
        Objectify ofy = ObjectifyService.begin();

        String service = req.getParameter(TaskConstants.SERVICE_TYPE).trim();
        validateParamName(service, "No service specified");
        
        if(service.equals(TaskConstants.SERVICE_CREATE_COMMENT))
        {
        	String loanKeyStr = req.getParameter(TaskConstants.COMMENT_PARENT_OBJ);
        	Key<? extends Object> leadKey = ofy.getFactory().stringToKey(loanKeyStr);
        	log.warning("running create comment task for: " + leadKey);        	
        	String[] comments = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
        	String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
        	validateParamName(loanKeyStr, "value most be specified for loan id");
        	validateParamName(comments, "at least 1 comment must be specified");
        	String loginUser = null;
        	try
        	{
        		validateParamName(updateUser, "ignore");
        		loginUser = updateUser;
        	}
        	catch(IllegalArgumentException EX)
        	{
        		loginUser = "anonymous";//LoginHelper.getLoggedInUser(req);
        	}
        	
        	AcitivityComment storedObj = CommentsDAO.createComment(comments, false, leadKey, loginUser);
        	log.warning(storedObj.getKey() + " comments associated with: " + storedObj.getParentKey());
        }        
        else if(service.equals(TaskConstants.SERVICE_SEND_MESSAGE))
        {
        	log.warning("Running send message task");
            //TODO, test to ensure this fails if duplicate tasks scheduled
            String controllerKeyStr = req.getParameter(TaskConstants.MSG_CONTROLLER_KEY_PARAM);
            String toAddress = req.getParameter(TaskConstants.TO_ADDR_PARAM);
            String[] messageBody = req.getParameterValues(TaskConstants.MSG_BODY_PARAM);
            String msgSubject = req.getParameter(TaskConstants.MSG_SUBJECT_PARAM);
            validateParamName(controllerKeyStr, "Value must be specified for message controller key");
            validateParamName(toAddress, "Value must be specified for recipeint address");
            validateParamName(messageBody, "Value must be specified for message body");
            validateParamName(msgSubject, "Value must be specified for subject field");


            Key<? extends MessagingController> controllerKey = ofy.getFactory().stringToKey(controllerKeyStr);
             
        	MessagingController controller = ofy.find(controllerKey);
        	if(controller == null)
        		throw new IllegalStateException("Unable to find controller key");
        	boolean status = controller.sendMessage(toAddress, messageBody, msgSubject);
        	
        	//TODO modify log to reflect byte size of entire messageBody object rather than just text component
        	String msgSuffix = "message of length " + messageBody[0].length() +
			" to " + toAddress + ". Controller Type: " + controller.getClass().getSimpleName();
        	if(status)
        		log.warning("Successfully sent " + msgSuffix);
        	else
        		log.severe("Error sending " + msgSuffix);
        }
        else if(service.equals(TaskConstants.SERVICE_CREATE_USER_BILL))
        {
            String billDescKeyStr = req.getParameter(TaskConstants.BILL_DESC_KEY_PARAM);
            String userKeyStr = req.getParameter(TaskConstants.STUDENT_KEY_PARAM);
            String generateCharge = req.getParameter(TaskConstants.STUDENT_GENERATE_CHARGE);
            log.warning("Task handler has user string as: " + userKeyStr);
            validateParamName(billDescKeyStr, "No bill description key specified");
            validateParamName(userKeyStr, "No user key specified");
            validateParamName(generateCharge, "No charge option specified");
            String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
            validateParamName(updateUser, "No audit trail user specified");
            try {
            	Key<BillTemplate> billDescKey = ofy.getFactory().stringToKey(billDescKeyStr);
            	Key<? extends User> userKey = ofy.getFactory().stringToKey(userKeyStr);
            	log.warning("Calling create bill with key: " + userKey);
            	BillTemplate template = ofy.get(billDescKey);
                InetBill b = InetDAO4Accounts.createBill(userKey, template, Boolean.valueOf(generateCharge), updateUser);
                log.warning("Created bill: " + b.getKey());
            }catch(DuplicateEntitiesException ex)
            {
                log.warning("IGNORING possible duplicate task run: " + ex.getMessage());
            } catch (MissingEntitiesException e) {
                log.warning("IGNORING missing entities: " + e.getMessage());
			} catch (ManualVerificationException e) {
                log.warning("IGNORING verification exception: " + e.getMessage());
			}      	            
        }
        else if(service.equals(TaskConstants.SERVICE_CREATE_DEPOSIT))
        {
            String userKeyStr = req.getParameter(TaskConstants.STUDENT_KEY_PARAM);
            String updateUser = req.getParameter(TaskConstants.UPDATE_USR_PARAM);            
            log.warning("Task handler has user string as: " + userKeyStr);
            validateParamName(userKeyStr, "No user key specified");
            validateParamName(updateUser, "No charge option specified");
            try {
            	Key<Student> userKey = ofy.getFactory().stringToKey(userKeyStr);
            	log.warning("Calling create deposit with key: " + userKey);
            	String result = AccountManagerImpl.createDeposits(userKey, new Date(),
            			"EARLY-PAYMENT-CLEARANCE ", updateUser);
            	log.warning("Task Succeeded: " + result);
            }catch (MissingEntitiesException e) {
                log.warning("IGNORING missing entities: " + e.getMessage());
			}      	            
        }
        else if(service.equals(TaskConstants.SERVICE_CREATE_RESIDENT_ACCTS))
        {
            String user = req.getParameter(TaskConstants.UPDATE_USR_PARAM);
            String tenantID = req.getParameter(TaskConstants.STUDENT_KEY_PARAM);
            String[] types = req.getParameterValues(TaskConstants.TO_ADDR_PARAM);
            try
            {
            	validateParamName(user, "User not specified");
            }
            catch(Exception ex)
            {
            	//TODO fix this
            	user="task-queue-admin";
            }
            validateParamName(tenantID, "Resident not specified");
            validateParamName(types, "Resident accounts not specified");
           
            try {
            	Key<Student> tk = ofy.getFactory().stringToKey(tenantID);
            	log.warning("Calling create resident accounts with key: " + tk);
                InetDAO4Accounts.createResidentAccount(tk, types, user, true, "NGN", 0);
            }catch(DuplicateEntitiesException ex)
            {
                log.warning("IGNORING possible duplicate task run: " + ex.getMessage());
            }      	            
        }
    }

    private void validateParamName(String param, String message)
    {
        if(param == null || param.length() == 0)
            throw new IllegalArgumentException(message);
    }
    
    private void validateParamName(String[] params, String message)
    {
    	log.warning("PARAMS: " + params);
        if( params == null || params.length == 0 || params[0] == null || params[0].length() == 0)
            throw new IllegalArgumentException(message);
    }    
}
