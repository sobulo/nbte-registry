/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server.tasks;

import java.util.logging.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Builder;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TaskQueueHelper {
    private static final Logger log = Logger.getLogger(TaskQueueHelper.class.getName());
    private final static String COMMON_TASK_HANDLER = "/tasks/runner";

    private static int MAX_RETRY_COUNT = 3;
        
	public static void scheduleMessageSending(String controllerId, String toAddress, String messages[], String subject)
	{
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	    	log.warning("scheduling sending of msg to: " + toAddress);
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE, 
    	                		TaskConstants.SERVICE_SEND_MESSAGE).
    	                param(TaskConstants.MSG_CONTROLLER_KEY_PARAM, controllerId).
    	                param(TaskConstants.TO_ADDR_PARAM, toAddress).
    	                param(TaskConstants.MSG_SUBJECT_PARAM, subject);
    	        
    	        for(String msg : messages)
    	        	if(msg != null && msg.length() > 0)
    	        		url = url.param(TaskConstants.MSG_BODY_PARAM, msg);
    	        url.method(Method.POST);
    	        queue.add(url);    	    			
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule message sending failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
	}
	
    public static void scheduleCreateUserBill(String billDescriptionKey, String userKey, 
    		boolean generatePayment, String user)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        log.warning("Scheduler received key: " + userKey);
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE,
    	                TaskConstants.SERVICE_CREATE_USER_BILL).
    	                param(TaskConstants.BILL_DESC_KEY_PARAM, billDescriptionKey).
    	                param(TaskConstants.STUDENT_GENERATE_CHARGE, String.valueOf(generatePayment)).
    	                param(TaskConstants.UPDATE_USR_PARAM, user).
    	                param(TaskConstants.STUDENT_KEY_PARAM, userKey).method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create user bill failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }

    public static void scheduleCreateDeposit(String studKey, String user)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        log.warning("Scheduler received key: " + studKey);
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE,
    	                TaskConstants.SERVICE_CREATE_DEPOSIT).
    	                param(TaskConstants.UPDATE_USR_PARAM, user).
    	                param(TaskConstants.STUDENT_KEY_PARAM, studKey).method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create user bill failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }    
    
    public static void scheduleCreateResidentAccount(String tenantID, String[] types, String user)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
    	        Queue queue = QueueFactory.getDefaultQueue();
    	        TaskOptions url = Builder.withUrl(COMMON_TASK_HANDLER).
    	                param(TaskConstants.SERVICE_TYPE,
    	                TaskConstants.SERVICE_CREATE_RESIDENT_ACCTS).
    	                param(TaskConstants.UPDATE_USR_PARAM, user).
    	                param(TaskConstants.STUDENT_KEY_PARAM, tenantID);
    	        for(String t : types)
    	        	url.param(TaskConstants.TO_ADDR_PARAM, t);
    	        url.method(Method.POST);

    	        queue.add(url);

    			succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create user bill failed: " + e.getMessage());
    		}
    	}
    	if(!succeeded)
    		throw new RuntimeException("Task scheduling failed, pls check logs");
    }    
    
    public static void scheduleCreateComment(String[] comments, String loanKeyStr, String updateUser)
    {
    	boolean succeeded = false;
    	int retryCount = 0;
        if( comments == null || comments.length == 0) 
            throw new IllegalArgumentException("Empty comments passed into task scheduler");
        
    	while(!succeeded && retryCount < MAX_RETRY_COUNT)
    	{
    		if(retryCount != 0)
    			log.warning("retrying schedule task, retry number " + retryCount);
    		try
    		{
	            Queue queue = QueueFactory.getDefaultQueue();
	            TaskOptions opt = Builder.withUrl(COMMON_TASK_HANDLER).
	                    param(TaskConstants.SERVICE_TYPE,
	                    TaskConstants.SERVICE_CREATE_COMMENT).
	                    param(TaskConstants.COMMENT_PARENT_OBJ,
	                    loanKeyStr).param(TaskConstants.UPDATE_USR_PARAM, updateUser);

    	        for(String msg : comments)
    	        	if(msg != null && msg.length() > 0)
    	        		opt = opt.param(TaskConstants.MSG_BODY_PARAM, msg);	            
	            queue.add(opt.method(Method.POST));
	            succeeded = true;
    		}
    		catch(Exception e)
    		{
    			retryCount++;
    			log.warning("schedule task create string options failed: " + e.getMessage());
    		}
    	}
    	
    	if(!succeeded)
    		log.severe("Task scheduling failed, comments will not be created. This is a silent failure, best debug info: [" + loanKeyStr + "]");
    }    
	
}
