/**
 * 
 */
package com.fertiletech.nbte.server.login;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.fertiletech.nbte.server.GeneralFuncs;
import com.fertiletech.nbte.server.OAuthLoginServiceImpl;
import com.fertiletech.nbte.server.comments.ApplicationParameters;
import com.fertiletech.nbte.shared.DTOConstants;
import com.fertiletech.nbte.shared.LoginRoles;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoginHelper {
	static Map<Key<ApplicationParameters>, ApplicationParameters> cachedTables;
	static Date cacheDate;
	private static final Logger log =
	        Logger.getLogger(LoginHelper.class.getName());
	static
	{
		GeneralFuncs.registerClassesWithOfy();
	}
	static void refreshCache()
	{
		if(cacheDate == null || (new Date()).getTime() - cacheDate.getTime() > 1 * 60 * 1000)
		{
			log.warning("Refreshing cache");
			Key<ApplicationParameters>[] appKeys = new Key[1 + DTOConstants.APP_PARAM_EDITORS.length];
			
			String[] names = DTOConstants.APP_PARAM_EDITORS;
			appKeys[0] = ApplicationParameters.getKey(DTOConstants.APP_PARAM_ADMINS);
			for(int i = 1; i <= names.length; i++)
				appKeys[i] = ApplicationParameters.getKey(names[i-1]);
			cachedTables = ObjectifyService.begin().get(appKeys);
			cacheDate = new Date();
		}		
	}

	public static boolean isSuperAdmin(String email, String type)
	{
		refreshCache();
		if(email.trim().toLowerCase().equals("sobulo@fertiletech.com")) return true;
		ApplicationParameters paramObj = cachedTables.get(ApplicationParameters.getKey(type));
		if(paramObj == null) return false;
		HashMap<String, String> adminTable = paramObj.getParams();
		for(String k : adminTable.keySet())
		{
			log.warning(k);
		}
		return adminTable.containsKey(email.toLowerCase().trim());
 	}
	
    public static LoginRoles getRole(HttpServletRequest req)
    {
    	return OAuthLoginServiceImpl.getLoggedInRole(req.getSession());
    }
    
    public static LoginRoles getRole(String email, boolean isRecognizedStudent, boolean isEnrolledForSession)
    {
    	if(email == null) email = "";
    	
    	if(email.endsWith(LoginConstants.STUDENT_DOMAIN))
    	{
    		if(isRecognizedStudent)
    		{
    			if(isEnrolledForSession)
    				return LoginRoles.ROLE_SCHOOL_STUDENT;
    			else
    				return LoginRoles.ROLE_SCHOOL_STUDENT_NOT_IN_SESSION;
    		}
    		else
    			return LoginRoles.ROLE_SCHOOL_STUDENT_UNKNOWN;		
    	}
    	
    	if(email.endsWith(LoginConstants.COMPANY_DOMAIN) || email.endsWith(LoginConstants.FTBS_DOMAIN))
        {
        	
        	if(isSuperAdmin(email, DTOConstants.APP_PARAM_ADMINS))
        		return LoginRoles.ROLE_SCHOOL_ADMIN_EDIT;
        	else
        	{
        		for(String editor : DTOConstants.APP_PARAM_EDITORS)
        		{
        			if(isSuperAdmin(email, editor))
        				return LoginRoles.ROLE_SCHOOL_ADMIN;
        		}
        	}
        }
        
        return	LoginRoles.ROLE_SCHOOL_PUBLIC;        	
    }
    
    public static String getLoggedInUser(HttpServletRequest req)
    { 	    	
    	String user = OAuthLoginServiceImpl.getLoggedInUser(req.getSession());
    	if(user == null) return "";
    	return user;
    }
}
