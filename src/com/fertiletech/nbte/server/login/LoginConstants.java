/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.nbte.server.login;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginConstants
{
    public final static String COMPANY_DOMAIN = "@fcahptib.edu.ng";
    public final static String FTBS_DOMAIN = "@fertiletech.com";
    public final static String STUDENT_DOMAIN = "@students.fcahptib.edu.ng";
}
